●制作環境
・OS
Windows Vista Home Premium
・ソフト
Microsoft Visual Studio 2005
・DirectX Version
9.0

●担当
プログラム(全て担当しました)

●既存のライブラリー等の使用部分
KX_Audio.cppのOGGの読み込み部分に既存のライブラリーを使用しました。
使用ファイルはLib/oggフォルダー内にあります。

KX_3DOBJ.cppで使用するスキンメッシュに学内配布ツールにより生成したファイルを使用しました。
その読み込み部分に学内配布ライブラリーの一部分を使用しました。
使用部分はIEX.h・KX_Resource.cpp(kxResource::load3DObjFromIEM)にあります。