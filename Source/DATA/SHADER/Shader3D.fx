//---------------------------------------------------------------------------
//
//	Shader.fx
//	エフェクトファイル
//
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//	グローバル変数
//---------------------------------------------------------------------------
// ローカルから射影の変換マトリックス
float4x4	mWVP;
// ローカルからワールド座標変換マトリックス
float4x4	mWorld;

// ライトの方向
float3	vLightDir = { 0.7f, -1.0f, 0.0f };

// 環境光
float4	ambient = { 0.3f, 0.3f, 0.3f, 1.0f };
// ライトの強さ
float4	diffuse = { 0.7f, 0.7f, 0.7f, 1.0f };
float	rate	= 1.0f;

float	alpha	= 1.0f;

//---------------------------------------------------------------------------
//	テクスチャー
//---------------------------------------------------------------------------
texture Texture;
sampler Samp = sampler_state
{
	Texture		= <Texture>;
	MinFilter	= LINEAR;
	MagFilter	= LINEAR;
	MipFilter	= NONE;
	
	AddressU	= Wrap;
	AddressV	= Wrap;
};

//---------------------------------------------------------------------------
//	頂点フォーマット
//---------------------------------------------------------------------------
// 出力フォーマット
struct VS_OUTPUT
{
	float4	pos		: POSITION;
	float4	color	: COLOR0;
	float2	tex		: TEXCOORD0;
};

// 出力フォーマット(LINE)
struct VS_OUTPUT_LINE
{
	float4	pos		: POSITION;
	float4	color	: COLOR0;
};

// 出力フォーマット(3D)
struct VS_OUTPUTL
{
	float4	pos		: POSITION;
	float4	color	: COLOR0;
	float3	normal	: NORMAL;
	float2	tex		: TEXCOORD0;
};

// 入力フォーマット
struct VS_INPUT
{
	float4	pos		: POSITION;
	float4	color	: COLOR0;
	float2	tex		: TEXCOORD0;
};

// 入力フォーマット(LINE)
struct VS_INPUT_LINE
{
	float4	pos		: POSITION;
	float4	color	: COLOR0;
};

// 入力フォーマット(3D)
struct VS_INPUTL
{
	float4	pos		: POSITION;
	float4	color	: COLOR0;
	float3	normal	: NORMAL;
	float2	tex		: TEXCOORD0;
};

//---------------------------------------------------------------------------
//	ライティング
//---------------------------------------------------------------------------
inline float4 LightDir(float3 L, float3 N)
{
	return ambient + diffuse * rate * max(0, dot(-L, N));
}

//---------------------------------------------------------------------------
//	頂点シェーダー
//---------------------------------------------------------------------------
// 基本
VS_OUTPUT VS_Basic( VS_INPUTL In )
{
	VS_OUTPUT Out = (VS_OUTPUT)0;

	Out.pos	= mul(In.pos, mWVP);
	Out.tex	= In.tex;
	
	Out.color = In.color;
	
	return Out;
}

// 基本
VS_OUTPUT VS_Copy( VS_INPUTL In )
{
	VS_OUTPUT Out = (VS_OUTPUT)0;

	Out.pos	= mul(In.pos, mWVP);
	Out.tex	= In.tex;

	float3 L = vLightDir;
	float3x3 m = mWVP;
	float3 N = mul(In.normal, m);
	N = normalize(N);

	Out.color.rgb = LightDir(L, N);
	Out.color.a = 1.0f;
	
	return Out;
}

//	透過
VS_OUTPUT VS_Alpha( VS_INPUTL In )
{
	VS_OUTPUT Out = (VS_OUTPUT)0;

	Out.pos	= mul(In.pos, mWVP);
	Out.tex	= In.tex;

	float3 L = vLightDir;
	float3 N = mul(In.normal, mWVP);
	N = normalize(N);

	Out.color.rgb = LightDir(L, N);
	Out.color.a = alpha;
	
	return Out;
}

// ライン
VS_OUTPUT_LINE VS_Line( VS_INPUT_LINE In )
{
	VS_OUTPUT_LINE Out = (VS_OUTPUT_LINE)0;
	
	Out.pos	= mul(In.pos, mWVP);
	Out.color = In.color;
	
	return Out;
}

//---------------------------------------------------------------------------
//	ピクセルシェーダー
//---------------------------------------------------------------------------
// 基本
float4	PS_Copy( VS_OUTPUT In ) : COLOR
{
	return In.color * tex2D( Samp, In.tex );
}

// テクスチャーのみ
float4	PS_Tex( VS_OUTPUT In ) : COLOR
{
	return tex2D( Samp, In.tex );
}

// ライン
float4	PS_Line( VS_OUTPUT_LINE In ) : COLOR
{
	return In.color;
}

//	パーティクル
float4	PS_Particle( VS_OUTPUT In ) : COLOR
{
	return In.color * tex2D( Samp, In.tex );
}

//---------------------------------------------------------------------------
//	テクニック
//---------------------------------------------------------------------------
// 基本
technique basic
{
	pass P0
	{
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;
		
		VertexShader	= compile vs_2_0 VS_Basic();
		PixelShader		= compile ps_2_0 PS_Copy();
	}
}

// 加算合成
technique add
{
	pass P0
	{
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = One;
		ZWriteEnable     = true;
		
		VertexShader	= compile vs_2_0 VS_Basic();
		PixelShader		= compile ps_2_0 PS_Tex();
	}
}

//	基本
technique copy
{
	pass P0
	{
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;
		
		VertexShader	= compile vs_2_0 VS_Copy();
		PixelShader		= compile ps_2_0 PS_Copy();
	}
}

// 透過付き描画
technique Alpha
{
	pass P0
	{
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;
		
		VertexShader	= compile vs_2_0 VS_Alpha();
		PixelShader		= compile ps_2_0 PS_Copy();
	}
}

// ライン用
technique Line
{
	pass P0
	{
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;
		
		VertexShader	= compile vs_2_0 VS_Line();
		PixelShader		= compile ps_2_0 PS_Line();
	}
}

// パーティクル用
technique Particle
{
	pass P0
	{
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = One;
		ZWriteEnable     = false;
		
		VertexShader	= compile vs_2_0 VS_Basic();
		PixelShader		= compile ps_2_0 PS_Particle();
	}
}

//============================================================================
//	END OF FILE
//============================================================================