//---------------------------------------------------------------------------
//
//	Shader.fx
//	エフェクトファイル
//
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//	テクスチャー
//---------------------------------------------------------------------------
texture Texture;
sampler Samp = sampler_state
{
	Texture		= <Texture>;
	MinFilter	= LINEAR;
	MagFilter	= LINEAR;
	MipFilter	= NONE;
	
	AddressU	= Wrap;
	AddressV	= Wrap;
};

//---------------------------------------------------------------------------
//	頂点フォーマット
//---------------------------------------------------------------------------
// 出力フォーマット
struct VS_OUTPUT
{
	float4	pos		: POSITION;
	float4	color	: COLOR0;
	float2	tex		: TEXCOORD0;
};

// 入力フォーマット
struct VS_INPUT
{
	float4	pos		: POSITION;
	float4	color	: COLOR0;
	float2	tex		: TEXCOORD0;
};


//---------------------------------------------------------------------------
//	頂点シェーダー
//---------------------------------------------------------------------------
// 基本
VS_OUTPUT VS_copy( VS_INPUT In )
{
	VS_OUTPUT Out = (VS_OUTPUT)0;
	
	Out.pos		= In.pos;
	Out.color	= In.color;
	Out.tex		= In.tex;
	
	return Out;
}

//---------------------------------------------------------------------------
//	ピクセルシェーダー
//---------------------------------------------------------------------------
// 基本
float4	PS_copy( VS_OUTPUT In ) : COLOR
{
	return In.color * tex2D( Samp, In.tex );
}

// 頂点色のみ
float4	PS_None( VS_OUTPUT In ) : COLOR
{
	return In.color;
}

//---------------------------------------------------------------------------
//	テクニック
//---------------------------------------------------------------------------
// 基本
technique copy
{
	pass P0	// 線形合成
	{
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;
		
		VertexShader	= compile vs_2_0 VS_copy();
		PixelShader		= compile ps_2_0 PS_copy();
	}
	pass P1	// 加算合成
	{
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = One;
		ZWriteEnable     = true;
		
		VertexShader	= compile vs_2_0 VS_copy();
		PixelShader		= compile ps_2_0 PS_copy();
	}
	pass P2	// 減算合成
	{
		AlphaBlendEnable = true;
		BlendOp          = RevSubtract;
		SrcBlend         = SrcAlpha;
		DestBlend        = One;
		ZWriteEnable     = true;
		
		VertexShader	= compile vs_2_0 VS_copy();
		PixelShader		= compile ps_2_0 PS_copy();
	}
	pass P3 // 乗算合成
	{
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = Zero;
		DestBlend        = SrcColor;
		ZWriteEnable     = true;
		
		VertexShader	= compile vs_2_0 VS_copy();
		PixelShader		= compile ps_2_0 PS_copy();
	}
}

// テクスチャー無し
technique None
{
	pass P0	// 線形合成
	{
		AlphaBlendEnable = true;
		BlendOp          = Add;
		SrcBlend         = SrcAlpha;
		DestBlend        = InvSrcAlpha;
		ZWriteEnable     = true;
		
		VertexShader	= compile vs_2_0 VS_copy();
		PixelShader		= compile ps_2_0 PS_None();
	}
}

//============================================================================
//	END OF FILE
//============================================================================