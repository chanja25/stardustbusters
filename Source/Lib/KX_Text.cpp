//---------------------------------------------------------------------------
//!
//!	@file	KX_Text.cpp
//!	@brief	テキストデータ解析クラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "KX.h"


//---------------------------------------------------------------------------
//	コストラクタ
//---------------------------------------------------------------------------
kxText::kxText()
: _pBuffer(NULL)
, _dataIP(0)
, _size(0)
{
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
kxText::~kxText()
{
	SAFE_DELETE(_pBuffer);
}

//---------------------------------------------------------------------------
//	読み込み
//!	@param	pFileName	[in] 読み込むファイル名
//---------------------------------------------------------------------------
void	kxText::load(const char* pFileName)
{
	File file;

	//	前回のスクリプトデータがあれば削除
	SAFE_DELETE(_pBuffer);

	//	データポインタを先頭にする
	_dataIP = 0;

	//	スクリプトデータの読み込み
	file.read(pFileName);

	//	スクリプトサイズの取得
	_size = file.getFileSize();

	//	スクリプトデータの取得
	_pBuffer = new char[_size];
	memcpy(_pBuffer, file.getFile(), sizeof(char) * _size);

	//	コマンドの初期化(使われないような適当な値を入れておく)
	ZeroMemory(_command, sizeof(char) * COMMAND_MAX);
	_command[0] = '~'; _command[1] = '~'; _command[2] = '\0';
}

//---------------------------------------------------------------------------
//	次のコマンドの取得
//---------------------------------------------------------------------------
void	kxText::getNextCommand(void)
{
	u32 i;
	//	次のコマンドまでスキップ
	skipCommand();
	//	スクリプトが存在すれば、コマンドを取得
	for( i = 0; _dataIP < _size; i++, _dataIP++ ) {
		//	"はスキップして取得する
		if( _pBuffer[_dataIP] == '"' ) {
			_dataIP++;
			for( ; _dataIP < _size; i++, _dataIP++ ) {
				if( _pBuffer[_dataIP] == '"' ) break;
				//	コマンドを読み込んでいく
				_command[i] = _pBuffer[_dataIP];
			}
			break;
		}
		//	次の文字が来た場合、コマンドの終りとする
		if( _pBuffer[_dataIP] == '\r' ) break;
		if( _pBuffer[_dataIP] == '\n' ) break;
		if( _pBuffer[_dataIP] == '\t' ) break;
		if( _pBuffer[_dataIP] == '\0' ) break;
		if( _pBuffer[_dataIP] == ';' )  break;
		if( _pBuffer[_dataIP] == ',' )  break;
		if( _pBuffer[_dataIP] == ' ' )  break;
		if( _pBuffer[_dataIP] == '{' )  break;
		if( _pBuffer[_dataIP] == '}' )  break;
		if( _pBuffer[_dataIP] == '(' )  break;
		if( _pBuffer[_dataIP] == ')' )  break;
		//	コマンドを読み込んでいく
		_command[i] = _pBuffer[_dataIP];
	}
	_dataIP++;
	//	コマンドの最後にNULL文字を入れる
	_command[i] = '\0';
}

//---------------------------------------------------------------------------
//	指定されたコマンドを取得するまでスキップ
//!	@param	pCommand	[in] 取得したいコマンド
//---------------------------------------------------------------------------
void	kxText::getCommand(char* pCommand)
{
	while( _command[0] != '\0' ) {
		if( strcmp(pCommand, _command) == 0 ) break;
		getNextCommand();
	}
}

//---------------------------------------------------------------------------
//	次のコマンドのまでスキップ
//---------------------------------------------------------------------------
void	kxText::skipCommand(void)
{
	//	スクリプトが存在すればスキップする
	for( ; _dataIP < _size; _dataIP++ ) {
		//	コメントの場合はスキップする(//)
		if( _pBuffer[_dataIP] == '/' && _pBuffer[_dataIP+1] == '/' ) {
			for( ; _dataIP < _size; _dataIP++ ) {
				if( _pBuffer[_dataIP] == '\n' ) break;
				if( IsDBCSLeadByte(_pBuffer[_dataIP]) ) _dataIP++;
			}
			continue;
		}
		//	コメントの場合はスキップする(/**/)
		if( _pBuffer[_dataIP] == '/' && _pBuffer[_dataIP+1] == '*' ) {
			for( ; _dataIP < _size; _dataIP++ ) {
				if( _pBuffer[_dataIP] == '*' && _pBuffer[_dataIP+1] == '/' ) break;
				if( IsDBCSLeadByte(_pBuffer[_dataIP]) ) _dataIP++;
			}
			continue;
		}
		//	次の文字が来た場合、スキップする
		if( _pBuffer[_dataIP] == '\r' ) continue;
		if( _pBuffer[_dataIP] == '\t' ) continue;
		if( _pBuffer[_dataIP] == '\n' ) continue;
		if( _pBuffer[_dataIP] == ',' )  continue;
		if( _pBuffer[_dataIP] == ' ' )  continue;
		if( _pBuffer[_dataIP] == '(' )  continue;
		if( _pBuffer[_dataIP] == ')' )  continue;
		if( _pBuffer[_dataIP] == '{' )  continue;
		if( _pBuffer[_dataIP] == '}' )  continue;
		if( _pBuffer[_dataIP] == ';' )  continue;
		break;
	}
}

//---------------------------------------------------------------------------
//	文字列パラメータの取得
//!	@retval	取得したデータ
//---------------------------------------------------------------------------
char*	kxText::getParamStr(void)
{
	getNextCommand();

	return _command;
}

//---------------------------------------------------------------------------
//	s32型パラメータの取得
//!	@retval	取得したデータ
//---------------------------------------------------------------------------
s32		kxText::getParamInt(void)
{
	getNextCommand();

	return atoi(_command);
}

//---------------------------------------------------------------------------
//	f32型パラメータの取得
//!	@retval	取得したデータ
//---------------------------------------------------------------------------
f32		kxText::getParamFloat(void)
{
	getNextCommand();

	return static_cast<f32>(atof(_command));
}

//---------------------------------------------------------------------------
//	DWORD型パラメータの取得
//!	@retval	取得したデータ
//---------------------------------------------------------------------------
DWORD	kxText::getParamDWORD(void)
{
	getNextCommand();

	//	16進数の場合、10進数に変換して読み込む
	if( _command[0] == '0' && _command[1] == 'x' ){
		u32 num = 0;
		u32 i = static_cast<u32>(strlen(_command)) - 1;
		u32 j = 0;
		//	下の桁から順番に変換していく
		for( ; _command[i] != 'x'; i--, j++ ){
			if( _command[i] == '1' )	  num += 1  << (j * 4);
			else if( _command[i] == '2' ) num += 2  << (j * 4);
			else if( _command[i] == '3' ) num += 3  << (j * 4);
			else if( _command[i] == '4' ) num += 4  << (j * 4);
			else if( _command[i] == '5' ) num += 5  << (j * 4);
			else if( _command[i] == '6' ) num += 6  << (j * 4);
			else if( _command[i] == '7' ) num += 7  << (j * 4);
			else if( _command[i] == '8' ) num += 8  << (j * 4);
			else if( _command[i] == '9' ) num += 9  << (j * 4);
			else if( _command[i] == 'a' ) num += 10 << (j * 4);
			else if( _command[i] == 'b' ) num += 11 << (j * 4);
			else if( _command[i] == 'c' ) num += 12 << (j * 4);
			else if( _command[i] == 'd' ) num += 13 << (j * 4);
			else if( _command[i] == 'e' ) num += 14 << (j * 4);
			else if( _command[i] == 'f' ) num += 15 << (j * 4);
			else if( _command[i] == 'A' ) num += 10 << (j * 4);
			else if( _command[i] == 'B' ) num += 11 << (j * 4);
			else if( _command[i] == 'C' ) num += 12 << (j * 4);
			else if( _command[i] == 'D' ) num += 13 << (j * 4);
			else if( _command[i] == 'E' ) num += 14 << (j * 4);
			else if( _command[i] == 'F' ) num += 15 << (j * 4);
		}
		return num;
	}

	return atol(_command);
}

//============================================================================
//	END OF FILE
//============================================================================