//---------------------------------------------------------------------------
//!
//!	@file	KX_Profiler.h
//!	@brief	プロファイラクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#ifndef	__KX_PROFILER_H__
#define __KX_PROFILER_H__

#pragma once

#if	DEBUG
	#define	USE_PROFILER 1
#else	//~#if DEBUG
	#define	USE_PROFILER 0
#endif	//~#if DEBUG

#if	USE_PROFILER

//===========================================================================
//!	プロファイラクラス
//===========================================================================
class kxProfiler{
public:
	//!	ログ表示色
	enum COLOR {
		COLOR_WHITE,
		COLOR_BLACK,
		COLOR_RED,
		COLOR_GREEN,
		COLOR_BLUE,
		COLOR_YELLOW,
		COLOR_PINK,
		COLOR_LIGHT_BULE,
		COLOR_PURPLE,
		COLOR_GRAY,
	};

	//!	プロファイルログ
	struct Log{
		Log*		_pParent;		//!< 親
		Log*		_pBrother;		//!< 兄弟
		Log*		_pChiled;		//!< 子供

		// 計測用パラメータ
		u32			_beginTime;		//!< 計測開始時間
		u32			_endTime;		//!< 計測終了時間
		u32			_tmpCount;		//!< 計測回数
		u32			_tmpTotalTime;	//!< 合計処理時間
		u32			_tmpMinTime;	//!< 最小処理時間
		u32			_tmpMaxTime;	//!< 最大処理時間

		// 表示用パラメータ
		const char*	_pName;			//!< 名前
		COLOR		_color;			//!< 表示色
		u32			_count;			//!< 計測回数
		u32			_totalTime;		//!< 合計処理時間
		u32			_averageTime;	//!< 平均処理時間
		u32			_minTime;		//!< 最小処理時間
		u32			_maxTime;		//!< 最大処理時間
	};

	//===========================================================================
	//! スコープ内処理時間計測プロファイラ
	//===========================================================================
	class ScopeProfiler
	{
	public:
		//!	コンストラクタ
		ScopeProfiler(const char* pName, kxProfiler::COLOR = kxProfiler::COLOR_BLUE);
		//!	デストラクタ
		~ScopeProfiler(void);
	private:
		const char* _pName;	//!< プロファイル名
	};

	//---------------------------------------------------------------------------
	//!	@name 初期化
	//---------------------------------------------------------------------------
	//!@{

	//! 初期化
	static void	init(void);
	//!	解放
	static void	cleanup(void);

	//!@}

	//-------------------------------------------------------------
	//!	@name プロファイル管理
	//-------------------------------------------------------------
	//!@{

	//!	フレームの開始をプロファイラに伝える
	void	start(void);
	//!	計測開始
	void	begin(const char* pName, COLOR color);
	//!	計測終了
	void	end(const char* pName);

	//!	プロセスバーの描画
	void	drawProcessBar(void);
	//!	プロセスバーの描画
	void	drawProcessBar(const Log* pLog, VECTOR2 pos);

	//!@}

	//-------------------------------------------------------------
	//!	@name 取得参照
	//-------------------------------------------------------------
	//!@{

	//!	ルートログの取得
	//!	@retval	ルートログ
	inline Log*	getRootLog(void) { return _pRoot; }

	//!	インスタンスの取得
	//! @retval	インスタンス
	inline static kxProfiler*	getInstance(void) { return _pInstance; }

	//!@}

private:
	//!	コンストラクタ
	kxProfiler();
	//!	デストラクタ
	~kxProfiler();

	//!	ログの情報生成
	Log*	createLog(const char* pName, Log* pParent);
	//!	ログの更新
	void	updateLog(Log* pLog);


	static kxProfiler*	_pInstance;	//! 唯一のインスタンス

	//!	プロセスバーの長さ
	static const f32 BAR_LENGTH;

	//!	ログの最大数
	static const u32 LOG_MAX	= 256;

	Timer	_timer;		//!< タイマー
	Log*	_pRoot;		//!< ルートログ
	Log*	_pCurrent;	//!< カレントログ
	u32		_logCount;	//!< ログの数
};

//	計測用マクロ
#define	PROFILER					(kxProfiler::getInstance())
#define START_PROFILE				(PROFILER->start())
#define BEGIN_PROFILE(name, color)	(PROFILER->begin(name, color))
#define END_PROFILE(name)			(PROFILER->end(name))
#define DRAW_PROFILER				(PROFILER->drawProcessBar())

#define	PROFILE(name, color)		kxProfiler::ScopeProfiler scopeProfiler(name, color)

#else	//~#if USE_PROFILER

#define	PROFILER
#define START_PROFILE
#define BEGIN_PROFILE(name, color)
#define END_PROFILE(name)
#define DRAW_PROFILER

#define	PROFILE(name, color)

#endif	//~#if USE_PROFILER

#endif	//~#if __KX_PROFILER_H__

//============================================================================
//	END OF FILE
//============================================================================
