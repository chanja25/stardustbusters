//===========================================================================
//!
//!	@file		KX_MATH.h
//!	@brief		計算クラス
//!
//===========================================================================
#ifndef	__KX_MATH_H__
#define	__KX_MATH_H__

#pragma once


//===========================================================================
//!	kxMathクラス
//===========================================================================
class kxMath
{
public:
	//---------------------------------------------------------------------------
	//! @name 計算
	//---------------------------------------------------------------------------
	//!@{

	//!	長さを2乗した数の取得
	//!	@param	vec	[in] 長さを取得するベクトル
	//!	@retval	長さの2乗
	inline f32	length2(const VECTOR3& vec) { return (vec.x * vec.x) + (vec.y * vec.y) + (vec.z * vec.z); };
	//!	長さの取得
	//!	@param	vec	[in] 長さを取得するベクトル
	//!	@retval	長さ
	inline f32	length(const VECTOR3& vec) { return sqrtf(length2(vec)); };
	//!	長さの2乗した数の取得
	//!	@param	x	[in] 長さを取得するベクトルのx軸
	//!	@param	y	[in] 長さを取得するベクトルのy軸
	//!	@retval	長さの2乗
	inline f32	length2(f32 x, f32 y) { return (x * x) + (y * y); };
	//!	長さの取得
	//!	@param	x	[in] 長さを取得するベクトルのx軸
	//!	@param	y	[in] 長さを取得するベクトルのy軸
	//!	@retval	長さ
	inline f32	length(f32 x, f32 y) { return sqrtf(length2(x, y)); };

	//!	正規化
	void	normalize(VECTOR3* pOut, const VECTOR3& vec);
	//!	内積
	f32		dot(const VECTOR3& vec1, const VECTOR3& vec2);
	//!	外積による2つの直線に垂直な線の取得
	void	cross(VECTOR3* pVec, const VECTOR3& vec1, const VECTOR3& vec2);
	//!	2次元における外積
	f32		cross(f32 x1, f32 y1, f32 x2, f32 y2);

	//!	初期化
	void	matrixIdentity(MATRIX* pOut);
	//!	座標変更行列作成
	void	matrixTranslation(MATRIX* pOut, const VECTOR3& pos);
	//!	X軸回りの回転行列作成
	void	rotationMatrixX(MATRIX* pOut, f32 angle);
	//!	Y軸回りの回転行列作成
	void	rotationMatrixY(MATRIX* pOut, f32 angle);
	//!	Z軸回りの回転行列作成
	void	rotationMatrixZ(MATRIX* pOut, f32 angle);
	//!	XYZ軸回りの回転行列作成
	void	rotationMatrixXYZ(MATRIX* pOut, f32 x, f32 y, f32 z);
	//!	スケール変更行列作成
	void	scalingMatrix(MATRIX* pOut, f32 x, f32 y, f32 z);
	//!	任意軸回転
	void	axisRotationMatrix(MATRIX* pOut, const VECTOR3& axis, f32 angle);
	//!	1つ目のベクトルから2つ目のベクトルに変換する行列作成
	void	vectorRotationMatrix(MATRIX* pOut, const VECTOR3& vec1, const VECTOR3& vec2);

	//!@}

	//---------------------------------------------------------------------------
	//! @name 取得参照
	//---------------------------------------------------------------------------
	//!@{

	//!	インスタンスの取得
	//!	@retval	インスタンス
	inline static kxMath* getInstance(void) { return &_instance; }

	//!@}

private:
	//	コンストラクタ
	kxMath(void){};

	static	kxMath	_instance;	//!< 唯一のインスタンス
};

#define	MATH	(kxMath::getInstance())

#endif	//~#if __KX_MATH_H__

//============================================================================
//	END OF FILE
//============================================================================