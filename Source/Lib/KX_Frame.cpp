//---------------------------------------------------------------------------
//!
//!	@file	KX_Frame.cpp
//!	@brief	フレーム管理
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "KX.h"


#if	USE_FRAME_CONTROL

//	インスタンスのポインタ生成
kxFrame*	kxFrame::_pInstance = NULL;

//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
kxFrame::kxFrame(void)
{
}

//---------------------------------------------------------------------------
//	初期化
//---------------------------------------------------------------------------
void	kxFrame::init(void)
{
	ASSERT(_pInstance == NULL, "既にフレーム制御は初期化されています");
	//	インスタンスの生成
	_pInstance = new kxFrame();

	//	1フレーム毎の時間設定
	_pInstance->_targetFPS = TARGET_FPS;
	_pInstance->_frameTime = Timer::RATE / _pInstance->_targetFPS;

	//	初期設定
	_pInstance->_secCount	 = 0;
	_pInstance->_nextFrame	 = _pInstance->_frameTime;
	_pInstance->_fpsFrame	 = 0;
	_pInstance->_renderFrame = 0;
	_pInstance->_fps		 = 0;
	_pInstance->_render		 = TRUE;
	_pInstance->_skip		 = TRUE;

	//	時間の計測開始
	_pInstance->_timer.reset();
}

void	kxFrame::cleanup(void)
{
	SAFE_DELETE(_pInstance);
}

//---------------------------------------------------------------------------
//	更新
//!	@retval	TRUE	更新可能
//!	@retval	FALSE	更新時間待ち
//---------------------------------------------------------------------------
b32		kxFrame::update(void)
{
	//	経過時間取得
	_time = _timer.get();

	#if USE_FRAME_FPS_ONLY
		//	経過時間を進める
		_secCount  += _time;
	#else	//~#if !USE_FRAME_FPS_ONLY
		//	次のフレームが来るまで待つ
		if( _secCount + _time < _nextFrame ) {
			return FALSE;
		}
		//	経過時間を進める
		_secCount  += _time;
		//	次の更新予定時間を設定
		_nextFrame += _frameTime;

		//	更新が大幅に遅れている場合、時間を調整し直す
		if( _secCount > (_nextFrame + 0.3 * Timer::RATE) ) {
			_nextFrame = _secCount;
		}

		// 描画のスキップの有無
		if( _skip ) {
			//	1フレームの所要時間が大きく過ぎている場合、描画フラグをFALSEにする
			_render = ( _secCount > (_nextFrame + 0.005 * Timer::RATE) )? FALSE: TRUE;
		}
	#endif	//~#if !USE_FRAME_FPS_ONLY

	//	1秒毎にFPSを更新する
	if( _secCount >= Timer::RATE ) {
		_fps  = _fpsFrame;
		_fps += (_renderFrame << 8);
		_fpsFrame	 = 0;
		_renderFrame = 0;
		_secCount	-= Timer::RATE;
		_nextFrame	-= Timer::RATE;
	}

	//	フレームを進める
	_fpsFrame++;
	//	時間の計測開始
	_timer.reset();

	return TRUE;
}

//---------------------------------------------------------------------------
//	更新
//!	@retval	TRUE	描画可能
//!	@retval	FALSE	描画スキップ
//---------------------------------------------------------------------------
b32		kxFrame::updateRender(void)
{
	//	描画フラグがFALSEなら描画をキャンセルする
	if( _render == FALSE ) return FALSE;

	//	描画フレームを進める
	_renderFrame++;
	return TRUE;
}

//---------------------------------------------------------------------------
//	ターゲットFPSの指定
//!	@param	[in] ターゲットFPS
//---------------------------------------------------------------------------
void	kxFrame::setTargetFPS(u32 targetFPS)
{
	_targetFPS = targetFPS;
	_frameTime = Timer::RATE / _targetFPS;
}

#endif	//~#if USE_FRAME

//============================================================================
//	END OF FILE
//============================================================================