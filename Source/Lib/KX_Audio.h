//---------------------------------------------------------------------------
//!
//!	@file	KX_Audio.h
//!	@brief	kxAudioクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#ifndef	__KX_AUDIO_H__
#define __KX_AUDIO_H__

#pragma once


//===========================================================================
//!	kxAudioクラス
//===========================================================================
class kxAudio
{
public:
	//===========================================================================
	//!	SoundBufferクラス
	//===========================================================================
	class SoundBuffer {
	public:
		//-------------------------------------------------------------
		//!	@name 初期化
		//-------------------------------------------------------------
		//!@{

		//!	コンストラクタ
		SoundBuffer(void);
		//!	コンストラクタ
		SoundBuffer(char* pFileName, LPDIRECTSOUND pDSound);
		//!	デストラクタ
		~SoundBuffer(void);

		//!@}

		//-------------------------------------------------------------
		//!	@name サウンド制御
		//-------------------------------------------------------------
		//!@{

		//!	ファイルの読み込み
		b32		load(char* pFileName, LPDIRECTSOUND pDSound);
		//!	再生
		void	play(b32 loop);
		//!	停止
		void	stop(void);
		//!	音量変更
		void	setVolume(s32 volume);
		//!	パン変更
		void	setPan(s32 pan);
		//!	再生速度変更
		void	setSpeed(f32 speed);
		//!	再生されているか調べる
		b32		isPlay(void);

		//!@}

	private:
		//!	WAVEの読み込み
		b32		loadWave(char* pFileName, LPDIRECTSOUND pDSound);
		//!	OGGの読み込み
		b32		loadOgg(char* pFileName, LPDIRECTSOUND pDSound);
		//!	セカンダリバッファの生成
		b32		createSecondaryBuffer(s8* pBuf, LPDSBUFFERDESC pDsbd, LPDIRECTSOUND pDSound);

		//!	Vorbisfileコールバック(ファイルの読み込み)
		static u32	CallbackRead(void* pPtr, u32 size, u32 nmemb, void* pDataSource);
		//!	Vorbisfileコールバック(ファイルシーク）
		static s32	CallbackSeek(void* pDataSource, s64 offset, s32 whence);
		//!	Vorbisfileコールバック(ファイルを閉じる）
		static s32	CallbackClose(void* pDataSource);
		//!	Vorbisfileコールバック(ファイルの現在位置取得）
		static long	CallbackTell(void* pDataSource);

		LPDIRECTSOUNDBUFFER	_pBuffer;	//!< 二次バッファ

		s32	_volume;	//!< 音量
		s32	_pan;		//!< 位置
		f32	_speed;		//!< 速度
		s32	_rate;		//!< 基本速度
	};

	//-------------------------------------------------------------
	//!	@name 初期化
	//-------------------------------------------------------------
	//!@{

	//!	初期化
	b32		init(void);
	//!	解放
	void	cleanup(void);

	//!@}

	//-------------------------------------------------------------
	//!	@name Audio管理
	//-------------------------------------------------------------
	//!@{

	//!	全てのサウンド解放
	void	cleanupAll(void);
	//!	サウンドの解放
	void	cleanup(u16 no);
	//!	読み込み
	b32		load(u16 no, char* pFileName);
	//!	再生
	void	play(u16 no, b32 loop = FALSE);
	//!	停止
	void	stop(u16 no);
	//!	音量変更
	void	setVolume(u16 no, s32 volume);
	//!	パン変更
	void	setPan(u16 no, s32 pan);
	//!	再生速度変更
	void	setSpeed(u16 no, f32 speed);
	//!	再生されているか調べる
	b32		isPlay(u16 no);

	//!@}

	//-------------------------------------------------------------
	//!	@name 取得参照
	//-------------------------------------------------------------
	//!@{

	//!	インスタンスの取得
	//!	@retval	インスタンス
	inline static kxAudio*	getInstance(void) { return &_instance; }

	//!@}

private:
	//!	コンストラクタ
	kxAudio(void);
	//!	デストラクタ
	~kxAudio(void);

	static kxAudio	_instance;	//!< 唯一のインスタンス

	//! サウンドの最大数
	static const u16	WAVE_MAX = 128;

	LPDIRECTSOUND		_pDSound;			//!< サウンドオブジェクト
	LPDIRECTSOUNDBUFFER	_pPrimary;			//!< 一次バッファ
	SoundBuffer*		_pBuffer[WAVE_MAX];	//!< 二次バッファクラス
};

//!	kxAudioのインスタンス取得マクロ
#define	AUDIO	(kxAudio::getInstance())

#endif	//~#if __KX_AUDIO_H__

//============================================================================
//	END OF FILE
//============================================================================