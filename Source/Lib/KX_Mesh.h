//---------------------------------------------------------------------------
//!
//!	@file	KX_Mesh.h
//!	@brief	メッシュクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#ifndef	__KX_MESH_H__
#define	__KX_MESH_H__

#pragma	once


//============================================================================
//!	メッシュクラス
//============================================================================
class kxMesh {
public:
	//---------------------------------------------------------------------------
	//! @name 初期化
	//---------------------------------------------------------------------------
	//!@{

	//!	コンストラクタ
	kxMesh(void);
	//!	コンストラクタ
	kxMesh(const char* pFileName);
	//!	デストラクタ
	virtual ~kxMesh(void);

	//!	解放
	virtual void	cleanup(void);

	//!@}

	//---------------------------------------------------------------------------
	//! @name Mesh管理
	//---------------------------------------------------------------------------
	//!@{

	//!	読み込み
	virtual b32		load(const char* pFileName);
	//!	描画
	virtual void	render(void);
	//!	描画
	virtual void	render(char* pTechnique);

	//!	UVアニメーション
	void	uvAnimation(f32 tu, f32 tv);
	//!	レイピック
	s32		rayPick(VECTOR3* pOut, const VECTOR3& spos, const VECTOR3& svec, f32* pDist, const MATRIX* pTransInv = NULL);

	//!@}

	//---------------------------------------------------------------------------
	//! @name 取得参照
	//---------------------------------------------------------------------------
	//!@{

	//!	メッシュの取得
	//!	@retval	メッシュ
	inline LPD3DXMESH	getMesh(void) { return _pMesh; }

	//!	転送行列の取得
	//!	@retval	転送行列
	inline MATRIX	getTransMatrix(void) { return _transMatrix; }
	//!	転送行列の設定
	//!	@param	mat	[in] セットする転送行列
	inline void		setTransMatrix(const MATRIX& mat) { _transMatrix = mat; }

	//!@}

protected:
	LPD3DXMESH		_pMesh;			//!< メッシュ
	MATRIX			_transMatrix;	//!< 転送行列

	u32				_materialCount;	//!< 材質数
	D3DMATERIAL9*	_pMaterial;		//!< 材質情報
	LPTEXTURE*		_ppTexture;		//!< テクスチャー
};

typedef	kxMesh	KXMESH, *LPKXMESH;

#endif	//~#if __KX_MESH_H__

//============================================================================
//	END OF FILE
//============================================================================