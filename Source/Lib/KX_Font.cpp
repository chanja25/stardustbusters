//---------------------------------------------------------------------------
//!
//!	@file	KX_Font.cpp
//!	@brief	フォントクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "KX.h"


//	インスタンスの定義
kxFont*	kxFont::_pInstance = NULL;

//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
kxFont::kxFont(void)
: _pFont(NULL)
, _pDebugFont(NULL)
, _pStrData(NULL)
{
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
kxFont::~kxFont(void)
{
}

//---------------------------------------------------------------------------
//	初期化
//!	@retval	S_OK	成功
//!	@retval E_FAIL	失敗
//---------------------------------------------------------------------------
b32		kxFont::init(void)
{
	ASSERT(_pInstance == NULL, "既にフォントは初期化されています");
	//	インスタンスの実体生成
	_pInstance = new kxFont();

	#if	USE_FONT
		//	デバッグ用フォントの生成
		if( FAILED(D3DXCreateFont(KXSYSTEM->getDevice(),
								  16,
								  0,
								  FW_NORMAL,
								  0,
								  FALSE,
								  DEFAULT_CHARSET,
								  OUT_DEFAULT_PRECIS,
								  DEFAULT_QUALITY,
								  DEFAULT_PITCH,
								  "Arial",
								  &_pInstance->_pDebugFont)) ) {
			MESSAGE("フォントの作成に失敗", "失敗");
			SAFE_DELETE(_pInstance);
			return FALSE;
		}
	#endif	//~#if USE_FONT

	_pInstance->_pStrData = new STRDATA[STR_MAX];
	ZeroMemory(_pInstance->_pStrData, sizeof(STRDATA) * STR_MAX);
	_pInstance->_numStr = 0;
	_pInstance->_height = 0;

	return TRUE;
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	kxFont::cleanup(void)
{
	//	インスタンスが存在すればフォントと共に解放する
	if( _pInstance ) {
		SAFE_RELEASE(_pInstance->_pDebugFont);
		SAFE_RELEASE(_pInstance->_pFont);
		for( u32 i=0; i < STR_MAX; i++ ) {
			STRDATA* pStrData = &_pInstance->_pStrData[i];
			SAFE_DELETE(pStrData->_pObj);
		}
		SAFE_DELETES(_pInstance->_pStrData);
		SAFE_DELETE(_pInstance);
	}
}


//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	kxFont::update(void)
{
}

//---------------------------------------------------------------------------
//	フォントの生成
//!	@param	width	[in] 横幅
//!	@param	height	[in] 高さ
//!	@param	weight	[in] 太さ(0〜1000)
//!	@param	italic	[in] 斜体
//!	@param	pFont	[in] フォント
//!	@param	effect	[in] 文字効果
//!	@param	power	[in] 効果の度合
//---------------------------------------------------------------------------
b32	kxFont::createFont(u32 height, u32 weight, b32 italic, char* pFont, u32 effect, u32 power)
{
	SAFE_RELEASE(_pFont);

	//	フォントの生成
	if( FAILED(D3DXCreateFont(KXSYSTEM->getDevice(),
							  height,
							  0,
							  weight,
							  0,
							  italic,
							  DEFAULT_CHARSET,
							  OUT_DEFAULT_PRECIS,
							  DEFAULT_QUALITY,
							  FIXED_PITCH | FF_DONTCARE,
							  pFont,
							  &_pFont)) ) {
		MESSAGE("フォントの作成に失敗", "失敗");
		return FALSE;
	}

	STRDATA* pStrData;
	if( _height != height ) {
		for( u32 i=0; i < STR_MAX; i++ ) {
			pStrData = &_pStrData[i];
			SAFE_DELETE(pStrData->_pObj);
			pStrData->_pObj = new KX2DOBJ(height, height);
		}
	}

	_height = height;
	_effect = effect;
	_power	= power;

	return TRUE;
}

//---------------------------------------------------------------------------
//	テキストのテクスチャ作成
//!	@param	str		[in] 表示する文字
//!	@retval	テクスチャー
//---------------------------------------------------------------------------
void	kxFont::createTexture(LPTEXTURE pTexture, const char* str)
{
	if( _pFont == NULL ) return;

	LPBYTE		 pBmp;
	GLYPHMETRICS gm;
	DWORD		 size;
	u32			 uChar;

	//	フォントのビットマップ情報を生成
	HDC hDC = _pFont->GetDC();

	if( IsDBCSLeadByte(str[0]) ) uChar = (str[0] << 8) | str[1] & 0xff;
	else						 uChar = str[0];
	uChar &= 0x0000ffff;
	MAT2 mat = {{0,1}, {0,0}, {0,0}, {0,1}};
	size = GetGlyphOutline(hDC, uChar, GGO_GRAY8_BITMAP, &gm, 0, NULL, &mat);

	pBmp = new BYTE[size];
	GetGlyphOutline(hDC, uChar, GGO_GRAY8_BITMAP, &gm, size, pBmp, &mat);

	TEXTMETRIC tm;
	GetTextMetrics(hDC, &tm);

	//	テクスチャー情報の書き込み
	D3DLOCKED_RECT lock;
	pTexture->LockRect(0, &lock, NULL, D3DLOCK_DISCARD);

	LPDWORD pData = reinterpret_cast<LPDWORD>(lock.pBits);

	//	テクスチャーのデータを初期化
	FillMemory(pData, lock.Pitch * _height, 0xaa00ff00);

	//	文字のサイズを取得
	u32 ofsY = tm.tmAscent - gm.gmptGlyphOrigin.y;
	u32 ofsX = gm.gmptGlyphOrigin.x;

	u32 bmpW = gm.gmBlackBoxX + (4 - gm.gmBlackBoxX%4)%4;
	u32 width = (lock.Pitch / sizeof(DWORD));

	u32 x, y;
	//	文字をテクスチャーに書き込む
	for( y = ofsY; y < ofsY + gm.gmBlackBoxY; y++ ) {
		for( x = ofsX; x < ofsX + bmpW; x++ ) {
			DWORD src = pBmp[x - ofsX + (y - ofsY) * bmpW] * 255 / 64;
			DWORD color = (src << 24) | 0x00ffffff;
			pData[x + y * width] = color;
		}
	}

	b32	flg = FALSE;

	//	効果の処理
	switch( _effect ) {
	case EFFECT_EDGE:
		for( x = ofsX; x < ofsX + bmpW; x++ ) {
			for( y = ofsY; y < ofsY + gm.gmBlackBoxY; y++ ) {
				if( flg && pData[x + y * width] >= 0xfe000000 && pData[x + y * width] & 0x00ffffff ) {
					for( u32 i=1; i < _power + 1; i++ ) {
						for( u32 j=1; j < _power + 1; j++ ) {
							f32 angle = atan2f(static_cast<f32>(j), static_cast<f32>(i));
							u32 k = static_cast<u32>(sinf(angle) * j);
							u32 l = static_cast<u32>(cosf(angle) * i);
							if( x + l < width && y + k < _height && x + l >= 0 && y + k >= 0 ) {
								if( pData[x + l + (y + k) * width] < 0xfe000000 ) pData[x + l + (y + k) * width] = 0xff000000;
							}
							if( x - l < width && y - k < _height && x - l >= 0 && y - k >= 0 ) {
								if( pData[x - l + (y - k) * width] < 0xfe000000 ) pData[x - l + (y - k) * width] = 0xff000000;
							}
							if( x + l < width && y - k < _height && x + l >= 0 && y - k >= 0 ) {
								if( pData[x + l + (y - k) * width] < 0xfe000000 ) pData[x + l + (y - k) * width] = 0xff000000;
							}
							if( x - l < width && y + k < _height && x - l >= 0 && y + k >= 0 ) {
								if( pData[x - l + (y + k) * width] < 0xfe000000 ) pData[x - l + (y + k) * width] = 0xff000000;
							}
						}
					}
				}

				if( pData[x + y * width] < 0xfe000000 || pData[x + y * width] & 0x00ffffff ) flg = TRUE;
				else flg = FALSE;
				if( pData[x + y * width] < 0xff000000 ) pData[x + y * width] = 0;
			}
		}
		break;
	case EFFECT_SHADOW:
		for( x = ofsX; x < ofsX + bmpW; x++ ) {
			for( y = ofsY; y < ofsY + gm.gmBlackBoxY; y++ ) {
				if( flg && pData[x + 1 + (y + 1) * width] < 0xf0000000 ) {
					u32 i;
					u8 alpha = 255;
					for( i = 1; i < _power + 1; i++ ) {
						if( x + i >= width && y + i >= _height ) break;
						if( pData[x + i + (y + i) * width] < 0xf0000000 ) {
							pData[x + i + (y + i) * width] = alpha << 24;
						} else break;
						alpha = alpha >> 1;
					}
				}
				if( pData[x + y * width] >= 0xf0000000 && pData[x + y * width] & 0x00ffffff ) flg = TRUE;
				else flg = FALSE;
			}
		}
		break;
	}
	pTexture->UnlockRect(0);

	SAFE_DELETES(pBmp);
}

//---------------------------------------------------------------------------
//	テクスチャで文字列描画
//!	@param	x		[in] x座標
//!	@param	y		[in] y座標
//!	@param	size	[in] 表示サイズ
//!	@param	color	[in] カラー
//!	@param	pStr	[in] 表示する文字
//!	@param	numW	[in] １列に表示できる文字数
//!	@param	dw		[in] 間隔の微調整用(横)
//!	@param	dw		[in] 間隔の微調整用(縦)
//---------------------------------------------------------------------------
void	kxFont::draw(f32 x, f32 y, f32 size, ARGB color, const char* pStr, u8 numW, f32 dw, f32 dh)
{
	if( _pFont == NULL ) return;

	static char temp[3];
	temp[2] = '\0';

	STRDATA* pStrData;
	u32 i, j;
	u32 num = static_cast<u32>(strlen(pStr));
	for( i = 0; i < num; i++ ) {
		temp[0] = pStr[i];
		temp[1] = '\0';
		if( IsDBCSLeadByte(pStr[i]) ) {
			temp[1] = pStr[++i];
		}
		for( j = 0; j < STR_MAX; j++ ) {
			if( strcmp(_pStrData[j]._str, temp) == 0 ) break;
		}
		if( j == STR_MAX ) {
			u8 num = _numStr;
			if( num < STR_MAX ) _numStr++;
			else {
				u32 count = -1;
				for( j = 0; j < STR_MAX; j++ ) {
					pStrData = &_pStrData[j];
					if( pStrData->_count == 1 ) continue;
					if( pStrData->_count < count ) {
						num = j;
						count = pStrData->_count;
					}
				}
			}
			pStrData = &_pStrData[num];
			strcpy(pStrData->_str, temp);
			createTexture(pStrData->_pObj->getTexture(), temp);
			pStrData->_count = 1;
			continue;
		}
		_pStrData[j]._count++;
	}

	//	文字を描画
	f32 height, width;
	f32 dx = x;
	f32 dy = y;
	b32	flg;
	height = static_cast<f32>(_height);
	u32 next = (numW << 1) - 1;
	for( i = 0; i < num; i++ ) {
		width = height;

		temp[0] = pStr[i];
		temp[1] = '\0';
		flg = IsDBCSLeadByte(pStr[i]);
		if( flg ) {
			temp[1] = pStr[++i];
		} else {
			width /= 2;
		}
		for( j = 0; j < _numStr; j++ ) {
			pStrData = &_pStrData[j];
			if( strcmp(pStrData->_str, temp) == 0 ) {
				pStrData->_pObj->render(dx, dy, (flg)? size: size / 2, size, 0, 0, width, height, "copy", color);
				break;
			}
		}

		if( i == next || (i == next - 1 &&  IsDBCSLeadByte(pStr[i + 1])) ) {
			dx	  = x;
			dy	 += size + dh;
			next += (i == next)? (numW << 1): (numW << 1) - 1;
		} else dx += (flg)? size + dw: (size + dw) / 2;
	}
}

//---------------------------------------------------------------------------
//	文字列の描画
//!	@param	x		[in] x座標
//!	@param	y		[in] y座標
//!	@param	color	[in] カラー
//!	@param	pStr	[in] 表示する文字
//!	@param	...		[in] printfの様に使う。例：("%d", 100)
//---------------------------------------------------------------------------
void	kxFont::drawText(s32 x, s32 y, ARGB color, const char* pStr, ...)
{
	if( _pFont == NULL ) return;

	//	printf("%s" ,a)のように文字列生成
	static va_list list;
	static char temp[PATH_MAX];
	va_start(list, pStr);
	vsprintf(temp, pStr, list);
	va_end(list);

	//	描画位置設定
	static RECT rect;
	rect.left	= x;
	rect.right	= x + _height * strlen(temp);
	rect.top	= y;
	rect.bottom	= y + _height * strlen(temp);
	_pFont->DrawText(NULL,
					 temp,
					 -1,
					 &rect,
					 DT_LEFT | DT_NOCLIP,
					 color);
}

//---------------------------------------------------------------------------
//	デバッグ用テキストの描画
//!	@param	x		[in] x座標
//!	@param	y		[in] y座標
//!	@param	color	[in] カラー
//!	@param	pStr	[in] 表示する文字
//!	@param	...		[in] printfの様に使う。例：("%d", 100)
//---------------------------------------------------------------------------
void	kxFont::drawDebugText(s32 x, s32 y, ARGB color, const char* pStr, ...)
{
	if( _pDebugFont == NULL ) return;

	//	printf("%s" ,a)のように文字列生成
	static va_list list;
	static char temp[PATH_MAX];
	va_start(list, pStr);
	vsprintf(temp, pStr, list);
	va_end(list);

	//	描画位置設定
	static RECT rect;
	rect.left	= x;
	rect.right	= x + 16 * static_cast<u32>(strlen(temp));
	rect.top	= y;
	rect.bottom	= y + 16 * static_cast<u32>(strlen(temp));
	_pDebugFont->DrawText(NULL,
						  temp,
						  -1,
						  &rect,
						  DT_LEFT | DT_NOCLIP,
						  color);
}

//---------------------------------------------------------------------------
//	デバイスの解放
//---------------------------------------------------------------------------
void	kxFont::lost(void)
{
	if( _pFont )		_pFont->OnLostDevice();
	if( _pDebugFont )	_pDebugFont->OnLostDevice();
}

//---------------------------------------------------------------------------
//	デバイスのリセット
//---------------------------------------------------------------------------
void	kxFont::reset(void)
{
	if( _pFont )		_pFont->OnResetDevice();
	if( _pDebugFont )	_pDebugFont->OnResetDevice();
}

//============================================================================
//	END OF FILE
//============================================================================