//---------------------------------------------------------------------------
//!
//!	@file	KX_Audio.cpp
//!	@brief	インプットクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "KX.h"


//	Ogg読み込み関連
# pragma comment(lib, "vorbis_static.lib")
# pragma comment(lib, "ogg_static.lib")
# pragma comment(lib, "vorbisfile_static.lib")

#include "ogg/vorbisfile.h"

//	インスタンスの実体生成
kxAudio	kxAudio::_instance;

//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
kxAudio::kxAudio(void)
: _pDSound(NULL)
, _pPrimary(NULL)
{
	//	初期化
	ZeroMemory(_pBuffer, sizeof(SoundBuffer*) * WAVE_MAX);
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
kxAudio::~kxAudio(void)
{
}

//---------------------------------------------------------------------------
//	初期化
//!	@retval	成功
//!	@retval	失敗
//---------------------------------------------------------------------------
b32		kxAudio::init(void)
{
	ASSERT(_pDSound == NULL, "既にオーディオは初期化されています");

	HRESULT hr;
	//	サウンドオブジェクトの生成
	hr = DirectSoundCreate(NULL, &_pDSound, NULL);
	if( FAILED(hr) ) return FALSE;

	//	協調レベルの設定
	hr = _pDSound->SetCooperativeLevel(KXSYSTEM->getWindowHandle(), DSSCL_PRIORITY);
	if( FAILED(hr) ) return FALSE;

	DSBUFFERDESC dsbdesc;
	//	DSBUFFERDESCの設定
	ZeroMemory(&dsbdesc, sizeof(DSBUFFERDESC));
	dsbdesc.dwSize	= sizeof(DSBUFFERDESC);
	dsbdesc.dwFlags	= DSBCAPS_CTRLVOLUME | DSBCAPS_PRIMARYBUFFER;

	//	一次バッファの生成
	hr = _pDSound->CreateSoundBuffer(&dsbdesc, &_pPrimary, NULL);
	if( FAILED(hr) ) return FALSE;

	return TRUE;
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	kxAudio::cleanup(void)
{
	//	サウンドバッファの解放
	cleanupAll();

	SAFE_RELEASE(_pPrimary);

	SAFE_RELEASE(_pDSound);
}

//---------------------------------------------------------------------------
//	全てのサウンドの解放
//---------------------------------------------------------------------------
void	kxAudio::cleanupAll(void)
{
	for( u16 i=0; i < WAVE_MAX; i++ ) {
		SAFE_DELETE(_pBuffer[i]);
	}
}

//---------------------------------------------------------------------------
//	サウンドの解放
//!	@param	no		[in] 解放する番号
//---------------------------------------------------------------------------
void	kxAudio::cleanup(u16 no)
{
	SAFE_DELETE(_pBuffer[no]);
}

//---------------------------------------------------------------------------
//	読み込み
//!	@param	no			[in] 読み込む番号
//!	@param	pFileName	[in] 読み込むファイル名
//!	@retval	TRUE	成功
//!	@retval	FALSE	失敗
//---------------------------------------------------------------------------
b32		kxAudio::load(u16 no, char* pFileName)
{
	if( _pBuffer[no] == NULL ) {
		//	サウンドバッファの生成
		_pBuffer[no] = new SoundBuffer();
	}

	return _pBuffer[no]->load(pFileName, _pDSound);
}

//---------------------------------------------------------------------------
//	再生
//!	@param	no		[in] 再生する番号
//!	@param	loop	[in] TRUEならループ再生
//---------------------------------------------------------------------------
void	kxAudio::play(u16 no, b32 loop)
{
	if( _pBuffer[no] == NULL ) return;

	_pBuffer[no]->play(loop);
}

//---------------------------------------------------------------------------
//	停止
//!	@param	no		[in] 停止する番号
//---------------------------------------------------------------------------
void	kxAudio::stop(u16 no)
{
	if( _pBuffer[no] == NULL ) return;

	_pBuffer[no]->stop();
}

//---------------------------------------------------------------------------
//	音量変更
//!	@param	no		[in] 変更する番号
//!	@param	volume	[in] 変更後の音量(0〜-10000)
//---------------------------------------------------------------------------
void	kxAudio::setVolume(u16 no, s32 volume)
{
	if( _pBuffer[no] == NULL ) return;

	_pBuffer[no]->setVolume(volume);
}

//---------------------------------------------------------------------------
//	パン変更
//!	@param	no		[in] 変更する番号
//!	@param	pan		[in] 変更後のパン(-10000〜10000)
//---------------------------------------------------------------------------
void	kxAudio::setPan(u16 no, s32 pan)
{
	if( _pBuffer[no] == NULL ) return;

	_pBuffer[no]->setPan(pan);
}

//---------------------------------------------------------------------------
//	再生速度変更
//!	@param	no			[in] 変更する番号
//!	@param	frequency	[in] 変更後の速度<1.0fが基本速度>
//---------------------------------------------------------------------------
void	kxAudio::setSpeed(u16 no, f32 speed)
{
	if( _pBuffer[no] == NULL ) return;

	_pBuffer[no]->setSpeed(speed);
}

//---------------------------------------------------------------------------
//	再生されているか調べる
//!	@param	no		[in] 調べる番号
//!	@retval	TRUE	再生されている
//!	@retval	FALSE	再生されていない
//---------------------------------------------------------------------------
b32		kxAudio::isPlay(u16 no)
{
	if( _pBuffer[no] == NULL ) return FALSE;

	return _pBuffer[no]->isPlay();
}

//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
kxAudio::SoundBuffer::SoundBuffer(void)
: _pBuffer(NULL)
{
}

//---------------------------------------------------------------------------
//	コンストラクタ
//!	@param	pFileName	[in] 読み込むファイル名
//!	@param	pDSound		[in] DirectSoundオブジェクト
//---------------------------------------------------------------------------
kxAudio::SoundBuffer::SoundBuffer(char* pFileName, LPDIRECTSOUND pDSound)
: _pBuffer(NULL)
{
	//	サウンドファイルの読み込み
	if( load(pFileName, pDSound) ) {
		MESSAGE("サウンドファイルの読み込みに失敗しました", "サウンド読み込みエラー");
	}
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
kxAudio::SoundBuffer::~SoundBuffer(void)
{
	SAFE_RELEASE(_pBuffer);
}

//---------------------------------------------------------------------------
//	読み込み
//!	@param	pFileName	[in] 読み込むファイル名
//!	@param	pDSound		[in] DirectSoundオブジェクト
//!	@retval	TRUE	成功
//!	@retval	FALSE	失敗
//---------------------------------------------------------------------------
b32		kxAudio::SoundBuffer::load(char* pFileName, LPDIRECTSOUND pDSound)
{
	//	データがあれば削除する
	SAFE_RELEASE(_pBuffer);

	//	拡張子の取得
	char type[PATH_MAX];
	RESOURCE->getExtension(type, pFileName);
	b32		read_ok = FALSE;

	//	ファイルの種類を識別して読み込む
	if( strcmp(type, "wav") == 0 )	read_ok = loadWave(pFileName, pDSound);
	if( strcmp(type, "ogg") == 0 )	read_ok = loadOgg(pFileName, pDSound);

	//	初期設定
	setVolume(0);
	setPan(0);
	setSpeed(1.0f);

	return read_ok;
}

//---------------------------------------------------------------------------
//	再生
//!	@param	loop	[in] TRUEならループ再生する
//---------------------------------------------------------------------------
void	kxAudio::SoundBuffer::play(b32 loop)
{
	if( _pBuffer == NULL ) return;

	stop();
	_pBuffer->SetCurrentPosition(0);
	_pBuffer->Play(0, 0, (( loop )? DSBPLAY_LOOPING: 0));
}

//---------------------------------------------------------------------------
//	停止
//---------------------------------------------------------------------------
void	kxAudio::SoundBuffer::stop(void)
{
	if( _pBuffer == NULL ) return;

	_pBuffer->Stop();
}

//---------------------------------------------------------------------------
//	音量変更
//!	@param	volume	[in] 変更後の音量(0〜-10000)
//---------------------------------------------------------------------------
void	kxAudio::SoundBuffer::setVolume(s32 volume)
{
	if( _pBuffer == NULL ) return;

	_volume = volume;
	_pBuffer->SetVolume(volume);
}

//---------------------------------------------------------------------------
//	パン変更
//!	@param	pan	[in] 変更後のパン(-10000〜10000)
//---------------------------------------------------------------------------
void	kxAudio::SoundBuffer::setPan(s32 pan)
{
	if( _pBuffer == NULL ) return;

	_pan = pan;
	_pBuffer->SetPan(pan);
}

//---------------------------------------------------------------------------
//	再生速度変更
//!	@param	frequency	[in] 変更後の速度<1.0fが基本速度>
//---------------------------------------------------------------------------
void	kxAudio::SoundBuffer::setSpeed(f32 speed)
{
	if( _pBuffer == NULL ) return;

	_speed = speed;
	s32 frequency = static_cast<s32>(static_cast<f32>(_rate) * speed); 
	_pBuffer->SetFrequency(frequency);
}

//---------------------------------------------------------------------------
//	再生されているか調べる
//!	@retval	TRUE	再生されている
//!	@retval	FALSE	再生されていない
//---------------------------------------------------------------------------
b32		kxAudio::SoundBuffer::isPlay(void)
{
	if( _pBuffer == NULL ) return FALSE;

	DWORD	ans;
	_pBuffer->GetStatus(&ans);
	return ( ans == DSBSTATUS_PLAYING )? TRUE: FALSE;
}

//---------------------------------------------------------------------------
//	WAVEファイルの読み込み
//!	@param	pFileName	[in] 読み込むファイル名
//!	@param	pDSound		[in] プライマリバッファ生成用のDirectSoundオブジェクト
//!	@retval	TRUE	読み込み成功
//!	@retval	FALSE	読み込み失敗
//---------------------------------------------------------------------------
b32		kxAudio::SoundBuffer::loadWave(char* pFileName, LPDIRECTSOUND pDSound)
{
	HMMIO	hmmio;
	b32		read_ok = FALSE;

	MMCKINFO		mainChunk;
	MMCKINFO		subChunk;
	WAVEFORMATEX	wfx;
	s8*				pBuf = NULL;

    DSBUFFERDESC    dsbd;

	// mmioファイルオープン
	hmmio = mmioOpen(pFileName, NULL, MMIO_ALLOCBUF | MMIO_READ);
	if ( hmmio == NULL ) {
		MESSAGE("指定されたWAVEファイルが存在しません", "AUDIO");
		return FALSE;
	}

	//---------------------------------------------------------------------------
	//	WAVEファイルの読み込み
	//---------------------------------------------------------------------------
	//	RIFFチャンクのWAVEタイプ検索
	mainChunk.fccType = mmioFOURCC('W','A', 'V', 'E');
	if( mmioDescend(hmmio, &mainChunk, NULL, MMIO_FINDRIFF) ) {
		MESSAGE("読み込んだファイルはWAVEファイルではありません", "AUDIO");
		return FALSE;
	}

	//	fmtチャンクの検索
	subChunk.ckid = mmioFOURCC('f', 'm', 't', ' ');
	if( mmioDescend(hmmio, &subChunk, &mainChunk, MMIO_FINDCHUNK) ) {
		MESSAGE("読み込んだWAVEファイルは壊れています", "AUDIO");
		return FALSE;
	}

	//	フォーマットの読み込み
	if( mmioRead(hmmio, (HPSTR)&wfx, sizeof(wfx)) != sizeof(wfx) ) {
		MESSAGE("読み込んだWAVEファイルのフォーマットが壊れているか、対応していません", "AUDIO");
		return FALSE;
	}
	//	フォーマットの確認
	if( wfx.wFormatTag != WAVE_FORMAT_PCM ) {
		MESSAGE("読み込んだWAVEファイルのフォーマットが対応してません", "AUDIO");
		return FALSE;
	}
	wfx.cbSize = 0;

	//	チャンクを戻る
	if( mmioAscend(hmmio, &subChunk, 0) ) {
		MESSAGE("WAVEファイルの読み込み中、チャンクから戻るのに失敗しました", "AUDIO");
		return FALSE;
	}

	//	dataチャンクを検索
	subChunk.ckid = mmioFOURCC('d', 'a', 't', 'a');
	if( mmioDescend(hmmio, &subChunk, &mainChunk, MMIO_FINDCHUNK) ) {
		MESSAGE("読み込んだWAVEファイルのデータが壊れています", "AUDIO");
		return FALSE;
	}

	//	メモリ確保
	pBuf = new s8[subChunk.cksize];

	//	バッファの読み込み
	if( (DWORD)mmioRead(hmmio, (HPSTR)pBuf, subChunk.cksize) == subChunk.cksize ) {
		//	セカンダリバッファ情報の設定
		ZeroMemory(&dsbd, sizeof(DSBUFFERDESC));

		dsbd.dwSize	 = sizeof(DSBUFFERDESC);
		dsbd.dwFlags = DSBCAPS_STATIC |			// サウンドデータがスタティック(ストリームではない)
					   DSBCAPS_CTRLVOLUME |		// ボリュームコントロール可能
					   DSBCAPS_CTRLPAN |		// パンコントロール可能
					   DSBCAPS_CTRLFREQUENCY;	// 周波数コントロール有効
		//	データサイズ
		dsbd.dwBufferBytes = subChunk.cksize;
		//	サウンドフォーマット
		dsbd.lpwfxFormat   = &wfx;

		_rate = wfx.nSamplesPerSec;

		//	セカンダリバッファの生成
		read_ok = createSecondaryBuffer(pBuf, &dsbd, pDSound);
	}

	//	メモリ解放
	SAFE_DELETES(pBuf);

	//	ファイルを閉じる
	mmioClose(hmmio, 0);

	return read_ok;
}

//---------------------------------------------------------------------------
//	OGGファイルの読み込み
//!	@param	pFileName	[in] 読み込むファイル名
//!	@param	pDSound		[in] プライマリバッファ生成用のDirectSoundオブジェクト
//!	@retval	TRUE	読み込み成功
//!	@retval	FALSE	読み込み失敗
//---------------------------------------------------------------------------
b32		kxAudio::SoundBuffer::loadOgg(char *pFileName, LPDIRECTSOUND pDSound)
{
	HANDLE			hFile;
	OggVorbis_File	vf;

	DSBUFFERDESC	dsbd;
	WAVEFORMATEX	wfx;

	s8*				pBuf;
	long			actualRead;
	char*			pAsd;

	//	ファイルを読み込む
	hFile = CreateFile(pFileName, GENERIC_READ, FILE_SHARE_READ, (LPSECURITY_ATTRIBUTES)NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, (HANDLE)NULL);
	if( hFile == (HANDLE)-1 ) return FALSE;

	//	Oggファイルを開く
	ov_callbacks oggCallbacks =	{ CallbackRead, CallbackSeek, CallbackClose, CallbackTell };
	if( ov_open_callbacks(hFile, &vf, NULL, 0, oggCallbacks) ) {
		CloseHandle(hFile);
		ASSERT(false, "Oggの読み込み失敗");
		return FALSE;
	}

	//	情報設定
	const vorbis_info* info = ov_info(&vf, -1);
	if( info == NULL ) ov_clear(&vf);

	//	WAVEフォーマット初期化
	ZeroMemory(&wfx, sizeof(WAVEFORMATEX));
	wfx.wFormatTag		= WAVE_FORMAT_PCM;
	wfx.nChannels		= info->channels;
	wfx.nSamplesPerSec	= info->rate;
	wfx.wBitsPerSample	= 16;

	wfx.nBlockAlign		= 4;
	wfx.nAvgBytesPerSec	= info->rate * 4;

	_rate = info->rate;

	//	セカンダリバッファ情報の設定
	ZeroMemory(&dsbd, sizeof(DSBUFFERDESC));
	dsbd.dwSize  = sizeof(DSBUFFERDESC);
	dsbd.dwFlags = DSBCAPS_STATIC |			// サウンドデータがスタティック(ストリームではない)
				   DSBCAPS_CTRLVOLUME |		// ボリュームコントロール可能
				   DSBCAPS_CTRLPAN |		// パンコントロール可能
				   DSBCAPS_CTRLFREQUENCY;	// 周波数コントロール有効
	//	バッファサイズ
	dsbd.dwBufferBytes = (DWORD)(ov_pcm_total(&vf, -1) * info->channels * (wfx.wBitsPerSample / 8));
	//	サウンドフォーマット
	dsbd.lpwfxFormat   = &wfx;

	//	メモリ確保
	pBuf = new s8[dsbd.dwBufferBytes];
	if( pBuf == NULL ) {
		ov_clear(&vf);
		ASSERT(false, "メモリー不足です。");
		return FALSE;
	}

	//	OggファイルをWaveデータに変換
	pAsd = reinterpret_cast<char*>(pBuf);
	do {
		actualRead = ov_read(&vf, pAsd, 4096, 0, 2, 1, NULL);
		pAsd += actualRead;
	} while( actualRead != 0 );

	//	セカンダリバッファの生成
	createSecondaryBuffer(pBuf, &dsbd, pDSound);

	//	メモリ解放
	SAFE_DELETES(pBuf);

	//	ファイルを閉じる
	ov_clear(&vf);

	return TRUE;
}

//---------------------------------------------------------------------------
//	セカンダリバッファの生成
//!	@param	pBuf	[in] セットするデータ
//!	@retval	TRUE	成功
//!	@retval	FALSE	失敗
//---------------------------------------------------------------------------
b32		kxAudio::SoundBuffer::createSecondaryBuffer(s8* pBuf, LPDSBUFFERDESC pDsbd, LPDIRECTSOUND pDSound)
{
	HRESULT hr;

	LPVOID	pQueue1, pQueue2;
	DWORD	queue1, queue2;

	//	セカンダリバッファの生成
	hr = pDSound->CreateSoundBuffer(pDsbd, &_pBuffer, NULL);
	if( SUCCEEDED(hr) ) {
		//	セカンダリバッファのロック
		_pBuffer->Lock(0, pDsbd->dwBufferBytes, &pQueue1, &queue1, &pQueue2, &queue2, 0);
		//	サウンドキュー(リングバッファ)へデータをコピー
		memcpy(pQueue1, pBuf, queue1);
		//	キューが回り込んでいるとき
		if( queue2 ) {
			//	回り込んでいる部分へ残りのデータをコピー
			memcpy(pQueue2, pBuf + queue1, queue2);
		}
		//	セカンダリバッファのロック解除
		_pBuffer->Unlock(pQueue1, queue1, pQueue2, queue2);
		return TRUE;
	}
	return FALSE;
}

//---------------------------------------------------------------------------
//	Vorbisfileコールバック(ファイル読み込み)
//!	@param	pPtr		[out]	読み込んだファイルを入れるポインタ
//!	@param	size		[in]	データのサイズ
//!	@param	nmemb		[in]	データの数
//!	@param	pDataSource	[in]	ファイルのハンドル
//!	@retval	読み込んだファイルサイズ
//---------------------------------------------------------------------------
u32		kxAudio::SoundBuffer::CallbackRead(void* pPtr, u32 size, u32 nmemb, void* pDataSource)
{
	HANDLE	hFile = (HANDLE)pDataSource;
	DWORD	work = 0;
	ReadFile(hFile, pPtr, (DWORD)(size * nmemb), &work, NULL);
	return work;
}

//---------------------------------------------------------------------------
//	Vorbisfileコールバック(ファイルシーク)
//!	@param	pDataSource	[in]	ファイルのハンドル
//!	@param	offset		[in]	データの位置
//!	@param	whence		[in]	シークするモード
//---------------------------------------------------------------------------
s32		kxAudio::SoundBuffer::CallbackSeek(void* pDataSource, s64 offset, s32 whence)
{
	HANDLE	hFile = (HANDLE)pDataSource;
	DWORD	result;

	if( whence == SEEK_CUR )	  result = SetFilePointer(hFile, (long)offset, NULL, FILE_CURRENT);
	else if( whence == SEEK_END ) result = SetFilePointer(hFile, (long)offset, NULL, FILE_END);
	else if( whence == SEEK_SET ) result = SetFilePointer(hFile, (long)offset, NULL, FILE_BEGIN);
	else return -1;

	return ( result == 0xffffffff )? -1 : 0;
}

//---------------------------------------------------------------------------
//	Vorbisfileコールバック(ファイルクローズ)
//!	@param	pDataSource	[in]	ファイルのハンドル
//!	@retval	0	成功
//!	@retval	EOF	失敗
//---------------------------------------------------------------------------
s32		kxAudio::SoundBuffer::CallbackClose(void* pDataSource)
{
	HANDLE hFile = (HANDLE)pDataSource;
	BOOL result = CloseHandle(hFile);
	return result ? 0 : EOF;
}

//---------------------------------------------------------------------------
//	Vorbisfileコールバック(ファイルの現在位置取得)
//!	@param	pDataSource	[in]	ファイルのハンドル
//!	@retval	ファイルの現在位置
//---------------------------------------------------------------------------
long	kxAudio::SoundBuffer::CallbackTell(void* pDataSource)
{
	HANDLE	hFile = (HANDLE)pDataSource;
	DWORD	offset = SetFilePointer(hFile, 0, NULL, FILE_CURRENT);
	return offset;
}

//============================================================================
//	END OF FILE
//============================================================================