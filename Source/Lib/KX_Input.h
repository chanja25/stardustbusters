//---------------------------------------------------------------------------
//!
//!	@file	KX_Input.h
//!	@brief	インプットクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#ifndef	__KX_INPUT_H__
#define	__KX_INPUT_H__

#pragma once


//===========================================================================
//! インプットクラス
//===========================================================================
class kxInput
{
public:
	//!	ジョイスティック用
	enum BUTTON {
		BUTTON_01, BUTTON_02, BUTTON_03, BUTTON_04,
		BUTTON_05, BUTTON_06, BUTTON_07, BUTTON_08,
		BUTTON_09, BUTTON_10, BUTTON_11, BUTTON_12,
		BUTTON_13, BUTTON_14, BUTTON_15, BUTTON_16,
		BUTTON_17, BUTTON_18, BUTTON_19, BUTTON_20,
		BUTTON_UP, BUTTON_DOWN, BUTTON_LEFT, BUTTON_RIGHT,
		BUTTON_MAX,
	};
	
	//!	キー用
	enum KEY {
		KEY_A, KEY_B, KEY_C,  KEY_D,
		KEY_L, KEY_R, KEY_L2, KEY_R2,
		KEY_START, KEY_SELECT,
		KEY_UP, KEY_DOWN, KEY_LEFT, KEY_RIGHT,
		KEY_MAX,
	};

	//---------------------------------------------------------------------------
	//!	@name 初期化
	//---------------------------------------------------------------------------
	//!@{

	//!	初期化
	b32		init(void);
	//!	解放
	void	cleanup(void);

	//!@}

	//!	更新
	void	update(void);

	//!	ボタンの配置変更
	b32		changeButton(u8 no, u8 button);
	//!	ボタンの配置保存
	void	saveButton(void);
	//!	ボタンの配置リセット
	void	defaultButton(void);

	//---------------------------------------------------------------------------
	//!	@name 取得参照
	//---------------------------------------------------------------------------
	//!@{

	//!	キーが押されているかチェック
	b32		isKeyPress(u32 key);
	//!	キーを押したかチェック
	b32		isKeyPush(u32 key);
	//!	キーが離されたかチェック
	b32		isKeyRelease(u32 key);

	//!	ボタンが押されているかチェック
	b32		isButtonPress(u32 button, u32 no = 0);
	//!	ボタンを押したかチェック
	b32		isButtonPush(u32 button, u32 no = 0);
	//!	ボタンが離されたかチェック
	b32		isButtonRelease(u32 button, u32 no = 0);

	//!	左スティックの状態の取得
	VECTOR2	getLeftAxis(u32 no = 0);
	//!	右スティックの状態の取得
	VECTOR2	getRightAxis(u32 no = 0);

	//!	パッドの数のセット
	//!	@retval	パッドの数
	inline u8	getPadNum(void) { return _padNum; }

	//!	インスタンスの取得
	//!	@retval	インスタンス
	inline static kxInput*	getInstance(void) { return &_instance; }

	//!@}

private:
	//!	コンストラクタ
	kxInput(void);
	//!	デストラクタ
	~kxInput(void){};

	static kxInput	_instance;	//!< 唯一のインスタンス

	//!	デバイス毎に呼び出されるコールバック関数
	static b32	CALLBACK	EnumJoyCallBack(const DIDEVICEINSTANCE* pDIDInstance, void* pContext);
	//!	ジョイスティックの軸を列挙するコールバック関数
	static b32	CALLBACK	EnumAxesCallBack(LPCDIDEVICEOBJECTINSTANCE pDDOI, void* pRef);

	//!	パッドの最大数
	static const u8		PAD_MAX = 4;
	//!	スティックの認知する範囲
	static const f32	PAD_DEADZONE;

	u8						_padNum;				//!< パッドの検出数

	DIJOYSTATE2				_dijs;					//!< パッドの状態取得用変数
	LPDIRECTINPUT8			_pDinput;				//!< DirectInputデバイス
	LPDIRECTINPUTDEVICE8	_pKeyDevice;			//!< キーボードデバイス(使用する予定がないのでコメントにします)
	LPDIRECTINPUTDEVICE8	_pPadDevice[PAD_MAX];	//!< ジョイパッドデバイス

	//!	キーマップ
	static const u8 KEYMAP[KEY_MAX];

	BYTE		_disk[256];			//!< キー情報取得用
	u8*			_pKeyData;			//!< キー情報格納用
	VECTOR2		_axis;				//!< 方向キーの状態

	//!	パッド情報格納構造体
	struct	PADDATA {
		u32		_button;		//!< ボタン情報
		u32		_oldButton;		//!< 前回のボタン情報
		VECTOR2	_leftStick;		//!< 左スティックの状態
		VECTOR2	_rightStick;	//!< 右スティックの状態
	};
	PADDATA*	_pPadData;		//!< パッド情報格納用ポインタ
	u8**		_ppButtonData;	//!< ボタンの配置情報格納用
};

//!	インスタンス取得用マクロ
#define INPUT	(kxInput::getInstance())

#endif	//~#if __KX_INPUT_H__

//============================================================================
//	END OF FILE
//============================================================================