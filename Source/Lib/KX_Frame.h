//---------------------------------------------------------------------------
//!
//!	@file	KX_Frame.h
//!	@brief	フレーム制御
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#ifndef	__KX_FRAME_H__
#define __KX_FRAME_H__

#pragma	once

//!	フレーム制御の使用フラグ
#define USE_FRAME_CONTROL	1
//!	FPSの測定のみの使用フラグ
#define USE_FRAME_FPS_ONLY	0

#if USE_FRAME_CONTROL

//===========================================================================
//!	フレーム制御クラス
//===========================================================================
class kxFrame
{
public:
	//---------------------------------------------------------------------------
	//!	@name 初期化
	//---------------------------------------------------------------------------
	//!@{

	//!	初期化
	static void	init(void);
	//!	解放
	static void	cleanup(void);

	//!@}

	//!	更新
	b32		update(void);
	//!	描画フレームの更新
	//!	@retval	描画フラグ
	b32		updateRender(void);

	//---------------------------------------------------------------------------
	//!	@name 取得参照
	//---------------------------------------------------------------------------
	//!@{

	//!	1秒間に実行するフレーム数の設定
	void	setTargetFPS(u32 targetFPS);
	//!	1秒間に実行するフレーム数の取得
	//!	@retval	ターゲットFPS
	inline	u32		getTargetFPS(void) { return _targetFPS; }
	//!	FPSの取得
	//!	@retval	FPS
	inline	u32		getFPS(void) { return (_fps & 0xff); }
	//!	描画FPSの取得
	//!	@retval	描画FPS
	inline	u32		getRenderFPS(void) { return (_fps >> 8); }
	//!	インスタンスの取得
	//!	@retval	インスタンス
	inline	static	kxFrame*	getInstance(void) { return _pInstance; }

	//!@}

private:
	//! コンストラクタ
	kxFrame(void);
	//!	デストラクタ
	~kxFrame(void){}

	static	kxFrame*	_pInstance;	//!<	唯一のインスタンス

	//!	ターゲットFPS
	static	const u8 TARGET_FPS = 60;

	Timer	_timer;					//!< タイマー

	b32		_skip;					//!< 描画のスキップの有無
	u8		_targetFPS;				//!< 1秒間に実行するフレーム数
	s32		_time;					//!< 1フレームの経過時間取得用
	s32		_frameTime;				//!< 1フレームにかける時間
	s32		_secCount;				//!< 1秒間のカウント
	s32		_nextFrame;				//!< 次のフレームまでの予定時間
	u8		_fpsFrame;				//!< 1秒間に実行されているフレーム数
	u8		_renderFrame;			//!< 1秒間に実行されている描画数
	u16		_fps;					//!< 現在のFPS
	b32		_render;				//!< 描画フラグ
};

#define FRAME				(kxFrame::getInstance())

#else	//~#if USE_FRAME_CONTROL

#define FRAME

#endif	//~#if USE_FRAME_CONTROL

#endif	//~#if __KX_FRAME_H__

//============================================================================
//	END OF FILE
//============================================================================