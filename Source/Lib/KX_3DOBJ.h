//---------------------------------------------------------------------------
//!
//!	@file	KX_3DOBJ.h
//!	@brief	kx3DObjectクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#ifndef	__KX_3DOBJ_H__
#define	__KX_3DOBJ_H__

#pragma once


//===========================================================================
//!	kx3DObjectクラス
//===========================================================================
class kx3DObject : public kxMesh
{
public:
	//!	アニメーション
	struct ANIME {
		u16			_rotNum;	//!< 回転キーフレーム数
		u16*		_pRotFrame;	//!< 回転キーフレーム
		QUATERNION*	_pRot;		//!< 回転用クオータニオン

		u16			_posNum;	//!< 位置キーフレーム数
		u16*		_pPosFrame;	//!< 位置キーフレーム
		VECTOR3*	_pPos;		//!< 位置
	};
	//!	ボーン
	struct BONE {
		MATRIX		_boneMatrix;	//!< ボーンマトリックス
		u16			_parent;		//!< 親ボーンの番号

		VECTOR3		_pos;			//!< 基本位置
		QUATERNION	_pose;			//!< 基本姿勢

		u16			_indexNum;		//!< 影響頂点数
		u16*		_pIndex;		//!< 影響頂点
		f32*		_pInfluence;	//!< 影響頂点の重み
	};

	//---------------------------------------------------------------------------
	//! @name 初期化
	//---------------------------------------------------------------------------
	//!@{

	//!	コンストラクタ
	kx3DObject(void);
	//!	コンストラクタ(引数付き)
	kx3DObject(const char* pFileName);
	//!	デストラクタ
	~kx3DObject(void);

	//!	解放
	void	cleanup(void);

	//!@}

	//!	読み込み
	b32		load(const char* pFileName);

	//!	モーションの変更
	void	setMotion(u16 motion, f32 frame = 0);
	//!	モーションブレンド
	void	setMotionBlend(u16 motion1, u16 motion2, f32 k = -1.0f, f32 frame = 0);

	//!	スキンメッシュのフレーム更新
	void	updateSkinMeshFrame(u16 n, f32 frame, QUATERNION* pOutQua, VECTOR3* pOutVec);
	//!	スキンメッシュのフレーム更新(モーションブレンド)
	void	updateMotionBlend(u16 n, f32 frame1, f32 frame2, QUATERNION* pOutQua, VECTOR3* pOutVec);
	//!	スキンメッシュのフレーム更新(モーション移行)
	void	updateMotionShift(void);
	//!	モーションの更新
	void	updateMotion(void);
	//!	ボーン行列の更新
	void	updateBone(void);
	//!	スキンメッシュの更新
	void	updateSkinMesh(void);

	//!	アニメーションの進行
	void	animation(void);

	//!	更新
	void	update(void);
	//!	描画
	void	render(void);
	//!	描画
	void	render(char* pTechnique);

	//---------------------------------------------------------------------------
	//! @name 取得参照
	//---------------------------------------------------------------------------
	//!@{

	//!	アニメーション速度の設定
	//!	@param	speed	[in] 速度
	inline	void	setAnimeSpeed(f32 speed) { _animeSpeed = speed; }
	//!	現在のアニメーション速度の取得
	//!	@retval	アニメ―ション速度
	inline	f32		getAnimeSpeed(void) { return _animeSpeed; }
	//!	ブレンドの度合い(係数)の設定
	//!	@param	ブレンドの度合い(係数)
	inline	void	setBlendPower(f32 k) { _blendPower = k; }
	//!	現在のブレンドの度合い(係数)の取得
	//!	@retval	ブレンドの度合い(係数)
	inline	f32		getBlendPower(void) { return _blendPower; }
	//!	現在のフレームの取得
	//!	@retval	フレーム
	inline	f32		getFrame(void) { return _frame; }
	//!	現在のモーション取得
	//!	@retval	モーション番号
	inline	u16		getMotion(void) { return _motion; }
	//!	ボーンの取得
	//!	@param	no	[in] 取得するボーン番号
	//!	@retval	ボーンのマトリックス
	inline	MATRIX*	getBone(u16 no) { return &_pBoneMatrix[no]; }
	//!	パラメータの設定
	//!	@param	no	[in] セットするパラメータ番号
	//!	@param	no	[in] セットするパラメータ
	inline	void	setParam(u8 no, u8 param) { _param[no] = param; }
	//!	パラメータの取得
	//!	@param	no	[in] 取得するパラメータ番号
	//!	@retval	現在のパラメータ
	inline	u8		getParam(u8 no) { return _param[no]; }

	//!@}

private:
	DWORD					_vertexNum;	//!< 頂点数
	kxGraphics::LPVERTEX3D	_pVertex;	//!< 基本頂点

	LPD3DXSKININFO	_pSkinInfo;		//!< スキンメッシュ情報

	u16				_boneNum;		//!< ボーンの数
	u16*			_pBoneParent;	//!< 親ボーンの番号
	MATRIX*			_pBoneMatrix;	//!< ボーンのマトリックス
	MATRIX*			_pOffsetMatrix;	//!< ボーンの基本マトリックス
	MATRIX*			_pMatrix;		//!< ボーンの最終的なマトリックス

	QUATERNION*		_pPose;			//!< ボーンの姿勢
	VECTOR3*		_pPos;			//!< ボーンの位置

	QUATERNION*		_pOrgPose;		//!< ボーンの基本姿勢
	VECTOR3*		_pOrgPos;		//!< ボーンの基本位置

	ANIME*			_pAnime;		//!< アニメーション
	f32				_animeSpeed;	//!< アニメーションの速度

	u16				_blendMotion;	//!< ブレンドするモーション
	f32				_blendPower;	//!< ブレンドする度合い(係数)

	u16				_motionNum;		//!< モーション数
	u16				_motion;		//!< 現在のモーション
	u16*			_pMotionOffset;	//!< モーション毎の先頭フレーム

	b32				_changeFlag;	//!< モーション移行フラグ
	QUATERNION*		_pBeforePose;	//!< 前回の姿勢
	VECTOR3*		_pBeforePos;	//!< 前回の位置
	f32				_changeFrame;	//!< 次のモーションに移行するまでのフレーム数

	u16				_frameNum;		//!< フレーム数
	f32				_frame;			//!< 現在のフレーム
	f32*			_pDrawFrame;	//!< 描画されているフレーム
	u16*			_pFrameFlag;	//!< フレーム毎のフラグ

	u8				_param[16];		//!< パラメータ
};

//!	KX3DOBJの定義
typedef	kx3DObject	KX3DOBJ, *LPKX3DOBJ;

#endif	//~#if __KX_3DOBJ_H__

//============================================================================
//	END OF FILE
//============================================================================