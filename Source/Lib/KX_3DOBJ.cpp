//---------------------------------------------------------------------------
//!
//!	@file	KX_3DOBJ.cpp
//!	@brief	kx3DObjectクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "KX.h"


//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
kx3DObject::kx3DObject(void)
: _pVertex(NULL)
, _pSkinInfo(NULL)
, _pBoneParent(NULL)
, _pBoneMatrix(NULL)
, _pOffsetMatrix(NULL)
, _pMatrix(NULL)
, _pPose(NULL)
, _pPos(NULL)
, _pOrgPose(NULL)
, _pOrgPos(NULL)
, _pAnime(NULL)
, _pMotionOffset(NULL)
, _pBeforePose(NULL)
, _pBeforePos(NULL)
, _pDrawFrame(NULL)
, _pFrameFlag(NULL)
{
}

//---------------------------------------------------------------------------
//	コンストラクタ(引数付き)
//!	@param	pFileName [in] 読み込むテクスチャーのファイル名
//---------------------------------------------------------------------------
kx3DObject::kx3DObject(const char* pFileName)
: _pVertex(NULL)
, _pSkinInfo(NULL)
, _pBoneParent(NULL)
, _pBoneMatrix(NULL)
, _pOffsetMatrix(NULL)
, _pMatrix(NULL)
, _pPose(NULL)
, _pPos(NULL)
, _pOrgPose(NULL)
, _pOrgPos(NULL)
, _pAnime(NULL)
, _pMotionOffset(NULL)
, _pBeforePose(NULL)
, _pBeforePos(NULL)
, _pDrawFrame(NULL)
, _pFrameFlag(NULL)
{
	load(pFileName);
}

//---------------------------------------------------------------------------
//!	デストラクタ
//---------------------------------------------------------------------------
kx3DObject::~kx3DObject(void)
{
	cleanup();
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	kx3DObject::cleanup(void)
{
	RESOURCE->cleanup(_pSkinInfo);
	_pSkinInfo	= NULL;
	_pMesh		= NULL;
	SAFE_DELETES(_pBeforePose);
	SAFE_DELETES(_pBeforePos);
}

//---------------------------------------------------------------------------
//	読み込み
//!	@param	pFileName [in] ファイル名
//!	@retval	成功
//!	@retval	失敗
//---------------------------------------------------------------------------
b32		kx3DObject::load(const char* pFileName)
{
	//	データを解放しておく
	cleanup();

	//	3DOBJを読み込む
	kxResource::OBJ3D* pData = RESOURCE->load3DOBJ(pFileName);
	if( pData == NULL ) return FALSE;

	//	読み込んだデータをセット
	_pMesh			= pData->_pMesh;
	_materialCount	= pData->_materialCount;
	_pMaterial		= pData->_pMaterial;
	_ppTexture		= pData->_ppTexture;

	_vertexNum		= pData->_vertexNum;
	_pVertex		= pData->_pVertex;

	_pSkinInfo		= pData->_pSkinInfo;

	_boneNum		= pData->_boneNum;
	_pBoneParent	= pData->_pBoneParent;
	_pBoneMatrix	= pData->_pBoneMatrix;
	_pOffsetMatrix	= pData->_pOffsetMatrix;
	_pMatrix		= pData->_pMatrix;

	_pPose			= pData->_pPose;
	_pPos			= pData->_pPos;
	_pOrgPose		= pData->_pOrgPose;
	_pOrgPos		= pData->_pOrgPos;

	_pAnime			= pData->_pAnime;

	_motionNum		= pData->_motionNum;
	_pMotionOffset	= pData->_pMotionOffset;
	_frameNum		= pData->_frameNum;

	_pDrawFrame		= pData->_pDrawFrame;
	_pFrameFlag		= pData->_pFrameFlag;

	MATH->matrixIdentity(&_transMatrix);

	_pBeforePose = new QUATERNION[_boneNum];
	ZeroMemory(_pBeforePose, sizeof(QUATERNION) * _boneNum);
	_pBeforePos	 = new VECTOR3[_boneNum];
	ZeroMemory(_pBeforePos, sizeof(VECTOR3) * _boneNum);

	_motion		 = 0;
	_blendMotion = 0xFFFF;
	_animeSpeed	 = 1.0f;
	_changeFlag	 = FALSE;
	_frame		 = 0;

	ZeroMemory(_param, sizeof(_param));
	return TRUE;
}

//---------------------------------------------------------------------------
//	モーションの変更
//!	@param	motion	[in] 変更後のモーション
//!	@param	frame	[in] 変更にかかる時間(フレーム数)
//---------------------------------------------------------------------------
void	kx3DObject::setMotion(u16 motion, f32 frame)
{
	if( _pMotionOffset[motion] == 0xFFFF ) return;
	_blendMotion = 0xFFFF;
	_motion		 = motion;

	u16	param;
	if( frame > 0 ) {
		_changeFlag	 = TRUE;
		_changeFrame = frame;
		_frame		 = 0;
	} else {
		_frame	= _pMotionOffset[motion];
		param	= _pFrameFlag[static_cast<u16>(_frame)];

		if( (param != 0xFFFF) && (param & 0x4000) ) this->_param[(param&0x0F00)>>8] = static_cast<u8>(param & 0x00FF);
	}
}

//---------------------------------------------------------------------------
//	モーションブレンド
//!	@param	motion1	[in] ブレンドするモーション1
//!	@param	motion1	[in] ブレンドするモーション2
//!	@param	k		[in] 係数(０〜１)、0以下の場合、両方を減衰なく動かす
//!	@param	frame	[in] 変更にかかる時間(フレーム数)
//---------------------------------------------------------------------------
void	kx3DObject::setMotionBlend(u16 motion1, u16 motion2, f32 k, f32 frame)
{
	if( _pMotionOffset[motion1] == 0xFFFF || _pMotionOffset[motion2] == 0xFFFF ) return;
	_motion		 = motion1;
	_blendMotion = motion2;
	_blendPower	 = k;

	u16	param;
	if( frame > 0 ) {
		_changeFlag	 = TRUE;
		_changeFrame = frame;
		_frame		 = 0;
	} else {
		_frame	= _pMotionOffset[motion1];
		param	= _pFrameFlag[static_cast<u16>(_frame)];

		if( (param != 0xFFFF) && (param & 0x4000) ) this->_param[(param&0x0F00)>>8] = static_cast<u8>(param & 0x00FF);
	}
}

//---------------------------------------------------------------------------
//	スキンメッシュのフレーム更新
//!	@param	n		[in]  ボーン番号
//!	@param	frame	[in]  フレーム
//!	@param	pOutQua	[out] 姿勢
//!	@param	pOutVec	[out] 位置
//---------------------------------------------------------------------------
void	kx3DObject::updateSkinMeshFrame(u16 n, f32 frame, QUATERNION* pOutQua, VECTOR3* pOutVec)
{
	u16		i;
	ANIME*	pAnime;
	f32		t;

	pAnime = &_pAnime[n];

	//	ポーズ設定
	if( pAnime->_rotNum == 0 ) {
		_pPose[n] = _pOrgPose[n];
	} if(	pAnime->_rotNum == 1 ) {
		_pPose[n] = pAnime->_pRot[0];
	} else {
		for( i = 0; i < pAnime->_rotNum; i++ ) {
			if( (frame >= pAnime->_pRotFrame[i]) && (frame < pAnime->_pRotFrame[i + 1]) ) {
				t = (frame - static_cast<f32>(pAnime->_pRotFrame[i])) / static_cast<f32>(pAnime->_pRotFrame[i + 1] - pAnime->_pRotFrame[i]);
				if( t > 0 )	D3DXQuaternionSlerp(pOutQua, &pAnime->_pRot[i], &pAnime->_pRot[i + 1], t);
				else		*pOutQua = pAnime->_pRot[i];
				break;
			}
		}
	}

	//	座標設定
	if( pAnime->_posNum == 0 ) {
		_pPos[n] = _pOrgPos[n];
	} else {
		for( i = 0; i < pAnime->_posNum; i++ ) {
			if( (frame >= pAnime->_pPosFrame[i]) && (frame < pAnime->_pPosFrame[i + 1]) ) {
				t = (frame - static_cast<f32>(pAnime->_pPosFrame[i])) / static_cast<f32>(pAnime->_pPosFrame[i + 1] - pAnime->_pPosFrame[i]);
				*pOutVec = pAnime->_pPos[i] + (pAnime->_pPos[i + 1] - pAnime->_pPos[i]) * t;
				break;
			}
		}
	}
}

//---------------------------------------------------------------------------
//	スキンメッシュのフレーム更新(モーションブレンド)
//!	@param	n		[in]  ボーン番号
//!	@param	frame1	[in]  フレーム
//!	@param	frame2	[in]  フレーム
//!	@param	pOutQua	[out] 姿勢
//!	@param	pOutVec	[out] 位置
//---------------------------------------------------------------------------
void	kx3DObject::updateMotionBlend(u16 n, f32 frame1, f32 frame2, QUATERNION* pOutQua, VECTOR3* pOutVec)
{
	u16			i;
	u8			check;
	ANIME*		pAnime;
	QUATERNION	pose1, pose2;
	VECTOR3		pos1, pos2;

	f32	t;

	pAnime = &_pAnime[n];

	//	ポーズ設定
	if( pAnime->_rotNum == 0 ) {
		_pPose[n] = _pOrgPose[n];
	} else {
		check = 0;
		for( i = 0; i < pAnime->_rotNum; i++ ) {
			if( (frame1 >= pAnime->_pRotFrame[i]) && (frame1 < pAnime->_pRotFrame[i + 1]) ) {
				t = (frame1 - static_cast<f32>(pAnime->_pRotFrame[i])) / static_cast<f32>(pAnime->_pRotFrame[i + 1] - pAnime->_pRotFrame[i]);
				if( t > 0 )	D3DXQuaternionSlerp(&pose1, &pAnime->_pRot[i], &pAnime->_pRot[i + 1], t);
				else		pose1 = pAnime->_pRot[i];
				check++;
			}
			if( (frame2 >= pAnime->_pRotFrame[i]) && (frame2 < pAnime->_pRotFrame[i + 1]) ) {
				t = (frame2 - static_cast<f32>(pAnime->_pRotFrame[i])) / static_cast<f32>(pAnime->_pRotFrame[i + 1] - pAnime->_pRotFrame[i]);
				if( t > 0 )	D3DXQuaternionSlerp(&pose2, &pAnime->_pRot[i], &pAnime->_pRot[i + 1], t);
				else		pose2 = pAnime->_pRot[i];
				check++;
			}
			if( check == 2 ) break;
		}

		if( _blendPower >= 0 ) {
			D3DXQuaternionSlerp(&pose1, &pAnime->_pRot[0], &pose1, _blendPower);
			D3DXQuaternionSlerp(&pose2, &pAnime->_pRot[0], &pose2, 1.0f - _blendPower);
		}
		pose1 = pose1 - pAnime->_pRot[0];
		pose2 = pose2 - pAnime->_pRot[0];
		*pOutQua = pAnime->_pRot[0] + pose1 + pose2;
	}

	//	座標設定
	if( pAnime->_posNum == 0 ) {
		_pPos[n] = _pOrgPos[n];
	} else {
		check = 0;
		for( i = 0; i < pAnime->_posNum; i++ ) {
			if( (frame1 >= pAnime->_pPosFrame[i]) && (frame1 < pAnime->_pPosFrame[i + 1]) ) {
				t = (frame1 - static_cast<f32>(pAnime->_pPosFrame[i])) / static_cast<f32>(pAnime->_pPosFrame[i + 1] - pAnime->_pPosFrame[i]);
				pos1 = pAnime->_pPos[i] + (pAnime->_pPos[i + 1] - pAnime->_pPos[i]) * t;
				check++;
			}
			if( (frame2 >= pAnime->_pPosFrame[i]) && (frame2 < pAnime->_pPosFrame[i + 1]) ) {
				t = (frame2 - static_cast<f32>(pAnime->_pPosFrame[i])) / static_cast<f32>(pAnime->_pPosFrame[i + 1] - pAnime->_pPosFrame[i]);
				pos2 = pAnime->_pPos[i] + (pAnime->_pPos[i + 1] - pAnime->_pPos[i]) * t;				if( t > 0 )	*pOutVec = pAnime->_pPos[i] + (pAnime->_pPos[i + 1] - pAnime->_pPos[i]) * t;

				check++;
			}
			if( check == 2 ) break;
		}

		pos1 = pos1 - pAnime->_pPos[0];
		pos2 = pos2 - pAnime->_pPos[0];
		if( _blendPower < 0 ) *pOutVec = pAnime->_pPos[0] + pos1 + pos2;
		else				  *pOutVec = pAnime->_pPos[0] + (pos1 * _blendPower) + (pos2 * (1 - _blendPower));
	}
}
//---------------------------------------------------------------------------
//	モーション移行
//---------------------------------------------------------------------------
void	kx3DObject::updateMotionShift(void)
{
	u16	i;
	f32	t = _frame / _changeFrame;

	for( i = 0; i < _boneNum; i++ ) {
		if( _blendMotion == 0xFFFF ) {
				updateSkinMeshFrame(i, _pMotionOffset[_motion], &_pPose[i], &_pPos[i]);
		} else {
			updateMotionBlend(i, _pMotionOffset[_motion], _pMotionOffset[_blendMotion], &_pPose[i], &_pPos[i]);
		}

		D3DXQuaternionSlerp(&_pPose[i], &_pBeforePose[i], &_pPose[i], t);
		_pPos[i] = _pBeforePos[i] + (_pPos[i] - _pBeforePos[i]) * t;
	}
}

//---------------------------------------------------------------------------
//	モーションの更新
//---------------------------------------------------------------------------
void	kx3DObject::updateMotion(void)
{
	u16 i;

	if( _blendMotion == 0xFFFF ) {
		//	通常のモーション更新
		for( i = 0; i < _boneNum; i++ ) {
			updateSkinMeshFrame(i, _frame, &_pPose[i], &_pPos[i]);
		}
	} else {
		//	モーションブレンドを使用した更新
		f32 scale;
		f32	frame = _frame - static_cast<f32>(_pMotionOffset[_motion]);

		if( _motionNum == _motion + 1 ) {
			scale = static_cast<f32>(_frameNum - _pMotionOffset[_motion]);
		} else scale = static_cast<f32>(_pMotionOffset[_motion + 1] - _pMotionOffset[_motion]);

		if( _motionNum == _blendMotion + 1 ) {
			scale = static_cast<f32>(_frameNum - _pMotionOffset[_blendMotion]) / scale;
			frame = frame * scale + _pMotionOffset[_blendMotion];
			if( frame > _frameNum - 1 ) {
				frame = static_cast<f32>(_frameNum - 1);
			}
		} else {
			scale = static_cast<f32>(_pMotionOffset[_blendMotion + 1] - _pMotionOffset[_blendMotion]) / scale;
			frame = frame * scale + _pMotionOffset[_blendMotion];
			if( frame > _pMotionOffset[_blendMotion + 1] - 1 ) {
				frame = static_cast<f32>(_pMotionOffset[_blendMotion + 1] - 1);
			}
		}

		for( i = 0; i < _boneNum; i++ ) {
			updateMotionBlend(i, _frame, frame, &_pPose[i], &_pPos[i]);
		}
	}
}

//---------------------------------------------------------------------------
//	ボーン行列の更新
//---------------------------------------------------------------------------
void	kx3DObject::updateBone(void)
{
	//	ボーンの更新
	for( u16 i=0; i < _boneNum; i++ ) {
		D3DXMatrixRotationQuaternion( &_pBoneMatrix[i], &_pPose[i] );
		_pBoneMatrix[i]._41 = _pPos[i].x;
		_pBoneMatrix[i]._42 = _pPos[i].y;
		_pBoneMatrix[i]._43 = _pPos[i].z;

		if( _pBoneParent[i] != 0xFFFF ) _pBoneMatrix[i] *= _pBoneMatrix[ _pBoneParent[i] ];
		_pMatrix[i] = _pOffsetMatrix[i] * _pBoneMatrix[i];

		if( _changeFlag == FALSE ) {
			_pBeforePose[i] = _pPose[i];
			_pBeforePos[i]  = _pPos[i];
		}
	}
}

//---------------------------------------------------------------------------
//	スキンメッシュの更新
//---------------------------------------------------------------------------
void	kx3DObject::updateSkinMesh(void)
{
	//	メッシュ更新
	kxGraphics::LPVERTEX3D pVertex;
	_pMesh->LockVertexBuffer(0, reinterpret_cast<void**>(&pVertex));
	_pSkinInfo->UpdateSkinnedMesh(_pMatrix, NULL, _pVertex, pVertex);
	_pMesh->UnlockVertexBuffer();
}

//---------------------------------------------------------------------------
//	アニメーションの更新
//---------------------------------------------------------------------------
void	kx3DObject::animation(void)
{
	u16	param;
	u16	work;

	if( _changeFlag ) {
		_frame += _animeSpeed;
		if( _frame < 0 ) _frame = 0;
		if( _frame >= _changeFrame ) {
			_changeFlag	= FALSE;
			work		= _pMotionOffset[_motion];
			_frame		= static_cast<f32>(work);
			param		= _pFrameFlag[work];
			if( (param != 0xFFFF) && (param & 0x4000) ) this->_param[(param&0x0F00)>>8] = static_cast<u8>(param & 0x00FF);
		}
		return;
	}

	work = static_cast<u16>(_frame);
	param = _pFrameFlag[work];
	if( param & 0x4000 ) param = 0xFFFF;
	if( param != 0xFFFF ) {
		//	アニメーションジャンプ
		if( param & 0x8000 ){
			setMotion(param & 0xFF);
		} else {
			work = param;
			_frame = static_cast<f32>(work);
		}
	} else {
		_frame += _animeSpeed;
		if( _motion == _motionNum - 1 ) {
			_frame = ( _frame > _frameNum - 1 )? _frameNum - 1: _frame;
		} else {
			_frame = ( _frame > _pMotionOffset[_motion + 1] - 1 )? _pMotionOffset[_motion + 1] - 1: _frame;
		}
		work = static_cast<u16>(_frame);
		if( _frame < 0 ) _frame = 0;
	}

	param = _pFrameFlag[work];
	if( (param != 0xFFFF) && (param & 0x4000) ) this->_param[(param & 0x0F00) >> 8] = static_cast<u8>(param & 0x00FF);
}

//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	kx3DObject::update(void)
{
	if( _pAnime == NULL ) return;

	*_pDrawFrame = -1.0f;

	if( _changeFlag ) {
		updateMotionShift();
	} else {
		updateMotion();
		if( _blendMotion == 0xFFFF ) *_pDrawFrame = _frame;
	}

	updateBone();
	updateSkinMesh();
}

//---------------------------------------------------------------------------
//	描画
//---------------------------------------------------------------------------
void	kx3DObject::render(void)
{
	//	Basicで描画
	render("copy");
}

//---------------------------------------------------------------------------
//	描画
//!	@param	pTechnique	[in]	テクニック
//---------------------------------------------------------------------------
void	kx3DObject::render(char* pTechnique)
{
	//	フレームが変わっていれば更新する
	if( _frame != *_pDrawFrame || _changeFlag ) update();
	//	描画
	kxMesh::render(pTechnique);
}

//============================================================================
//	END OF FILE
//============================================================================