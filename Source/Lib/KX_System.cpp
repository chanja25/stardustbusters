//---------------------------------------------------------------------------
//!
//!	@file	KX_System.cpp
//!	@brief	kxSystemクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "KX.h"


//	DirectX関連のLibの読み込み
#pragma comment(lib, "dxguid.lib")
#pragma	comment(lib, "d3d9.lib")
#pragma	comment(lib, "d3dx9.lib")
#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dsound.lib")

#pragma comment(lib, "winmm.lib")

//	インスタンスの実体生成
kxSystem	kxSystem::_instance;

//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
kxSystem::kxSystem(void) :
_pD3D(NULL),
_pDevice(NULL)
{
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
kxSystem::~kxSystem(void)
{
}

//---------------------------------------------------------------------------
//	初期化
//!	@retval	S_OK	成功
//!	@retval E_FAIL	失敗
//---------------------------------------------------------------------------
b32		kxSystem::init(HWND hWnd, u32 width, u32 height, b32 fullScreen)
{
	ASSERT(_pD3D == NULL, "既にKX_SYSTEMは初期化されています");

	//	ウインドウハンドルの取得
	_hWnd	= hWnd;
	_width	= width;
	_height	= height;

	//---------------------------------------------------------------------------
	//	Direct3Dオブジェクトの作成
	//---------------------------------------------------------------------------
	if( (_pD3D = Direct3DCreate9(D3D_SDK_VERSION)) == NULL ) {
		MESSAGE("Direct3Dの作成に失敗しました", "失敗");
		return FALSE;
	}

	//	画面のモード取得
	D3DDISPLAYMODE	d3ddm;
	if( FAILED(_pD3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &d3ddm)) ) {
		MESSAGE("画面のモード取得を失敗しました", "失敗");
		return FALSE;
	}

	//	Direct3D初期化設定
	ZeroMemory(&_d3dpp, sizeof(D3DPRESENT_PARAMETERS));

	if( fullScreen ) {
		_d3dpp.Windowed						= FALSE;
		_d3dpp.FullScreen_RefreshRateInHz	= D3DPRESENT_RATE_DEFAULT;
	} else {
		_d3dpp.Windowed				= TRUE;
	}
	_d3dpp.BackBufferWidth			= _width;
	_d3dpp.BackBufferHeight			= _height;

	_d3dpp.Flags					= 0;
	_d3dpp.BackBufferCount			= 1;
	_d3dpp.BackBufferFormat			= d3ddm.Format;
	_d3dpp.MultiSampleType			= D3DMULTISAMPLE_NONE;
	_d3dpp.SwapEffect				= D3DSWAPEFFECT_DISCARD;
	_d3dpp.hDeviceWindow			= _hWnd;

	_d3dpp.EnableAutoDepthStencil	= TRUE;
	_d3dpp.AutoDepthStencilFormat	= D3DFMT_D16;

	#if	USE_FRAME_FPS_ONLY
		_d3dpp.PresentationInterval		= D3DPRESENT_INTERVAL_ONE;
	#else	//~#if	USE_FRAME_FPS_ONLY
		_d3dpp.PresentationInterval		= D3DPRESENT_INTERVAL_IMMEDIATE;
	#endif	//~#if	USE_FRAME_FPS_ONLY

	//	シェーダーのバージョン確認
	D3DCAPS9	caps;
	ZeroMemory(&caps, sizeof(caps));
	_pD3D->GetDeviceCaps(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, &caps);

	//	PCのシェーダーの性能を調べ、作成するデバイスを決定する
	if( caps.VertexShaderVersion >= D3DVS_VERSION( 1, 0 ) ) {
		//	描画と頂点処理をハードウェアで行う
		if( FAILED(_pD3D->CreateDevice(	D3DADAPTER_DEFAULT,
										D3DDEVTYPE_HAL,
										hWnd,
										D3DCREATE_HARDWARE_VERTEXPROCESSING | D3DCREATE_MULTITHREADED,
										&_d3dpp, &_pDevice )) ) {
			//	描画と頂点処理をハードウェアとCPUの両方で行う
			if( FAILED(_pD3D->CreateDevice(	D3DADAPTER_DEFAULT,
											D3DDEVTYPE_HAL,
											hWnd,
											D3DCREATE_MIXED_VERTEXPROCESSING | D3DCREATE_MULTITHREADED,
											&_d3dpp, &_pDevice )) ) {
				//	描画をハードウェアで行い、頂点処理をCPUで行う
				if( FAILED(_pD3D->CreateDevice(	D3DADAPTER_DEFAULT,
												D3DDEVTYPE_HAL,
												hWnd,
												D3DCREATE_SOFTWARE_VERTEXPROCESSING | D3DCREATE_MULTITHREADED,
												&_d3dpp, &_pDevice )) ) {
					//	失敗
					MESSAGE("Direct3Dの初期化に失敗しました", "失敗");
					return FALSE;
				}
			}
		}
	} else {
		//	描画と頂点処理をソフトウェアで行う
		if( FAILED(_pD3D->CreateDevice(	D3DADAPTER_DEFAULT,
										D3DDEVTYPE_REF,
										hWnd,
										D3DCREATE_SOFTWARE_VERTEXPROCESSING | D3DCREATE_PUREDEVICE | D3DCREATE_MULTITHREADED,
										&_d3dpp, &_pDevice )) ) {
			//	描画をハードウェアで行い、頂点処理をCPUで行う
			if( FAILED(_pD3D->CreateDevice(	D3DADAPTER_DEFAULT,
											D3DDEVTYPE_HAL,
											hWnd,
											D3DCREATE_SOFTWARE_VERTEXPROCESSING | D3DCREATE_MULTITHREADED,
											&_d3dpp, &_pDevice )) ) {
				//	失敗
				MESSAGE("Direct3Dの初期化に失敗しました", "失敗");
				return FALSE;
			}
		}
	}


	D3DVIEWPORT9 vp;
	vp.X		= 0;
	vp.Y		= 0;
	vp.Width	= _d3dpp.BackBufferWidth;
	vp.Height	= _d3dpp.BackBufferHeight;
	vp.MinZ		= 0.0f;
	vp.MaxZ		= 1.0f;

	if( FAILED(_pDevice->SetViewport(&vp)) ) return FALSE;

	return TRUE;
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	kxSystem::cleanup(void)
{
	SAFE_RELEASE(_pD3D);
	SAFE_RELEASE(_pDevice);
}

//---------------------------------------------------------------------------
//	描画開始
//---------------------------------------------------------------------------
void	kxSystem::beginScene(void)
{
	//	描画開始
	_pDevice->BeginScene();

	//	バックバッファとZバッファのクリア
	_pDevice->Clear(0,
					NULL,
					D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER,
					0xff000000,
					1.0f,
					0);

}

//---------------------------------------------------------------------------
//	描画終了
//---------------------------------------------------------------------------
void	kxSystem::endScene(void)
{
	//	描画終了
	_pDevice->EndScene();

	//	画面に反映させる
	if( FAILED(_pDevice->Present(NULL, NULL, NULL, NULL)) ) {
		_pDevice->Reset(&_d3dpp);
	}
}

//---------------------------------------------------------------------------
//	スクリーンモード変更
//---------------------------------------------------------------------------
void	kxSystem::changeScreenMode(void)
{
	#if	USE_FONT
		FONT->lost();
	#endif	//~#if USE_FONT
	GRAPHICS->getShader2D()->lost();
	GRAPHICS->getShader3D()->lost();

	//	モードの変更
	_d3dpp.Windowed = !_d3dpp.Windowed;
	_pDevice->Reset(&_d3dpp);

	#if	USE_FONT
		FONT->reset();
	#endif	//~#if USE_FONT
	GRAPHICS->getShader2D()->reset();
	GRAPHICS->getShader3D()->reset();
}

//============================================================================
//	END OF FILE
//============================================================================