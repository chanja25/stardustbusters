//---------------------------------------------------------------------------
//!
//!	@file	KX_System.h
//!	@brief	kxSystemクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#ifndef	__KX_SYSTEM_H__
#define	__KX_SYSTEM_H__

#pragma once


//===========================================================================
//!	kxSystemクラス
//===========================================================================
class kxSystem
{
public:
	//---------------------------------------------------------------------------
	//!	@name 初期化
	//---------------------------------------------------------------------------
	//!@{

	//!	初期化
	b32		init(HWND hWnd, u32 width, u32 height, b32 fullScreen);
	//!	解放
	void	cleanup(void);

	//!@}

	//!	描画開始
	void	beginScene(void);
	//!	描画終了
	void	endScene(void);

	//!	スクリーンモード変更
	void	changeScreenMode(void);

	//-------------------------------------------------------------
	//!	@name 取得参照
	//-------------------------------------------------------------
	//!@{

	//!	ウインドウハンドルの取得
	//!	@retval	ウインドウハンドル
	inline HWND					getWindowHandle(void) { return _hWnd; };
	//!	DirectXデバイスの取得
	//!	@retval DirectXデバイス
	inline LPDIRECT3D9			getD3D(void) { return _pD3D; };
	//!	Direct3Dデバイスの取得
	//!	@retval Direct3DDevice
	inline LPDIRECT3DDEVICE9	getDevice(void) { return _pDevice; };
	//!	バックバッファのフォーマット
	//!	@retval バックバッファ
	inline D3DFORMAT			getBackBufferFormat(void) { return _d3dpp.BackBufferFormat; };
	//!	画面のサイズ取得
	//!	@retval	横幅
	inline	u32					getWidth(void)	{ return _width; };
	//!	画面のサイズ取得
	//!	@retval	縦幅
	inline	u32					getHeight(void)	{ return _height; };

	//!	インスタンスの取得
	//!	@retval	インスタンス
	inline static kxSystem*		getInstance(void) { return &_instance; };

	//!@}

private:
	//!	コンストラクタ
	kxSystem(void);
	//!	デストラクタ
	~kxSystem(void);

	static kxSystem _instance;			//!< 唯一のインスタンス

	HWND					_hWnd;		//!< ウィンドウハンドル
	LPDIRECT3D9				_pD3D;		//!< DirectXデバイス
	LPDIRECT3DDEVICE9		_pDevice;	//!< Direct3DDevice
	D3DPRESENT_PARAMETERS	_d3dpp;		//!< Direct3Dの設定用
	u32						_width;		//!< 画面の横幅
	u32						_height;	//!< 画面の縦幅
};

//!	kxSystemのインスタンス取得マクロ
#define	KXSYSTEM	(kxSystem::getInstance())

#endif	//~#if __KX_SYSTEM_H__

//============================================================================
//	END OF FILE
//============================================================================