//---------------------------------------------------------------------------
//!
//!	@file	KX_Converter.cpp
//!	@brief	kxConverterクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "KX.h"

//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
kxConverter::kxConverter(void)
{
}

//---------------------------------------------------------------------------
//!	デストラクタ
//---------------------------------------------------------------------------
kxConverter::~kxConverter(void)
{
	cleanup();
}

//---------------------------------------------------------------------------
//	Mqoファイルの読み込み(アニメーション付き)
//!	@param	pFileName	 [in] ファイル名
//!	@param	pSrcFileName [in] ファイル名
//!	@retval	TRUE	成功
//!	@retval	FALSE	失敗
//---------------------------------------------------------------------------
b32		kxConverter::saveKAMFromMQO(const char* pFileName, const char* pSrcFileName)
{
	Text data;

	char path[PATH_MAX];

	//	パスの取得
	strcpy(path, pSrcFileName);

	//	ボーンの読み込み(MKI)
	path[strlen(path)-3] = '\0';
	strcat(path, "mki");
	BONE* pBone = loadBoneFromMKI(path);
	if( pBone == NULL ) {
		MESSAGE("ボーンの読み込み失敗", "失敗");
		return FALSE;
	}

	//	モーションの読み込み(MKM)
	path[strlen(path)-3] = '\0';
	strcat(path, "mkm");
	MOTION* pMotion = loadMotionFromMKM(path);

	//	メッシュデータ読み込み
	kxResource::MESHDATA meshData;
	meshData = loadMQO(pSrcFileName);
	if( meshData._pMesh == NULL ) {
		MESSAGE("モデルデータの読み込み失敗", "失敗");
		return FALSE;
	}

	//	アンカーの読み込みと頂点の関連付け(MQO)
	ANCHOR* pAnchor;
	u8 numAnchor = loadAnchorFromMQO(&pAnchor, pSrcFileName, meshData._pMesh);
	if( pAnchor == NULL ) {
		MESSAGE("アンカーの読み込み失敗", "失敗");
		return FALSE;
	}

	//	ボーンの読み込みとアンカーとの関連付け(MQO)
	loadBoneFromMQO(pBone, pSrcFileName, pAnchor, numAnchor);
	if( pBone == NULL ) {
		MESSAGE("ボーンの読み込み失敗(MQO)", "失敗");
		return FALSE;
	}

	return TRUE;
}

//---------------------------------------------------------------------------
//	ボーンの読み込み(MKI)
//!	@param	pFileName	[in]	ファイル名
//!	@retval	読み込んだボーンデータ
//---------------------------------------------------------------------------
kxConverter::BONE*	kxConverter::loadBoneFromMKI(const char* pFileName)
{
	u32	i, j;
	Text	data;

	//	MKIファイルの読み込み
	data.load(pFileName);

	//	読み込んだデータがMKIファイルか確かめる
	char* pCheckMKI[4] = { "Mikoto", "Intermediate", "Ver", "1" };
	for( i = 0; i < 4; i++ ) {
		if( strcmp(data.getParamStr(), pCheckMKI[i]) ) return NULL;
	}

	BONE*	pBone = new BONE;
	ZeroMemory(pBone, sizeof(BONE));

	//	ボーン情報を取得
	data.getCommand("Locate");
	while( strcmp(data.getCommand(), "Locate") == 0 ) {
		data.getCommand("name");
		data.getNextCommand();
		strcpy(pBone->_posName[pBone->_numPos], data.getParamStr());

		data.getCommand("spos");
		data.getNextCommand();
		pBone->_pos[pBone->_numPos].x = data.getParamFloat();
		pBone->_pos[pBone->_numPos].y = data.getParamFloat();
		pBone->_pos[pBone->_numPos].z = data.getParamFloat();
		pBone->_numPos++;

		data.getCommand("Bone");
		if( strcmp(data.getCommand(), "Bone") == 0 ) {
			data.getCommand("name");
			data.getNextCommand();
			strcpy(pBone->_boneName[pBone->_numBone], data.getParamStr());
			if( strcmp(data.getParamStr(), "coordinate") == 0 ) {
				data.getNextCommand();
				strcpy(pBone->_parent[pBone->_numBone], data.getParamStr());
			}
			data.getCommand("srot");
			data.getNextCommand();
			pBone->_pose[pBone->_numBone].x = data.getParamFloat();
			pBone->_pose[pBone->_numBone].y = data.getParamFloat();
			pBone->_pose[pBone->_numBone].z = data.getParamFloat();
			pBone->_pose[pBone->_numBone].w = data.getParamFloat();

			data.getCommand("start");
			data.getNextCommand();
			strcpy(pBone->_bonePosName[pBone->_numBone], data.getParamStr());
			pBone->_numBone++;
			data.getNextCommand();
		}

		data.getCommand("Locate");
	}

	//	ボーンを必要数生成して初期化しておく
	pBone->_pBone = new kx3DObject::BONE[pBone->_numBone];
	ZeroMemory(pBone->_pBone, sizeof(kx3DObject::BONE) * pBone->_numBone);

	//	基本マトリックス生成と親子付け
	for( i = 0; i < pBone->_numBone; i++ ) {

		//	クオータニオンから回転マトリックス生成
		pBone->_pBone[i]._pose = pBone->_pose[i];
		D3DXMatrixRotationQuaternion(&pBone->_pBone[i]._boneMatrix, &pBone->_pose[i]);

		//	マトリックスに位置を設定
		for( j = 0; j < pBone->_numPos; j++ ) {
			if( strcmp(pBone->_bonePosName[i], pBone->_posName[j]) == 0 ) {
				pBone->_pBone[i]._pos = pBone->_pos[j];

				MATRIX mat;
				MATH->matrixTranslation(&mat, pBone->_pBone[i]._pos);
				pBone->_pBone[i]._boneMatrix = mat * pBone->_pBone[i]._boneMatrix;
				break;
			}
		}

		//	親が存在しないとき0xFFFFを入れておく 
		pBone->_pBone[i]._parent = 0xFFFF;
		if( pBone->_parent[i][0] == '\0' ) continue;

		//	親の番号をセットする
		for( j = 0; j < pBone->_numBone; j++ ) {
			if( i == j ) continue;
			if( strcmp(pBone->_parent[i], pBone->_boneName[j]) == 0 ) {
				MATRIX m, m2;
				VECTOR3 p = pBone->_pBone[i]._pos;
				D3DXMatrixRotationQuaternion(&m, &pBone->_pose[j]);
				D3DXVec3TransformCoord(&p, &p, &m);
				pBone->_pBone[i]._pos = pBone->_pBone[j]._pos + p;
				MATH->matrixTranslation(&pBone->_pBone[i]._boneMatrix, pBone->_pBone[i]._pos);

				D3DXMatrixRotationQuaternion(&m, &pBone->_pose[i]);
				D3DXMatrixRotationQuaternion(&m2, &pBone->_pose[j]);
				m = m2 * m;

				pBone->_pBone[i]._boneMatrix = pBone->_pBone[i]._boneMatrix * m;
				pBone->_pBone[i]._parent = j;
				break;
			}
		}
	}

	return pBone;
}

//---------------------------------------------------------------------------
//	モーションの読み込み(MKM)
//!	@param	pFileName	[in]	ファイル名
//!	@retval	アンカー数
//---------------------------------------------------------------------------
kxConverter::MOTION*		kxConverter::loadMotionFromMKM(const char* pFileName)
{
	MOTION*	pMotion = new MOTION;
	ZeroMemory(pMotion, sizeof(MOTION));

	return pMotion;
}

//---------------------------------------------------------------------------
//	アンカーの読み込み(MQO)
//!	@param	pAnchor		[out]	読み込んだアンカー
//!	@param	pFileName	[in]	ファイル名
//!	@param	pMesh		[in]	メッシュデータ
//!	@retval	アンカー数
//---------------------------------------------------------------------------
u8		kxConverter::loadAnchorFromMQO(ANCHOR** ppAnchor, const char* pFileName, const LPD3DXMESH pMesh)
{
	u32 i, j, k, l;
	Text data;

	//メタセコイアのモデルデータ読み込み
	data.load(pFileName);

	//	面数と頂点数を取得
	DWORD numFace	= pMesh->GetNumFaces();
	DWORD numVertex	= pMesh->GetNumVertices();

	//	アンカーの生成
	ANCHOR* pAnchor = new ANCHOR[ANCHOR_MAX];
	ZeroMemory(pAnchor, sizeof(ANCHOR) * ANCHOR_MAX);

	i = 0;
	//	アンカー情報を取得
	data.getCommand("Object");
	while( strcmp(data.getCommand(), "Object") == 0 ) {
		char temp[PATH_MAX];
		memcpy(temp, data.getParamStr(), sizeof(char) * 6);
		temp[6] = '\0';
		if( strcmp(temp, "anchor") ) {
			data.getCommand("Object");
			continue;
		}

		//	頂点を取得
		data.getCommand("vertex");
		pAnchor[i]._numPos = data.getParamInt();
		pAnchor[i]._pPos = new VECTOR3[pAnchor[i]._numPos];
		ZeroMemory(pAnchor[i]._pPos,  sizeof(VECTOR3) * pAnchor[i]._numPos);
		for( j = 0; j < pAnchor[i]._numPos; j++ ) {
			pAnchor[i]._pPos[j].x = -data.getParamFloat();
			pAnchor[i]._pPos[j].y =  data.getParamFloat();
			pAnchor[i]._pPos[j].z =  data.getParamFloat();
		}

		//	面情報取得
		data.getCommand("face");
		k = data.getParamInt();
		pAnchor[i]._numFace = k * 2;
		pAnchor[i]._pFace  = new u16[pAnchor[i]._numFace * 3];
		ZeroMemory(pAnchor[i]._pFace,  sizeof(u16) * pAnchor[i]._numFace * 3);

		for( j = 0; j < k; j++ ) {
			b32 flg = ( data.getParamInt() == 4 )? TRUE: FALSE;

			if( !flg ) {
				pAnchor[i]._numFace--;
			}

			data.getCommand("V");
			pAnchor[i]._pFace[pAnchor[i]._numIndex * 3 + 0] = data.getParamInt();
			pAnchor[i]._pFace[pAnchor[i]._numIndex * 3 + 1] = data.getParamInt();
			pAnchor[i]._pFace[pAnchor[i]._numIndex * 3 + 2] = data.getParamInt();

			if( flg ) {
				pAnchor[i]._pFace[pAnchor[i]._numIndex * 3 + 3] = pAnchor[i]._pFace[pAnchor[i]._numIndex * 3 + 0];
				pAnchor[i]._pFace[pAnchor[i]._numIndex * 3 + 4] = pAnchor[i]._pFace[pAnchor[i]._numIndex * 3 + 2];
				pAnchor[i]._pFace[pAnchor[i]._numIndex * 3 + 5] = data.getParamInt();
				pAnchor[i]._numIndex++;
			}
			pAnchor[i]._numIndex++;

			data.getCommand("UV");
			u32	m = (flg)? 8: 6;
			for( l = 0; l < m; l++ ) {
				data.getNextCommand();
			}
		}

		i++;
		data.getCommand("Object");
	}

	//	アンカーの数を取得
	u8 numAnchor = i;

	//	メッシュの頂点情報を取り出す
	kxGraphics::VERTEX3D* pVertex;
	pMesh->LockVertexBuffer(D3DLOCK_READONLY , reinterpret_cast<void**>(&pVertex));

	//	アンカーに頂点の関連付けをする
	for( i = 0; i < numAnchor; i++ ) {
		l = 0;
		//	アンカーの生成
		pAnchor[i]._pIndex = new DWORD[numVertex];
		ZeroMemory(pAnchor[i]._pIndex, sizeof(DWORD) * numVertex);

		//	アンカー毎の内側にある頂点を検索する
		for( j = 0; j < numVertex; j++ ) {
			VECTOR3 pos;

			//	頂点の座標を取得する
			pos.x = pVertex[j].x;
			pos.y = pVertex[j].y;
			pos.z = pVertex[j].z;

			VECTOR3 v1, v2, n, v;
			u16	a, b, c;

			//	全ての面の内側にあるか確かめる
			for( k = 0; k < pAnchor[i]._numFace; k++ ) {
				a = pAnchor[i]._pFace[k * 3 + 0];
				b = pAnchor[i]._pFace[k * 3 + 1];
				c = pAnchor[i]._pFace[k * 3 + 2];

				v1 = pAnchor[i]._pPos[b] - pAnchor[i]._pPos[a];
				v2 = pAnchor[i]._pPos[c] - pAnchor[i]._pPos[a];
				MATH->cross(&n, v1, v2);
				v = (pAnchor[i]._pPos[a] + pAnchor[i]._pPos[b] + pAnchor[i]._pPos[c]) / 3.0f;
				v = v - pos;

				//	内積の結果が0より大きければ面の内側にある
				if( MATH->dot(n, v) < 0 ) break;
			}

			//	全ての面の内側ならアンカーに頂点を設定する
			if( k == pAnchor[i]._numFace ) {
				pAnchor[i]._pIndex[l] = j;
				l++;
			}
		}
		//	設定した頂点数を取得しておく
		pAnchor[i]._numIndex = l;

		//	頂点数分だけ重みを生成しておく
		pAnchor[i]._pInfluence = new f32[l];
		ZeroMemory(pAnchor[i]._pInfluence, sizeof(f32) * l);
	}

	pMesh->UnlockVertexBuffer();

	//	作成したアンカーデータを入れておく
	*ppAnchor = pAnchor;

	return numAnchor;
}

//---------------------------------------------------------------------------
//	ボーンの読み込み(MQO)
//!	@param	pBone		[in/out] ボーンの情報
//!	@param	pFileName	[in]	 ファイル名
//!	@param	pAnchor		[in]	 アンカー情報
//---------------------------------------------------------------------------
void	kxConverter::loadBoneFromMQO(BONE* pBone, const char* pFileName, const ANCHOR* pAnchor, u32	numAnchor)
{
	u32	i, j, k, l;
	Text data;

	u16		 numPos;
	VECTOR3* pPos;
	u16		 numFace;
	u16*	 pFace;

	//	メタセコイアデータの読み込み
	data.load(pFileName);

	i = 0;
	//	ボーン情報を取得
	data.getCommand("Object");
	while( strcmp(data.getCommand(), "Object") == 0 ) {
		char temp[PATH_MAX];
		memcpy(temp, data.getParamStr(), sizeof(char) * 6);
		temp[4] = '\0';
		if( strcmp(temp, "bone") ) {
			data.getCommand("Object");
			continue;
		}

		//	頂点を取得
		data.getCommand("vertex");
		numPos = data.getParamInt();
		pPos = new VECTOR3[numPos];
		ZeroMemory(pPos, sizeof(VECTOR3) * numPos);
		for( j = 0; j < numPos; j++ ) {
			pPos[j].x = -data.getParamFloat();
			pPos[j].y =  data.getParamFloat();
			pPos[j].z =  data.getParamFloat();
		}

		//	面情報取得
		data.getCommand("face");
		numFace = data.getParamInt();
		pFace = new u16[numFace * 3];
		ZeroMemory(pFace, sizeof(u16) * numFace * 3);
		for( j = 0; j < numFace; j++ ) {
			if( data.getParamInt() == 2 ) {
				pFace[j * 3] = 0xFFFF;
				continue;
			}
			data.getCommand("V");
			pFace[j * 3 + 0] = data.getParamInt();
			pFace[j * 3 + 1] = data.getParamInt();
			pFace[j * 3 + 2] = data.getParamInt();
		}

		i++;
		data.getCommand("Object");
	}

	VECTOR3* pBonePos = new VECTOR3[pBone->_numBone * 2];
	ZeroMemory(pBonePos, sizeof(VECTOR3) * pBone->_numBone * 2);

	l = 0;
	//	ボーン毎の頂点情報取得
	for( i = 0; i < numFace; i++ ) {
		if( pFace[i * 3] == 0xFFFF ) continue;
		f32	len[3];
		u8	r[3];

		//	三角形の辺毎の長さを取得
		len[0] = MATH->length2(pPos[pFace[i * 3 + 0]] - pPos[pFace[i * 3 + 1]]);
		len[1] = MATH->length2(pPos[pFace[i * 3 + 1]] - pPos[pFace[i * 3 + 2]]);
		len[2] = MATH->length2(pPos[pFace[i * 3 + 2]] - pPos[pFace[i * 3 + 0]]);

		//	辺の長さに順位を付ける
		r[0] = r[1] = r[2] = 2;
		for( j = 0; j < 3; j++ ) {
			for( k = 0; k < 3; k++ ) {
				if( j == k ) continue;
				if( len[j] > len[k] ) {
					r[j]--;
				}
			}
		}

		//	ボーンの頂点を取得する
		if( r[0] == 1 ) {
			if( r[1] == 0 )	{
				pBonePos[l * 2 + 0] = pPos[pFace[i * 3 + 0]];
				pBonePos[l * 2 + 1] = pPos[pFace[i * 3 + 1]];
			} else {
				pBonePos[l * 2 + 0] = pPos[pFace[i * 3 + 1]];
				pBonePos[l * 2 + 1] = pPos[pFace[i * 3 + 0]];
			}
		}

		if( r[1] == 1 ) {
			if( r[2] == 0 )	{
				pBonePos[l * 2 + 0] = pPos[pFace[i * 3 + 1]];
				pBonePos[l * 2 + 1] = pPos[pFace[i * 3 + 2]];
			} else {
				pBonePos[l * 2 + 0] = pPos[pFace[i * 3 + 2]];
				pBonePos[l * 2 + 1] = pPos[pFace[i * 3 + 1]];
			}
		}

		if( r[2] == 1 ) {
			if( r[0] == 0 )	{
				pBonePos[l * 2 + 0] = pPos[pFace[i * 3 + 2]];
				pBonePos[l * 2 + 1] = pPos[pFace[i * 3 + 0]];
			} else {
				pBonePos[l * 2 + 0] = pPos[pFace[i * 3 + 0]];
				pBonePos[l * 2 + 1] = pPos[pFace[i * 3 + 2]];
			}
		}

		l++;
	}

	u16*	pBoneNum	 = new u16[pBone->_numBone];
	u16*	pBoneIndex	 = new u16[pBone->_numBone];
	u16*	pAnchorNum	 = new u16[numAnchor];
	u16*	pAnchorIndex = new u16[numAnchor];

	ZeroMemory(pBoneNum,	sizeof(u16) * pBone->_numBone);
	ZeroMemory(pBoneIndex,	sizeof(u16) * pBone->_numBone);
	ZeroMemory(pAnchorNum,	sizeof(u16) * numAnchor);
	ZeroMemory(pAnchorNum,	sizeof(u16) * numAnchor);

	u16 check = 0;

	//	アンカーとボーンの関連付け
	for( l = 0; l < 3; l++ ) {
		for( i = 0; i < numAnchor; i++ ) {
			if( pAnchorNum[i] == 0xFFFF ) continue;
			pAnchorNum[i] = 0;

			const ANCHOR* pAnc = &pAnchor[i];

			for( j = 0; j < pBone->_numBone; j++ ) {
				if( pBoneNum[j] == 0xFFFF ) continue;
				pBoneNum[j] = 0;

				//	ボーン毎の位置を取得
				VECTOR3 pos1 = pBonePos[j * 2 + 0];
				VECTOR3 pos2 = pBonePos[j * 2 + 1];

				VECTOR3 v1, v2, n, v;
				u16	a, b, c;
				//	ボーンの位置が面の内側にあるか調べる
				for( k = 0; k < pAnc->_numFace; k++ ) {
					a = pAnc->_pFace[k * 3 + 0];
					b = pAnc->_pFace[k * 3 + 1];
					c = pAnc->_pFace[k * 3 + 2];

					v1 = pAnc->_pPos[b] - pAnc->_pPos[a];
					v2 = pAnc->_pPos[c] - pAnc->_pPos[a];
					MATH->cross(&n, v1, v2);
					v = (pAnc->_pPos[a] + pAnc->_pPos[b] + pAnc->_pPos[c]) / 3.0f;
					v1 = v - pos1;
					v2 = v - pos2;

					//	内積の結果が0より大きければ面の内側にある
					if( MATH->dot(n, v1) < 0 ) break;
					if( MATH->dot(n, v2) < 0 ) break;
				}

				//	全ての面の内側ならアンカーに頂点を設定する
				if( k == pAnc->_numFace ) {
					pAnchorNum[i]++;
					pAnchorIndex[i] = j;
					pBoneNum[j]++;
					pBoneIndex[j] = i;
				}
			}

			//	アンカーの中に入っているボーンが一つだけの場合
			//	そのボーンとアンカーを関連付けする
			if( pAnchorNum[i] == 1 ) {
				pAnchorNum[i] = 0xFFFF;
				pBoneNum[pAnchorIndex[i]] = 0xFFFF;
				pBoneIndex[pAnchorIndex[i]] = i;
				check++;
			}
		}

		//	一つのアンカーにしか入っていないボーンがあればそれをアンカーと関連付ける
		for( i = 0; i < pBone->_numBone; i++ ) {
			if( pBoneNum[i] != 1 ) continue;

			pBoneNum[i] = 0xFFFF;
			pAnchorNum[pBoneIndex[i]] = 0xFFFF;
			pAnchorIndex[pBoneIndex[i]] = i;
			check++;
		}

		//	全てのボーンが関連付けされていれば終わる
		if( check == pBone->_numBone ) break;
	}

	//	アンカーとボーンの関連付け
	for( l = 0; l < 3; l++ ) {
		if( check == pBone->_numBone ) break;

		for( i = 0; i < numAnchor; i++ ) {
			if( pAnchorNum[i] == 0xFFFF ) continue;
			pAnchorNum[i] = 0;

			const ANCHOR* pAnc = &pAnchor[i];

			for( j = 0; j < pBone->_numBone; j++ ) {
				if( pBoneNum[j] == 0xFFFF ) continue;
				pBoneNum[j] = 0;

				//	ボーン毎の中間位置を取得
				VECTOR3 pos = (pBonePos[j * 2 + 0] + pBonePos[j * 2 + 1]) / 2.0f;

				VECTOR3 v1, v2, n, v;
				u16	a, b, c;
				//	ボーンの位置が面の内側にあるか調べる
				for( k = 0; k < pAnc->_numFace; k++ ) {
					a = pAnc->_pFace[k * 3 + 0];
					b = pAnc->_pFace[k * 3 + 1];
					c = pAnc->_pFace[k * 3 + 2];

					v1 = pAnc->_pPos[b] - pAnc->_pPos[a];
					v2 = pAnc->_pPos[c] - pAnc->_pPos[a];
					MATH->cross(&n, v1, v2);
					v = (pAnc->_pPos[a] + pAnc->_pPos[b] + pAnc->_pPos[c]) / 3.0f;
					v = v - pos;

					//	内積の結果が0より大きければ面の内側にある
					if( MATH->dot(n, v) < 0 ) break;
				}

				//	全ての面の内側ならアンカーに頂点を設定する
				if( k == pAnc->_numFace ) {
					pAnchorNum[i]++;
					pAnchorIndex[i] = j;
					pBoneNum[j]++;
					pBoneIndex[j] = i;
				}
			}

			//	アンカーの中に入っているボーンが一つだけの場合
			//	そのボーンとアンカーを関連付けする
			if( pAnchorNum[i] == 1 ) {
				pAnchorNum[i] = 0xFFFF;
				pBoneNum[pAnchorIndex[i]] = 0xFFFF;
				pBoneIndex[pAnchorIndex[i]] = i;
				check++;
			}
		}

		//	一つのアンカーにしか入っていないボーンがあればそれをアンカーと関連付ける
		for( i = 0; i < pBone->_numBone; i++ ) {
			if( pBoneNum[i] != 1 ) continue;

			pBoneNum[i] = 0xFFFF;
			pAnchorNum[pBoneIndex[i]] = 0xFFFF;
			pAnchorIndex[pBoneIndex[i]] = i;
			check++;
		}

		if( check == pBone->_numBone ) break;
	}

	//	データの解放
	SAFE_DELETES(pBoneNum);
	SAFE_DELETES(pBoneIndex);
	SAFE_DELETES(pAnchorNum);
	SAFE_DELETES(pAnchorIndex);
}

//---------------------------------------------------------------------------
//	メタセコイアデータの読み込み
//!	@param	pFileName [in] ファイル名
//!	@retval	メッシュデータ
//---------------------------------------------------------------------------
kxResource::MESHDATA	kxConverter::loadMQO(const char* pFileName)
{
	u32 i, j, k, l;

	//	メッシュデータの作成
	kxResource::MESHDATA meshData;
	ZeroMemory(&meshData, sizeof(kxResource::MESHDATA));

	//	ファイル名のコピー
	strcpy(meshData._fileName, pFileName);

	//	メタセコイアのモデルデータ読み込み
	Text data;
	data.load(pFileName);

	//	読み込んだデータがメタセコイアのモデルファイル(Ver 1.0)か確かめる
	char* pCheck[6] = { "Metasequoia", "Document", "Format", "Text", "Ver", "1.0" };
	for( i = 0; i < 6; i++ ) {
		if( strcmp(data.getParamStr(), pCheck[i]) ) return meshData;
	}

	//	パスの取得
	char path[PATH_MAX];
	strcpy(path, pFileName);
	for( i = static_cast<u32>(strlen(path)) - 1; i > 0; i-- ) {
		if( IsDBCSLeadByte(path[i - 1]) ) {
			i--;
			continue;
		}
		if( path[i] == '\\' || path[i] == '/' ) {
			path[i + 1] = '\0';
			break;
		}
	}
	if( i == 0 ) path[0] = '\0';

	//---------------------------------------------------------------------------
	//	材質取得
	//---------------------------------------------------------------------------
	data.getCommand("Material");
	meshData._materialCount = data.getParamInt();

	//	材質とテクスチャーを必要数生成
	meshData._pMaterial = new D3DMATERIAL9[meshData._materialCount];
	meshData._ppTexture = new LPTEXTURE[meshData._materialCount];
	ZeroMemory(meshData._pMaterial, sizeof(D3DMATERIAL9) * meshData._materialCount);
	ZeroMemory(meshData._ppTexture, sizeof(LPTEXTURE) * meshData._materialCount);

	//	材質データ取得
	for( i = 0; i < meshData._materialCount; i++ ) {
		D3DMATERIAL9* pMaterial = &meshData._pMaterial[i];
		data.getCommand("col");
		f32	r = data.getParamFloat();
		f32	g = data.getParamFloat();
		f32	b = data.getParamFloat();
		f32	a = data.getParamFloat();
		data.getCommand("dif");
		f32 tmp = data.getParamFloat();
		pMaterial->Diffuse.a = a;
		pMaterial->Diffuse.r = tmp * r;
		pMaterial->Diffuse.g = tmp * g;
		pMaterial->Diffuse.b = tmp * b;

		data.getCommand("amb");
		tmp = data.getParamFloat();
		pMaterial->Ambient.a = a;
		pMaterial->Ambient.r = tmp * r;
		pMaterial->Ambient.g = tmp * g;
		pMaterial->Ambient.b = tmp * b;

		data.getCommand("emi");
		tmp = data.getParamFloat();
		pMaterial->Emissive.a = a;
		pMaterial->Emissive.r = tmp * r;
		pMaterial->Emissive.g = tmp * g;
		pMaterial->Emissive.b = tmp * b;

		data.getCommand("spc");
		tmp = data.getParamFloat();
		pMaterial->Specular.a = a;
		pMaterial->Specular.r = tmp * r;
		pMaterial->Specular.g = tmp * g;
		pMaterial->Specular.b = tmp * b;

		data.getCommand("power");
		pMaterial->Power = data.getParamFloat();

		//	テクスチャーの取得
		meshData._ppTexture[i] = NULL;
		if( strcmp(data.getParamStr(), "tex") == 0 ) {
			char temp[PATH_MAX];
			sprintf_s(temp, "%s%s", path, data.getParamStr());
			meshData._ppTexture[i] = RESOURCE->loadTexture(temp);
		}
	}

	//---------------------------------------------------------------------------
	//	オブジェクトデータ取得
	//---------------------------------------------------------------------------
	data.getCommand("Object");

	u32 numObject = 0;
	u32 numFace[OBJECT_MAX];
	u32 numFaceAll = 0;

	//	オブジェクト数と面数を取得
	while( strcmp(data.getCommand(), "Object") == 0 ) {
		char temp[PATH_MAX];
		memcpy(temp, data.getParamStr(), sizeof(char) * 6);
		if( temp[0] == 'b' ) {
			temp[4] = '\0';
			if( strcmp(temp, "bone") == 0 ) {
				data.getCommand("Object");
				continue;
			}
		}
		if( temp[0] == 'a' ) {
			temp[6] = '\0';
			if( strcmp(temp, "anchor") == 0 ) {
				data.getCommand("Object");
				continue;
			}
		}

		data.getCommand("face");
		numFace[numObject] = data.getParamInt();
		j = numFace[numObject];
		for( i = 0; i < j; i++ ) {
			if( data.getParamInt() == 4 ) {
				numFace[numObject]++;
				data.getCommand("M");
				data.getNextCommand();
				if( strcmp(data.getParamStr(), "UV") == 0 ) {
					for( k = 0; k < 8; k++ ) data.getParamFloat();
				}
			} else {
				data.getCommand("M");
				data.getNextCommand();
				if( strcmp(data.getParamStr(), "UV") == 0 ) {
					for( k = 0; k < 6; k++ ) data.getParamFloat();
				}
			}
		}
		numFaceAll += numFace[numObject];
		numObject++;
		data.getCommand("Object");
	}

	//	データの最初まで戻る
	data.setIP(0);

	//	頂点を必要数生成
	kxGraphics::VERTEX3D* pVertex = new kxGraphics::VERTEX3D[numFaceAll * 3];
	//	頂点番号を必要数生成
	u16*		pIndex	= new u16[numFaceAll * 3];
	u16*		pIndex2	= new u16[numFaceAll * 3];
	//	属性を必要数生成
	DWORD*		pAtt	= new DWORD[numFaceAll];

	ZeroMemory(pVertex,	sizeof(kxGraphics::VERTEX3D) * numFaceAll * 3);
	ZeroMemory(pIndex,	sizeof(u16) * numFaceAll * 3);
	ZeroMemory(pIndex2,	sizeof(u16) * numFaceAll * 3);
	ZeroMemory(pAtt,	sizeof(DWORD) * numFaceAll);

	k = l = 0;
	//	オブジェクト情報を取得
	data.getCommand("Object");
	while( strcmp(data.getCommand(), "Object") == 0 ) {
		char temp[PATH_MAX];
		memcpy(temp, data.getParamStr(), sizeof(char) * 6);
		if( temp[0] == 'b' ) {
			temp[4] = '\0';
			if( strcmp(temp, "bone") == 0 ) continue;
		}
		if( temp[0] == 'a' ) {
			temp[6] = '\0';
			if( strcmp(temp, "anchor") == 0 ) continue;
		}

		//	頂点取得
		data.getCommand("vertex");
		u32 tmp = data.getParamInt();
		for( i = 0; i < tmp; i++ ) {
			pVertex[k].x = -data.getParamFloat();
			pVertex[k].y =  data.getParamFloat();
			pVertex[k].z =  data.getParamFloat();
			k++;
		}

		//	面情報取得
		data.getCommand("face");
		u32 tmp2 = data.getParamInt();
		for( i = 0; i < tmp2; i++ ) {
			b32 numFlg = ( data.getParamInt() == 4 )? TRUE: FALSE;

			//	頂点番号取得
			data.getCommand("V");
			pIndex[l * 3 + 0] = pIndex2[l * 3 + 0] = data.getParamInt() + k - tmp;
			pIndex[l * 3 + 1] = pIndex2[l * 3 + 1] = data.getParamInt() + k - tmp;
			pIndex[l * 3 + 2] = pIndex2[l * 3 + 2] = data.getParamInt() + k - tmp;
			if( numFlg ) {
				pIndex[l * 3 + 3] = pIndex2[l * 3 + 3] = pIndex[l * 3 + 0];
				pIndex[l * 3 + 4] = pIndex2[l * 3 + 4] = pIndex[l * 3 + 2];
				pIndex[l * 3 + 5] = pIndex2[l * 3 + 5] = data.getParamInt() + k - tmp;
			}

			//	材質番号取得
			data.getCommand("M");
			u32 a = pAtt[l] = data.getParamInt();
			if( numFlg ) pAtt[l + 1] = pAtt[l];

			//	UV座標取得
			if( strcmp(data.getParamStr(), "UV") == 0 ) {
				for( u32 n=0; n < 3; n++ ) {
					VECTOR2	texUV;
					u32 flg = FALSE;
					texUV.x = data.getParamFloat();
					texUV.y = data.getParamFloat();

					for( u32 m=0; m < l * 3; m++ ) {
						if( pIndex2[l * 3 + n] == pIndex2[m] ) {
							if( texUV.x == pVertex[pIndex[m]].tu &&
								texUV.y == pVertex[pIndex[m]].tv ) {
								pIndex[l * 3 + n] = pIndex[m];
								flg = FALSE;
								break;
							} else {
								flg = TRUE;
							}
						}
					}
					if( flg ) {
						pVertex[k] = pVertex[pIndex[l * 3 + n]];
						pIndex[l * 3 + n] = k;
						k++;
						tmp++;
					}
					pVertex[pIndex[l * 3 + n]].tu = texUV.x;
					pVertex[pIndex[l * 3 + n]].tv = texUV.y;
				}

				if( numFlg ) {
					VECTOR2	texUV;
					u32 flg = FALSE;
					texUV.x = data.getParamFloat();
					texUV.y = data.getParamFloat();
					
					pIndex[l * 3 + 3] = pIndex[l * 3 + 0];
					pIndex[l * 3 + 4] = pIndex[l * 3 + 2];

					for( u32 m=0; m < l * 3; m++ ) {
						if( pIndex2[l * 3 + 5] == pIndex2[m] ) {
							if( texUV.x == pVertex[pIndex[m]].tu &&
								texUV.y == pVertex[pIndex[m]].tv ) {
								pIndex[l * 3 + 5] = pIndex[m];
								flg = FALSE;
								break;
							} else {
								flg = TRUE;
							}
						}
					}
					if( flg ) {
						pVertex[k] = pVertex[pIndex[l * 3 + 5]];
						pIndex[l * 3 + 5] = k;
						k++;
						tmp++;
					}
					pVertex[pIndex[l * 3 + 5]].tu = texUV.x;
					pVertex[pIndex[l * 3 + 5]].tv = texUV.y;
				}
			}
			l = ( numFlg )? l + 2: l + 1;
		}
		data.getCommand("Object");
	}
	u32 numVertex = k;

	//	法線の算出
	for( i = 0; i < numFaceAll; i++ ) {
		u32	a, b, c;
		a = pIndex[i * 3 + 0];
		b = pIndex[i * 3 + 1];
		c = pIndex[i * 3 + 2];

		VECTOR3 n, v1, v2;
		v1.x = pVertex[b].x - pVertex[a].x;
		v1.y = pVertex[b].y - pVertex[a].y;
		v1.z = pVertex[b].z - pVertex[a].z;

		v2.x = pVertex[c].x - pVertex[a].x;
		v2.y = pVertex[c].y - pVertex[a].y;
		v2.z = pVertex[c].z - pVertex[a].z;

		MATH->cross(&n, v1, v2);
		MATH->normalize(&n, n);

		pVertex[a].nx += n.x;
		pVertex[a].ny += n.y;
		pVertex[a].nz += n.z;
		pVertex[b].nx += n.x;
		pVertex[b].ny += n.y;
		pVertex[b].nz += n.z;
		pVertex[c].nx += n.x;
		pVertex[c].ny += n.y;
		pVertex[c].nz += n.z;
	}
	for( i = 0; i < numVertex; i++ ) {
		VECTOR3 n(pVertex[i].nx, pVertex[i].ny, pVertex[i].nz);
		MATH->normalize(&n, n);
		pVertex[i].nx = n.x;
		pVertex[i].ny = n.y;
		pVertex[i].nz = n.z;
	}

	u8*	pVertexs;
	u8*	pFace;
	DWORD*	pAttribute;

	//	メッシュの生成
	D3DXCreateMeshFVF(numFaceAll, numVertex, D3DXMESH_MANAGED, kxGraphics::D3DFVF_VERTEX3D, KXSYSTEM->getDevice(), &meshData._pMesh);
	LPD3DXMESH pMesh = meshData._pMesh;

	//	頂点情報セット
	pMesh->LockVertexBuffer(0, reinterpret_cast<void**>(&pVertexs));
	memcpy(pVertexs, pVertex, sizeof(kxGraphics::VERTEX3D) * numVertex);
	pMesh->UnlockVertexBuffer();

	//	面情報セット
	pMesh->LockIndexBuffer(0, reinterpret_cast<void**>(&pFace));
	memcpy(pFace, pIndex, sizeof(u16) * numFaceAll * 3);
	pMesh->UnlockIndexBuffer();

	//	属性セット
	pMesh->LockAttributeBuffer(0, &pAttribute);
	memcpy(pAttribute, pAtt, sizeof(DWORD) * numFaceAll);
	pMesh->UnlockAttributeBuffer();

	//	データの解放
	SAFE_DELETES(pVertex);
	SAFE_DELETES(pIndex);
	SAFE_DELETES(pIndex2);
	SAFE_DELETES(pAtt);

	return meshData;
}

//---------------------------------------------------------------------------
//	KMOファイルの読み込み
//!	@param	pFileName [in] ファイル名
//!	@retval	メッシュデータ
//---------------------------------------------------------------------------
kxResource::MESHDATA	kxConverter::loadKMO(const char* pFileName)
{
	u16 i;

	//	メッシュデータの作成
	kxResource::MESHDATA meshData;
	ZeroMemory(&meshData, sizeof(kxResource::MESHDATA));
	strcpy(meshData._fileName, pFileName);

	//	ファイルの読み込み
	File file;
	file.readingFile(pFileName);

	//	パスの取得
	char path[PATH_MAX];
	strcpy(path, pFileName);
	for( i = static_cast<u32>(strlen(path) - 1); i > 0; i-- ) {
		if( IsDBCSLeadByte(path[i - 1]) ) {
			i--;
			continue;
		}
		if( path[i] == '\\' || path[i] == '/' ) {
			path[i + 1] = '\0';
			break;
		}
	}
	if( i == 0 ) path[0] = '\0';

	//	ファイルチェック
	char temp[PATH_MAX];
	temp[4] = '\0';
	file.reading(temp, 4);
	if( strcmp(temp, "KMO ") ) return meshData;
	file.reading(temp, 4);
	f32 version = static_cast<f32>(atof(temp));

	//	材質数取得
	file.reading(&meshData._materialCount, sizeof(DWORD));

	//	材質とテクスチャーを必要数生成
	meshData._pMaterial = new D3DMATERIAL9[meshData._materialCount];
	ASSERT(meshData._pMaterial, "メモリー不足です");
	meshData._ppTexture = new LPTEXTURE[meshData._materialCount];
	ASSERT(meshData._ppTexture, "メモリー不足です");
	ZeroMemory(meshData._pMaterial, sizeof(D3DMATERIAL9) * meshData._materialCount);
	ZeroMemory(meshData._ppTexture, sizeof(LPTEXTURE) * meshData._materialCount);

	//	テクスチャーの取得
	for( i = 0; i < meshData._materialCount; i++ ) {
		u8 size;
		file.reading(&size, sizeof(u8));
		if( size ) {
			char temp[PATH_MAX];
			char fileName[PATH_MAX];
			temp[size] = '\0';
			file.reading(temp, size);
			sprintf_s(fileName, "%s%s", path, temp);
			meshData._ppTexture[i] = RESOURCE->loadTexture(fileName);
		}
	}

	//	材質の取得
	file.reading(meshData._pMaterial, sizeof(D3DMATERIAL9) * meshData._materialCount);

	//	メッシュの書き出し
	u8*		pVertexs;
	u8*		pIndex;
	DWORD*	pAttribute;
	DWORD	numVertexs;
	DWORD	numFaces;

	//	頂点数、面数の取得
	file.reading(&numVertexs,	sizeof(DWORD));
	file.reading(&numFaces,		sizeof(DWORD));

	//	メッシュの生成
	D3DXCreateMeshFVF(numFaces, numVertexs, D3DXMESH_MANAGED, kxGraphics::D3DFVF_VERTEX3D, KXSYSTEM->getDevice(), &meshData._pMesh);
	LPD3DXMESH pMesh = meshData._pMesh;

	//	頂点情報セット
	pMesh->LockVertexBuffer(D3DLOCK_DISCARD, reinterpret_cast<void**>(&pVertexs));
	file.reading(pVertexs, sizeof(kxGraphics::VERTEX3D) * numVertexs);
	pMesh->UnlockVertexBuffer();

	//	面情報セット
	pMesh->LockIndexBuffer(D3DLOCK_DISCARD, reinterpret_cast<void**>(&pIndex));
	file.reading(pIndex, sizeof(u16) * numFaces * 3);
	pMesh->UnlockIndexBuffer();

	//	属性セット
	pMesh->LockAttributeBuffer(D3DLOCK_DISCARD, &pAttribute);
	file.reading(pAttribute, sizeof(DWORD) * numFaces);
	pMesh->UnlockAttributeBuffer();

	//	ファイルを閉じる
	file.closeFile();

	return meshData;
}

//---------------------------------------------------------------------------
//	KMOファイルの書き出し
//!	@param	pFileName	 [in] 書き出すファイル名
//!	@param	pSrcFileName [in] 読み込むファイル名
//!	@retval	TRUE	成功
//!	@retval	FALSE	失敗
//---------------------------------------------------------------------------
b32		kxConverter::saveKMOFromX(const char* pFileName, const char* pSrcFileName)
{
	HRESULT	hr;

	LPD3DXBUFFER			pMaterialBuffer;
	kxResource::MESHDATA	meshData;

	//	メッシュの読み込み
	hr = D3DXLoadMeshFromX( pSrcFileName,
							D3DXMESH_MANAGED,
							KXSYSTEM->getDevice(),
							NULL,
							&pMaterialBuffer,
							NULL,
							&meshData._materialCount,
							&meshData._pMesh );
	if( FAILED(hr) ) return FALSE;

	//	法線がない場合、法線を計算し追加する
	if( meshData._pMesh->GetFVF() != (D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1) ) {
		LPD3DXMESH	pMeshTemp;
		meshData._pMesh->CloneMeshFVF(meshData._pMesh->GetOptions(),
										D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1,
										KXSYSTEM->getDevice(),
										&pMeshTemp);
		D3DXComputeNormals(pMeshTemp, NULL);
		SAFE_RELEASE(meshData._pMesh);
		meshData._pMesh = pMeshTemp;
	}

	//	材質の取得
	D3DXMATERIAL* pMaterials = (D3DXMATERIAL*)pMaterialBuffer->GetBufferPointer();

	//	面の数だけ材質とテクスチャーを作成する
	meshData._pMaterial = new D3DMATERIAL9[meshData._materialCount];
	ASSERT(meshData._pMaterial, "メモリー不足です");
	ZeroMemory(meshData._pMaterial, sizeof(D3DMATERIAL9) * meshData._materialCount);

	//---------------------------------------------------------------------------
	//	KMOファイルの書き出し
	//---------------------------------------------------------------------------
	File file;

	//	ファイルチェック用文字列書き出し
	file.writtingFile(pFileName);
	file.writting("KMO ", 4);
	//	バージョン書き出し
	file.writting("0.00", 4);

	//	材質数を書き出し
	file.writting(&meshData._materialCount, sizeof(DWORD));

	//	材質とテクスチャーパスを取得する
	for( u32 i = 0; i < meshData._materialCount; i++ ) {
		//	材質取得
		D3DMATERIAL9*	pMaterial = &meshData._pMaterial[i];
		*pMaterial = pMaterials[i].MatD3D;
		pMaterial->Ambient = meshData._pMaterial[i].Diffuse;

		//	テクスチャーパス取得
		char temp[PATH_MAX];
		ZeroMemory(temp, sizeof(char) * PATH_MAX);
		if( pMaterials[i].pTextureFilename ) {
			strcpy(temp, pMaterials[i].pTextureFilename);
			//	パスサイズ書き出し
			u8 size = static_cast<u8>(strlen(temp));
			file.writting(&size, sizeof(u8));
			//	パスの書き出し
			file.writting(temp, sizeof(char) * size);
		} else {
			//	テクスチャーが存在しない場合、サイズに0を入れておく
			u8 size = 0;
			file.writting(&size, sizeof(u8));
		}
	}
	//	材質の書き出し
	file.writting(meshData._pMaterial, sizeof(D3DMATERIAL9) * meshData._materialCount);

	//	メッシュの書き出し
	u8*		pVertexs;
	u8*		pIndex;
	DWORD*	pAttribute;

	LPD3DXMESH pMesh = meshData._pMesh;

	//	頂点数、面数の取得
	DWORD numVertexs = pMesh->GetNumVertices();
	DWORD numFaces	 = pMesh->GetNumFaces();
	//	頂点数、面数の書き出し
	file.writting(&numVertexs,	sizeof(DWORD));
	file.writting(&numFaces,	sizeof(DWORD));

	//	頂点情報書き出し
	pMesh->LockVertexBuffer(D3DLOCK_READONLY, reinterpret_cast<void**>(&pVertexs));
	file.writting(pVertexs, sizeof(kxGraphics::VERTEX3D) * numVertexs);
	pMesh->UnlockVertexBuffer();

	//	面情報書き出し
	pMesh->LockIndexBuffer(D3DLOCK_READONLY, reinterpret_cast<void**>(&pIndex));
	file.writting(pIndex, sizeof(u16) * numFaces * 3);
	pMesh->UnlockIndexBuffer();

	//	属性書き出し
	pMesh->LockAttributeBuffer(D3DLOCK_READONLY, &pAttribute);
	file.writting(pAttribute, sizeof(DWORD) * numFaces);
	pMesh->UnlockAttributeBuffer();

	//	データの解放
	SAFE_RELEASE(pMaterialBuffer);
	SAFE_RELEASE(meshData._pMesh);
	SAFE_DELETES(meshData._pMaterial);

	//	ファイルを閉じる
	file.closeFile();

	return TRUE;
}

//---------------------------------------------------------------------------
//	KMOファイルの書き出し
//!	@param	pFileName	 [in] 書き出すファイル名
//!	@param	pSrcFileName [in] 読み込むファイル名
//!	@retval	TRUE	成功
//!	@retval	FALSE	失敗
//---------------------------------------------------------------------------
b32		kxConverter::saveKMOFromMQO(const char* pFileName, const char* pSrcFileName)
{
	kxResource::MESHDATA meshData;
	//	MQOの読み込み
	meshData = loadMQO(pSrcFileName);
	if( meshData._materialCount == NULL ) return FALSE;

	//---------------------------------------------------------------------------
	//	KMOファイルの書き出し
	//---------------------------------------------------------------------------
	File file;

	//	ファイルチェック用文字列書き出し
	file.writtingFile(pFileName);
	file.writting("KMO ", 4);
	//	バージョン書き出し
	file.writting("0.00", 4);

	//	材質数を書き出し
	file.writting(&meshData._materialCount, sizeof(DWORD));

	//	MQOの読み込み
	Text data;
	data.load(pSrcFileName);

	//	材質データ取得
	data.getCommand("Material");
	for( u32 i = 0; i < meshData._materialCount; i++ ) {
		data.getCommand("power");
		data.getParamFloat();

		char temp[PATH_MAX];
		u8 size = 0;
		ZeroMemory(temp, sizeof(char) * PATH_MAX);
		//	テクスチャーパスの取得
		if( strcmp(data.getParamStr(), "tex") == 0 ) {
			strcpy(temp, data.getParamStr());
			//	パスサイズ書き出し
			size = static_cast<u8>(strlen(temp));
			file.writting(&size, sizeof(u8));
			//	パスの書き出し
			file.writting(temp, sizeof(char) * size);
		} else {
			file.writting(&size, sizeof(u8));
		}
	}

	//	材質の書き出し
	file.writting(meshData._pMaterial, sizeof(D3DMATERIAL9) * meshData._materialCount);

	//	メッシュの書き出し
	u8*		pVertexs;
	u8*		pIndex;
	DWORD*	pAttribute;

	LPD3DXMESH pMesh = meshData._pMesh;

	//	頂点数、面数の取得
	DWORD numVertexs = pMesh->GetNumVertices();
	DWORD numFaces	 = pMesh->GetNumFaces();
	//	頂点数、面数の書き出し
	file.writting(&numVertexs,	sizeof(DWORD));
	file.writting(&numFaces,	sizeof(DWORD));

	//	頂点情報書き出し
	pMesh->LockVertexBuffer(D3DLOCK_READONLY, reinterpret_cast<void**>(&pVertexs));
	file.writting(pVertexs, sizeof(kxGraphics::VERTEX3D) * numVertexs);
	pMesh->UnlockVertexBuffer();

	//	面情報書き出し
	pMesh->LockIndexBuffer(D3DLOCK_READONLY, reinterpret_cast<void**>(&pIndex));
	file.writting(pIndex, sizeof(u16) * numFaces * 3);
	pMesh->UnlockIndexBuffer();

	//	属性書き出し
	pMesh->LockAttributeBuffer(D3DLOCK_READONLY, &pAttribute);
	file.writting(pAttribute, sizeof(DWORD) * numFaces);
	pMesh->UnlockAttributeBuffer();

	//	データの解放
	SAFE_RELEASE(meshData._pMesh);
	SAFE_DELETES(meshData._pMaterial);
	for( u32 i=0; i < meshData._materialCount; i++ ) {
		RESOURCE->cleanup(meshData._ppTexture[i]);
	}
	SAFE_DELETES(meshData._ppTexture);

	//	ファイルを閉じる
	file.closeFile();

	return TRUE;
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	kxConverter::cleanup(void)
{
}

//============================================================================
//	END OF FILE
//============================================================================