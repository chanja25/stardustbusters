//---------------------------------------------------------------------------
//!
//!	@file	KX_Converter.h
//!	@brief	kxConverterクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#ifndef	__KX_CONVERTER_H__
#define __KX_CONVERTER_H__

#pragma once


//===========================================================================
//!	コンバータークラス
//===========================================================================
class kxConverter{
public:
	//!	読み込み時のアンカーの最大数
	static const u8 ANCHOR_MAX = 64;
	//!	MKIを読み込む時に用意する最大ボーン数
	static const u8	BONE_MAX = 64;
	//!	MKMを読み込む時に用意する最大モーション数
	static const u8	MOTION_MAX = 64;

	struct MOTION {
	};

	struct BONE {
		u16					_numBone;
		kx3DObject::BONE*	_pBone;

		char		_boneName[BONE_MAX][PATH_MAX];
		char		_parent[BONE_MAX][PATH_MAX];
		u16			_numPos;
		char		_posName[BONE_MAX][PATH_MAX];
		VECTOR3		_pos[BONE_MAX];
		u16			_numPose;
		char		_bonePosName[BONE_MAX][PATH_MAX];
		QUATERNION	_pose[BONE_MAX];
	};

	struct ANCHOR {
		DWORD	 _numIndex;
		DWORD*	 _pIndex;
		f32*	 _pInfluence;	//!< 影響頂点の重み

		u16		 _numFace;
		u16*	 _pFace;

		u16		 _numPos;
		VECTOR3* _pPos;
	};

	//---------------------------------------------------------------------------
	//! @name 初期化
	//---------------------------------------------------------------------------
	//!@{

	//!	コンストラクタ
	kxConverter(void);
	//!	デストラクタ
	~kxConverter(void);

	//! 解放
	void	cleanup(void);

	//!@}

	//---------------------------------------------------------------------------
	//! @name コンバート関連
	//---------------------------------------------------------------------------
	//!@{

	//!	KAMファイルの書き出し
	b32		saveKAMFromMQO(const char* pFileName, const char* pSrcFileName);
	//!	ボーンの読み込み(MKI)
	BONE*	loadBoneFromMKI(const char* pFileName);
	//!	モーションの読み込み(MKM)
	MOTION*	loadMotionFromMKM(const char* pFileName);
	//!	アンカー読み込み(MQO)
	u8		loadAnchorFromMQO(ANCHOR** ppAnchor, const char* pFileName, const LPD3DXMESH pMesh);
	//!	ボーンの読み込み(MQO)
	void	loadBoneFromMQO(BONE* pBone, const char* pFileName, const ANCHOR* pAnchor, u32 numAnchor);

	//!	メタセコイアデータの読み込み
	kxResource::MESHDATA	loadMQO(const char* pFileName);
	//!	KMOファイルの読み込み
	kxResource::MESHDATA	loadKMO(const char* pFileName);

	//	KMOファイルの書き出し
	b32		saveKMOFromX(const char* pFileName, const char* pSrcFileName);
	//	KMOファイルの書き出し
	b32		saveKMOFromMQO(const char* pFileName, const char* pSrcFileName);

	//!@}

private:
	//!	メタセコイアデータ読み込み時に作成する最大オブジェクト数
	static const u8	OBJECT_MAX = 64;
};

#endif	//~#if __KX_CONVERTER_H__

//============================================================================
//	END OF FILE
//============================================================================