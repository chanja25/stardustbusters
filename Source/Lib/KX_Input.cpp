//---------------------------------------------------------------------------
//!
//!	@file	KX_Input.cpp
//!	@brief	インプットクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "KX.h"


//	インスタンスの実体生成
kxInput	kxInput::_instance;

//	スティックの認知する範囲
const f32	kxInput::PAD_DEADZONE = 0.25f;

//	キーマップ
const u8	kxInput::KEYMAP[kxInput::KEY_MAX] = {
	DIK_Z, DIK_X, DIK_C, DIK_V,
	DIK_A, DIK_S, DIK_Q, DIK_W,
	DIK_RETURN, DIK_SPACE,
	DIK_UP, DIK_DOWN, DIK_LEFT, DIK_RIGHT
};

//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
kxInput::kxInput(void)
: _pDinput(NULL)
, _pKeyDevice(NULL)
, _pPadData(NULL)
, _ppButtonData(NULL)
{
	//	パッドデバイスのアドレスをNULLにしておく
	ZeroMemory(_pPadDevice, sizeof(LPDIRECTINPUTDEVICE8) * PAD_MAX);
}

//---------------------------------------------------------------------------
//	初期化
//!	@retval	TRUE	成功
//!	@retval	FALSE	失敗
//---------------------------------------------------------------------------
b32		kxInput::init(void)
{
	ASSERT(_pDinput == NULL, "既にインプットは初期化されています");

	HRESULT hr;
	//	ウインドウハンドルの取得
	HWND hWnd = KXSYSTEM->getWindowHandle();

	//	キーデータの初期化
	_pKeyData = new u8[KEY_MAX];
	ZeroMemory(_pKeyData, sizeof(u8) * KEY_MAX);

	//「DirectInput」オブジェクトの作成
	hr = DirectInput8Create(GetModuleHandle(NULL), DIRECTINPUT_VERSION, IID_IDirectInput8, (VOID**)&_pDinput, NULL);
	if( FAILED(hr) ) return FALSE;

	//キーボード用デバイスの作成
	hr = _pDinput->CreateDevice(GUID_SysKeyboard, &_pKeyDevice, NULL);
	if( FAILED(hr) ) return FALSE;
	//デバイスをキーボードに設定
	hr = _pKeyDevice->SetDataFormat(&c_dfDIKeyboard);
	if( FAILED(hr) ) return FALSE;

	//協調レベルの設定
	hr = _pKeyDevice->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);
	if( FAILED(hr) )return FALSE;
	//デバイスを取得する
	_pKeyDevice->Acquire();

	//	パッドの初期化
	_pDinput->EnumDevices(DI8DEVCLASS_GAMECTRL, EnumJoyCallBack, &hWnd, DIEDFL_ATTACHEDONLY);

	if( _padNum ) {
		//	存在するパッドの数だけパッド情報格納構造体を生成する
		_pPadData = new PADDATA[_padNum];
		ZeroMemory(_pPadData, sizeof(PADDATA) * _padNum);
		//	存在するパッドの数だけボタン配置情報格納構造体ポインタを生成する
		_ppButtonData = new u8*[_padNum];
		ZeroMemory(_ppButtonData, sizeof(u8*) * _padNum);
	}

	//	存在するパッドの数だけボタン配置情報格納構造体ポインタを生成する
	for( u32 i=0; i < _padNum; i++ ) {
		_ppButtonData[i] = new u8[BUTTON_MAX];
		ZeroMemory(_ppButtonData[i], sizeof(u8) * BUTTON_MAX);
	}

	File file;
	//	ボタン配置データの読み込み
	u8*	pData = reinterpret_cast<u8*>(file.read("DATA\\pad_data"));
	if( pData ) {
		//	読み込んだデータをボタン配置に設定
		for( u32 i=0; i < _padNum; i++ ) {
			for( u32 j=0; j < BUTTON_MAX; j++ ) {
				_ppButtonData[i][j] = *pData++;
			}
		}
	} else {
		//	データがなかったので、デフォルト設定
		for( u32 i=0; i < _padNum; i++ ) {
			for( u32 j=0; j < BUTTON_MAX; j++ ) {
				_ppButtonData[i][j] = j;
			}
		}
	}

	return TRUE;
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	kxInput::cleanup(void)
{
	//	ボタン配置格納ポインタを解放
	if( _ppButtonData ) {
		for( u32 i=0; i < _padNum; i++ ) {
			SAFE_DELETES(_ppButtonData[i]);
		}
		SAFE_DELETES(_ppButtonData);
	}

	//	パッドデータ格納ポインタを解放
	SAFE_DELETES(_pPadData);

	//	パッドデバイスの解放
	for( u32 i=0; i < PAD_MAX; i++ ) {
		SAFE_RELEASE(_pPadDevice[i]);
	}

	//	キーデータの解放
	SAFE_DELETES(_pKeyData);

	//	キーボードデバイスの解放
	SAFE_RELEASE(_pKeyDevice);

	//	DirectInputデバイスの解放
	SAFE_RELEASE(_pDinput);
}

//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	kxInput::update(void)
{
	HRESULT hr;
	u32 i, j;

	b32	 key;
	_axis.x = 0;
	_axis.y = 0;

	//	キーボードの状態取得
	hr = _pKeyDevice->GetDeviceState(sizeof(_disk), &_disk);
	if( hr == DIERR_INPUTLOST ) _pKeyDevice->Acquire();
	//	配置されたキーの状態を取得していく
	for( i = 0; i < KEY_MAX; i++ ) {
		if( _disk[KEYMAP[i]] & 0x80 )	key = TRUE;
		else							key = FALSE;

		if( _pKeyData[i] & 0x01 ) { if( key ) _pKeyData[i] = 1; else _pKeyData[i] = 2; }
		else					  { if( key ) _pKeyData[i] = 3; else _pKeyData[i] = 0; }

		if( KEYMAP[i] == DIK_UP		&& key ) _axis.y += 1.0f;
		if( KEYMAP[i] == DIK_DOWN	&& key ) _axis.y -= 1.0f;
		if( KEYMAP[i] == DIK_LEFT	&& key ) _axis.x -= 1.0f;
		if( KEYMAP[i] == DIK_RIGHT	&& key ) _axis.x += 1.0f;
	}

	//	パッド毎のボタンの情報を取得する
	for( i = 0; i < _padNum; i++ ) {
		_instance._pPadDevice[i]->Poll();
		//	パッドの状態取得
		hr = _instance._pPadDevice[i]->GetDeviceState(sizeof(_dijs),&_dijs);
		if( hr==DIERR_INPUTLOST )_instance._pPadDevice[i]->Acquire();

		//	前回のボタンの状態を入れておく
		_pPadData[i]._oldButton = _pPadData[i]._button;
		//	ボタンの状態をリセットする
		_pPadData[i]._button = 0;

		//	ボタンの状態を取得する
		for( j = 0; j < BUTTON_MAX; j++ ) {
			if( j == BUTTON_UP ) {
				_pPadData[i]._button += (((_dijs.rgdwPOV[0] == 0) | (_dijs.rgdwPOV[0] == 4500) | (_dijs.rgdwPOV[0] == 31500))? 1: 0) << j;
			} else if( j == BUTTON_DOWN ) {
				_pPadData[i]._button += (((_dijs.rgdwPOV[0] == 13500) | (_dijs.rgdwPOV[0] == 18000) | (_dijs.rgdwPOV[0] == 22500))? 1: 0) << j;
			} else if( j == BUTTON_LEFT ) {
				_pPadData[i]._button += (((_dijs.rgdwPOV[0] == 22500) | (_dijs.rgdwPOV[0] == 27000) | (_dijs.rgdwPOV[0] == 31500))? 1: 0) << j;
			} else if( j == BUTTON_RIGHT ) {
				_pPadData[i]._button += (((_dijs.rgdwPOV[0] == 4500) | (_dijs.rgdwPOV[0] == 9000) | (_dijs.rgdwPOV[0] == 13500))? 1: 0) << j;
			} else {
				_pPadData[i]._button += ((_dijs.rgbButtons[j] & 0x80)? 1: 0) << j;
			}
		}

		//	左スティックの状態取得
		_pPadData[i]._leftStick.x = static_cast<f32>(_dijs.lX) / 1000;
		_pPadData[i]._leftStick.y = static_cast<f32>(_dijs.lY) / 1000;
		//	スティックの値の切り捨て
		if( (_pPadData[i]._leftStick.x < PAD_DEADZONE) && (_pPadData[i]._leftStick.y < PAD_DEADZONE) &&
				(_pPadData[i]._leftStick.x > -PAD_DEADZONE) && (_pPadData[i]._leftStick.y > -PAD_DEADZONE)) {
			_pPadData[i]._leftStick.x = _pPadData[i]._leftStick.y = 0;
		}

		//	右スティックの状態取得
		_pPadData[i]._rightStick.x =  static_cast<f32>(_dijs.lZ)  / 1000;
		_pPadData[i]._rightStick.y =  static_cast<f32>(_dijs.lRz) / 1000;
		//	スティックの値の切り捨て
		if( (_pPadData[i]._rightStick.x < PAD_DEADZONE) && (_pPadData[i]._rightStick.y < PAD_DEADZONE) &&
				(_pPadData[i]._rightStick.x > -PAD_DEADZONE) && (_pPadData[i]._rightStick.y > -PAD_DEADZONE)) {
			_pPadData[i]._rightStick.x = _pPadData[i]._rightStick.y = 0;
		}
	}
}

//---------------------------------------------------------------------------
//	ボタンの配置変更
//!	@param	no		[in] 配置を変更したいパッドの番号
//!	@param	button	[in] 配置を変更したいボタンの番号
//!	@retval	TRUE	変更された
//!	@retval	FALSE	変更待ち
//---------------------------------------------------------------------------
b32		kxInput::changeButton(u8 no, u8 button)
{
	ASSERT(no < PAD_MAX, "バッドの最大数を超えています");
	if( _padNum <= no ) return FALSE;

	u32 i;
	//	離された瞬間のボタンを探す
	for( i = 0; i < BUTTON_MAX; i++ ) {
		if( ((_pPadData[no]._oldButton ^ _pPadData[no]._button) & _pPadData[no]._oldButton) & (1 << i) ) break;
	}

	//	ボタンが検知されていればセットする
	if( i < BUTTON_MAX ) {
		_ppButtonData[no][button] = i;
		return TRUE;
	}

	return FALSE;
}

//---------------------------------------------------------------------------
//	ボタンの配置保存
//---------------------------------------------------------------------------
void	kxInput::saveButton(void)
{
	File file;
	//	現在のボタン配置を保存する
	u8*	pData = new u8[PAD_MAX * BUTTON_MAX];
	ZeroMemory(pData, sizeof(u8) * PAD_MAX * BUTTON_MAX);
	u8* pTop = pData;
	for( u32 i=0; i < PAD_MAX; i++ ) {
		for( u32 j=0; j < BUTTON_MAX; j++ ) {
			if( _padNum <= i ) {
				*pData++ = j;
			} else {
				*pData++ = _ppButtonData[i][j];
			}
		}
	}

	pData = pTop;
	//	ボタンデータの書き出し
	file.write("DATA\\pad_data", pData, sizeof(s8) * PAD_MAX * BUTTON_MAX);

	SAFE_DELETES(pData);
}

//---------------------------------------------------------------------------
//	ボタン配置を初期設定に戻す
//---------------------------------------------------------------------------
void	kxInput::defaultButton(void)
{
	//	現在のボタン配置を初期値に戻す
	for( u32 i=0; i < _padNum; i++ ) {
		for( u32 j=0; j < BUTTON_MAX; j++ ) {
			_ppButtonData[i][j] = j;
		}
	}
}

//---------------------------------------------------------------------------
//	キーが押されているかチェック
//!	@param	key		[in] 取得するボタンの番号
//!	@retval	TRUE	押されている
//!	@retval	FALSE	押されていない
//---------------------------------------------------------------------------
b32		kxInput::isKeyPress(u32 key)
{
	if( key >= KEY_MAX || GetForegroundWindow() != KXSYSTEM->getWindowHandle() ) {
		return FALSE;
	}

	return _pKeyData[key] & 0x01;
}

//---------------------------------------------------------------------------
//	キーを押したかチェック
//!	@param	key		[in] 取得するボタンの番号
//!	@retval	TRUE	押された
//!	@retval	FALSE	押されていない
//---------------------------------------------------------------------------
b32		kxInput::isKeyPush(u32 key)
{
	if( key >= KEY_MAX || GetForegroundWindow() != KXSYSTEM->getWindowHandle() ) {
		return FALSE;
	}

	return _pKeyData[key] == 3;
}

//---------------------------------------------------------------------------
//	キーが離されたかチェック
//!	@param	key		[in] 取得するボタンの番号
//!	@retval	TRUE	離された
//!	@retval	FALSE	離されていない
//---------------------------------------------------------------------------
b32		kxInput::isKeyRelease(u32 key)
{
	if( key >= KEY_MAX || GetForegroundWindow() != KXSYSTEM->getWindowHandle() ) {
		return FALSE;
	}

	return _pKeyData[key] == 2;
}

//---------------------------------------------------------------------------
//	ボタンが押されているかチェック
//!	@param	button	[in] 取得するボタンの番号
//!	@param	no		[in] 取得するパッドの番号
//!	@retval	TRUE	押されている
//!	@retval	FALSE	押されていない
//---------------------------------------------------------------------------
b32		kxInput::isButtonPress(u32 button, u32 no)
{
	ASSERT(no < PAD_MAX, "バッドの最大数を超えています");
	if( _padNum <= no || GetForegroundWindow() != KXSYSTEM->getWindowHandle() ) {
		return FALSE;
	}

	return _pPadData[no]._button & (1 << _ppButtonData[no][button]);
}

//---------------------------------------------------------------------------
//	ボタンを押したかチェック
//!	@param	button	[in] 取得するボタンの番号
//!	@param	no		[in] 取得するパッドの番号
//!	@retval	TRUE	押された
//!	@retval	FALSE	押されていない
//---------------------------------------------------------------------------
b32		kxInput::isButtonPush(u32 button, u32 no)
{
	ASSERT(no < PAD_MAX, "バッドの最大数を超えています");
	if( _padNum <= no || GetForegroundWindow() != KXSYSTEM->getWindowHandle() ) {
		return FALSE;
	}

	return ((_pPadData[no]._button ^ _pPadData[no]._oldButton) & _pPadData[no]._button) & (1 << _ppButtonData[no][button]);
}

//---------------------------------------------------------------------------
//	ボタンが離されたかチェック
//!	@param	button	[in] 取得するボタンの番号
//!	@param	no		[in] 取得するパッドの番号
//!	@retval	TRUE	離された
//!	@retval	FALSE	離されていない
//---------------------------------------------------------------------------
b32		kxInput::isButtonRelease(u32 button, u32 no)
{
	ASSERT(no < PAD_MAX, "バッドの最大数を超えています");
	if( _padNum <= no || GetForegroundWindow() != KXSYSTEM->getWindowHandle() ) {
		return FALSE;
	}

	return ((_pPadData[no]._oldButton ^ _pPadData[no]._button) & _pPadData[no]._oldButton) & (1 << _ppButtonData[no][button]);
}

//---------------------------------------------------------------------------
//	左スティックの状態の取得
//!	@param	no		[in] 取得するパッドの番号
//!	@retval	左スティックの状態
//---------------------------------------------------------------------------
VECTOR2		kxInput::getLeftAxis(u32 no)
{
	if( _padNum == 0 ) return _axis;

	ASSERT(no < PAD_MAX, "バッドの最大数を超えています");
	if( _padNum <= no || GetForegroundWindow() != KXSYSTEM->getWindowHandle() ) {
		return VECTOR2(0, 0);
	}

	return _pPadData[no]._leftStick;
}

//---------------------------------------------------------------------------
//	右スティックの状態の取得
//!	@param	no		[in] 取得するパッドの番号
//!	@retval	右スティックの状態
//---------------------------------------------------------------------------
VECTOR2		kxInput::getRightAxis(u32 no)
{
	ASSERT(no < PAD_MAX, "バッドの最大数を超えています");
	if( _padNum <= no || GetForegroundWindow() != KXSYSTEM->getWindowHandle() ) {
		return VECTOR2(0, 0);
	}

	return _pPadData[no]._rightStick;
}

//---------------------------------------------------------------------------
//	デバイス毎に呼び出されるコールバック関数
//!	@param	pDIDInstance	[in] DirectInputデバイスのインスタンス
//!	@param	pContext		[in] コンテキスト
//!	@retval	TRUE	成功
//!	@retval	FALSE	失敗
//---------------------------------------------------------------------------
b32	CALLBACK kxInput::EnumJoyCallBack(const DIDEVICEINSTANCE* pDIDInstance, void* pContext)
{
	//	最大パッド数を超えていれば終了する
	if( _instance._padNum >= PAD_MAX ) return DIENUM_STOP;

	HRESULT hr;
	//	パッドデバイスの生成
	hr = _instance._pDinput->CreateDevice(pDIDInstance->guidInstance, &_instance._pPadDevice[_instance._padNum], NULL);
	if( FAILED(hr) ) return DIENUM_STOP;
	//	デバイスのフォーマット設定
	hr = _instance._pPadDevice[_instance._padNum]->SetDataFormat(&c_dfDIJoystick2);

	//	協調レベルの設定
	hr = _instance._pPadDevice[_instance._padNum]->SetCooperativeLevel(*((HWND*)pContext), DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);
	if( FAILED(hr) ) {
		SAFE_RELEASE(_instance._pPadDevice[_instance._padNum]);
		return DIENUM_STOP;
	}

	//	ジョイスティックの軸の初期化
	hr = _instance._pPadDevice[_instance._padNum]->EnumObjects( EnumAxesCallBack, NULL, DIDFT_AXIS );
	if( FAILED(hr) ) {
		SAFE_RELEASE(_instance._pPadDevice[_instance._padNum]);
		return DIENUM_STOP;
	}

	//	デバイスを取得する
	_instance._pPadDevice[_instance._padNum]->Acquire();
	//	パッドの数をインクリメントする
	_instance._padNum++;

	return DIENUM_CONTINUE;
}

//---------------------------------------------------------------------------
//	ジョイスティックの軸を列挙するコールバック関数
//!	@param	pDDOI	[in] DirectInputDeviceインスタンス
//!	@param	pRef	[in] リファレンスデータ
//!	@retval	TRUE	成功
//!	@retval	FALSE	失敗
//---------------------------------------------------------------------------
b32	CALLBACK kxInput::EnumAxesCallBack(LPCDIDEVICEOBJECTINSTANCE pDDOI, LPVOID pRef)
{
	HRESULT hr;

	//デバイス入力データの設定
	DIPROPRANGE diprg;

	ZeroMemory(&diprg , sizeof(diprg));
	diprg.diph.dwSize = sizeof(DIPROPRANGE);
	diprg.diph.dwHeaderSize = sizeof(diprg.diph);
	diprg.diph.dwObj = pDDOI->dwType;
	diprg.diph.dwHow = DIPH_BYID;
	diprg.lMin = -1000;
	diprg.lMax = 1000;
	hr = _instance._pPadDevice[INPUT->_padNum]->SetProperty(DIPROP_RANGE, &diprg.diph);

	if( FAILED(hr) ) return DIENUM_STOP;
	return DIENUM_CONTINUE;
}

//============================================================================
//	END OF FILE
//============================================================================