//---------------------------------------------------------------------------
//!
//!	@file	KXxProfiler.cpp
//!	@brief	プロファイラクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "KX.h"


#if	USE_PROFILER

//	インスタンスの定義
kxProfiler*	kxProfiler::_pInstance = NULL;
// プロセスバーの長さ
const f32 kxProfiler::BAR_LENGTH = 500.0f;

//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
kxProfiler::kxProfiler(void)
: _pRoot(NULL)
, _pCurrent(NULL)
, _logCount(0)
{
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
kxProfiler::~kxProfiler(void)
{
}

//---------------------------------------------------------------------------
//	初期化
//---------------------------------------------------------------------------
void	kxProfiler::init(void)
{
	ASSERT(_pInstance == NULL, "既にプロファイラは初期化されています");
	//	インスタンスの実体化
	_pInstance = new kxProfiler();

	//	ログ情報の取得スペースを生成する
	_pInstance->_pRoot = new Log[LOG_MAX];
	//	取得したスペースを初期化する
	ZeroMemory(_pInstance->_pRoot, sizeof(Log) * LOG_MAX);

	//	カレントログを生成する
	_pInstance->_pCurrent = _pInstance->createLog("root", NULL);
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	kxProfiler::cleanup(void)
{
	//	インスタンスが存在すればログ情報と共に解放する
	if( _pInstance ) {
		SAFE_DELETES(_pInstance->_pRoot);
		SAFE_DELETE(_pInstance);
	}
}

//---------------------------------------------------------------------------
//	フレームの開始をプロファイラに伝える
//---------------------------------------------------------------------------
void	kxProfiler::start(void)
{
	//	ルートログを取得
	Log* pLog = _pRoot;

	//	全てのログをする
	if( pLog->_pChiled ) {
		updateLog(pLog->_pChiled);
	}

	//	計測を開始する
	_timer.reset();
}

//---------------------------------------------------------------------------
//	計測開始
//	@param	pName	[in] プロファイル名
//	@param	color	[in] 表示色
//---------------------------------------------------------------------------
void	kxProfiler::begin(const char* pName, COLOR color)
{
	//	現在のカレントログを親に設定する
	Log* pParent  = _pCurrent;
	Log* pBrother = NULL;

	//	指定のプロファイル名を持つ既存のログが存在するか探す
	Log* pLog = pParent->_pChiled;
	while( pLog ) {
		if( pLog->_pName == pName ) break;
		pBrother = pLog;
		pLog	 = pLog->_pBrother;
	}

	if( pLog == NULL ) {
		//	指定のプロファイルが存在しないので、新規ログの生成
		pLog = createLog(pName, pParent);
		//	カラーの設定
		pLog->_color = color;

		//	親兄関係設定
		if( pParent->_pChiled == NULL ) {
			//	子供がいなければ、第一子にセット
			pParent->_pChiled = pLog;
		} else {
			//	既に子供がいた場合、兄にリンクされて、第二子以降にセット
			pBrother->_pBrother = pLog;
		}
	}

	//	計測開始時間取得
	pLog->_beginTime = static_cast<u32>(_timer.get());
	//	カレントログに自分をセット
	_pCurrent = pLog;
}

//---------------------------------------------------------------------------
//	計測終了
//	@param	pName	[in] プロファイル名
//---------------------------------------------------------------------------
void	kxProfiler::end(const char* pName)
{
	ASSERT(_pCurrent->_pName == pName, "計測開始と計測終了が対になっていません");

	//	計測終了時間取得
	_pCurrent->_endTime = static_cast<u32>(_timer.get());

	//	処理時間取得
	u32 time = _pCurrent->_endTime - _pCurrent->_beginTime;

	//	合計処理時間の加算
	_pCurrent->_tmpTotalTime += time;
	//	計測回数のインクリメント
	_pCurrent->_tmpCount++;
	//	最小処理時間更新
	_pCurrent->_tmpMinTime = ( _pCurrent->_tmpMinTime > time )? time: _pCurrent->_tmpMinTime;
	//	最大処理時間更新
	_pCurrent->_tmpMaxTime = ( _pCurrent->_tmpMaxTime < time )? time: _pCurrent->_tmpMaxTime;

	//	計測が終了したのでカレントを親にしなおす
	_pCurrent = _pCurrent->_pParent;
}

//---------------------------------------------------------------------------
//	ログ情報の生成
//!	@param	pName	[in] ログの名前
//!	@param	pParent	[in] 親となるログ情報ポインタ
//!	@retval	新規のログ情報ポインタ
//---------------------------------------------------------------------------
kxProfiler::Log*	kxProfiler::createLog(const char* pName, Log* pParent)
{
	ASSERT(_logCount + 1 < LOG_MAX, "これ以上ログの追加はできません");

	// ログの初期化
	Log* pLog = &_pRoot[_logCount];
	_logCount++;

	pLog->_pParent	= pParent;
	pLog->_pBrother = NULL;
	pLog->_pChiled	= NULL;

	// 計測用パラメータ初期化
	pLog->_beginTime		= 0;
	pLog->_endTime			= 0;
	pLog->_tmpCount			= 0;
	pLog->_tmpTotalTime		= 0;
	pLog->_tmpMinTime		= 0xFFFFFFFF;
	pLog->_tmpMaxTime		= 0;

	// 表示用パラメータの初期化
	pLog->_pName			= pName;
	pLog->_color			= pParent ? pParent->_color : COLOR_WHITE;
	pLog->_count			= 0;
	pLog->_totalTime		= 0;
	pLog->_averageTime		= 0;
	pLog->_minTime			= 0xFFFFFFFF;
	pLog->_maxTime			= 0;

	return pLog;
}

//---------------------------------------------------------------------------
//	ログ情報の更新
//!	@param	pLog	[in/out] 更新されるログ情報
//---------------------------------------------------------------------------
void	kxProfiler::updateLog(Log* pLog)
{
	while( pLog ) {
		//	ログ情報の更新
		pLog->_count			= pLog->_tmpCount;
		pLog->_totalTime		= pLog->_tmpTotalTime;
		pLog->_averageTime		= pLog->_count != 0 ? (pLog->_totalTime / pLog->_count) : 0;
		pLog->_minTime			= pLog->_tmpMinTime;
		pLog->_maxTime			= pLog->_tmpMaxTime;

		//	計測用変数の初期化
		pLog->_beginTime		= 0;
		pLog->_endTime			= 0;
		pLog->_tmpCount			= 0;
		pLog->_tmpTotalTime		= 0;

		//	子がいれば再帰的に更新処理を行う
		if( pLog->_pChiled ) {
			updateLog(pLog->_pChiled);
		}

		//	次の兄弟に進める
		pLog = pLog->_pBrother;
	}
}

//---------------------------------------------------------------------------
//	プロセスバーの描画
//---------------------------------------------------------------------------
void	kxProfiler::drawProcessBar(void)
{
	//	カレントログを取得
	Log* pLog = _pRoot;

	VECTOR2 pos(1.0f, 1.0f);

	GRAPHICS->drawRect(pos.x, pos.y, static_cast<f32>(BAR_LENGTH), 5, 0xffffffff);

	pos.y += 5.0f;
	if( pLog->_pChiled ) {
		drawProcessBar(pLog->_pChiled, pos);
	}
}

//---------------------------------------------------------------------------
//	プロセスバーの描画
//!	@param	pLog	[in] 表示するログ情報
//!	@param	pos		[in] 表示位置
//---------------------------------------------------------------------------
void	kxProfiler::drawProcessBar(const Log* pLog, VECTOR2 pos)
{
	//	目標のフレームレートを設定
	f32 targetNanoSec = static_cast<f32>(Timer::RATE / FRAME->getTargetFPS());

	//	デバッグ文字表示用
	//char buf[PATH_MAX];

	while( pLog ) {
		//	処理時間からバーの長さを設定する
		f32	length = (static_cast<f32>(pLog->_totalTime) / targetNanoSec * BAR_LENGTH);

		//sprintf_s(buf, sizeof(buf), "名前(%s)\n", pLog->_pName);
		//OutputDebugString(buf);
		//sprintf_s(buf, sizeof(buf), "	呼び出し回数(%d)\n", pLog->_count);
		//OutputDebugString(buf);
		//sprintf_s(buf, sizeof(buf), "	合計時間(%d)\n", pLog->_totalTime);
		//OutputDebugString(buf);
		//sprintf_s(buf, sizeof(buf), "	平均時間(%d)\n", pLog->_averageTime);
		//OutputDebugString(buf);
		//sprintf_s(buf, sizeof(buf), "	最小時間(%d)\n", pLog->_minTime);
		//OutputDebugString(buf);
		//sprintf_s(buf, sizeof(buf), "	最大時間(%d)\n", pLog->_maxTime);
		//OutputDebugString(buf);

		//	描画色をセットする
		ARGB color = 0xffffffff;
		switch( pLog->_color ) {
		case COLOR_WHITE:
			color = 0xffffffff;
			break;
		case COLOR_BLACK:
			color = 0xff000000;
			break;
		case COLOR_RED:
			color = 0xffff0000;
			break;
		case COLOR_GREEN:
			color = 0xff00ff00;
			break;
		case COLOR_BLUE:
			color = 0xff0000ff;
			break;
		case COLOR_YELLOW:
			color = 0xffffff00;
			break;
		case COLOR_PINK:
			color = 0xffffaaff;
			break;
		case COLOR_LIGHT_BULE:
			color = 0xffaaffff;
			break;
		case COLOR_PURPLE:
			color = 0xffff00ff;
			break;
		case COLOR_GRAY:
			color = 0xff888888;
			break;
		}

		//	バーの描画
		GRAPHICS->drawRect(pos.x, pos.y, length, 10, color);

		//	再帰的に子の描画をする
		if( pLog->_pChiled ) {
			drawProcessBar(pLog->_pChiled, pos);
		}

		//	座標をずらす
		pos.x += length;
		//	次の兄弟へ進む
		pLog = pLog->_pBrother;
	}
}

//---------------------------------------------------------------------------
//	コンストラクタ
//!	@param	pName	[in] プロファイル名
//!	@param	color	[in] 表示色
//---------------------------------------------------------------------------
kxProfiler::ScopeProfiler::ScopeProfiler(const char* pName, kxProfiler::COLOR color)
: _pName(pName)
{
	PROFILER->begin(_pName, color);
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
kxProfiler::ScopeProfiler::~ScopeProfiler(void)
{
	PROFILER->end(_pName);
}

#endif	//~#if USE_PROFILER

//============================================================================
//	END OF FILE
//============================================================================