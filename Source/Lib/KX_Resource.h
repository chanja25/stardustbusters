//---------------------------------------------------------------------------
//!
//!	@file	KX_Resource.h
//!	@brief	リソース管理クラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#ifndef	__KX_RESOURCE_H__
#define	__KX_RESOURCE_H__

#pragma once


#include "IEX.h"

//===========================================================================
//!	テクスチャー管理クラス
//===========================================================================
class kxResource
{
public:
	//!	メッシュ情報格納用構造体
	struct MESHDATA {
		char			_fileName[PATH_MAX];	//!< ファイル名
		u32				_count;					//!< 使用している数

		LPD3DXMESH		_pMesh;					//!< メッシュ
		DWORD			_materialCount;			//!< 材質数
		D3DMATERIAL9*	_pMaterial;				//!< 材質情報
		LPTEXTURE*		_ppTexture;				//!< テクスチャー
	};

	//!	3Dオブジェクト情報格納用構造体
	struct OBJ3D {
		char					_fileName[PATH_MAX];	//!< ファイル名
		u32						_count;					//!< 使用している数

		LPD3DXMESH				_pMesh;					//!< メッシュ
		DWORD					_materialCount;			//!< 材質数
		D3DMATERIAL9*			_pMaterial;				//!< 材質情報
		LPTEXTURE*				_ppTexture;				//!< テクスチャー

		DWORD					_vertexNum;				//!< 頂点数
		kxGraphics::LPVERTEX3D	_pVertex;				//!< 基本頂点

		LPD3DXSKININFO			_pSkinInfo;				//!< スキンメッシュ情報

		u16						_boneNum;				//!< ボーンの数
		u16*					_pBoneParent;			//!< ボーンの親番号
		MATRIX*					_pBoneMatrix;			//!< ボーンのマトリックス
		MATRIX*					_pOffsetMatrix;			//!< ボーンの基本マトリックス
		MATRIX*					_pMatrix;				//!< ボーンの最終的なマトリックス

		QUATERNION*				_pPose;					//!< ボーンの姿勢
		VECTOR3*				_pPos;					//!< ボーンの位置

		QUATERNION*				_pOrgPose;				//!< ボーンの基本姿勢
		VECTOR3*				_pOrgPos;				//!< ボーンの基本位置

		kx3DObject::ANIME*		_pAnime;				//!< アニメーション

		u16						_motionNum;				//!< フレーム数
		u16*					_pMotionOffset;			//!< モーション毎の先頭フレーム
		u16						_frameNum;				//!< フレーム数
		f32*					_pDrawFrame;			//!< 描画フレーム
		u16*					_pFrameFlag;			//!< フレーム毎のフラグ
	};

	//---------------------------------------------------------------------------
	//!	@name 初期化
	//---------------------------------------------------------------------------
	//!@{

	//!	初期化
	void	init(void);
	//!	解放
	void	cleanup(void);

	//!@}

	//---------------------------------------------------------------------------
	//!	@name テクスチャー管理
	//---------------------------------------------------------------------------
	//!@{

	//!	テクスチャーの読み込み
	LPTEXTURE	loadTexture(const char* pFileName);
	//!	テクスチャーの解放
	void		cleanup(LPTEXTURE pTexture);

	//!@}

	//---------------------------------------------------------------------------
	//!	@name メッシュ管理
	//---------------------------------------------------------------------------
	//!@{

	//!	メッシュの読み込み
	MESHDATA*	loadMesh(const char* pFileName);
	//!	メッシュの解放
	void		cleanup(LPD3DXMESH pMesh);

	//!	3DOBJの読み込み
	OBJ3D*		load3DOBJ(const char* pFileName);
	//! 3DOBJの解放
	void		cleanup(LPD3DXSKININFO pSkin);

	//!@}

	//-------------------------------------------------------------
	//!	@name 取得参照
	//-------------------------------------------------------------
	//!@{

	//!	ディレクトリの取得
	void	getPath(char* pOut, const char* pPath);
	//!	拡張子の取得
	void	getExtension(char* pOut, const char* pFileName);
	//!	大文字を小文字に変換
	char	changeLower(char str);
	//!	文字列を全て小文字に変換
	void	changeLower(char* pOut, const char* pStr);

	//!	インスタンスの取得
	//!	@retval	インスタンス
	inline static kxResource*	getInstance(void) { return &_instance; }

	//!@}

private:
	//!	コンストラクタ
	kxResource(void);
	//!	デストラクタ
	~kxResource(void);

	//---------------------------------------------------------------------------
	//!	@name メッシュ読み込み
	//---------------------------------------------------------------------------
	//!@{

	//!	メッシュの読み込み(Xファイル)
	void	loadMeshFromX(const char* pFileName, u32 no);
	//!	メッシュの読み込み(MQOファイル)
	void	loadMeshFromMQO(const char* pFileName, u32 no);
	//!	メッシュの読み込み(KMOファイル)
	void	loadMeshFromKMO(const char* pFileName, u32 no);
	
	//!	3DOBJの読み込み(MQOファイル)
	void	load3DObjFromMQO(const char* pFileName, u32 no);
	//!	3DOBJの読み込み(KAMファイル)
	void	load3DObjFromKAM(const char* pFileName, u32 no);
	//!	3DOBJの読み込み(IEMファイル)
	void	load3DObjFromIEM(const char* pFileName, u32 no);

	//!@}

	static	kxResource	_instance;	//!	唯一のインスタンス

	//!	ロード可能なテクスチャーの数
	static const u8 TEXTURE_MAX = 128;

	//!	テクスチャー情報格納用構造体
	struct	TEXTUREDATA {
		char		_fileName[PATH_MAX];	//!< ファイル名
		u32			_count;					//!< 使用している数

		LPTEXTURE	_pTexture;				//!< テクスチャー情報
	};

	TEXTUREDATA*	_pTexData;	//!	テクスチャー管理データ

	//!	ロード可能なメッシュの数
	static const u8 MESH_MAX = 64;

	MESHDATA*		_pMeshData;	//! メッシュ管理データ

	//!	ロード可能な3DOBJの数
	static const u8 OBJ3D_MAX = 64;

	OBJ3D*			_pObj3D;	//! 3DOBJ管理データ
};

//!	kxResourceのインスタンス取得マクロ
#define	RESOURCE	(kxResource::getInstance())

#endif	//~#if __KX_RESOURCE_H__

//============================================================================
//	END OF FILE
//============================================================================