//---------------------------------------------------------------------------
//!
//!	@file	KX_Timer.cpp
//!	@brief	時間計測
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "KX.h"


//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
kxTimer::kxTimer(void)
{
	LARGE_INTEGER li;
	QueryPerformanceFrequency( &li );
	_freq = li.QuadPart;

	reset();
}

//---------------------------------------------------------------------------
//	タイマースタート
//---------------------------------------------------------------------------
void	kxTimer::reset(void)
{
	LARGE_INTEGER li;
	QueryPerformanceCounter( &li );
	_begin = li.QuadPart;
}

//---------------------------------------------------------------------------
//	タイマー終了
//!	@retval	経過時間
//---------------------------------------------------------------------------
u32		kxTimer::get(void)
{
	LARGE_INTEGER li;
	QueryPerformanceCounter( &li );
	return (u32)(((li.QuadPart - _begin) * RATE) / _freq);
}

//============================================================================
//	END OF FILE
//============================================================================