//---------------------------------------------------------------------------
//!
//!	@file	KX_2DOBJ.h
//!	@brief	kx2DObjectクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#ifndef	__KX_2DOBJ_H__
#define	__KX_2DOBJ_H__

#pragma once


//===========================================================================
//!	kx2DObjectクラス
//===========================================================================
class kx2DObject
{
public:
	//!	中心の設定用
	enum CENTER {
		CENTER_LEFT_TOP,
		CENTER_CENTER,
		CENTER_CENTER_BOTTOM,
	};

	//---------------------------------------------------------------------------
	//! @name 初期化
	//---------------------------------------------------------------------------
	//!@{

	//!	コンストラクタ
	kx2DObject(void);
	//!	コンストラクタ(引数付き)
	kx2DObject(u32 width, u32 height, b32 render = FALSE);
	//!	コンストラクタ(引数付き)
	kx2DObject(const char* pFileName);
	//!	デストラクタ
	~kx2DObject(void);

	//!	解放
	void	cleanup(void);

	//!@}

	//---------------------------------------------------------------------------
	//! @name テクスチャー管理
	//---------------------------------------------------------------------------
	//!@{

	//!	テクスチャーの作成
	void	create(u32 width, u32 height, b32 render = FALSE);
	//!	読み込み
	void	load(const char* pFileName);

	//!	描画
	void	render(f32 x, f32 y, f32 width, f32 height, f32 srcX, f32 srcY, f32 srcW, f32 srcH, char* pTechnique, ARGB color = 0xffffffff, u32 flag = 0);
	//!	描画
	void	render(f32 x, f32 y, f32 width, f32 height, f32 srcX, f32 srcY, f32 srcW, f32 srcH, ARGB color = 0xffffffff, u32 flag = 0);
	//!	描画
	void	render(f32 x, f32 y, f32 width, f32 height, f32 angle, f32 srcX, f32 srcY, f32 srcW, f32 srcH, char* pTechnique, ARGB color = 0xffffffff, u32 flag = 0);
	//!	描画										  
	void	render(f32 x, f32 y, f32 width, f32 height, f32 angle, f32 srcX, f32 srcY, f32 srcW, f32 srcH, ARGB color = 0xffffffff, u32 flag = 0);

	//!@}

	//-------------------------------------------------------------
	//!	@name 取得参照
	//-------------------------------------------------------------
	//!@{

	//!	テクスチャーの取得
	//!	@retval	テクスチャー
	LPTEXTURE	getTexture(void) { return _pTexture; }

	//!	中心の設定
	//!	@param	center	[in]	中心の設定
	void		setCenter(CENTER center) { _center = center; }

	//!@}

private:
	kxGraphics::VERTEX2D	_vertex[4];	//!< 頂点設定用

	LPTEXTURE _pTexture;	//!< テクスチャー

	u8	_center;			//!< 中心の設定フラグ
	f32	_width;				//!< 横幅
	f32	_height;			//!< 縦幅
};

//!	KX2DOBJの定義
typedef	kx2DObject	KX2DOBJ, *LPKX2DOBJ;

#endif	//~#if __KX_2DOBJ_H__

//============================================================================
//	END OF FILE
//============================================================================