//---------------------------------------------------------------------------
//!
//!	@file	KX_Typedef.h
//!	@brief	データ定義
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#ifndef	__KX_TYPEDEF_H__
#define	__KX_TYPEDEF_H__

#pragma	once


//---------------------------------------------------------------------------
//! @name データ定義
//---------------------------------------------------------------------------
//!@{

#define RAD			0.017453292f

#define	PATH_MAX	256

typedef	signed		char	s8;		//!< 符号あり  8bit整数
typedef	unsigned	char	u8;		//!< 符号なし  8bit整数
typedef	signed		short	s16;	//!< 符号あり 16bit整数
typedef	unsigned	short	u16;	//!< 符号なし 16bit整数
typedef	signed		int		s32;	//!< 符号あり 32bit整数
typedef	unsigned	int		u32;	//!< 符号なし 32bit整数
typedef	signed		__int64	s64;	//!< 符号あり 64bit整数
typedef	unsigned	__int64	u64;	//!< 符号なし 64bit整数

typedef		float	f32;			//!< 単精度浮動少数点数
typedef		double	f64;			//!< 倍精度浮動少数点数
typedef		BOOL	b32;			//!< 32bitフラグ

typedef		DWORD	ARGB;			//!< カラー

typedef		D3DXVECTOR2		VECTOR2,	*LPVECTOR2;		//!< 2Dベクトル
typedef		D3DXVECTOR3		VECTOR3,	*LPVECTOR3;		//!< 3Dベクトル
typedef		D3DXVECTOR4		VECTOR4,	*LPVECTOR4;		//!< 3Dベクトル
typedef		D3DXMATRIX		MATRIX,		*LPMATRIX;		//!< マトリックス
typedef		D3DXQUATERNION	QUATERNION,	*LPQUATERNION;	//!< クオータニオン

typedef		LPDIRECT3DTEXTURE9	LPTEXTURE;	//!< テクスチャー

//!@}

#endif	//~#if __KX_TYPEDEF_H__

//============================================================================
//	END OF FILE
//============================================================================