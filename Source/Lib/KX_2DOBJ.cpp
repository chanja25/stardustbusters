//---------------------------------------------------------------------------
//!
//!	@file	KX_2DOBJ.cpp
//!	@brief	kx2DObjectクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "KX.h"


//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
kx2DObject::kx2DObject(void)
: _pTexture(NULL)
, _center(0)
{
}

//---------------------------------------------------------------------------
//	コンストラクタ(引数付き)
//!	@param	width	[in] 横幅
//!	@param	height	[in] 縦幅
//!	@param	render	[in] 描画フラグ
//---------------------------------------------------------------------------
kx2DObject::kx2DObject(u32 width, u32 height, b32 render)
: _pTexture(NULL)
, _center(0)
{
	create(width, height, render);
}

//---------------------------------------------------------------------------
//	コンストラクタ(引数付き)
//!	@param	pFileName [in] 読み込むテクスチャーのファイル名
//---------------------------------------------------------------------------
kx2DObject::kx2DObject(const char* pFileName)
: _pTexture(NULL)
, _center(0)
{
	load(pFileName);
}

//---------------------------------------------------------------------------
//!	デストラクタ
//---------------------------------------------------------------------------
kx2DObject::~kx2DObject(void)
{
	cleanup();
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	kx2DObject::cleanup(void)
{
	RESOURCE->cleanup(_pTexture);
	_pTexture = NULL;
}

//---------------------------------------------------------------------------
//	テクスチャーの作成
//!	@param	width	[in] 横幅
//!	@param	height	[in] 縦幅
//!	@param	render	[in] 描画フラグ
//---------------------------------------------------------------------------
void	kx2DObject::create(u32 width, u32 height, b32 render)
{
	//	テクスチャー解放
	cleanup();

	u32			usage	= 0;
	D3DFORMAT	fmt		= D3DFMT_A8R8G8B8;
	D3DPOOL		pool	= D3DPOOL_MANAGED;

	if( render ) {
		usage	= D3DUSAGE_RENDERTARGET;
		fmt		= KXSYSTEM->getBackBufferFormat();
		pool	= D3DPOOL_DEFAULT;
	}

	//	テクスチャーを生成
	D3DXCreateTexture(	KXSYSTEM->getDevice(),
						width,
						height,
						0,
						usage,
						fmt,
						pool,
						&_pTexture );

	_width  = static_cast<f32>(width);
	_height = static_cast<f32>(height);
}

//---------------------------------------------------------------------------
//	テクスチャー読み込み
//!	@param	pFileName [in] ファイル名
//---------------------------------------------------------------------------
void	kx2DObject::load(const char* pFileName)
{
	//	テクスチャー解放
	cleanup();

	_pTexture = RESOURCE->loadTexture(pFileName);
	if( _pTexture == NULL ) return;

	//	テクスチャー情報取得
	LPDIRECT3DSURFACE9	temp;
	D3DSURFACE_DESC		desc;
	_pTexture->GetSurfaceLevel(0, &temp);
	temp->GetDesc(&desc);

	SAFE_RELEASE(temp);

	//	テクスチャーのサイズ取得
	_width  = static_cast<f32>(desc.Width);
	_height = static_cast<f32>(desc.Height);
}

//---------------------------------------------------------------------------
//	描画
//!	@param	x			[in] x座標
//!	@param	y			[in] y座標
//!	@param	width		[in] 横幅
//!	@param	height		[in] 縦幅
//!	@param	srcX		[in] ソース画像のX座標
//!	@param	srcY		[in] ソース画像のy座標
//!	@param	srcW		[in] ソース画像の横幅
//!	@param	srcH		[in] ソース画像の縦幅
//!	@param	pTechnique	[in] 使用するテクニック
//!	@param	color		[in] カラー
//!	@param	flag		[in] 描画モードの設定フラグ
//---------------------------------------------------------------------------
void	kx2DObject::render(f32 x, f32 y, f32 width, f32 height, f32 srcX, f32 srcY, f32 srcW, f32 srcH, char* pTechnique, ARGB color, u32 flag)
{
	//	表示位置セット
	_vertex[0].x = _vertex[2].x = (x);
	_vertex[1].x = _vertex[3].x = (x + width);

	_vertex[0].y = _vertex[1].y = (y);
	_vertex[2].y = _vertex[3].y = (y + height);

	//	テクスチャーの位置セット
	_vertex[0].tu = _vertex[2].tu = (srcX + 0.5f) / _width;
	_vertex[1].tu = _vertex[3].tu = (srcX + srcW) / _width;

	_vertex[0].tv = _vertex[1].tv = (srcY + 0.5f) / _height;
	_vertex[2].tv = _vertex[3].tv = (srcY + srcH) / _height;

	//	その他情報セット
	_vertex[0].z	 = _vertex[1].z		= _vertex[2].z		= _vertex[3].z		= 0.0f;
	_vertex[0].rhw	 = _vertex[1].rhw	= _vertex[2].rhw	= _vertex[3].rhw	= 1.0f;
	_vertex[0].color = _vertex[1].color	= _vertex[2].color	= _vertex[3].color	= color;

	//	頂点情報の描画
	GRAPHICS->render2D(_vertex, 2, _pTexture, flag, pTechnique);
}

//---------------------------------------------------------------------------
//	描画
//!	@param	x		[in] x座標
//!	@param	y		[in] y座標
//!	@param	width	[in] 横幅
//!	@param	height	[in] 縦幅
//!	@param	srcX	[in] ソース画像のX座標
//!	@param	srcY	[in] ソース画像のy座標
//!	@param	srcW	[in] ソース画像の横幅
//!	@param	srcH	[in] ソース画像の縦幅
//!	@param	color	[in] カラー
//!	@param	flag	[in] 描画モードの設定フラグ
//---------------------------------------------------------------------------
void	kx2DObject::render(f32 x, f32 y, f32 width, f32 height, f32 srcX, f32 srcY, f32 srcW, f32 srcH, ARGB color, u32 flag)
{
	render(x, y, width, height, srcX, srcY, srcW, srcH, "copy", color, flag);
}

//---------------------------------------------------------------------------
//	描画
//!	@param	x			[in] x座標
//!	@param	y			[in] y座標
//!	@param	width		[in] 横幅
//!	@param	height		[in] 縦幅
//!	@param	angle		[in] 回転角
//!	@param	srcX		[in] ソース画像のX座標
//!	@param	srcY		[in] ソース画像のy座標
//!	@param	srcW		[in] ソース画像の横幅
//!	@param	srcH		[in] ソース画像の縦幅
//!	@param	pTechnique	[in] 使用するテクニック
//!	@param	color		[in] カラー
//!	@param	flag		[in] 描画モードの設定フラグ
//---------------------------------------------------------------------------
void	kx2DObject::render(f32 x, f32 y, f32 width, f32 height, f32 angle, f32 srcX, f32 srcY, f32 srcW, f32 srcH, char* pTechnique, ARGB color, u32 flag)
{
	f32	s, c;

	s = sinf(angle);
	c = cosf(angle);

	if( _center == CENTER_LEFT_TOP ) {
		//	表示位置セット
		_vertex[0].x = c + -s + x;
		_vertex[0].y = s +  c + y;

		_vertex[1].x = (width * c) + -s + x;
		_vertex[1].y = (width * s) +  c + y;

		_vertex[2].x = c + (height * -s) + x;
		_vertex[2].y = s + (height *  c) + y;

		_vertex[3].x = (width * c) + (height * -s) + x;
		_vertex[3].y = (width * s) + (height *  c) + y;
	} else if( _center == CENTER_CENTER ) {
		f32	dx, dy;
		dx = width	/ 2.0f;
		dy = height	/ 2.0f;

		//	表示位置セット
		_vertex[0].x = (-dx * c) + (-dy * -s) + x;
		_vertex[0].y = (-dx * s) + (-dy *  c) + y;

		_vertex[1].x = (dx * c) + (-dy * -s) + x;
		_vertex[1].y = (dx * s) + (-dy *  c) + y;

		_vertex[2].x = (-dx * c) + (dy * -s) + x;
		_vertex[2].y = (-dx * s) + (dy *  c) + y;

		_vertex[3].x = (dx * c) + (dy * -s) + x;
		_vertex[3].y = (dx * s) + (dy *  c) + y;
	} else if( _center == CENTER_CENTER_BOTTOM ) {
		f32	dx, dy;
		dx = width / 2.0f;
		dy = height;

		//	表示位置セット
		_vertex[0].x = (-dx * c) + (-dy * -s) + x;
		_vertex[0].y = (-dx * s) + (-dy *  c) + y;

		_vertex[1].x = (dx * c) + (-dy * -s) + x;
		_vertex[1].y = (dx * s) + (-dy *  c) + y;

		_vertex[2].x = (-dx * c) + -s + x;
		_vertex[2].y = (-dx * s) +  c + y;

		_vertex[3].x = (dx * c) + -s + x;
		_vertex[3].y = (dx * s) +  c + y;
	}

	//	テクスチャーの位置セット
	_vertex[0].tu = _vertex[2].tu = (srcX + 0.5f) / _width;
	_vertex[1].tu = _vertex[3].tu = (srcX + srcW) / _width;

	_vertex[0].tv = _vertex[1].tv = (srcY + 0.5f) / _height;
	_vertex[2].tv = _vertex[3].tv = (srcY + srcH) / _height;

	//	その他情報セット
	_vertex[0].z	 = _vertex[1].z		= _vertex[2].z		= _vertex[3].z		= 0.0f;
	_vertex[0].rhw	 = _vertex[1].rhw	= _vertex[2].rhw	= _vertex[3].rhw	= 1.0f;
	_vertex[0].color = _vertex[1].color	= _vertex[2].color	= _vertex[3].color	= color;

	//	頂点情報の描画
	GRAPHICS->render2D(_vertex, 2, _pTexture, flag, pTechnique);
}

//---------------------------------------------------------------------------
//	描画
//!	@param	x		[in] x座標
//!	@param	y		[in] y座標
//!	@param	width	[in] 横幅
//!	@param	height	[in] 縦幅
//!	@param	angle	[in] 回転角
//!	@param	srcX	[in] ソース画像のX座標
//!	@param	srcY	[in] ソース画像のy座標
//!	@param	srcW	[in] ソース画像の横幅
//!	@param	srcH	[in] ソース画像の縦幅
//!	@param	color	[in] カラー
//!	@param	flag	[in] 描画モードの設定フラグ
//---------------------------------------------------------------------------
void	kx2DObject::render(f32 x, f32 y, f32 width, f32 height, f32 angle, f32 srcX, f32 srcY, f32 srcW, f32 srcH, ARGB color, u32 flag)
{
	render(x, y, width, height, angle, srcX, srcY, srcW, srcH, "copy", color, flag);
}

//============================================================================
//	END OF FILE
//============================================================================