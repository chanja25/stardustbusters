//---------------------------------------------------------------------------
//!
//!	@file	KX.h
//!	@brief	KXヘッダーインクルードファイル
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#ifndef	__KX_H__
#define __KX_H__

#pragma once

#if(_WIN32_WINNT < 0x501)
#undef	_WIN32_WINNT
#define	_WIN32_WINNT 0x0501
#endif

#define WIN32_LEAN_AND_MEAN

//---------------------------------------------------------------------------
//! @name デバッグ用マクロ
//---------------------------------------------------------------------------
//!@{

//! デバッグフラグ
#ifdef	_DEBUG
	#define	DEBUG 1
#else	//~#if _DEBUG
	#define DEBUG 0
#endif	//~#if _DEBUG

#if	DEBUG
	#include <assert.h>
	#include <crtdbg.h>

	#define ASSERT(__condition__, __string__)	assert(__condition__ && __string__)
	#define CHECKMEMORY							_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)
#else	//~#if DEBUG
	#define ASSERT(__condition__, __string__)
	#define CHECKMEMORY
#endif	//~#if DEBUG

#define	MESSAGE(__string__, __caption__)	MessageBox(NULL, __string__, __caption__, MB_OK)

//!@}

//---------------------------------------------------------------------------
//! @name データ解放用マクロ
//---------------------------------------------------------------------------
//!@{

#define	SAFE_RELEASE(__p__)	if( __p__ ) { __p__->Release();	__p__ = NULL; }
#define	SAFE_DELETE(__p__)	if( __p__ ) { delete __p__;		__p__ = NULL; }
#define	SAFE_DELETES(__p__)	if( __p__ ) { delete [] __p__;	__p__ = NULL; }

//!@}

//	警告対策
#pragma	warning(disable:4996)

//	DirectInputのバージョン設定
#define		DIRECTINPUT_VERSION	0x0800

//---------------------------------------------------------------------------
//! @name ヘッダー
//---------------------------------------------------------------------------
//!@{

#include <windows.h>
#include <stdio.h>
#include <mmsystem.h>
#include <commdlg.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <dinput.h>
#include <dsound.h>

//!@}

//---------------------------------------------------------------------------
//!	@name KXヘッダーインクルード
//---------------------------------------------------------------------------
//!@{

#include "KX_typedef.h"
#include "KX_System.h"
#include "KX_Math.h"
#include "KX_File.h"
#include "KX_Text.h"
#include "KX_Timer.h"
#include "KX_Frame.h"
#include "KX_Profiler.h"

#include "KX_Input.h"

#include "KX_Shader.h"
#include "KX_Graphics.h"
#include "KX_Audio.h"

#include "KX_DebugCamera.h"
#include "KX_Camera.h"

#include "KX_2DOBJ.h"
#include "KX_Mesh.h"
#include "KX_3DOBJ.h"

#include "KX_Font.h"

#include "KX_Resource.h"
#include "KX_Converter.h"

#include "KX_Particle.h"

//!@}

#endif	//~#if __KX_H__

//============================================================================
//	END OF FILE
//============================================================================