//---------------------------------------------------------------------------
//!
//!	@file	KX_Graphics.cpp
//!	@brief	kxGraphicsクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "KX.h"


//	インスタンスの実体生成
kxGraphics	kxGraphics::_instance;

//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
kxGraphics::kxGraphics(void)
: _initFlag(FALSE)
{
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
kxGraphics::~kxGraphics(void)
{
}

//---------------------------------------------------------------------------
//	初期化
//---------------------------------------------------------------------------
void	kxGraphics::init(void)
{
	ASSERT(_initFlag == FALSE, "既にグラフィックスは初期化されています");
	_initFlag = TRUE;
	setRenderState();

	_shader2D.init("DATA\\SHADER\\Shader2D.fx");
	_shader3D.init("DATA\\SHADER\\Shader3D.fx");

	#if	DEBUG
		//	ライン用頂点の初期化
		_numLineVertex = 0;
		ZeroMemory(_lineVertex, sizeof(LINEVERTEX) * LINEVERTEX_MAX);
	#endif	//~#if DEBUG

	ZeroMemory(&_material, sizeof(D3DMATERIAL9));
	_material.Ambient.a = _material.Diffuse.a = 1.0f;
	_material.Ambient.r = _material.Diffuse.r = 1.0f;
	_material.Ambient.g = _material.Diffuse.g = 1.0f;
	_material.Ambient.b = _material.Diffuse.b = 1.0f;
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	kxGraphics::cleanup(void)
{
	_initFlag = FALSE;
	_shader2D.cleanup();
	_shader3D.cleanup();
}

//---------------------------------------------------------------------------
//	描画
//---------------------------------------------------------------------------
void	kxGraphics::render(void)
{
	#if	DEBUG
		//	ラインの描画
		if( _numLineVertex > 0 ) renderLine();

		_numLineVertex = 0;
	#endif	//~#if DEBUG
}

//---------------------------------------------------------------------------
//	ラインの描画
//---------------------------------------------------------------------------
void	kxGraphics::renderLine(void)
{
	LPDIRECT3DDEVICE9	pDevice = KXSYSTEM->getDevice();

	//	シェーダー開始
	_shader3D.setTechnique("Line");
	_shader3D.beginShader();

	//	マトリックスの設定
	static MATRIX world;
	MATH->matrixIdentity(&world);
	_shader3D.setMatrix(world);

	//	シェーダーを使用しているか確認
	if( _shader2D.getEffect() == NULL ) {
		pDevice->SetTransform(D3DTS_WORLD, &world);
		setRenderState(0, NULL, &_material);
	}

	//	シェーダーのタイプ設定
	pDevice->SetFVF(D3DFVF_LINEVERTEX);
	_shader3D.beginPass(0);
	_shader3D.commitChanges();

	#if	DEBUG
		//	レンダリング
		pDevice->DrawPrimitiveUP(D3DPT_LINELIST, (_numLineVertex >> 1), _lineVertex, sizeof(LINEVERTEX));
	#endif	//~#if DEBUG

	//	シェーダーの終了
	_shader3D.endPass();
	_shader3D.endShader();
}

//---------------------------------------------------------------------------
//	スフィアの描画要請
//!	@param	pos			[in] 表示位置
//!	@param	radius		[in] 半径のサイズ
//!	@param	color		[in] 色
//!	@param	divideCount	[in] 球の精度
//---------------------------------------------------------------------------
void	kxGraphics::drawSphere(const VECTOR3& pos, f32 radius, ARGB color, u32 divideCount)
{
	#if	DEBUG
		//	プリミティブの表示数の制限	
		divideCount = ( divideCount > 64 )? 64: ( divideCount < 4 )? 4: divideCount;

		//	プリミティブが表示可能数を超えていれば終わる
		if( (_numLineVertex + divideCount * 6) >= LINEVERTEX_MAX ) return;

		//	一つのプリミティブ当りの角度
		f32 angle = D3DX_PI/180 * (360.0f / static_cast<f32>(divideCount));

		//	スフィアの頂点の設定
		LINEVERTEX vertex[384];

		//	XY軸上のスフィア設定
		u32 i;
		for( i = 1; i < divideCount * 2; i++ ) {
			vertex[i - 1].x = pos.x - cosf((i / 2) * angle) * radius;
			vertex[i - 1].y = pos.y - sinf((i / 2) * angle) * radius;
			vertex[i - 1].z = pos.z;
			vertex[i - 1].color = color;
		}

		vertex[i - 1].x = pos.x - cosf((i / 2) * angle) * radius;
		vertex[i - 1].y = pos.y - sinf((i / 2) * angle) * radius;
		vertex[i - 1].z = pos.z;
		vertex[i - 1].color = color;

		//	YZ軸上のスフィア設定
		for( i++; i < divideCount * 4; i++ ) {
			vertex[i - 1].x = pos.x;
			vertex[i - 1].y = pos.y - sinf((i / 2) * angle) * radius;
			vertex[i - 1].z = pos.z - cosf((i / 2) * angle) * radius;
			vertex[i - 1].color = color;
		}

		vertex[i - 1].x = pos.x;
		vertex[i - 1].y = pos.y - sinf((i / 2) * angle) * radius;
		vertex[i - 1].z = pos.z - cosf((i / 2) * angle) * radius;
		vertex[i - 1].color = color;

		//	ZX軸上のスフィア設定
		for( i++; i < divideCount * 6; i++ ) {
			vertex[i - 1].x = pos.x - sinf((i / 2) * angle) * radius;
			vertex[i - 1].y = pos.y;
			vertex[i - 1].z = pos.z - cosf((i / 2) * angle) * radius;
			vertex[i - 1].color = color;
		}

		vertex[i - 1].x = pos.x - sinf((i / 2) * angle) * radius;
		vertex[i - 1].y = pos.y;
		vertex[i - 1].z = pos.z - cosf((i / 2) * angle) * radius;
		vertex[i - 1].color = color;

		//	頂点情報のセット
		memcpy(&_lineVertex[_numLineVertex], vertex, sizeof(LINEVERTEX) * divideCount * 6);

		_numLineVertex += divideCount * 6;		
	#endif	//~#if DEBUG
}

//---------------------------------------------------------------------------
//	カプセルの描画要請
//!	@param	pos1		[in] 半球の上部の位置
//!	@param	pos2		[in] 半球の下部の位置
//!	@param	radius		[in] 半径のサイズ
//!	@param	color		[in] 色
//!	@param	divideCount	[in] 球の精度
//---------------------------------------------------------------------------
void	kxGraphics::drawCapsule(const VECTOR3& pos1, const D3DXVECTOR3& pos2, f32 radius, ARGB color, u32 divideCount)
{
	#if	DEBUG
		//	プリミティブの表示数の制限	
		divideCount = ( divideCount > 64 )? 64: ( divideCount < 4 )? 4: divideCount;

		//	2点が同じ位置ならスフィアを作る
		if( pos1 == pos2 ) {
			drawSphere(pos1, radius, color, divideCount);
			return;
		}

		//	プリミティブが表示可能数を超えていれば終わる
		if( _numLineVertex + divideCount * 8 + 8 >= LINEVERTEX_MAX ) return;

		VECTOR3 cross, vec1, vec2;
		vec1 = pos2 - pos1;
		vec2 = VECTOR3(0, 0, 1);

		MATH->cross(&cross, vec1, vec2);
		if( cross == VECTOR3(0, 0, 0) ) cross = VECTOR3(1, 0, 0);
		else MATH->normalize(&cross, cross);
		MATH->normalize(&vec1, vec1);

		f32 angle = MATH->dot(vec1, vec2) / (MATH->length(vec1) * MATH->length(vec2));

		angle = acosf(angle);
		MATRIX	trans;

		MATH->axisRotationMatrix(&trans, cross, angle);

		VECTOR3 vec;

		//	一つ当りのプリミティブ角度
		angle = RAD * (360.0f / static_cast<f32>(divideCount));

		//	スフィアの頂点の設定
		LINEVERTEX vertex[520];

		//	１つ目のXY軸上のスフィア設定
		u32 i;
		for( i = 1; i < divideCount * 2; i++ ) {
			vec.x = cosf((i / 2) * angle) * radius;
			vec.y = sinf((i / 2) * angle) * radius;
			vec.z = 0;
			D3DXVec3TransformNormal(&vec, &vec, &trans);
			vertex[i - 1].x = pos1.x - vec.x;
			vertex[i - 1].y = pos1.y - vec.y;
			vertex[i - 1].z = pos1.z - vec.z;
			vertex[i - 1].color = color;
		}

		vec.x = cosf((i / 2) * angle) * radius;
		vec.y = sinf((i / 2) * angle) * radius;
		vec.z = 0;
		D3DXVec3TransformNormal(&vec, &vec, &trans);
		vertex[i - 1].x = pos1.x - vec.x;
		vertex[i - 1].y = pos1.y - vec.y;
		vertex[i - 1].z = pos1.z - vec.z;
		vertex[i - 1].color = color;

		//	２つ目のXY軸上のスフィア設定
		for( i++; i < divideCount * 4; i++ ) {
			vec.x = cosf((i / 2) * angle) * radius;
			vec.y = sinf((i / 2) * angle) * radius;
			vec.z = 0;
			D3DXVec3TransformNormal(&vec, &vec, &trans);
			vertex[i - 1].x = pos2.x - vec.x;
			vertex[i - 1].y = pos2.y - vec.y;
			vertex[i - 1].z = pos2.z - vec.z;
			vertex[i - 1].color = color;
		}

		vec.x = cosf((i / 2) * angle) * radius;
		vec.y = sinf((i / 2) * angle) * radius;
		vec.z = 0;
		D3DXVec3TransformNormal(&vec, &vec, &trans);
		vertex[i - 1].x = pos2.x - vec.x;
		vertex[i - 1].y = pos2.y - vec.y;
		vertex[i - 1].z = pos2.z - vec.z;
		vertex[i - 1].color = color;

		//	1つ目のYZ軸上のスフィア設定
		for( i++; i < divideCount * 5; i++ ) {
			vec.x = 0;
			vec.y = sinf((i / 2) * angle - (RAD * 90)) * radius;
			vec.z = cosf((i / 2) * angle - (RAD * 90)) * radius;
			D3DXVec3TransformNormal(&vec, &vec, &trans);
			vertex[i - 1].x = pos1.x - vec.x;
			vertex[i - 1].y = pos1.y - vec.y;
			vertex[i - 1].z = pos1.z - vec.z;
			vertex[i - 1].color = color;
		}

		vec.x = 0;
		vec.y = sinf((i / 2) * angle - (RAD * 90)) * radius;
		vec.z = cosf((i / 2) * angle - (RAD * 90)) * radius;
		D3DXVec3TransformNormal(&vec, &vec, &trans);
		vertex[i - 1].x = pos1.x - vec.x;
		vertex[i - 1].y = pos1.y - vec.y;
		vertex[i - 1].z = pos1.z - vec.z;
		vertex[i - 1].color = color;

		//	2つ目のYZ軸上のスフィア設定
		for( i++; i < divideCount * 6; i++ ) {
			vec.x = 0;
			vec.y = sinf((i / 2) * angle - (RAD * 90)) * radius;
			vec.z = cosf((i / 2) * angle - (RAD * 90)) * radius;
			D3DXVec3TransformNormal(&vec, &vec, &trans);
			vertex[i - 1].x = pos2.x - vec.x;
			vertex[i - 1].y = pos2.y - vec.y;
			vertex[i - 1].z = pos2.z - vec.z;
			vertex[i - 1].color = color;
		}

		vec.x = 0;
		vec.y = sinf((i / 2) * angle - (RAD * 90)) * radius;
		vec.z = cosf((i / 2) * angle - (RAD * 90)) * radius;
		D3DXVec3TransformNormal(&vec, &vec, &trans);
		vertex[i - 1].x = pos2.x - vec.x;
		vertex[i - 1].y = pos2.y - vec.y;
		vertex[i - 1].z = pos2.z - vec.z;
		vertex[i - 1].color = color;

		//	1つ目のZX軸上のスフィア設定
		for( i++; i < divideCount * 7; i++ ) {
			vec.x = sinf((i / 2) * angle - (RAD * 90)) * radius;
			vec.y = 0;
			vec.z = cosf((i / 2) * angle - (RAD * 90)) * radius;
			D3DXVec3TransformNormal(&vec, &vec, &trans);
			vertex[i - 1].x = pos1.x - vec.x;
			vertex[i - 1].y = pos1.y - vec.y;
			vertex[i - 1].z = pos1.z - vec.z;
			vertex[i - 1].color = color;
		}

		vec.x = sin((i / 2) * angle - (RAD * 90)) * radius;
		vec.y = 0;					  
		vec.z = cos((i / 2) * angle - (RAD * 90)) * radius;
		D3DXVec3TransformNormal(&vec, &vec, &trans);
		vertex[i - 1].x = pos1.x - vec.x;
		vertex[i - 1].y = pos1.y - vec.y;
		vertex[i - 1].z = pos1.z - vec.z;
		vertex[i - 1].color = color;

		//	2つ目のZX軸上のスフィア設定
		for( i++; i < divideCount * 8; i++ ) {
			vec.x = sinf((i / 2) * angle - (RAD * 90)) * radius;
			vec.y = 0;					   
			vec.z = cosf((i / 2) * angle - (RAD * 90)) * radius;
			D3DXVec3TransformNormal(&vec, &vec, &trans);
			vertex[i - 1].x = pos2.x - vec.x;
			vertex[i - 1].y = pos2.y - vec.y;
			vertex[i - 1].z = pos2.z - vec.z;
			vertex[i - 1].color = color;
		}

		vec.x = sinf((i / 2) * angle - (RAD * 90)) * radius;
		vec.y = 0;					   
		vec.z = cosf((i / 2) * angle - (RAD * 90)) * radius;
		D3DXVec3TransformNormal(&vec, &vec, &trans);
		vertex[i - 1].x = pos2.x - vec.x;
		vertex[i - 1].y = pos2.y - vec.y;
		vertex[i - 1].z = pos2.z - vec.z;
		vertex[i - 1].color = color;

		//	前後をつなぐライン設定
		for( i++; i < divideCount * 8 + 8; i += 2 ) {
			vec.x = cosf((i / 2) * (RAD * 90)) * radius;
			vec.y = sinf((i / 2) * (RAD * 90)) * radius;
			vec.z = 0;
			D3DXVec3TransformNormal(&vec, &vec, &trans);
			vertex[i - 1].x = pos1.x - vec.x;
			vertex[i - 1].y = pos1.y - vec.y;
			vertex[i - 1].z = pos1.z - vec.z;
			vertex[i - 1].color = color;

			vec.x = cosf((i / 2) * (RAD * 90)) * radius;
			vec.y = sinf((i / 2) * (RAD * 90)) * radius;
			vec.z = 0;
			D3DXVec3TransformNormal(&vec, &vec, &trans);
			vertex[i].x = pos2.x - vec.x;
			vertex[i].y = pos2.y - vec.y;
			vertex[i].z = pos2.z - vec.z;
			vertex[i].color = color;
		}

		//	頂点情報のセット
		memcpy(&_lineVertex[_numLineVertex], vertex, sizeof(LINEVERTEX) * (divideCount * 8 + 8));

		_numLineVertex += divideCount * 8 + 8;
	#endif	//~#if DEBUG
}

//---------------------------------------------------------------------------
//	2D描画
//!	@param	pVertex		[in] 頂点情報
//!	@param	num			[in] 面の数
//!	@param	pTexture	[in] テクスチャー
//!	@param	flag		[in] 描画モードの設定フラグ
//---------------------------------------------------------------------------
void	kxGraphics::render2D(LPVERTEX2D pVertex, s32 num, LPTEXTURE pTexture, u32 flag, char* pTechnique)
{
	//	Direct3Dデバイスの取得
	LPDIRECT3DDEVICE9	pDevice = KXSYSTEM->getDevice();

	//	シェーダー開始
	_shader2D.setTechnique(pTechnique);
	_shader2D.beginShader();

	//	シェーダーを使用しているか確認
	if( _shader2D.getEffect() == NULL ) {
		//	レンダーステートの設定
		setRenderState(flag, pTexture);
	}

	//	シェーダーのタイプ設定
	pDevice->SetFVF(D3DFVF_VERTEX2D);
	_shader2D.setTexture(pTexture);
	_shader2D.beginPass(flag);
	_shader2D.commitChanges();

	//	レンダリング
	pDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, num, pVertex, sizeof(VERTEX2D));

	//	シェーダーの終了
	_shader2D.endPass();
	_shader2D.endShader();
}

//---------------------------------------------------------------------------
//	2D描画
//!	@param	x		[in] x座標
//!	@param	y		[in] y座標
//!	@param	width	[in] 横幅
//!	@param	height	[in] 縦幅
//!	@param	color	[in] 描画色
//---------------------------------------------------------------------------
void	kxGraphics::drawRect(f32 x, f32 y, f32 width, f32 height, ARGB color)
{
	VERTEX2D vertex[4];

	//	表示位置セット
	vertex[0].x = vertex[2].x = x;
	vertex[1].x = vertex[3].x = x + width;

	vertex[0].y = vertex[1].y = y;
	vertex[2].y = vertex[3].y = y + height;

	//	テクスチャーの位置セット
	vertex[0].tu = vertex[2].tu = 0;
	vertex[1].tu = vertex[3].tu = 0;

	vertex[0].tv = vertex[1].tv = 0;
	vertex[2].tv = vertex[3].tv = 0;

	//	その他情報セット
	vertex[0].z		= vertex[1].z		= vertex[2].z		= vertex[3].z		= 0.0f;
	vertex[0].rhw	= vertex[1].rhw		= vertex[2].rhw		= vertex[3].rhw		= 1.0f;
	vertex[0].color	= vertex[1].color	= vertex[2].color	= vertex[3].color	= color;

	//	頂点情報の描画
	GRAPHICS->render2D(vertex, 2, NULL, 0, "None");
}

//---------------------------------------------------------------------------
//	3D描画
//!	@param	pVertex		[in] 頂点情報
//!	@param	num			[in] 面の数
//!	@param	pTexture	[in] テクスチャー
//!	@param	flag		[in] 描画モードの設定フラグ
//---------------------------------------------------------------------------
void	kxGraphics::render3D(LPVERTEX2D pVertex, s32 num, LPTEXTURE pTexture, u32 flag, char* pTechnique)
{
	//	Direct3Dデバイスの取得
	LPDIRECT3DDEVICE9	pDevice = KXSYSTEM->getDevice();

	//	シェーダー開始
	_shader3D.setTechnique(pTechnique);
	_shader3D.beginShader();

	//	シェーダーを使用しているか確認
	if( _shader3D.getEffect() == NULL ) {
		//	レンダーステートの設定
		setRenderState(flag, pTexture);
	}

	//	シェーダーのタイプ設定
	pDevice->SetFVF(D3DFVF_VERTEX);
	_shader2D.setTexture(pTexture);
	_shader2D.beginPass(flag);
	_shader2D.commitChanges();

	//	レンダリング
	pDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, num, pVertex, sizeof(D3DFVF_VERTEX));

	//	シェーダーの終了
	_shader3D.endPass();
	_shader3D.endShader();
}

//---------------------------------------------------------------------------
//	レンダーステートの設定
//---------------------------------------------------------------------------
void	kxGraphics::setRenderState(void)
{
	//	Direct3Dデバイスの取得
	LPDIRECT3DDEVICE9	pDevice = KXSYSTEM->getDevice();

	//	サンプラーステータスの設定
	pDevice->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_NONE);
	pDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
	pDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
	pDevice->SetSamplerState(0, D3DSAMP_ADDRESSU,  D3DTADDRESS_WRAP);
	pDevice->SetSamplerState(0, D3DSAMP_ADDRESSV,  D3DTADDRESS_WRAP);

	//	アルファブレンドを有効にする
	pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE,	TRUE);
	pDevice->SetRenderState(D3DRS_ALPHATESTENABLE,	TRUE);
	pDevice->SetRenderState(D3DRS_ALPHAFUNC,		D3DCMP_NOTEQUAL);
	pDevice->SetRenderState(D3DRS_ALPHAREF,			FALSE);

	//	Zバッファを有効にする
	pDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);

	pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
	pDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	pDevice->SetRenderState(D3DRS_SHADEMODE, D3DSHADE_GOURAUD);

	//	テクスチャーの合成方法設定
	pDevice->SetTextureStageState(0, D3DTSS_COLOROP,   D3DTOP_MODULATE);
	pDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_CURRENT);
	pDevice->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_TEXTURE);
	pDevice->SetTextureStageState(0, D3DTSS_ALPHAOP,   D3DTOP_MODULATE);
	pDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_CURRENT);
	pDevice->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_TEXTURE);

	//	ライトの設定
	pDevice->SetRenderState(D3DRS_AMBIENT,		  0x888888);
	pDevice->SetRenderState(D3DRS_LIGHTING,		  TRUE);
	pDevice->SetRenderState(D3DRS_SPECULARENABLE, TRUE);
	
	pDevice->SetRenderState(D3DRS_NORMALIZENORMALS, TRUE);
}

//---------------------------------------------------------------------------
//	レンダーステートの設定
//!	@param	flag		[in] 描画方法設定フラグ
//!	@param	pTexture	[in] テクスチャー
//!	@param	pMaterial	[in] マテリアル
//---------------------------------------------------------------------------
void	kxGraphics::setRenderState(u32 flag, LPTEXTURE pTexture, D3DMATERIAL9* pMaterial)
{
	//	Direct3Dデバイスの取得
	LPDIRECT3DDEVICE9 pDevice = KXSYSTEM->getDevice();

	#define	RS	pDevice->SetRenderState

	switch( flag ) {
	//	書き込み
	case RS_COPY:
		RS(D3DRS_BLENDOP,	D3DBLENDOP_ADD);
		RS(D3DRS_DESTBLEND,	D3DBLEND_INVSRCALPHA);
		RS(D3DRS_SRCBLEND,	D3DBLEND_SRCALPHA);
		break;
	//	加算合成
	case RS_ADD:
		RS(D3DRS_BLENDOP,	D3DBLENDOP_ADD);
		RS(D3DRS_DESTBLEND,	D3DBLEND_ONE);
		RS(D3DRS_SRCBLEND,	D3DBLEND_SRCALPHA);
		break;
	//	減算合成
	case RS_SUB:
		RS(D3DRS_BLENDOP,	D3DBLENDOP_REVSUBTRACT);
		RS(D3DRS_DESTBLEND,	D3DBLEND_ONE);
		RS(D3DRS_SRCBLEND,	D3DBLEND_SRCALPHA);
		break;
	//	乗算合成
	case RS_MUL:
		RS(D3DRS_BLENDOP,	D3DBLENDOP_ADD);
		RS(D3DRS_DESTBLEND,	D3DBLEND_SRCCOLOR);
		RS(D3DRS_SRCBLEND,	D3DBLEND_ZERO);
		break;

	default:
		ASSERT(false, "レンダーステートエラー");
		break;
	}

	//	テクスチャーの設定
	pDevice->SetTexture(0, pTexture);
	//	マテリアルの設定
	if( pMaterial ) pDevice->SetMaterial(pMaterial);
}


//============================================================================
//	END OF FILE
//============================================================================