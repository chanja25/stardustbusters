//---------------------------------------------------------------------------
//!
//!	@file	KX_Shader.cpp
//!	@brief	kxShaderクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "KX.h"


//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
kxShader::kxShader(void) :
_pEffect(NULL)
{
}

//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
kxShader::kxShader(char* pFileName) :
_pEffect(NULL)
{
	//	初期化
	init(pFileName);
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
kxShader::~kxShader(void)
{
}

//---------------------------------------------------------------------------
//	初期化
//!	@param	pFileName [in] 読み込むファイル名
//!	@retval	TRUE	成功
//!	@retval	FALSE	失敗
//---------------------------------------------------------------------------
b32		kxShader::init(char* pFileName)
{
	SAFE_RELEASE(_pEffect);

	HRESULT	hr;
	LPD3DXBUFFER	pErr = NULL;

	//	エフェクトファイルの読み込み
	hr = D3DXCreateEffectFromFile(	KXSYSTEM->getDevice(),
									pFileName,
									NULL,
									NULL,
									D3DXSHADER_DEBUG,
									NULL,
									&_pEffect,
									&pErr );
	if( FAILED(hr) ) {
		//	読み込みに失敗した場合、エフェクトファイルのエラー内容を表示
		MESSAGE((LPCSTR)pErr->GetBufferPointer(), "Shader読み込みエラー" );
	} else {
		//	関連付け
		_technique	= _pEffect->GetTechniqueByName("copy");
		_mWVP		= _pEffect->GetParameterByName(NULL, "mWVP");
		_mWorld		= _pEffect->GetParameterByName(NULL, "mWorld");
		_texture	= _pEffect->GetParameterByName(NULL, "Texture");
	}

	SAFE_RELEASE(pErr);

	_pass = 0;
	return SUCCEEDED(hr);
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	kxShader::cleanup(void)
{
	SAFE_RELEASE(_pEffect);
}

//---------------------------------------------------------------------------
//	コミットチェンジ
//---------------------------------------------------------------------------
void	kxShader::commitChanges(void)
{
	if( !_pEffect ) return;
	_pEffect->CommitChanges();
}

//---------------------------------------------------------------------------
//	シェーダーの開始
//---------------------------------------------------------------------------
b32		kxShader::beginShader(void)
{
	if( !_pEffect ) return FALSE;
	_pEffect->SetTechnique(_technique);
	_pEffect->Begin(&_pass, 0);
	return TRUE;
}

//---------------------------------------------------------------------------
//	シェーダーの終了
//---------------------------------------------------------------------------
void	kxShader::endShader(void)
{
	if( !_pEffect ) return;
	_pEffect->End();
}

//---------------------------------------------------------------------------
//	テクニックの設定
//!	@param	pTechnique [in] 使用するテクニック
//---------------------------------------------------------------------------
void	kxShader::setTechnique(char* pTechnique)
{
	if( !_pEffect ) return;
	_technique = _pEffect->GetTechniqueByName(pTechnique);
}

//---------------------------------------------------------------------------
//	パスの開始
//!	@param	pass	[in] 開始するパス
//---------------------------------------------------------------------------
void	kxShader::beginPass(u32 pass)
{
	if( !_pEffect ) return;
	_pEffect->BeginPass(pass);
}

//---------------------------------------------------------------------------
//	パスの終了
//---------------------------------------------------------------------------
void	kxShader::endPass(void)
{
	if( !_pEffect ) return;
	_pEffect->EndPass();
}

//---------------------------------------------------------------------------
//	マトリックスの設定
//!	@param	mat		[in] マトリックスデータ
//---------------------------------------------------------------------------
void	kxShader::setMatrix(const MATRIX &world)
{
	if( !_pEffect ) return;

	_pEffect->SetMatrix(_mWorld, &world);

	//	ローカルからを射影の変換マトリックス生成
	MATRIX wvp = world * CAMERA->getView() * CAMERA->getProjection();

	_pEffect->SetMatrix(_mWVP, &wvp);
}

//---------------------------------------------------------------------------
//	テクスチャーの設定
//!	@param	pTexture	[in] テクスチャーデータ
//---------------------------------------------------------------------------
void		kxShader::setTexture(LPTEXTURE pTexture)
{
	if( !_pEffect ) return;
	_pEffect->SetTexture(_texture, pTexture);
}

//---------------------------------------------------------------------------
//	パラメーターの設定(TEXTURE)
//!	@param	pStr	[in] セットする変数名
//!	@param	param	[in] TEXTURE型データ
//---------------------------------------------------------------------------
void		kxShader::setValue(LPSTR pStr, const LPTEXTURE pParam)
{
	if( !_pEffect ) return;
	_pEffect->SetTexture(pStr, pParam);
}

//---------------------------------------------------------------------------
//	パラメーターの設定(MATRIX)
//!	@param	pStr	[in] セットする変数名
//!	@param	param	[in] MATRIX型データ
//---------------------------------------------------------------------------
void		kxShader::setValue(LPSTR pStr, const MATRIX& param)
{
	if( !_pEffect ) return;
	_pEffect->SetMatrix(pStr, &param);
}

//---------------------------------------------------------------------------
//	パラメーターの設定(VECTOR4)
//!	@param	pStr	[in] セットする変数名
//!	@param	param	[in] VECTOR4型データ
//---------------------------------------------------------------------------
void		kxShader::setValue(LPSTR pStr, const VECTOR4& param)
{
	if( !_pEffect ) return;
	_pEffect->SetVector(pStr, &param);
}

//---------------------------------------------------------------------------
//	パラメーターの設定(VECTOR3)
//!	@param	pStr	[in] セットする変数名
//!	@param	param	[in] VECTOR3型データ
//---------------------------------------------------------------------------
void		kxShader::setValue(LPSTR pStr, const VECTOR3& param)
{
	if( !_pEffect ) return;
	_pEffect->SetFloatArray(pStr, reinterpret_cast<const f32*>(&param), 3);
}

//---------------------------------------------------------------------------
//	パラメーターの設定(f32)
//!	@param	pStr	[in] セットする変数名
//!	@param	param	[in] f32型データ
//---------------------------------------------------------------------------
void		kxShader::setValue(LPSTR pStr, f32 param)
{
	if( !_pEffect ) return;
	_pEffect->SetFloat(pStr, param);
}

//---------------------------------------------------------------------------
//	パラメーターの設定(s32)
//!	@param	pStr	[in] セットする変数名
//!	@param	param	[in] s32型データ
//---------------------------------------------------------------------------
void		kxShader::setValue(LPSTR pStr, s32 param)
{
	if( !_pEffect ) return;
	_pEffect->SetInt(pStr, param);
}

//---------------------------------------------------------------------------
//	パラメーターの設定(DWORD)
//!	@param	pStr	[in] セットする変数名
//!	@param	param	[in] DWORD型データ
//---------------------------------------------------------------------------
void		kxShader::setValue(LPSTR pStr, DWORD param)
{
	if( !_pEffect ) return;
	_pEffect->SetValue(pStr, &param, 4);
}

//============================================================================
//	END OF FILE
//============================================================================