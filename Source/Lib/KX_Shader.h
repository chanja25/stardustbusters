//---------------------------------------------------------------------------
//!
//!	@file	KX_Shader.h
//!	@brief	kxShaderクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#ifndef	__KX_SHADER_H__
#define	__KX_SHADER_H__

#pragma once


//===========================================================================
//!	kxShaderクラス
//===========================================================================
class kxShader
{
public:
	//---------------------------------------------------------------------------
	//! @name 初期化
	//---------------------------------------------------------------------------
	//!@{

	//!	コンストラクタ
	kxShader(void);
	//!	コンストラクタ(引数付き)
	kxShader(char* pFileName);
	//!	デストラクタ
	~kxShader(void);

	//!	初期化
	b32		init(char* pFileName);
	//!	解放
	void	cleanup(void);

	//!@}

	//!	コミットチェンジ
	void	commitChanges(void);
	//!	シェーダーの開始
	b32		beginShader(void);
	//!	シェーダーの終了
	void	endShader(void);
	//!	パスの開始
	void	beginPass(u32 pass);
	//!	パスの終了
	void	endPass(void);

	//!	デバイスの解放
	void	lost(void) { _pEffect->OnLostDevice(); }
	//!	デバイスのリセット
	void	reset(void) { _pEffect->OnResetDevice(); }

	//---------------------------------------------------------------------------
	//! @name 取得参照
	//---------------------------------------------------------------------------
	//!@{

	//!	テクニックのセット
	void	setTechnique(char* pTechnique);
	//!	マトリックスの設定
	void	setMatrix(const MATRIX& world);
	//!	テクスチャーの設定
	void	setTexture(LPTEXTURE pTexture);

	//!	パラメーターの設定(TEXTURE)
	void	setValue(LPSTR pStr, const LPTEXTURE pParam);
	//!	パラメーターの設定(MATRIX)
	void	setValue(LPSTR pStr, const MATRIX& param);
	//!	パラメーターの設定(VECTOR4)
	void	setValue(LPSTR pStr, const VECTOR4& param);
	//!	パラメーターの設定(VECTOR3)
	void	setValue(LPSTR pStr, const VECTOR3& param);
	//!	パラメーターの設定(f32)
	void	setValue(LPSTR pStr, f32 param);
	//!	パラメーターの設定(s32)
	void	setValue(LPSTR pStr, s32 param);
	//!	パラメーターの設定(DWORD)
	void	setValue(LPSTR pStr, DWORD param);

	//!	パス数取得
	//!	@retval	パスの数
	inline u32			getPass(void) { return ( _pEffect )? _pass: 1; }
	//!	エフェクトの取得
	//!	@retval	エフェクトデータ
	inline LPD3DXEFFECT	getEffect(void) { return _pEffect; }

	//!@}

private:
	LPD3DXEFFECT	_pEffect;	//!< エフェクト
	D3DXHANDLE		_technique;	//!< テクニック
	D3DXHANDLE		_mWVP;		//!< ローカルから射影変換までのマトリックス
	D3DXHANDLE		_mWorld;	//!< ローカルからワールド座標変換マトリックス
	D3DXHANDLE		_texture;	//!< テクスチャー情報

	u32				_pass;		//!< 使用するパス
};

//!	SHADERの定義
typedef	kxShader	SHADER, *LPSHADER;

#endif	//~#if __KX_SHADER_H__

//============================================================================
//	END OF FILE
//============================================================================