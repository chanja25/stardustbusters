//---------------------------------------------------------------------------
//!
//!	@file	KX_MATH.cpp
//!	@brief	計算クラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "KX.h"


//	インスタンスの実体生成
kxMath	kxMath::_instance;

//---------------------------------------------------------------------------
//	正規化
//!	@param	pOut	[out] 正規化したベクトル
//!	@param	vec		[in]  正規化するベクトル
//---------------------------------------------------------------------------
void	kxMath::normalize(VECTOR3* pOut, const VECTOR3& vec)
{
	*pOut = vec / kxMath::length(vec);
}

//---------------------------------------------------------------------------
//	内積による|a||b|cosθの取得
//!	@param	vec1	[in] 内積を取る1つ目のベクトル
//!	@param	vec2	[in] 内積を取る2つ目のベクトル
//!	@retval	2つのベクトルの角度(|a||b|cosθ)
//---------------------------------------------------------------------------
f32		kxMath::dot(const VECTOR3& vec1, const VECTOR3& vec2)
{
	return (vec1.x * vec2.x) + (vec1.y * vec2.y) + (vec1.z * vec2.z);
}

//---------------------------------------------------------------------------
//	外積による2つの直線に垂直な線の取得
//!	@param	pVec	[out] 外積により求めた2つの直線に垂直な線
//!	@param	vec1	[in]  外積を取る1つ目のベクトル
//!	@param	vec2	[in]  外積を取る2つ目のベクトル
//---------------------------------------------------------------------------
void	kxMath::cross(VECTOR3* pVec, const VECTOR3& vec1, const VECTOR3& vec2)
{
	pVec->x = (vec1.y * vec2.z) - (vec2.y * vec1.z);
	pVec->y = (vec1.z * vec2.x) - (vec2.z * vec1.x);
	pVec->z = (vec1.x * vec2.y) - (vec2.x * vec1.y);
}

//---------------------------------------------------------------------------
//	外積
//!	@param	x1	[in] 1つ目のベクトルのx軸
//!	@param	y1	[in] 1つ目のベクトルのy軸
//!	@param	x2	[in] 2つ目のベクトルのx軸
//!	@param	y2	[in] 2つ目のベクトルのy軸
//!	@retval	2つのベクトルの角度(|a||b|sinθ)
//---------------------------------------------------------------------------
f32		kxMath::cross(f32 x1, f32 y1, f32 x2, f32 y2)
{
	return (x1 * y2) - (x2 * y1);
}

//---------------------------------------------------------------------------
//	マトリックスの初期化
//!	@param	pOut	[out] 変更するマトリックス
//---------------------------------------------------------------------------
void	kxMath::matrixIdentity(MATRIX *pOut)
{
	pOut->_11 = pOut->_22 = pOut->_33 = pOut->_44 = 1.0f;

	pOut->_12 = pOut->_13 = pOut->_14 = 0;
	pOut->_21 = pOut->_23 = pOut->_24 = 0;
	pOut->_31 = pOut->_32 = pOut->_34 = 0;
	pOut->_41 = pOut->_42 = pOut->_43 = 0;
}

//---------------------------------------------------------------------------
//	座標変更行列作成
//!	@param	pOut	[out] 作成された座標変更行列
//!	@param	pos		[in]  セットする座標
//---------------------------------------------------------------------------
void	kxMath::matrixTranslation(MATRIX* pOut, const VECTOR3& pos)
{
	pOut->_11 = pOut->_22 = pOut->_33 = pOut->_44 = 1.0f;

	pOut->_12 = pOut->_13 = pOut->_14 = 0;
	pOut->_21 = pOut->_23 = pOut->_24 = 0;
	pOut->_31 = pOut->_32 = pOut->_34 = 0;

	pOut->_41 = pos.x;
	pOut->_42 = pos.y;
	pOut->_43 = pos.z;
}

//---------------------------------------------------------------------------
//	X軸周りの回転行列作成
//!	@param	pOut	[out] 作成された回転行列
//!	@param	angle	[in]  角度
//---------------------------------------------------------------------------
void	kxMath::rotationMatrixX(MATRIX* pOut, f32 angle)
{
	pOut->_11 = pOut->_44 = 1.0f;

	pOut->_12 = pOut->_13 = pOut->_14 = 0;
	pOut->_21 = pOut->_24 = 0;
	pOut->_31 = pOut->_34 = 0;
	pOut->_41 = pOut->_42 = pOut->_43 = 0;

	pOut->_22 =  cosf(angle);
	pOut->_23 =  sinf(angle);

	pOut->_32 = -sinf(angle);
	pOut->_33 =  cosf(angle);
}

//---------------------------------------------------------------------------
//	Y軸周りの回転行列作成
//!	@param	pOut	[out] 作成された回転行列
//!	@param	angle	[in]  角度
//---------------------------------------------------------------------------
void	kxMath::rotationMatrixY(MATRIX* pOut, f32 angle)
{
	pOut->_22 = pOut->_44 = 1.0f;

	pOut->_12 = pOut->_14 = 0;
	pOut->_21 = pOut->_23 = pOut->_24 = 0;
	pOut->_32 = pOut->_34 = 0;
	pOut->_41 = pOut->_42 = pOut->_43 = 0;

	pOut->_11 =  cosf(angle);
	pOut->_13 = -sinf(angle);

	pOut->_31 =  sinf(angle);
	pOut->_33 =  cosf(angle);
}

//---------------------------------------------------------------------------
//	Z軸周りの回転行列作成
//!	@param	pOut	[out] 作成された回転行列
//!	@param	angle	[in]  角度
//---------------------------------------------------------------------------
void	kxMath::rotationMatrixZ(MATRIX* pOut, f32 angle)
{
	pOut->_33 = pOut->_44 = 1.0f;

	pOut->_13 = pOut->_14 = 0;
	pOut->_23 = pOut->_24 = 0;
	pOut->_31 = pOut->_32 = pOut->_34 = 0;
	pOut->_41 = pOut->_42 = pOut->_43 = 0;

	pOut->_11 =  cosf(angle);
	pOut->_12 =  sinf(angle);

	pOut->_21 = -sinf(angle);
	pOut->_22 =  cosf(angle);
}

//---------------------------------------------------------------------------
//	回転行列作成(YawPicthRoll)
//!	@param	pOut	[out] 作成された回転行列
//!	@param	x		[in]  X軸の角度
//!	@param	y		[in]  Y軸の角度
//!	@param	z		[in]  Z軸の角度
//---------------------------------------------------------------------------
void	kxMath::rotationMatrixXYZ(MATRIX* pOut, f32 x, f32 y, f32 z)
{
	f32	sx = sinf(x);
	f32	sy = sinf(y);
	f32	sz = sinf(z);

	f32	cx = cosf(x);
	f32	cy = cosf(y);
	f32	cz = cosf(z);

	//	rotationZ * rotationX * rotationY
	pOut->_11 =  cz * cy + sz * sx * sy;
	pOut->_12 =  sz * cx;
	pOut->_13 = -cz * sy + sz * sx * cy;
	pOut->_14 =  0;

	pOut->_21 = -sz * cy + cz * sx * sy;
	pOut->_22 =  cz * cx;
	pOut->_23 =  sz * sy + cz * sx * cy;
	pOut->_24 =  0;

	pOut->_31 =  cx * sy;
	pOut->_32 = -sx;
	pOut->_33 =  cx * cy;
	pOut->_34 =  0;
	
	pOut->_41 =  0;
	pOut->_42 =  0;
	pOut->_43 =  0;
	pOut->_44 =  1.0f;
}

//---------------------------------------------------------------------------
//	スケール変更行列作成
//!	@param	pOut	[out] 作成されたスケール変更行列
//!	@param	x		[in]  x軸のスケール
//!	@param	y		[in]  y軸のスケール
//!	@param	z		[in]  z軸のスケール
//---------------------------------------------------------------------------
void	kxMath::scalingMatrix(MATRIX* pOut, f32 x, f32 y, f32 z)
{
	pOut->_12 = pOut->_13 = pOut->_14 = 0;
	pOut->_21 = pOut->_23 = pOut->_24 = 0;
	pOut->_31 = pOut->_32 = pOut->_34 = 0;
	pOut->_41 = pOut->_42 = pOut->_43 = 0;

	pOut->_11 = x;
	pOut->_22 = y;
	pOut->_33 = z;
	pOut->_44 = 1.0f;
}

//---------------------------------------------------------------------------
//	任意軸回転行列の作成
//!	@param	pOut	[out] 作成された回転行列
//!	@param	axis	[in]  回転させる軸
//!	@param	angle	[in]  回転量
//---------------------------------------------------------------------------
void	kxMath::axisRotationMatrix(MATRIX* pOut, const VECTOR3& axis, f32 angle)
{
	f32	sin = sinf(angle);
	f32	cos = cosf(angle);

	pOut->_11 = axis.x * axis.x * (1 - cos) + cos;
	pOut->_12 = axis.x * axis.y * (1 - cos) - axis.z * sin;
	pOut->_13 = axis.x * axis.z * (1 - cos) + axis.y * sin;
	pOut->_14 = 0;

	pOut->_21 = axis.y * axis.x * (1 - cos) + axis.z * sin;
	pOut->_22 = axis.y * axis.y * (1 - cos) + cos;
	pOut->_23 = axis.y * axis.z * (1 - cos) - axis.x * sin;
	pOut->_24 = 0;

	pOut->_31 = axis.z * axis.x * (1 - cos) - axis.y * sin;
	pOut->_32 = axis.z * axis.y * (1 - cos) + axis.x * sin;
	pOut->_33 = axis.z * axis.z * (1 - cos) + cos;
	pOut->_34 = 0;

	pOut->_41 = 0;
	pOut->_42 = 0;
	pOut->_43 = 0;
	pOut->_44 = 1;
}

//---------------------------------------------------------------------------
//	1つ目のベクトルから2つ目のベクトルに変換する行列作成
//!	@param	pOut	[out] 作成された変換行列
//!	@param	vec		[in]  変換させる初期ベクトル
//!	@param	vec		[in]  変換後のベクトル
//---------------------------------------------------------------------------
void	kxMath::vectorRotationMatrix(MATRIX* pOut, const VECTOR3& vec1, const VECTOR3& vec2)
{
	VECTOR3 axis;
	//	外積により2つのベクトルに垂直なベクトルを取得
	cross(&axis, vec1, vec2);
	//	内積により2つのベクトルのcosθを取得
	f32 angle = dot(vec1, vec2) / (length(vec1) * length(vec2));
	//	cos⁻¹によりラジアン角取得
	angle = acosf(angle);

	//	変換行列の生成
	axisRotationMatrix(pOut, axis, angle);
}

//============================================================================
//	END OF FILE
//============================================================================