//---------------------------------------------------------------------------
//!
//!	@file	KX_Particle.h
//!	@brief	パーティクル
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#ifndef	__KX_PARTICLE_H__
#define	__KX_PARTICLE_H__

#pragma	once


//============================================================================
//!	パーティクルクラス
//============================================================================
class kxParticle {
public:
	//!	パーティクル用
	struct PARTICLE_DATA {
		u8		no;				//!< 使用するテクスチャー番号
		u8		type;			//!< タイプ

		VECTOR3	pos;			//!< 位置
		VECTOR3	move;			//!< 移動量
		VECTOR3	power;			//!< フレーム毎の加速度

		f32		angle;			//!< 回転角
		f32		rotate;			//!< フレーム毎の回転度

		f32		scale;			//!< サイズ
		f32		stretch;		//!< フレーム毎のサイズ変更度

		u16		startFrame;		//!< 開始フレーム
		ARGB	startColor;		//!< カラー

		u16		middleFrame;	//!< 中間フレーム
		ARGB	middleColor;	//!< 中間カラー

		u16		endFrame;		//!< 終了フレーム
		ARGB	endColor;		//!< カラー

		char	technique[32];	//!< 使用するテクニック

		kxGraphics::VERTEX v[4];	//!< 頂点データ
	};
	//============================================================================
	//!	パーティクルデータクラス
	//============================================================================
	class ParticleData {
	public:
		//---------------------------------------------------------------------------
		//! @name 初期化
		//---------------------------------------------------------------------------
		//!@{

		//!	コンストラクタ
		ParticleData(void);
		//!	デストラクタ
		~ParticleData(void);

		//!	パーティクルのセット
		void	set(const PARTICLE_DATA& particle);

		//!@}

		//!	更新
		void	update(void);
		//!	描画
		void	render(const MATRIX& mat);

		//-------------------------------------------------------------
		//!	@name 取得参照
		//-------------------------------------------------------------
		//!@{

		//!	現在の状態取得
		//!	@retval	状態
		inline	b32	isAlive(void) { return _isAlive; }
		//!	テクスチャー番号の状態取得
		//!	@retval	使用するテクスチャー番号
		inline	u8	getNo(void) { return _particle.no; }

		//!@}

	private:
		b32				_isAlive;	//!< 状態確認用
		u16				_frame;		//!< 現在のフレーム
		ARGB			_color;		//!< 現在のカラー
		PARTICLE_DATA	_particle;	//!< パーティクルデータ
	};

	//---------------------------------------------------------------------------
	//! @name 初期化
	//---------------------------------------------------------------------------
	//!@{

	//!	初期化
	void	init(void);
	//!	解放
	void	cleanup(void);
	//!	テクスチャーの解放
	void	cleanup(u8 no);

	//!@}

	//!	ファイルの読み込み
	void	load(const char* pFileName, u8 no);	

	//!	パーティクルのセット
	void	setParticle(const PARTICLE_DATA& particle);
	//!	パーティクルのセット
	void	setParticle(u8 no, u8 type, u16 sFrame, ARGB sColor, u16 mFrame, ARGB mColor, u16 eFrame, ARGB eColor,
						const VECTOR3& pos, const VECTOR3& move, const VECTOR3& power,
						f32 rotate, f32 scale, f32 stretch = 1.0f, const char* pTechnique = NULL);

	//!	更新
	void	update(void);
	//!	描画
	void	render(void);

	//!	インスタンスの取得
	//!	@retval	インスタンス
	inline static kxParticle*	getInstance(void) { return &_instance; }
private:
	//!	コンストラクタ
	kxParticle(void);
	//!	デストラクタ
	~kxParticle(void);

	static	kxParticle	_instance;	//!< 唯一のインスタンス

	//!	最大パーティクル数
	static const u32	PARTICLE_MAX = 5000;

	ParticleData*		_pParticle;				//!< パーティクルデータ

	//!	最大テクスチャー格納数
	static const u8		TEXTURE_MAX = 8;

	LPTEXTURE			_pTexture[TEXTURE_MAX];	//!< パーティクル用テクスチャー
};

#define	PARTICLE	(kxParticle::getInstance())

#endif	//~#if __KX_PARTICLE_H__

//============================================================================
//	END OF FILE
//============================================================================