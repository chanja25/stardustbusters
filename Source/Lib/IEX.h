//---------------------------------------------------------------------------
//!
//!	@file	IEX.h
//!	@brief	IEX関連
//!
//!	@author IGA
//---------------------------------------------------------------------------
#ifndef	__IEX__
#define __IEX__

#pragma once


typedef struct tagIEMBONE {
	MATRIX		BoneMatrix;			//	ボーン行列
	u16			parent;				//	親ボーン

	QUATERNION	orgPose;			//	基本姿勢
	VECTOR3		orgPos;				//	基本座標

	u16			IndexNum;			//	影響頂点数
	u32*		Index;				//	影響頂点Index
	f32*		Influence;			//	影響力
} IEMBONE, *LPIEMBONE;

typedef struct tagIEMMOTION {
	u16			NumRotate;			//	回転キーフレーム数
	u16*		RotateFrame;		//	回転キーフレーム
	QUATERNION*	Rotate;				//	ボーンの状態クォータニオン

	u16			NumPosition;		//	座標キーフレーム数
	u16*		PositionFrame;		//	座標キーフレーム
	LPVECTOR3	Position;			//	座標
} IEMMOTION, *LPIEMMOTION;

typedef struct tagIEMFILE {
	//	メッシュ情報
	u16				NumVertex;			//	頂点数

	kxGraphics::LPVERTEX3D	lpVertex;	//	頂点バッファ

	u16				NumFace;			//	ポリゴン数
	u16*			lpFace;				//	ポリゴンインデックス
	u32*			lpAtr;				//	ポリゴン材質

	u16				NumMaterial;		//	マテリアル数
	D3DMATERIAL9	Material[32];		//	マテリアル
	char			Texture[32][64];	//	テクスチャファイル

	//	ボーン情報
	u16				NumBone;
	LPIEMBONE		lpBone;

	//	モーション情報
	u16				MaxFrame;
	u16				NumMotion;
	u16				M_Offset[256];
	u16				FrameFlag[65535];

	LPIEMMOTION		lpMotion;

} IEMFILE, *LPIEMFILE;

//	アニメーション用構造体
typedef struct tagIEXANIME {
	u32					rotNum;
	LPWORD				rotFrame;
	LPQUATERNION	rot;

	u32					posNum;
	LPWORD				posFrame;
	LPVECTOR3			pos;
} IEXANIME, *LPIEXANIME;

#endif	//~#if __IEX__

//============================================================================
//	END OF FILE
//============================================================================