//---------------------------------------------------------------------------
//!
//!	@file	KX_Graphics.h
//!	@brief	kxGraphicsクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#ifndef	__KX_GRAPHICS_H__
#define	__KX_GRAPHICS_H__

#pragma once


//===========================================================================
//!	kxGraphicsクラス
//===========================================================================
class kxGraphics
{
public:
	//---------------------------------------------------------------------------
	//!	@name 初期化
	//---------------------------------------------------------------------------
	//!@{

	//!	初期化
	void	init(void);
	//!	解放
	void	cleanup(void);

	//!@}

	//!	レンダーステート
	enum RS{
		RS_COPY,	// 合成
		RS_ADD,		// 加算合成
		RS_SUB,		// 減算合成
		RS_MUL,		// 乗算合成
	};

	//!	ライン用頂点
	static	const s32 D3DFVF_LINEVERTEX = D3DFVF_XYZ | D3DFVF_DIFFUSE;
	//!	ライン頂点情報
	struct	LINEVERTEX{
		f32		x, y, z;	//!< 頂点座標
		ARGB	color;		//!< 頂点カラー
	};

	//!	2D用頂点
	static	const s32 D3DFVF_VERTEX2D = D3DFVF_XYZRHW | D3DFVF_DIFFUSE | D3DFVF_TEX1;
	//!	2D頂点情報
	struct	VERTEX2D{
		f32		x, y, z, rhw;	//!< 頂点座標
		ARGB	color;			//!< 頂点カラー
		f32		tu, tv;			//!< テクスチャーのUV座標
	};
	typedef	VERTEX2D*	LPVERTEX2D;

	//!	3D用頂点
	static	const s32 D3DFVF_VERTEX3D = D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1;
	//!	3D頂点情報
	struct	VERTEX3D{
		f32		x, y, z;	//!< 頂点座標
		f32		nx, ny, nz;	//!< 法線の向き
		f32		tu, tv;		//!< テクスチャーのUV座標
	};
	typedef	VERTEX3D*	LPVERTEX3D;

	//!	頂点
	static	const s32 D3DFVF_VERTEX = D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE | D3DFVF_TEX1;
	//!	頂点情報
	struct	VERTEX{
		f32		x, y, z;	//!< 頂点座標
		f32		nx, ny, nz;	//!< 法線の向き
		ARGB	color;		//!< 頂点カラー
		f32		tu, tv;		//!< テクスチャーのUV座標
	};
	typedef	VERTEX*	LPVERTEX;

	//---------------------------------------------------------------------------
	//!	@name 描画関連
	//---------------------------------------------------------------------------
	//!@{

	//!	描画
	void	render(void);
	//!	2Dの描画
	void	render2D(LPVERTEX2D pVertex, s32 num, LPTEXTURE pTexture, u32 flag, char* pTechnique);
	//! 2Dの面描画
	void	drawRect(f32 x, f32 y, f32 width, f32 height, ARGB color);
	//!	3Dの面描画
	void	kxGraphics::render3D(LPVERTEX2D pVertex, s32 num, LPTEXTURE pTexture, u32 flag, char* pTechnique);

	//!	ラインの描画
	void	renderLine(void);
	//!	スフィアの描画要請
	void	drawSphere(const VECTOR3& pos, f32 radius, ARGB color, u32 divideCount);
	//!	カプセルの描画要請
	void	drawCapsule(const VECTOR3& pos1, const VECTOR3& pos2, f32 radius, ARGB color, u32 divideCount);

	//!	レンダーステートの初期設定
	void	setRenderState(void);
	//!	レンダーステートの設定
	void	setRenderState(u32 flag, LPTEXTURE pTexture, D3DMATERIAL9* pMaterial = NULL);

	//!@}

	//-------------------------------------------------------------
	//!	@name 取得参照
	//-------------------------------------------------------------
	//!@{

	//!	2Dシェーダーの取得
	//!	@retval	2Dシェーダ
	inline LPSHADER	getShader2D(void)	{ return &_shader2D; }
	//!	3Dシェーダーの取得
	//!	@retval	3Dシェーダ
	inline LPSHADER	getShader3D(void)	{ return &_shader3D; }

	//!	インスタンスの取得
	//!	@retval	インスタンス
	inline static kxGraphics*	getInstance(void) { return &_instance; }

	//!@}

private:
	//!	コンストラクタ
	kxGraphics(void);
	//!	デストラクタ
	~kxGraphics(void);

	static	kxGraphics	_instance;	//!< 唯一のインスタンス

	b32			_initFlag;			//!< 初期化フラグ
	SHADER		_shader2D;			//!< 2Dシェーダー
	SHADER		_shader3D;			//!< 3Dシェーダー

	#if	DEBUG
		//!	ライン頂点情報の最大格納数
		static const u32 LINEVERTEX_MAX = 10000 * 2;

		u32			_numLineVertex;					//!< 使用頂点数(LINE)
		LINEVERTEX	_lineVertex[LINEVERTEX_MAX];	//!< 頂点格納配列(LINE)
	#endif	//~#if DEBUG

	D3DMATERIAL9	_material;						//!< 基本材質
};

//!	kxGraphicsのインスタンス取得マクロ
#define	GRAPHICS	(kxGraphics::getInstance())

#endif	//~#if __KX_GRAPHICS_H__

//============================================================================
//	END OF FILE
//============================================================================