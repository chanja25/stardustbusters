//---------------------------------------------------------------------------
//!
//!	@file	KX_DebugCamera.h
//!	@brief	kxDebugCameraクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#ifndef	__KX_DEBUGCAMERA_H__
#define __KX_DEBUGCAMERA_H__

#pragma once

#if	DEBUG
	#define	USE_DEBUGCAMERA 1
#else	//~#if DEBUG
	#define	USE_DEBUGCAMERA 0
#endif	//~#if DEBUG

#if	USE_DEBUGCAMERA

//===========================================================================
//!	kxDebugCameraクラス
//===========================================================================
class kxDebugCamera
{
public:
	//---------------------------------------------------------------------------
	//!	@name 初期化
	//---------------------------------------------------------------------------
	//!@{

	//!	初期化
	void	init(void);
	//!	解放
	void	cleanup(void);

	//!@}

	//!	更新
	void	update(void);

	//---------------------------------------------------------------------------
	//!	@name 取得参照
	//---------------------------------------------------------------------------
	//!@{

	//!	プロジェクションのセット
	void	setProjection(f32 fovY, f32 aspect, f32 near, f32 far);

	//!	位置のセット
	//!	@param	pos		[in] セットする位置
	inline void		setPos(const VECTOR3& pos) { _pos = pos; }
	//!	ターゲットのセット
	//!	@param	target	[in] セットするターゲット
	inline void		setTarget(const VECTOR3& target) { _target = target; }

	//! ワールド、ビュー系変換マトリッ
	//!	@retval	ワールド、ビュー系変換マトリックス
	inline MATRIX	getView(void) { return _view; }
	//! ビュー、射影系変換マトリックス
	//!	@retval	ビュー、射影系変換マトリックス
	inline MATRIX	getProjection(void) { return _proj; }

	//!	インスタンスの取得
	//!	@retval	インスタンス
	inline static kxDebugCamera*	getInstance(void) { return &_instance; }

	//!@}

private:
	//!	コンストラクタ
	kxDebugCamera(void);
	//!	デストラクタ
	~kxDebugCamera(void);

	static kxDebugCamera _instance;	//!< 唯一のインスタンス

	VECTOR3	_pos;		//!< カメラのポジション
	VECTOR3 _target;	//!< カメラのターゲット
	VECTOR3 _up;		//!< カメラの上を指すベクトル
	VECTOR3 _vec;		//!< ターゲットまでの距離と向き

	f32		_fovY;		//!< 視野角
	f32		_aspect;	//!< アスペクト比
	f32		_near;		//!< 前方クリップ面
	f32		_far;		//!< 後方クリップ面

	MATRIX	_view;		//!< ワールド、ビュー系変換マトリックス
	MATRIX	_proj;		//!< ビュー、射影系変換マトリックス
};

//!	kxDebugCameraのインスタンス取得マクロ
#define	DEBUGCAMERA	(kxDebugCamera::getInstance())

//	デバッグカメラの位置、ターゲット変更マクロ
#define	DEBUG_CPOS(pos)			(DEBUGCAMERA->setPos(pos))
#define	DEBUG_CTARGET(target)	(DEBUGCAMERA->setTarget(target))

#else

#define	DEBUG_CPOS(pos)
#define	DEBUG_CTARGET(target)

#endif	//~#if USE_DEBUGCAMERA

#endif	//~#if __KX_DEBUGCAMERA_H__

//============================================================================
//	END OF FILE
//============================================================================