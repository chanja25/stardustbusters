//---------------------------------------------------------------------------
//!
//!	@file	KX_File.h
//!	@brief	ファイル操作クラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#ifndef	__KX_FILE_H__
#define __KX_FILE_H__

#pragma once


//===========================================================================
//!	ファイルクラス
//===========================================================================
class kxFile{
public:
	//---------------------------------------------------------------------------
	//!	@name 初期化
	//---------------------------------------------------------------------------
	//!@{

	//!	コンストラクタ
	kxFile();
	//!	デストラクタ
	~kxFile();

	//!@}

	//---------------------------------------------------------------------------
	//!	@name ファイルの読み書き
	//---------------------------------------------------------------------------
	//!@{

	//!	ファイルの読み込み
	void*	read(const char* pFileName);
	//!	ファイルを参照して読み込み
	void*	read(void);
	//!	ファイルの書き込み
	void	write(const char* pFileName, const void* pBuffer, u32 size);
	//!	ファイルの書き込み
	void	write(const void* pBuffer, u32 size);
	//!	読み込み中のファイル取得
	void*	getFile(void);
	//!	読み込んでいるファイルのサイズ
	int		getFileSize(void);

	//!	ファイルの書き込み開始
	b32		readingFile(const char* pFileName);
	//!	ファイルの書き込み開始
	void	reading(void* pBuffer, u32 size);
	//!	ファイルの書き込み開始
	b32		writtingFile(const char* pFileName);
	//!	ファイルの書き込み開始
	void	writting(const void* pBuffer, u32 size);
	//!	ファイルを閉じる
	void	closeFile(void);

	//---------------------------------------------------------------------------
	//!	参照ファイルの設定用
	//---------------------------------------------------------------------------
	enum ATTRIBUTE {
		ATT_BMP		= 0x0001,	//!< *.bmp
		ATT_JPG		= 0x0002,	//!< *.jpeg
		ATT_PNG		= 0x0004,	//!< *.png
		ATT_TEXT	= 0x0008,	//!< *.txt

		ATT_ALL		= 0x4000,	//!< *.*
		ATT_ORIGIN	= 0x8000,	//!< *.(オリジナル拡張子)
	};
	static const u32 ORIGIN_MAX = 8; //!< オリジナル拡張子の登録可能数

	//!	参照したファイル名の取得
	//!	@retval	参照したファイル名
	char*	getReadFileName(void) { return _pReadFileName; }
	//!	参照したファイル名の取得
	//!	@retval	参照したファイル名
	char*	getWriteFileName(void) { return _pWriteFileName; }
	//! 参照可能なファイルの設定
	void	setAttribute(u16 attribute);
	//!	参照可能なファイルの取得
	//!	@retval	参照可能なファイル
	inline u16	getAttribute(void) { return _attribute; }

	//!	参照可能なオリジナルファイルの設定
	void	setOrigin(const char* pName, const char* pFile);
	//!	参照可能ファイルのリセット
	void	resetOrigin(void);

	//!	参照可能なファイルの取得
	void	getAttribute(char* attribute);
	//!	ファイルを参照する（読み込み）
	char*	readReference(void);
	//!	ファイルを参照する（書き込み）
	char*	writeReference(void);

	//!@}

private:
	HANDLE	_hFile;				//!< ファイル読み込み用のハンドル

	s8*		_pFileBuffer;		//!< 読み込んだファイル
	int		_fileSize;			//!< 読み込んだファイルサイズ


	char*	_pReadFileName;		//!< 参照したファイル名
	char*	_pWriteFileName;	//!< 参照したファイル名
	u16		_attribute;			//!< 参照可能なデータ情報
	u8		_originNum;			//!< オリジナルファイルの数

	char	_pOriginName[ORIGIN_MAX][PATH_MAX];	//!< オリジナルファイル名(説明)
	char	_pOriginFile[ORIGIN_MAX][PATH_MAX];	//!< オリジナルの拡張子
};

typedef	kxFile	File;

#endif	//~#if __KX_FILE_H__

//============================================================================
//	END OF FILE
//============================================================================