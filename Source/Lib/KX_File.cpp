//---------------------------------------------------------------------------
//!
//!	@file	KX_File.cpp
//!	@brief	ファイル操作クラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "KX.h"


//---------------------------------------------------------------------------
//	コストラクタ
//---------------------------------------------------------------------------
kxFile::kxFile()
: _pFileBuffer(NULL)
, _hFile(NULL)
, _fileSize(0)
, _pReadFileName(NULL)
, _pWriteFileName(NULL)
, _attribute(0)
, _originNum(0)
{
	ZeroMemory(_pOriginName, sizeof(char) * ORIGIN_MAX * PATH_MAX);
	ZeroMemory(_pOriginFile, sizeof(char) * ORIGIN_MAX * PATH_MAX);
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
kxFile::~kxFile()
{
	//	ファイル名を解放
	SAFE_DELETES(_pReadFileName);
	SAFE_DELETES(_pWriteFileName);

	//	読み込んだファイルを破棄する
	SAFE_DELETES(_pFileBuffer);
}

//---------------------------------------------------------------------------
//	ファイルの読み込み
//!	@param	pFileName [in] 読み込むファイル名
//!	@retval	読み込んだファイルデータ
//---------------------------------------------------------------------------
void*		kxFile::read(const char* pFileName)
{
	HANDLE hFile;
	DWORD dum;

	//	今まで読み込んでいたファイルを破棄する
	SAFE_DELETES(_pFileBuffer);

	//---------------------------------------------------------------------------
	//	ファイルを開く
	//---------------------------------------------------------------------------
	hFile = CreateFile( pFileName,
						GENERIC_READ,
						FILE_SHARE_READ,
						NULL,
						OPEN_EXISTING,
						FILE_ATTRIBUTE_NORMAL,
						(HANDLE) NULL );

	//	ファイルが正常に開けていたら実行
	if( hFile != INVALID_HANDLE_VALUE ){
		_fileSize = GetFileSize(hFile, NULL);
		_pFileBuffer = new s8[_fileSize + 1];
		ReadFile(hFile, _pFileBuffer, _fileSize, &dum, NULL);
		CloseHandle(hFile);
		_pFileBuffer[_fileSize] = '\0';
	}

	return _pFileBuffer;
}

//---------------------------------------------------------------------------
//	ファイルを参照して読み込み
//!	@retval	読み込んだファイルデータ
//---------------------------------------------------------------------------
void*	kxFile::read(void)
{
	//	参照して読み込んだファイルを返す
	readReference();
	return read(_pReadFileName);
}

//---------------------------------------------------------------------------
//	ファイルの書き込み
//!	@param	pFileName [in] 書き込むファイル名
//!	@param	pBuffer	[in] 書き込むデータ
//!	@param	size	[in] 書き込むデータサイズ
//---------------------------------------------------------------------------
void	kxFile::write(const char* pFileName, const void* pBuffer, u32 size)
{
	HANDLE hFile;
	DWORD dum;

	//---------------------------------------------------------------------------
	//	ファイルを作成する(存在すれば上書きする)
	//---------------------------------------------------------------------------
	hFile = CreateFile( pFileName,
						GENERIC_WRITE,
						FILE_SHARE_WRITE,
						NULL,
						CREATE_ALWAYS,
						FILE_ATTRIBUTE_NORMAL,
						NULL );

	if( hFile == INVALID_HANDLE_VALUE ) {
		// ファイルの作成にしっぱい
		char buf[PATH_MAX];
		sprintf_s(buf, sizeof(buf), "ファイルの読み込み失敗(%s)", pFileName);
		ASSERT(false, buf);
	}
	//	読み込んだファイルの先頭に行く
	SetFilePointer(hFile, 0, NULL, FILE_END);

	//ここで指定された文字列を出力します。
	WriteFile(hFile, pBuffer, size, &dum, NULL);

	CloseHandle(hFile);
}

//---------------------------------------------------------------------------
//	ファイルの参照して読み込む
//!	@param	pBuffer	[in] 書き込むデータ
//!	@param	size	[in] 書き込むデータサイズ
//---------------------------------------------------------------------------
void	kxFile::write(const void* pBuffer, u32 size)
{
	writeReference();
	write(_pWriteFileName, pBuffer, size);
}

//---------------------------------------------------------------------------
//	読み込んでいるファイルを受け取る
//!	@retval	読み込んでいるファイルデータ
//---------------------------------------------------------------------------
void*	kxFile::getFile(void)
{
	return _pFileBuffer;
}

//---------------------------------------------------------------------------
//	読み込んでいるファイルのサイズ
//!	@retval 読み込んでいるファイルサイズ
//---------------------------------------------------------------------------
s32		kxFile::getFileSize(void)
{
	return _fileSize;
}

//---------------------------------------------------------------------------
//	読み込んでいるファイル
//!	@param	pFileName [in] 読み込むファイル名
//---------------------------------------------------------------------------
b32		kxFile::readingFile(const char* pFileName)
{
	//---------------------------------------------------------------------------
	//	ファイルを作成する(存在すれば上書きする)
	//---------------------------------------------------------------------------
	_hFile = CreateFile( pFileName,
						GENERIC_READ,
						FILE_SHARE_READ,
						NULL,
						OPEN_EXISTING,
						FILE_ATTRIBUTE_NORMAL,
						(HANDLE) NULL );

	if( _hFile == INVALID_HANDLE_VALUE ) {
		// ファイルの読み込みにしっぱい
		char buf[PATH_MAX];
		sprintf_s(buf, sizeof(buf), "ファイルの読み込み失敗(%s)", pFileName);
		MESSAGE(buf, "エラー");
		return FALSE;
	}

	return TRUE;
}

//---------------------------------------------------------------------------
//	読み込む
//!	@param	pBuffer	[in] 読み込むデータ
//!	@param	size	[in] 読み込むデータサイズ
//---------------------------------------------------------------------------
void	kxFile::reading(void* pBuffer, u32 size)
{
	DWORD dum;
	ReadFile(_hFile, pBuffer, size, &dum, NULL);
}

//---------------------------------------------------------------------------
//	書き込んでいるファイル
//!	@param	pFileName [in] 書き込むファイル名
//---------------------------------------------------------------------------
b32		kxFile::writtingFile(const char* pFileName)
{
	//---------------------------------------------------------------------------
	//	ファイルを作成する(存在すれば上書きする)
	//---------------------------------------------------------------------------
	_hFile = CreateFile( pFileName,
						GENERIC_WRITE,
						FILE_SHARE_WRITE,
						NULL,
						CREATE_ALWAYS,
						FILE_ATTRIBUTE_NORMAL,
						NULL );

	if( _hFile == INVALID_HANDLE_VALUE ) {
		// ファイルの作成にしっぱい
		MESSAGE("ファイルの書き込み失敗", "エラー");
		return FALSE;
	}
	//	読み込んだファイルの先頭に行く
	SetFilePointer(_hFile, 0, NULL, FILE_END);

	return TRUE;
}

//---------------------------------------------------------------------------
//	書き込む
//!	@param	pBuffer	[in] 書き込むデータ
//!	@param	size	[in] 書き込むデータサイズ
//---------------------------------------------------------------------------
void	kxFile::writting(const void* pBuffer, u32 size)
{
	DWORD dum;
	WriteFile(_hFile, pBuffer, size, &dum, NULL);
}

//---------------------------------------------------------------------------
//	ファイルを閉じる
//---------------------------------------------------------------------------
void	kxFile::closeFile(void)
{
	CloseHandle(_hFile);
}

//---------------------------------------------------------------------------
//	参照可能なファイルの設定
//!	@param	attribute [in] 参照可能にするファイルの種類
//---------------------------------------------------------------------------
void	kxFile::setAttribute(u16 attribute)
{
	_attribute = attribute;
}

//---------------------------------------------------------------------------
//	参照可能なオリジナルファイルの設定
//!	@param	pName [in] オリジナルファイルの説明
//!	@param	pFile [in] オリジナルファイルの拡張子
//---------------------------------------------------------------------------
void	kxFile::setOrigin(const char* pName, const char* pFile)
{
	if( _originNum >= ORIGIN_MAX ) return;

	char	str[PATH_MAX];
	wsprintf(str, "%s(*.%s)", pName, pFile);
	strcpy(_pOriginName[_originNum], str);
	wsprintf(str, "*.%s", pFile);
	strcpy(_pOriginFile[_originNum], str);

	_originNum++;
}

//---------------------------------------------------------------------------
//	参照可能ファイルのリセット
//---------------------------------------------------------------------------
void	kxFile::resetOrigin(void)
{
	ZeroMemory(_pOriginName, sizeof(char) * ORIGIN_MAX * PATH_MAX);
	ZeroMemory(_pOriginFile, sizeof(char) * ORIGIN_MAX * PATH_MAX);
	_originNum = 0;
}

//---------------------------------------------------------------------------
//	参照可能なファイルの設定
//!	@retval 参照可能なファイルの取得
//---------------------------------------------------------------------------
void	kxFile::getAttribute(char* attribute)
{
	//	ファイルポインタ
	u32	pointer = 0;

	//	BMP
	if( _attribute & ATT_BMP ) {
		wsprintf(&attribute[pointer], "BITMAP(*.bmp)");
		pointer += static_cast<u32>(strlen(&attribute[pointer])) + 1;
		wsprintf(&attribute[pointer], "*.bmp");
		pointer += static_cast<u32>(strlen(&attribute[pointer])) + 1;
	}

	//	JPG
	if( _attribute & ATT_JPG ) {
		wsprintf(&attribute[pointer], "JPEG(*jpeg,*.jpg)");
		pointer += static_cast<u32>(strlen(&attribute[pointer])) + 1;
		wsprintf(&attribute[pointer], "*.jpeg;*.jpg");
		pointer += static_cast<u32>(strlen(&attribute[pointer])) + 1;
	}

	//	PNG
	if( _attribute & ATT_PNG ) {
		wsprintf(&attribute[pointer], "PNG(*.png)");
		pointer += static_cast<u32>(strlen(&attribute[pointer])) + 1;
		wsprintf(&attribute[pointer], "*.png");
		pointer += static_cast<u32>(strlen(&attribute[pointer])) + 1;
	}

	//	TEXT
	if( _attribute & ATT_TEXT ) {
		wsprintf(&attribute[pointer], "TEXT(*.txt)");
		pointer += static_cast<u32>(strlen(&attribute[pointer])) + 1;
		wsprintf(&attribute[pointer], "*.txt");
		pointer += static_cast<u32>(strlen(&attribute[pointer])) + 1;
	}

	//	オリジナルファイル
	if( (_attribute & ATT_ORIGIN) ) {
		for( u32 i=0; i < _originNum; i++ ) {
			wsprintf(&attribute[pointer], "%s", _pOriginName[i]);
			pointer += static_cast<u32>(strlen(_pOriginName[i])) + 1;
			wsprintf(&attribute[pointer], "%s", _pOriginFile[i]);
			pointer += static_cast<u32>(strlen(_pOriginFile[i])) + 1;
		}
	}

	//	ALL Files
	if( (_attribute & ATT_ALL) || (_attribute == 0) ) {
		wsprintf(&attribute[pointer], "All Files(*.*)");
		pointer += static_cast<u32>(strlen(&attribute[pointer])) + 1;
		wsprintf(&attribute[pointer], "*.*");
		pointer += static_cast<u32>(strlen(&attribute[pointer])) + 1;
	}

	attribute[pointer] = '\0';
}

//---------------------------------------------------------------------------
//	ファイルを参照する（読み込み）
//!	@retval	参照したファイル名
//---------------------------------------------------------------------------
char*	kxFile::readReference(void)
{
	SAFE_DELETES(_pReadFileName);

	//---------------------------------------------------------------------------
	//	ファイルの参照
	//---------------------------------------------------------------------------
	OPENFILENAME ofn;
	char fileName[PATH_MAX];
	fileName[0] = '\0';

	char attribute[PATH_MAX];
	getAttribute(attribute);

	memset( &ofn, 0, sizeof(OPENFILENAME) );
	ofn.lStructSize		= sizeof(OPENFILENAME);
	ofn.hwndOwner		= KXSYSTEM->getWindowHandle();
	ofn.lpstrFilter		= attribute;
	ofn.lpstrFile		= fileName;
	ofn.lpstrFileTitle	= NULL;
	ofn.nMaxFile		= PATH_MAX;
	ofn.Flags			= OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
	ofn.lpstrDefExt		= "";
	ofn.lpstrTitle		= "ファイルを開く";
	if( GetOpenFileName(&ofn) == 0 ) return NULL;

	_pReadFileName = new char[strlen(fileName) + 1];
	strcpy(_pReadFileName, fileName);
	return _pReadFileName;
}

//---------------------------------------------------------------------------
//	ファイルを参照する（書き込み）
//!	@retval	参照したファイル名
//---------------------------------------------------------------------------
char*	kxFile::writeReference(void)
{
	SAFE_DELETES(_pWriteFileName);

	//---------------------------------------------------------------------------
	//	ファイルの参照
	//---------------------------------------------------------------------------
	OPENFILENAME ofn;
	char fileName[PATH_MAX];
	fileName[0] = '\0';

	char	attribute[PATH_MAX];
	getAttribute(attribute);

	memset( &ofn, 0, sizeof(OPENFILENAME) );
	ofn.lStructSize			= sizeof(OPENFILENAME);
	ofn.hwndOwner			= KXSYSTEM->getWindowHandle();
	ofn.lpstrFilter			= attribute;
	ofn.lpstrFile			= fileName;	
	ofn.nMaxFile			= PATH_MAX;
	ofn.nFilterIndex		= 0;
	ofn.lpstrCustomFilter	= attribute;
	ofn.nMaxCustFilter		= PATH_MAX;
	ofn.lpstrFileTitle		= NULL;
	ofn.Flags				= OFN_OVERWRITEPROMPT | OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_CREATEPROMPT;
	ofn.lpstrDefExt			= "";
	ofn.lpstrTitle			= "ファイルの保存";
	if( GetSaveFileName(&ofn) == 0 ) return NULL;

	_pWriteFileName = new char[strlen(fileName) + 1];
	strcpy(_pWriteFileName, fileName);
	return _pWriteFileName;
}

//============================================================================
//	END OF FILE
//============================================================================