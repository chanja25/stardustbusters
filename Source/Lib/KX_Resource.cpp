//---------------------------------------------------------------------------
//!
//!	@file	KX_Resource.cpp
//!	@brief	リソース管理クラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "KX.h"


//	インスタンスの実体生成
kxResource	kxResource::_instance;

//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
kxResource::kxResource(void)
: _pTexData(NULL)
, _pMeshData(NULL)
, _pObj3D(NULL)
{
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
kxResource::~kxResource(void)
{
}

//---------------------------------------------------------------------------
//	初期化
//---------------------------------------------------------------------------
void	kxResource::init(void)
{
	ASSERT(_pTexData == NULL, "既にKX_Resourceは初期化されています");

	//	テクスチャー情報を最大格納数分生成
	_pTexData = new TEXTUREDATA[TEXTURE_MAX];

	ASSERT(_pTexData, "メモリー不足です");
	ZeroMemory(_pTexData, sizeof(TEXTUREDATA) * TEXTURE_MAX);

	//	メッシュ情報を最大格納数分生成
	_pMeshData = new MESHDATA[MESH_MAX];

	ASSERT(_pMeshData, "メモリー不足です");
	ZeroMemory(_pMeshData, sizeof(MESHDATA) * MESH_MAX);

	//	3DOBJ情報を最大格納数分生成
	_pObj3D = new OBJ3D[OBJ3D_MAX];

	ASSERT(_pObj3D, "メモリー不足です");
	ZeroMemory(_pObj3D, sizeof(OBJ3D) * OBJ3D_MAX);
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	kxResource::cleanup(void)
{
	char buf[PATH_MAX];
	u32 i;

	//	3DOBJの解放
	for( i = 0; i < OBJ3D_MAX; i++ ) {
		if( _pObj3D[i]._pSkinInfo == NULL ) continue;
		sprintf_s(buf, sizeof(buf), "%sが%d個、処理中に解放されていません", _pObj3D[i]._fileName, _pObj3D[i]._count);
		MESSAGE(buf, "リソース解放");
		cleanup(_pObj3D[i]._pSkinInfo);
	}
	SAFE_DELETES(_pObj3D);

	//	メッシュデータの解放
	for( i = 0; i < MESH_MAX; i++ ) {
		if( _pMeshData[i]._pMesh == NULL ) continue;
		sprintf_s(buf, sizeof(buf), "%sが%d個、処理中に解放されていません", _pMeshData[i]._fileName, _pMeshData[i]._count);
		MESSAGE(buf, "リソース解放");
		cleanup(_pMeshData[i]._pMesh);
	}
	SAFE_DELETES(_pMeshData);

	//	テクスチャーの解放
	for( i = 0; i < TEXTURE_MAX; i++ ) {
		if( _pTexData[i]._pTexture == NULL ) continue;
		sprintf_s(buf, sizeof(buf), "%sが%d個、処理中に解放されていません", _pTexData[i]._fileName, _pTexData[i]._count);
		MESSAGE(buf, "リソース解放");
		SAFE_RELEASE(_pTexData[i]._pTexture);
	}
	SAFE_DELETES(_pTexData);
}

//---------------------------------------------------------------------------
//	テクスチャーの読み込み
//!	@param	pFileName	[in] ファイル名
//!	@retval	読み込んだテクスチャー
//---------------------------------------------------------------------------
LPTEXTURE	kxResource::loadTexture(const char* pFileName)
{
	u32 i, no = -1;
	char fileName[PATH_MAX];

	//	ファイル名を全て小文字にしておく
	changeLower(fileName, pFileName);

	//	既に同じテクスチャーが使用されていないか確認する
	for( i = 0; i < TEXTURE_MAX; i++ ) {
		if( _pTexData[i]._pTexture == NULL ) continue;
		if( strcmp(_pTexData[i]._fileName, fileName) == 0 ) {
			no = i;
			break;
		}
	}

	//	読み込まれていない場合、未使用の場所を探し登録する
	if( no == -1 ) {
		for( no = 0; no < TEXTURE_MAX; no++ ) {
			if( _pTexData[no]._count == 0 ) break;
		}
		if( no == TEXTURE_MAX ) {
			ASSERT(false, "読み込めるテクスチャーの最大数を超えています");
			return NULL;
		}

		//	ファイル名のコピー
		strcpy(_pTexData[no]._fileName, fileName);
		HRESULT hr;
#if	0
		//	テクスチャーの読み込み
		hr = D3DXCreateTextureFromFileEx( KXSYSTEM->getDevice(),
										fileName,
										D3DX_DEFAULT,
										D3DX_DEFAULT,
										1,
										0,
										D3DFMT_A8R8G8B8,
										D3DPOOL_MANAGED,
										D3DX_FILTER_POINT,
										D3DX_FILTER_POINT,
										0x00000000,
										NULL,
										NULL,
										&_pTexData[no]._pTexture );
#else	// 1
				File xFile;
				void* pData = xFile.read(fileName);
				int iSize	= xFile.getFileSize();
				hr = D3DXCreateTextureFromFileInMemoryEx(	KXSYSTEM->getDevice(),
															pData,
															iSize,
															D3DX_DEFAULT,
															D3DX_DEFAULT,
															1,
															0,
															D3DFMT_A8R8G8B8,
															D3DPOOL_MANAGED,
															D3DX_FILTER_POINT,
															D3DX_FILTER_POINT,
															0x00000000,
															NULL,
															NULL,
															&_pTexData[no]._pTexture );
#endif	//	1
		//	ファイルの読み込み失敗
		if( FAILED(hr) ) {
			char str[PATH_MAX];
			wsprintf(str, "%sの読み込みに失敗しました", pFileName);
			MESSAGE(str, "テクスチャー");
			return NULL;
		}
	}

	//	テクスチャーの使用数を増やす
	_pTexData[no]._count++;
	return _pTexData[no]._pTexture;
}

//---------------------------------------------------------------------------
//	テクスチャーの解放
//!	@param	pTexture	[in] 解放するテクスチャー
//---------------------------------------------------------------------------
void	kxResource::cleanup(LPTEXTURE pTexture)
{
	if( pTexture == NULL ) return;
	u32 i;

	//	テクスチャーが管理されている場所を探す。
	for( i = 0; i < TEXTURE_MAX; i++ ) {
		if( _pTexData[i]._pTexture == NULL ) continue;
		if( _pTexData[i]._pTexture == pTexture ) {
			break;
		}
	}

	if( i != TEXTURE_MAX ) {
		TEXTUREDATA* pTexData = &_pTexData[i];

		//	テクスチャーの使用数が0になっていれば解放する
		pTexData->_count--;
		if( pTexData->_count > 0 ) return;
		SAFE_RELEASE(pTexData->_pTexture);
	} else {
		//	管理していないリソースの場合も解放する
		SAFE_RELEASE(pTexture);
	}
}

//---------------------------------------------------------------------------
//	メッシュの読み込み
//!	@param	pFileName	[in] ファイル名
//!	@retval	読み込んだメッシュデータ
//---------------------------------------------------------------------------
kxResource::MESHDATA*	kxResource::loadMesh(const char *pFileName)
{
	u32 i, no = -1;
	char fileName[PATH_MAX];

	//	ファイル名を全て小文字にしておく
	changeLower(fileName, pFileName);

	//	既に同じメッシュが使用されていないか確認する
	for( i = 0; i < MESH_MAX; i++ ) {
		if( _pMeshData[i]._pMesh == NULL ) continue;
		if( strcmp(_pMeshData[i]._fileName, fileName) == 0 ) {
			no = i;
			break;
		}
	}

	//	読み込まれていない場合、未使用の場所を探し登録する
	if( no == -1 ) {
		for( no = 0; no < MESH_MAX; no++ ) {
			if( _pMeshData[no]._count == 0 ) break;
		}
		if( no == MESH_MAX ) {
			ASSERT(false, "読み込めるメッシュの最大数を超えています");
			return NULL;
		}

		//	拡張子取得
		char type[PATH_MAX];
		getExtension(type, fileName);

		//	xファイル読み込み
		if( strcmp(type, "x") == 0 )		loadMeshFromX(fileName, no);
		//	mqoファイル読み込み
		else if( strcmp(type, "mqo") == 0 )	loadMeshFromMQO(fileName, no);
		//	kmoファイル読み込み
		else if( strcmp(type, "kmo") == 0 )	loadMeshFromKMO(fileName, no);
		//	失敗
		else {
			MESSAGE("対応していないファイルです", "メッシュ読み込みエラー");
			return NULL;
		}
	}

	//	メッシュの使用数を増やす
	_pMeshData[no]._count++;
	return &_pMeshData[no];
}

//---------------------------------------------------------------------------
//	メッシュの解放
//!	@param	pTexture	[in] 解放するメッシュ
//---------------------------------------------------------------------------
void	kxResource::cleanup(LPD3DXMESH pMesh)
{
	if( pMesh == NULL ) return;
	u32 i;

	//	メッシュが管理されている場所を探す。
	for( i = 0; i < MESH_MAX; i++ ) {
		if( _pMeshData[i]._pMesh == NULL ) continue;
		if( _pMeshData[i]._pMesh == pMesh ) {
			break;
		}
	}

	if( i != MESH_MAX ) {
		MESHDATA* pMeshData = &_pMeshData[i];

		//	メッシュの使用数が0になっていれば解放する
		pMeshData->_count--;
		if( pMeshData->_count > 0 ) return;
		SAFE_RELEASE(pMeshData->_pMesh);
		SAFE_DELETES(pMeshData->_pMaterial);
		for( u32 j=0; j < pMeshData->_materialCount; j++ ) {
			RESOURCE->cleanup(pMeshData->_ppTexture[j]);
		}
		SAFE_DELETES(pMeshData->_ppTexture);
	} else {
		//	管理していないリソースの場合も解放する
		MESSAGE("管理されていないMESHがあります", "警告");
		SAFE_RELEASE(pMesh);
	}
}

//---------------------------------------------------------------------------
//	3DOBJの読み込み
//!	@param	pFileName	[in] ファイル名
//!	@retval	読み込んだ3DOBJデータ
//---------------------------------------------------------------------------
kxResource::OBJ3D*	kxResource::load3DOBJ(const char *pFileName)
{
	u32 i, no = -1;
	char fileName[PATH_MAX];

	//	ファイル名を全て小文字にしておく
	changeLower(fileName, pFileName);

	//	既に同じメッシュが使用されていないか確認する
	for( i = 0; i < OBJ3D_MAX; i++ ) {
		if( _pObj3D[i]._pSkinInfo == NULL ) continue;
		if( strcmp(_pObj3D[i]._fileName, fileName) == 0 ) {
			no = i;
			break;
		}
	}

	//	読み込まれていない場合、未使用の場所を探し登録する
	if( no == -1 ) {
		for( no = 0; no < OBJ3D_MAX; no++ ) {
			if( _pObj3D[no]._count == 0 ) break;
		}
		if( no == OBJ3D_MAX ) {
			ASSERT(false, "読み込めるメッシュの最大数を超えています");
			return NULL;
		}

		//	拡張子取得
		char type[PATH_MAX];
		getExtension(type, fileName);

		//	xファイル読み込み
		if( strcmp(type, "mqo") == 0 )		return NULL;	//実装予定　load3DObjFromMQO(fileName, no);
		//	mqoファイル読み込み
		else if( strcmp(type, "kam") == 0 )	return NULL;	//実装予定　load3DObjFromKAM(fileName, no);
		//	kmoファイル読み込み
		else if( strcmp(type, "iem") == 0 )	load3DObjFromIEM(fileName, no);
		//	エラー
		else {
			MESSAGE("対応していないファイルです", "3DOBJ読み込みエラー");
			return NULL;
		}
	}

	//	3DOBJの使用数を増やす
	_pObj3D[no]._count++;
	return &_pObj3D[no];
}

//---------------------------------------------------------------------------
//	3DOBJの解放
//!	@param	pSkin	[in] 解放する3DOBJのスキン情報
//---------------------------------------------------------------------------
void	kxResource::cleanup(LPD3DXSKININFO pSkin)
{
	if( pSkin == NULL ) return;
	u32 i;

	//	テクスチャーが管理されている場所を探す。
	for( i = 0; i < OBJ3D_MAX; i++ ) {
		if( _pObj3D[i]._pSkinInfo == NULL ) continue;
		if( _pObj3D[i]._pSkinInfo == pSkin ) {
			break;
		}
	}

	if( i != OBJ3D_MAX ) {
		OBJ3D* pObj = &_pObj3D[i];

		//	テクスチャーの使用数が0になっていれば解放する
		pObj->_count--;
		if( pObj->_count > 0 ) return;

		SAFE_RELEASE(pObj->_pMesh);
		SAFE_DELETES(pObj->_pMaterial);
		for( u32 j=0; j < pObj->_materialCount; j++ ) {
			RESOURCE->cleanup(pObj->_ppTexture[j]);
		}
		SAFE_DELETES(pObj->_ppTexture);

		SAFE_DELETES(pObj->_pVertex);
		SAFE_RELEASE(pObj->_pSkinInfo);

		SAFE_DELETES(pObj->_pBoneParent);
		SAFE_DELETES(pObj->_pBoneMatrix);
		SAFE_DELETES(pObj->_pOffsetMatrix);
		SAFE_DELETES(pObj->_pMatrix);

		SAFE_DELETES(pObj->_pPose);
		SAFE_DELETES(pObj->_pPos);
		SAFE_DELETES(pObj->_pOrgPose);
		SAFE_DELETES(pObj->_pOrgPos);

		for( i = 0; i < pObj->_boneNum; i++ ) {
			SAFE_DELETES(pObj->_pAnime[i]._pRotFrame);
			SAFE_DELETES(pObj->_pAnime[i]._pRot);
			SAFE_DELETES(pObj->_pAnime[i]._pPosFrame);
			SAFE_DELETES(pObj->_pAnime[i]._pPos);
		}
		SAFE_DELETES(pObj->_pAnime);
		SAFE_DELETES(pObj->_pMotionOffset);
		SAFE_DELETE(pObj->_pDrawFrame);
		SAFE_DELETES(pObj->_pFrameFlag);
	} else {
		//	管理していないリソースの場合は解放して警告
		MESSAGE("管理されていない3DOBJがあります", "警告");
		SAFE_RELEASE(pSkin);
	}
}

//---------------------------------------------------------------------------
//	ディレクトリの取得
//!	@param	pOut	[out]	返すパス
//!	@param	pPath	[in]	ディレクトリを取得するパス
//---------------------------------------------------------------------------
void	kxResource::getPath(char* pOut, const char* pPath)
{
	u32	i;
	char path[PATH_MAX];
	strcpy(path, pPath);
	for( i = static_cast<u32>(strlen(path) - 1); i > 0; i-- ) {
		if( IsDBCSLeadByte(path[i - 1]) ) {
			i--;
			continue;
		}
		if( path[i] == '\\' || path[i] == '/' ) {
			path[i + 1] = '\0';
			break;
		}
	}
	if( i == 0 ) path[0] = '\0';
	strcpy(pOut, path);
}

//---------------------------------------------------------------------------
//	拡張子の取得
//!	@param	pOut	[out]	返す文字列
//!	@param	pPath	[in]	拡張子を取得するパス
//---------------------------------------------------------------------------
void	kxResource::getExtension(char* pOut, const char* pFileName)
{
	u32 i, j;
	char type[PATH_MAX];
	for( i = static_cast<u32>(strlen(pFileName) - 1); i > 0; i-- ) {
		if( pFileName[i] == '.' ) {
			i++;
			for( j = 0; j + i < strlen(pFileName); j++ ) {
				type[j] = changeLower(pFileName[j + i]);
			}
			type[j] = '\0';
			break;
		}
	}
	if( i == 0 ) {
		pOut[0] = '\0';
		return;
	}

	strcpy(pOut, type);
}

//---------------------------------------------------------------------------
//	大文字を小文字に変換(大文字以外を送った場合はそのまま返す)
//!	@param	str		[in] 変換する文字
//!	@retval	小文字に変換した文字
//---------------------------------------------------------------------------
char	kxResource::changeLower(char str)
{
	if( str >= 'A' && str <= 'Z' ) {
		return str - 'A' + 'a';
	}
	return str;
}

//---------------------------------------------------------------------------
//	大文字を小文字に変換(大文字以外を送った場合はそのまま返す)
//!	@param	pOut		[in] 変換後の文字
//!	@param	pStr		[in] 変換する文字
//---------------------------------------------------------------------------
void	kxResource::changeLower(char* pOut, const char* pStr)
{
	u32 i;
	char str[PATH_MAX];
	strcpy(str, pStr);

	//	ファイル名を全て小文字にしておく
	for( i = 0; i < strlen(str); i++ ) {
		if( IsDBCSLeadByte(str[i]) ) {
			pOut[i] = str[i++];
			pOut[i] = str[i];
		} else {
			pOut[i] = changeLower(str[i]);
		}
	}
	pOut[i] = '\0';
}

//---------------------------------------------------------------------------
//	メッシュの読み込み(X)
//!	@param	pFileName	[in] ファイル
//!	@param	no			[in] 使用するデータの番号
//---------------------------------------------------------------------------
void	kxResource::loadMeshFromX(const char* pFileName, u32 no)
{
	u32 i;

	kxResource::MESHDATA* pMeshData = &_pMeshData[no];
	//	ファイル名のコピー
	strcpy(pMeshData->_fileName, pFileName);

	//	パスの取得
	char path[PATH_MAX];
	getPath(path, pFileName);

	HRESULT	hr;
	LPD3DXBUFFER pMaterialBuffer;
	//	メッシュの読み込み
	hr = D3DXLoadMeshFromX( pFileName,
							D3DXMESH_MANAGED,
							KXSYSTEM->getDevice(),
							NULL,
							&pMaterialBuffer,
							NULL,
							&pMeshData->_materialCount,
							&pMeshData->_pMesh );
	if( FAILED(hr) ) return;

	//	法線がない場合、法線を追加する
	if( pMeshData->_pMesh->GetFVF() != (D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1) ) {
		LPD3DXMESH	pMeshTemp;
		pMeshData->_pMesh->CloneMeshFVF(pMeshData->_pMesh->GetOptions(),
										D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1,
										KXSYSTEM->getDevice(),
										&pMeshTemp);
		D3DXComputeNormals(pMeshTemp, NULL);
		SAFE_RELEASE(pMeshData->_pMesh);
		pMeshData->_pMesh = pMeshTemp;
	}

	//	材質の取得
	D3DXMATERIAL* pMaterials = (D3DXMATERIAL*)pMaterialBuffer->GetBufferPointer();

	//	面の数だけ材質とテクスチャーを作成する
	pMeshData->_pMaterial = new D3DMATERIAL9[pMeshData->_materialCount];
	ASSERT(pMeshData->_pMaterial, "メモリー不足です");
	pMeshData->_ppTexture  = new LPTEXTURE[pMeshData->_materialCount];
	ASSERT(pMeshData->_ppTexture, "メモリー不足です");

	ZeroMemory(pMeshData->_pMaterial, sizeof(D3DMATERIAL9) * pMeshData->_materialCount);
	ZeroMemory(pMeshData->_ppTexture, sizeof(LPTEXTURE) * pMeshData->_materialCount);

	//	材質とテクスチャーを取得する
	char temp[PATH_MAX];
	for( i = 0; i < pMeshData->_materialCount; i++ ) {
		D3DMATERIAL9* pMaterial = &pMeshData->_pMaterial[i];
		*pMaterial = pMaterials[i].MatD3D;
		pMaterial->Ambient = pMeshData->_pMaterial[i].Diffuse;
		pMaterial->Ambient.a = pMaterial->Diffuse.a = 1.0f;

		if( pMaterials[i].pTextureFilename ) {
			sprintf_s(temp, sizeof(temp), "%s%s", path, pMaterials[i].pTextureFilename);
			pMeshData->_ppTexture[i] = RESOURCE->loadTexture(temp);
		}
	}
	SAFE_RELEASE(pMaterialBuffer);
}

//---------------------------------------------------------------------------
//	メッシュの読み込み(MQO)
//!	@param	pFileName	[in] ファイル名
//!	@param	no			[in] 使用するデータの番号
//---------------------------------------------------------------------------
void	kxResource::loadMeshFromMQO(const char* pFileName, u32 no)
{
	kxResource::MESHDATA* pMeshData = &_pMeshData[no];

	//	MQOファイルの読み込み
	kxConverter	converter;
	*pMeshData = converter.loadMQO(pFileName);
}

//---------------------------------------------------------------------------
//	メッシュの読み込み(KMO)
//!	@param	pFileName	[in] ファイル名
//!	@param	no			[in] 使用するデータの番号
//---------------------------------------------------------------------------
void	kxResource::loadMeshFromKMO(const char* pFileName, u32 no)
{
	u16 i;

	kxResource::MESHDATA* pMeshData = &_pMeshData[no];
	//	メッシュデータの作成
	strcpy(pMeshData->_fileName, pFileName);

	//	ファイルの読み込み
	File file;
	if( file.readingFile(pFileName) == FALSE ) return;

	//	パスの取得
	char path[PATH_MAX];
	getPath(path, pFileName);

	//	ファイルチェック
	char temp[PATH_MAX];
	temp[4] = '\0';
	file.reading(temp, 4);
	if( strcmp(temp, "KMO ") ) return;
	file.reading(temp, 4);
	f32 version = static_cast<f32>(atof(temp));

	//	材質数取得
	file.reading(&pMeshData->_materialCount, sizeof(DWORD));

	//	材質とテクスチャーを必要数生成
	pMeshData->_pMaterial = new D3DMATERIAL9[pMeshData->_materialCount];
	ASSERT(pMeshData->_pMaterial, "メモリー不足です");
	pMeshData->_ppTexture = new LPTEXTURE[pMeshData->_materialCount];
	ASSERT(pMeshData->_ppTexture, "メモリー不足です");
	ZeroMemory(pMeshData->_pMaterial, sizeof(D3DMATERIAL9) * pMeshData->_materialCount);
	ZeroMemory(pMeshData->_ppTexture, sizeof(LPTEXTURE) * pMeshData->_materialCount);

	//	テクスチャーの取得
	for( i = 0; i < pMeshData->_materialCount; i++ ) {
		u8 size;
		file.reading(&size, sizeof(u8));
		if( size ) {
			char temp[PATH_MAX];
			char fileName[PATH_MAX];
			temp[size] = '\0';
			file.reading(temp, size);
			sprintf_s(fileName, "%s%s", path, temp);
			pMeshData->_ppTexture[i] = RESOURCE->loadTexture(fileName);
		}
	}

	//	材質の取得
	file.reading(pMeshData->_pMaterial, sizeof(D3DMATERIAL9) * pMeshData->_materialCount);

	//	メッシュの書き出し
	u8*		pVertexs;
	u8*		pIndex;
	DWORD*	pAttribute;
	DWORD	numVertexs;
	DWORD	numFaces;

	//	頂点数、面数の取得
	file.reading(&numVertexs,	sizeof(DWORD));
	file.reading(&numFaces,		sizeof(DWORD));

	//	メッシュの生成
	D3DXCreateMeshFVF(numFaces, numVertexs, D3DXMESH_MANAGED, kxGraphics::D3DFVF_VERTEX3D, KXSYSTEM->getDevice(), &pMeshData->_pMesh);
	LPD3DXMESH pMesh = pMeshData->_pMesh;

	//	頂点情報セット
	pMesh->LockVertexBuffer(D3DLOCK_DISCARD, reinterpret_cast<void**>(&pVertexs));
	file.reading(pVertexs, sizeof(kxGraphics::VERTEX3D) * numVertexs);
	pMesh->UnlockVertexBuffer();

	//	面情報セット
	pMesh->LockIndexBuffer(D3DLOCK_DISCARD, reinterpret_cast<void**>(&pIndex));
	file.reading(pIndex, sizeof(u16) * numFaces * 3);
	pMesh->UnlockIndexBuffer();

	//	属性セット
	pMesh->LockAttributeBuffer(D3DLOCK_DISCARD, &pAttribute);
	file.reading(pAttribute, sizeof(DWORD) * numFaces);
	pMesh->UnlockAttributeBuffer();

	//	ファイルを閉じる
	file.closeFile();
}

//---------------------------------------------------------------------------
//	3DOBJの読み込み(MQOファイル)
//!	@param	pFileName	[in] ファイル名
//!	@param	no			[in] 使用するデータの番号
//---------------------------------------------------------------------------
void	kxResource::load3DObjFromMQO(const char* pFileName, u32 no)
{
	//	追加予定
}

//---------------------------------------------------------------------------
//	3DOBJの読み込み(KAMファイル)
//!	@param	pFileName	[in] ファイル名
//!	@param	no			[in] 使用するデータの番号
//---------------------------------------------------------------------------
void	kxResource::load3DObjFromKAM(const char* pFileName, u32 no)
{
	//	追加予定
}

//---------------------------------------------------------------------------
//	3DOBJの読み込み(IEMファイル)
//!	@param	pFileName	[in] ファイル名
//!	@param	no			[in] 使用するデータの番号
//---------------------------------------------------------------------------
void	kxResource::load3DObjFromIEM(const char* pFileName, u32 no)
{
	File file;
	IEMFILE	iem;
	u32 fileID, version;
	u32 i;
	OBJ3D*	pObj = &_pObj3D[no];

	//	データの読み込み
	if( file.readingFile(pFileName) == FALSE ) return;
	strcpy(pObj->_fileName, pFileName);

	//	パスの取得
	char path[PATH_MAX];
	getPath(path, pFileName);

	//	ID確認
	file.reading(&fileID, 4);
	if( fileID == '1MEI' )		version = 1;
	else if( fileID == '2MEI' )	version = 2;
	else version = -1;

	//	頂点取得
	file.reading(&iem.NumVertex, sizeof(u16));
	iem.lpVertex = new kxGraphics::VERTEX3D[iem.NumVertex];
	file.reading(iem.lpVertex, sizeof(kxGraphics::VERTEX3D) * iem.NumVertex);
	//	面取得
	file.reading(&iem.NumFace, sizeof(u16));
	iem.lpFace = new u16[iem.NumFace * 3];
	iem.lpAtr  = new u32[iem.NumFace];
	file.reading(iem.lpFace, sizeof(u16) * iem.NumFace * 3);
	file.reading(iem.lpAtr,  sizeof(u32) * iem.NumFace);
	//	材質取得
	file.reading(&iem.NumMaterial, sizeof(u16));
	file.reading(iem.Material, sizeof(D3DMATERIAL9) * iem.NumMaterial);
	file.reading(iem.Texture,  sizeof(char) * iem.NumMaterial * 64);

	//	ボーン取得
	file.reading(&iem.NumBone, sizeof(u16));
	iem.lpBone = new IEMBONE[iem.NumBone];
	for( i = 0; i < iem.NumBone; i++ ) {
		file.reading(&iem.lpBone[i], sizeof(IEMBONE));

		iem.lpBone[i].Index = new u32[iem.lpBone[i].IndexNum];
		file.reading(iem.lpBone[i].Index, sizeof(DWORD) * iem.lpBone[i].IndexNum);

		iem.lpBone[i].Influence = new f32[iem.lpBone[i].IndexNum];
		file.reading(iem.lpBone[i].Influence, sizeof(f32) * iem.lpBone[i].IndexNum);
	}

	//	モーション取得
	file.reading(&iem.NumMotion, sizeof(u16));
	file.reading(&iem.MaxFrame,  sizeof(u16));
	file.reading(&iem.M_Offset,  sizeof(u16) * 256);
	file.reading(&iem.FrameFlag, sizeof(u16) * iem.MaxFrame);
	iem.lpMotion = new IEMMOTION[iem.NumBone];

	for( i = 0; i < iem.NumBone; i++ ) {
		file.reading(&iem.lpMotion[i], sizeof(IEMMOTION));

		iem.lpMotion[i].Rotate		= new QUATERNION[iem.lpMotion[i].NumRotate];
		iem.lpMotion[i].RotateFrame	= new u16[iem.lpMotion[i].NumRotate];
		file.reading(iem.lpMotion[i].Rotate,	  sizeof(QUATERNION) * iem.lpMotion[i].NumRotate);
		file.reading(iem.lpMotion[i].RotateFrame, sizeof(u16) * iem.lpMotion[i].NumRotate);

		iem.lpMotion[i].Position		= new VECTOR3[iem.lpMotion[i].NumPosition];
		iem.lpMotion[i].PositionFrame	= new u16[iem.lpMotion[i].NumPosition];
		file.reading(iem.lpMotion[i].Position,		sizeof(VECTOR3) * iem.lpMotion[i].NumPosition);
		file.reading(iem.lpMotion[i].PositionFrame, sizeof(u16) * iem.lpMotion[i].NumPosition);
	}

	//	ファイルを閉じる
	file.closeFile();

	//	スキン情報の生成
	D3DXCreateSkinInfoFVF(iem.NumVertex, kxGraphics::D3DFVF_VERTEX3D, iem.NumBone, &pObj->_pSkinInfo);
	//	ボーン設定
	for( i = 0; i < iem.NumBone; i++ ) {
		pObj->_pSkinInfo->SetBoneInfluence(i, iem.lpBone[i].IndexNum, reinterpret_cast<DWORD*>(iem.lpBone[i].Index), iem.lpBone[i].Influence);
	}

	u8			*pVertex, *pIndex;
	DWORD		*pData;

	//	メッシュの生成
	D3DXCreateMeshFVF(iem.NumFace, iem.NumVertex, D3DXMESH_MANAGED, kxGraphics::D3DFVF_VERTEX3D, KXSYSTEM->getDevice(), &pObj->_pMesh);

	//	頂点設定
	pObj->_pMesh->LockVertexBuffer(0, reinterpret_cast<void**>(&pVertex));
	memcpy(pVertex, iem.lpVertex, sizeof(kxGraphics::VERTEX3D) * iem.NumVertex);
	pObj->_pMesh->UnlockVertexBuffer();

	//	面設定
	pObj->_pMesh->LockIndexBuffer(0, reinterpret_cast<void**>(&pIndex));
	memcpy(pIndex, iem.lpFace, sizeof(u16) * iem.NumFace * 3);
	pObj->_pMesh->UnlockIndexBuffer();

	//	属性設定
	pObj->_pMesh->LockAttributeBuffer(0, &pData);
	memcpy(pData, iem.lpAtr, sizeof(DWORD) * iem.NumFace);
	pObj->_pMesh->UnlockAttributeBuffer();

	//	頂点情報コピー
	pObj->_vertexNum = iem.NumVertex;
	pObj->_pVertex = new kxGraphics::VERTEX3D[pObj->_vertexNum];
	memcpy(pObj->_pVertex, iem.lpVertex, sizeof(kxGraphics::VERTEX3D) * pObj->_vertexNum);

	//	材質設定
	pObj->_materialCount = iem.NumMaterial;
	pObj->_pMaterial = new D3DMATERIAL9[pObj->_materialCount];
	memcpy(pObj->_pMaterial, iem.Material, sizeof(D3DMATERIAL9) * pObj->_materialCount);

	//	テクスチャー設定
	pObj->_ppTexture = new LPTEXTURE[pObj->_materialCount];
	ZeroMemory(pObj->_ppTexture, sizeof(LPTEXTURE) * pObj->_materialCount);

	for( i = 0; i < pObj->_materialCount; i++ ) {
		if( iem.Texture[i][0] == '\0' ) continue;
		char temp[256];
		sprintf_s(temp, sizeof(temp), "%s%s", path, iem.Texture[i]);
		pObj->_ppTexture[i] = RESOURCE->loadTexture(temp);
	}

	//	ボーン、アニメーション設定
	pObj->_boneNum		 = iem.NumBone;
	pObj->_pBoneParent	 = new u16[pObj->_boneNum];
	pObj->_pBoneMatrix	 = new MATRIX[pObj->_boneNum];
	pObj->_pOffsetMatrix = new MATRIX[pObj->_boneNum];
	pObj->_pMatrix		 = new MATRIX[pObj->_boneNum];

	pObj->_pOrgPose	= new QUATERNION[pObj->_boneNum];
	pObj->_pOrgPos	= new VECTOR3[pObj->_boneNum];
	pObj->_pPose	= new QUATERNION[pObj->_boneNum];
	pObj->_pPos		= new VECTOR3[pObj->_boneNum];

	pObj->_motionNum = iem.NumMotion;
	pObj->_frameNum	 = iem.MaxFrame;
	pObj->_pMotionOffset = new u16[pObj->_motionNum];
	memcpy(pObj->_pMotionOffset, iem.M_Offset, sizeof(u16) * pObj->_motionNum);
	pObj->_pFrameFlag = new u16[pObj->_frameNum];
	memcpy(pObj->_pFrameFlag, iem.FrameFlag, sizeof(u16) * pObj->_frameNum);

	pObj->_pAnime = new kx3DObject::ANIME[pObj->_boneNum];

	for( i = 0; i < pObj->_boneNum; i++ ) {
		//	ボーン設定
		IEMBONE* pBone = &iem.lpBone[i];
		pObj->_pBoneParent[i]	= pBone->parent;
		pObj->_pOffsetMatrix[i]	= pBone->BoneMatrix;
		pObj->_pOrgPos[i]		= pBone->orgPos;
		pObj->_pOrgPose[i]		= pBone->orgPose;

		kx3DObject::ANIME* pAnime = &pObj->_pAnime[i];
		IEMMOTION* pMotion = &iem.lpMotion[i];

		//	クォータニオンコピー
		pAnime->_rotNum		= pMotion->NumRotate;
		pAnime->_pRotFrame	= new u16[pAnime->_rotNum];
		pAnime->_pRot		= new QUATERNION[pAnime->_rotNum];
		memcpy(pAnime->_pRotFrame, pMotion->RotateFrame, sizeof(u16) * pAnime->_rotNum);
		memcpy(pAnime->_pRot, pMotion->Rotate, sizeof(QUATERNION) * pAnime->_rotNum);

		//	ポジションコピー
		pAnime->_posNum		= pMotion->NumPosition;
		pAnime->_pPosFrame	= new u16[pAnime->_posNum];
		pAnime->_pPos		= new VECTOR3[pAnime->_posNum];
		memcpy(pAnime->_pPosFrame, pMotion->PositionFrame, sizeof(u16) * pAnime->_posNum); 
		memcpy(pAnime->_pPos, pMotion->Position, sizeof(VECTOR3) * pAnime->_posNum); 
	}
	pObj->_pDrawFrame = new f32;
	*pObj->_pDrawFrame = 0xFFFF;

	//	読み込んだデータを解放する
	for( i = 0; i < iem.NumBone; i++ ) {
		SAFE_DELETES(iem.lpBone[i].Index);
		SAFE_DELETES(iem.lpBone[i].Influence);
		SAFE_DELETES(iem.lpMotion[i].Rotate);
		SAFE_DELETES(iem.lpMotion[i].RotateFrame);
		SAFE_DELETES(iem.lpMotion[i].Position);
		SAFE_DELETES(iem.lpMotion[i].PositionFrame);
	}
	SAFE_DELETES(iem.lpVertex);
	SAFE_DELETES(iem.lpFace);
	SAFE_DELETES(iem.lpAtr);
	SAFE_DELETES(iem.lpBone);
	SAFE_DELETES(iem.lpMotion);
}

//============================================================================
//	END OF FILE
//============================================================================