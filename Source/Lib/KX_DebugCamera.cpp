//----------------------------------------------------------------------------
//!
//!	@file	KX_DebugCamera.cpp
//!	@brief	kxDebugCameraクラス
//!
//!	@author S.Kawamoto
//----------------------------------------------------------------------------
#include "KX.h"


#if	USE_DEBUGCAMERA

//	インスタンスの実体生成
kxDebugCamera	kxDebugCamera::_instance;

//----------------------------------------------------------------------------
//	コンストラクタ
//----------------------------------------------------------------------------
kxDebugCamera::kxDebugCamera(void)
{
}

//----------------------------------------------------------------------------
//	デストラクタ
//----------------------------------------------------------------------------
kxDebugCamera::~kxDebugCamera(void)
{
}

//----------------------------------------------------------------------------
//	初期化
//----------------------------------------------------------------------------
void	kxDebugCamera::init(void)
{
	_pos	= VECTOR3(0, 1, -5);
	_target	= VECTOR3(0, 0, 5);
	_up		= VECTOR3(0, 1, 0);
	_vec	= VECTOR3(0, 0, 0);

	_fovY	= D3DX_PI / 4;
	_aspect	= static_cast<f32>(KXSYSTEM->getWidth()) / static_cast<f32>(KXSYSTEM->getHeight());
	_near	= 1.0f;
	_far	= 300.0f;
}

//----------------------------------------------------------------------------
//	解放
//----------------------------------------------------------------------------
void	kxDebugCamera::cleanup(void)
{
}

//----------------------------------------------------------------------------
//	更新
//----------------------------------------------------------------------------
void	kxDebugCamera::update(void)
{
	// 右スティックの状態取得
	VECTOR2 thumb = INPUT->getRightAxis(0);

	if( GetAsyncKeyState(VK_CONTROL) & 0x8000 ) {
		if( INPUT->isKeyPress(kxInput::KEY_UP) )	thumb.y = -1.0f;
		if( INPUT->isKeyPress(kxInput::KEY_DOWN) )	thumb.y =  1.0f;
		if( INPUT->isKeyPress(kxInput::KEY_LEFT) )	thumb.x = -1.0f;
		if( INPUT->isKeyPress(kxInput::KEY_RIGHT) )	thumb.x =  1.0f;
	}

	// 敵とプレイヤーのポジションの差を取る
	VECTOR3 dist = _target - _pos;

	// 左スティックが押されていれば縮小拡大、押されていなければ回転
	if( INPUT->isKeyPress(kxInput::KEY_SELECT) ) {
		// カメラとターゲットの向きから距離を変えていく
		VECTOR3 vec = dist;
		MATH->normalize(&vec, vec);
		_pos += vec * 0.1f * thumb.x;
	}else {
		// 右スティックの値によって回転行列を作り回転させる
		MATRIX rot;
		D3DXMatrixRotationYawPitchRoll(&rot, D3DX_PI/180 * thumb.x, D3DX_PI/180 * thumb.y, 0);
		D3DXVec3TransformNormal(&dist, &dist, &rot);

		_pos = _target - dist;
	}

	//	左ステックの値で並行移動させる
	thumb = INPUT->getLeftAxis(0);
	if( !(GetAsyncKeyState(VK_CONTROL) & 0x8000) ) {
		if( INPUT->isKeyPress(kxInput::KEY_UP) )	thumb.y =  1.0f;
		if( INPUT->isKeyPress(kxInput::KEY_DOWN) )	thumb.y = -1.0f;
		if( INPUT->isKeyPress(kxInput::KEY_LEFT) )	thumb.x = -1.0f;
		if( INPUT->isKeyPress(kxInput::KEY_RIGHT) )	thumb.x =  1.0f;
	}

	MATH->normalize(&dist, dist);
	dist *= 0.2f;
	dist.y = 0;
	_vec.x = dist.z;
	_vec.z = -dist.x;

	_pos	+= _vec * thumb.x;
	_target += _vec * thumb.x;
	_pos	+= dist * thumb.y;
	_target += dist * thumb.y;

	LPDIRECT3DDEVICE9 pDevice = KXSYSTEM->getDevice();

	//	ビューマトリックスの作成、設定
	D3DXMatrixLookAtLH(	&_view,
						&_pos,
						&_target,
						&_up);
	pDevice->SetTransform(D3DTS_VIEW, &_view);

	//	プロジェクションマトリックスの作成,設定
	D3DXMatrixPerspectiveFovLH(	&_proj,
								_fovY,
								_aspect,
								_near,
								_far);
	pDevice->SetTransform(D3DTS_PROJECTION, &_proj);
}

//----------------------------------------------------------------------------
//	プロジェクションのセット
//!	@param	fovY	[in] 視野角
//!	@param	aspect	[in] アスペクト比
//!	@param	zn		[in] 前方クリップ面
//!	@param	zf		[in] 後方クリップ面
//----------------------------------------------------------------------------
void	kxDebugCamera::setProjection(f32 fovY, f32 aspect, f32 zn, f32 zf)
{
	_fovY	= fovY;
	_aspect	= aspect;
	_near	= zn;
	_far	= zf;
}

#endif	//~#if USE_DEBUGCAMERA

//============================================================================
//	END OF FILE
//============================================================================