//---------------------------------------------------------------------------
//!
//!	@file	KX_Camera.h
//!	@brief	kxCameraクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#ifndef	__KX_CAMERA_H__
#define __KX_CAMERA_H__

#pragma once


//===========================================================================
//!	kxCameraクラス
//===========================================================================
class kxCamera
{
public:
	//---------------------------------------------------------------------------
	//!	@name 初期化
	//---------------------------------------------------------------------------
	//!@{

	//!	初期化
	void	init(void);
	//!	解放
	void	cleanup(void);

	//!@}

	//!	更新
	void	update(void);

	//---------------------------------------------------------------------------
	//!	@name 取得参照
	//---------------------------------------------------------------------------
	//!@{

	//!	バイブレーションのセット
	void	setVibration(u8 x, u8 y, u8 z, u16 time);
	//!	バイブレーションのセット
	void	setVibration(u8 xyz, u16 time);

	//!	フォグのセット
	void	setFog(f32 zn, f32 zf, ARGB color);
	//!	プロジェクションのセット
	void	setProjection(f32 fovY, f32 zn, f32 zf);

	//!	位置のセット
	//!	@param	pos		[in] 位置
	inline void		setPos(const VECTOR3& pos) { _pos = pos; }
	//!	位置の取得
	//!	@retval	位置
	inline VECTOR3	getPos(void) { return _pos; }
	//!	ターゲットのセット
	//!	@param	target	[in] ターゲット
	inline void		setTarget(const VECTOR3& target) { _target = target; }
	//!	ターゲットの取得
	//!	@retval	ターゲット
	inline VECTOR3	getTarget(void) { return _target; }
	//!	上方向のセット
	//!	@param	up		[in] 上方向
	inline void		setUp(const VECTOR3& up) { _up = up; }
	//!	上方向の取得
	//!	@retval	上方向
	inline VECTOR3	getUp(void) { return _up; }

	//! ワールド、ビュー系変換マトリッ
	//!	@retval	ワールド、ビュー系変換マトリックス
	inline MATRIX	getView(void) {
		#if	USE_DEBUGCAMERA
			if( _debugCamera ) return DEBUGCAMERA->getView();
		#endif	//~#if USE_DEBUGCAMERA
		return _view;
	}
	//! ビュー、射影系変換マトリックス
	//!	@retval	ビュー、射影系変換マトリックス
	inline MATRIX	getProjection(void) {
		#if	USE_DEBUGCAMERA
			if( _debugCamera ) return DEBUGCAMERA->getProjection();
		#endif	//~#if USE_DEBUGCAMERA
		return _proj;
	}

	//!	インスタンスの取得
	//!	@retval	インスタンス
	inline static kxCamera*	getInstance(void) { return &_instance; };

	//!@}

private:
	//!	コンストラクタ
	kxCamera(void);
	//!	デストラクタ
	~kxCamera(void);

	static kxCamera _instance;	//!< 唯一のインスタンス

	#if	USE_DEBUGCAMERA
		b32	_debugCamera;
	#endif	//~#if USE_DEBUGCAMERA

	VECTOR3	_pos;		//!< カメラのポジション
	VECTOR3 _target;	//!< カメラのターゲット
	VECTOR3 _up;		//!< カメラの上を指すベクトル

	f32		_fovY;		//!< 視野角
	f32		_aspect;	//!< アスペクト比
	f32		_near;		//!< 前方クリップ面
	f32		_far;		//!< 後方クリップ面

	MATRIX	_view;		//!< ワールド、ビュー系変換マトリックス
	MATRIX	_proj;		//!< ビュー、射影系変換マトリックス

	s16		_vibTimer;	//!< バイブレーション用タイマー
	u8		_vibX;		//!< X軸方向への力
	u8		_vibY;		//!< Y軸方向への力
	u8		_vibZ;		//!< Z軸方向への力
	f32		_vibPower;	//!< バイブレーションの強さ
};

//!	kxCameraのインスタンス取得マクロ
#define	CAMERA	(kxCamera::getInstance())

#endif	//~#if __KX_CAMERA_H__

//============================================================================
//	END OF FILE
//============================================================================