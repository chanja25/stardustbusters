//---------------------------------------------------------------------------
//!
//!	@file	KX_Font.h
//!	@brief	kxFontクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#ifndef	__KX_FONT_H__
#define	__KX_FONT_H__

#pragma once


#if	DEBUG
	#define	USE_FONT 1
#else	//~#if DEBUG
	#define	USE_FONT 0
#endif	//~#if DEBUG

//===========================================================================
//!	kxSystemクラス
//===========================================================================
class kxFont
{
public:
	//!	文字効果用
	enum EFFECT {
		EFFECT_NONE = 0,
		EFFECT_EDGE,
		EFFECT_SHADOW,
	};

	//---------------------------------------------------------------------------
	//!	@name 初期化
	//---------------------------------------------------------------------------
	//!@{

	//!	初期化
	static b32	init(void);
	//!	解放
	static void	cleanup(void);

	//!	更新
	void	update(void);

	//!	フォントの生成
	b32		createFont(u32 height, u32 weight, b32 italic, char* pFont, u32 effect = EFFECT_NONE, u32 power = 2);

	//!@}

	//!	テキストのテクスチャ作成
	void	createTexture(LPTEXTURE pTexture, const char* pStr);

	//!	テクスチャで文字列描画
	void	draw(f32 x, f32 y, f32 size, ARGB color, const char* pStr, u8 numW = STR_MAX, f32 dw = 0, f32 dh = 0);

	//!	文字列の描画
	void	drawText(s32 x, s32 y, ARGB color, const char* pStr, ...);
	//!	デバッグ用文字の描画
	void	drawDebugText(s32 x, s32 y, ARGB color, const char* pStr, ...);

	//!	デバイスの解放
	void	lost(void);
	//!	デバイスのリセット
	void	reset(void);

	//-------------------------------------------------------------
	//!	@name 取得参照
	//-------------------------------------------------------------
	//!@{

	//!	インスタンスの取得
	//!	@retval	インスタンス
	inline static kxFont*	getInstance(void) { return _pInstance; }

	//!	文字の高さの取得
	//!	@retval	高さ
	u32		getHeight(void) { return _height; };

	//!@}

private:
	//!	コンストラクタ
	kxFont(void);
	//!	デストラクタ
	~kxFont(void);

	static	kxFont*	_pInstance;		//!< 唯一のインスタンス

	LPD3DXFONT	_pDebugFont;	//!< デバッグ用フォントデバイス

	LPD3DXFONT	_pFont;				//!< フォントデバイス

	u32			_height;			//!< 高さ
	u32			_effect;			//!< 文字効果
	u32			_power;				//!< 効力

	//!	一度に表示できる文字の種類
	static const u8 STR_MAX = 64;
	//!	文字の制御用データ
	struct STRDATA {
		char		_str[3];		//!< 文字
		u32			_count;			//!< 使用数

		LPKX2DOBJ	_pObj;			//!< テクスチャー用
	};

	STRDATA*	_pStrData;			//!< 文字表示用
	u8			_numStr;			//!< 現在の文字数
};

#define	FONT		(kxFont::getInstance())

#endif	//~#if __KX_FONT_H__

//============================================================================
//	END OF FILE
//============================================================================