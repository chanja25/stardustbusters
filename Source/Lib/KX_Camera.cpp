//----------------------------------------------------------------------------
//!
//!	@file	KX_Camera.cpp
//!	@brief	kxCameraクラス
//!
//!	@author S.Kawamoto
//----------------------------------------------------------------------------
#include "KX.h"

//	インスタンスの実体生成
kxCamera	kxCamera::_instance;

//----------------------------------------------------------------------------
//	コンストラクタ
//----------------------------------------------------------------------------
kxCamera::kxCamera(void)
{
}

//----------------------------------------------------------------------------
//	デストラクタ
//----------------------------------------------------------------------------
kxCamera::~kxCamera(void)
{
}

//----------------------------------------------------------------------------
//	初期化
//----------------------------------------------------------------------------
void	kxCamera::init(void)
{
	_pos	= VECTOR3(0, 1, -5);
	_target	= VECTOR3(0, 0, 5);
	_up		= VECTOR3(0, 1, 0);

	_fovY	= RAD * 45;
	_aspect	= static_cast<f32>(KXSYSTEM->getWidth()) / static_cast<f32>(KXSYSTEM->getHeight());
	_near	= 1.0f;
	_far	= 1500.0f;

	_vibTimer = 0;
	_vibPower = 0.01f;

	#if	USE_DEBUGCAMERA
		_debugCamera = FALSE;
	#endif	//~#if USE_DEBUGCAMERA

	update();
}

//----------------------------------------------------------------------------
//	解放
//----------------------------------------------------------------------------
void	kxCamera::cleanup(void)
{
}

//----------------------------------------------------------------------------
//	更新
//----------------------------------------------------------------------------
void	kxCamera::update(void)
{
	LPDIRECT3DDEVICE9 pDevice = KXSYSTEM->getDevice();

	VECTOR3	pos	   = _pos;
	VECTOR3 target = _target;

	if( _vibTimer > 0 ) {
		VECTOR3 vib;
		vib.x = (rand()%_vibX * 2 - _vibX + 1) * _vibPower;
		vib.y = (rand()%_vibY * 2 - _vibY + 1) * _vibPower;
		vib.z = (rand()%_vibZ * 2 - _vibZ + 1) * _vibPower;
		pos += vib;
		target += vib;
		_vibTimer--;
	}

	//	ビューマトリックスの作成、設定
	D3DXMatrixLookAtLH(	&_view,
						&pos,
						&target,
						&_up);
	pDevice->SetTransform(D3DTS_VIEW, &_view);

	//	プロジェクションマトリックスの作成,設定
	D3DXMatrixPerspectiveFovLH(	&_proj,
								_fovY,
								_aspect,
								_near,
								_far);
	pDevice->SetTransform(D3DTS_PROJECTION, &_proj);

	//	デバッグカメラの起動
	#if	USE_DEBUGCAMERA
		if( GetAsyncKeyState(VK_F1) & 0x8000 ) _debugCamera = TRUE;
		if( GetAsyncKeyState(VK_F2) & 0x8000 ) _debugCamera = FALSE;
		if( _debugCamera ) {
			if( GetAsyncKeyState(VK_F3) & 0x8000 ) {
				DEBUG_CPOS(_pos);
				DEBUG_CTARGET(_target);
			}
			DEBUGCAMERA->update();
		}
	#endif	//~#if USE_DEBUGCAMERA
}

//----------------------------------------------------------------------------
//	バイブレーションのセット
//!	@param	x	 [in] X軸方向への力
//!	@param	y	 [in] Y軸方向への力
//!	@param	z	 [in] Z軸方向への力
//!	@param	time [in] 継続時間
//----------------------------------------------------------------------------
void	kxCamera::setVibration(u8 x, u8 y, u8 z, u16 time)
{
	_vibX		= x + 1;
	_vibY		= y + 1;
	_vibZ		= z + 1;

	_vibTimer	= time;
}

//----------------------------------------------------------------------------
//	バイブレーションのセット
//!	@param	x	 [in] X軸方向への力
//!	@param	y	 [in] Y軸方向への力
//!	@param	z	 [in] Z軸方向への力
//!	@param	time [in] 継続時間
//----------------------------------------------------------------------------
void	kxCamera::setVibration(u8 xyz, u16 time)
{
	_vibX		= xyz + 1;
	_vibY		= xyz + 1;
	_vibZ		= xyz + 1;

	_vibTimer	= time;
}

//----------------------------------------------------------------------------
//	フォグのセット
//!	@param	fovY	[in] 視野角
//!	@param	aspect	[in] アスペクト比
//!	@param	zn		[in] 前方クリップ面
//!	@param	zf		[in] 後方クリップ面
//----------------------------------------------------------------------------
void	kxCamera::setFog(f32 zn, f32 zf, ARGB color)
{
	LPDIRECT3DDEVICE9	pDevice = KXSYSTEM->getDevice();

	pDevice->SetRenderState(D3DRS_FOGENABLE, TRUE);

	//	フォグカラー設定
	pDevice->SetRenderState(D3DRS_FOGCOLOR, color);

	//	フォグモード設定
	pDevice->SetRenderState(D3DRS_FOGVERTEXMODE, D3DFOG_NONE);
	pDevice->SetRenderState(D3DRS_FOGTABLEMODE,  D3DFOG_LINEAR);

	//	パラメータ設定
	pDevice->SetRenderState(D3DRS_FOGSTART,	*reinterpret_cast<DWORD*>(&zn));
	pDevice->SetRenderState(D3DRS_FOGEND,	*reinterpret_cast<DWORD*>(&zf));
}

//----------------------------------------------------------------------------
//	プロジェクションのセット
//!	@param	fovY	[in] 視野角
//!	@param	aspect	[in] アスペクト比
//!	@param	zn		[in] 前方クリップ面
//!	@param	zf		[in] 後方クリップ面
//----------------------------------------------------------------------------
void	kxCamera::setProjection(f32 fovY, f32 zn, f32 zf)
{
	_fovY	= fovY;
	_near	= zn;
	_far	= zf;
}

//============================================================================
//	END OF FILE
//============================================================================