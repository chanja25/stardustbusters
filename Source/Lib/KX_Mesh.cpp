//---------------------------------------------------------------------------
//!
//!	@file	KX_Mesh.cpp
//!	@brief	インクルード
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "KX.h"


//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
kxMesh::kxMesh(void)
: _pMesh(NULL)
, _pMaterial(NULL)
, _ppTexture(NULL)
{
}

//---------------------------------------------------------------------------
//	コンストラクタ
//!	@param	pFileName	[in] 読み込むファイル名
//---------------------------------------------------------------------------
kxMesh::kxMesh(const char* pFileName)
: _pMesh(NULL)
, _pMaterial(NULL)
, _ppTexture(NULL)
{
	load(pFileName);
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
kxMesh::~kxMesh(void)
{
	cleanup();
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	kxMesh::cleanup(void)
{
	RESOURCE->cleanup(_pMesh);
	_pMesh = NULL;
}

//---------------------------------------------------------------------------
//	読み込み
//!	@param	pFileName	[in] 読み込むファイル名
//!	@retval	成功
//!	@retval	失敗
//---------------------------------------------------------------------------
b32		kxMesh::load(const char* pFileName)
{
	//	メッシュを読み込む
	kxResource::MESHDATA* pData;

	pData = RESOURCE->loadMesh(pFileName);
	if( pData == NULL ) return FALSE;

	//	読み込んだデータをセット
	_pMesh			= pData->_pMesh;
	_materialCount	= pData->_materialCount;
	_pMaterial		= pData->_pMaterial;
	_ppTexture		= pData->_ppTexture;

	MATH->matrixIdentity(&_transMatrix);
	return TRUE;
}

//---------------------------------------------------------------------------
//	描画
//---------------------------------------------------------------------------
void	kxMesh::render(void)
{
	render("copy");
}

//---------------------------------------------------------------------------
//	描画
//!	@param	pTechnique	[in] 使用するテクニック
//!	@param	pass		[in] 使用するパス
//---------------------------------------------------------------------------
void	kxMesh::render(char* pTechnique)
{
	if( _pMesh == NULL ) return;

	LPSHADER pShader = GRAPHICS->getShader3D();

	//	マトリックスをセット
	pShader->setMatrix(_transMatrix);
	if( pShader->getEffect() == NULL ) KXSYSTEM->getDevice()->SetTransform(D3DTS_WORLD, &_transMatrix);

	//	シェーダーのタイプ設定
	pShader->setTechnique(pTechnique);

	//	シェーダー開始
	pShader->beginShader();

	for( u32 p=0; p < pShader->getPass(); p++ ) {
		pShader->beginPass(p);

		//	メッシュの描画
		for( u32 i=0; i < _materialCount; i++ ) {
			if( pShader->getEffect() == NULL ) GRAPHICS->setRenderState(0, _ppTexture[i], &_pMaterial[i]);
			pShader->setTexture(_ppTexture[i]);

			pShader->commitChanges();
			_pMesh->DrawSubset(i);
		}

		pShader->endPass();
	}

	//	シェーダーの終了
	pShader->endShader();
}

//---------------------------------------------------------------------------
//	UVアニメーション
//!	@param	tu	[in]	tuの変化量
//!	@param	tv	[in]	tvの変化量
//---------------------------------------------------------------------------
void	kxMesh::uvAnimation(f32 tu, f32 tv)
{
	kxGraphics::VERTEX3D* pVertex;
	//	メッシュの取得
	DWORD numVertex = _pMesh->GetNumVertices();

	//	頂点を取り出し
	_pMesh->LockVertexBuffer(0, reinterpret_cast<void**>(&pVertex));
	for( u32 i=0; i < numVertex; i++ ) {
		pVertex[i].tu += tu;
		pVertex[i].tv += tv;
	}
	_pMesh->UnlockVertexBuffer();
}

//---------------------------------------------------------------------------
//	レイピック
//!	@param	pOut		[out]	 交差した点
//!	@param	spos		[in]	 位置
//!	@param	vec			[in]	 向き
//!	@param	pDist		[in/out] 判定する距離の範囲/交差した点までの距離
//!	@param	pTransInv	[in]	 転送行列の逆行列	
//!	@retval	交差した面の番号(-1なら交差していない)
//---------------------------------------------------------------------------
s32		kxMesh::rayPick(VECTOR3* pOut, const VECTOR3& spos, const VECTOR3& svec, f32* pDist, const MATRIX* pTransInv)
{
	s32	ret = -1;
	u32	i, a, b, c;
	f32	neart, t;

	f32*	pVertexs;
	u16*	pIndex;

	MATRIX	transInv;
	VECTOR3	pos, vec;

	//	メッシュの逆行列を求め、位置と向きをモデル座標に変換する
	if( pTransInv == NULL )	D3DXMatrixInverse(&transInv, NULL, &_transMatrix);
	else					transInv = *pTransInv;
	D3DXVec3TransformCoord(&pos, &spos, &transInv);
	D3DXVec3TransformCoord(&vec, &svec, &transInv);
	//	向きを正規化しておく
	MATH->normalize(&vec, vec);

	//	適当に大きな値を入れておく
	neart = 1000000.0f;

	//	距離の2乗を求めておく
	f32 dist = *pDist;
	dist *= dist;

	//	メッシュのFVFを取得する
	u32	fvf = _pMesh->GetFVF();

	//	FVFより頂点サイズを求める
	s32	vertexSize = D3DXGetFVFVertexSize(fvf) / sizeof(f32);

	//	面数を取得する
	u32 numFace = _pMesh->GetNumFaces();

	//	頂点情報と面の頂点番号を取得する
	_pMesh->LockVertexBuffer(D3DLOCK_READONLY, reinterpret_cast<void**>(&pVertexs));
	_pMesh->LockIndexBuffer(D3DLOCK_READONLY,  reinterpret_cast<void**>(&pIndex));

	//	面数だけ比較する
	for( i = 0; i < numFace; i++ ) {
		//	面の頂点番号を取得する
		a = pIndex[i * 3 + 0];
		b = pIndex[i * 3 + 1];
		c = pIndex[i * 3 + 2];

		//	頂点の座標を取得する
		VECTOR3 p1(pVertexs[a * vertexSize], pVertexs[a * vertexSize + 1], pVertexs[a * vertexSize + 2]);
		VECTOR3 p2(pVertexs[b * vertexSize], pVertexs[b * vertexSize + 1], pVertexs[b * vertexSize + 2]);
		VECTOR3 p3(pVertexs[c * vertexSize], pVertexs[c * vertexSize + 1], pVertexs[c * vertexSize + 2]);

		//	面との距離判定
		if( MATH->length2(((p1 + p2 + p3) / 3.0f) - pos) > dist ) continue;

		VECTOR3 n;
		//	外積で法線を算出
		MATH->cross(&n, (p2 - p1), (p3 - p1));
		//	内積により面の向きを確認
		if( MATH->dot(n, vec) > 0 ) continue;

		VECTOR3 p;
		//	面との交点算出(交点p = pos + t * vec)
		//	t = (p1 - pos) * n / (p1 * n)
		t = MATH->dot(p1 - pos, n) / MATH->dot(p1, n);
		if( t < 0 || t > neart ) continue;
		p = pos + t * vec;

		VECTOR3 v1, v2, v3;
		//	交点が面の内側にあるか確認
		MATH->cross(&v1, p1 - p, p2 - p1);
		if( MATH->dot(v1, n) < 0 ) continue;

		MATH->cross(&v2, p2 - p, p3 - p2);
		if( MATH->dot(v2, n) < 0 ) continue;

		MATH->cross(&v3, p3 - p, p1 - p3);
		if( MATH->dot(v3, n) < 0 ) continue;

		//	交差した点と面の番号、距離を保存しておく
		*pOut = p;
		ret = i;
		neart = t;
	}

	_pMesh->UnlockIndexBuffer();
	_pMesh->UnlockVertexBuffer();

	*pDist = neart;
	return ret;
}

//============================================================================
//	END OF FILE
//============================================================================