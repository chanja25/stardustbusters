//---------------------------------------------------------------------------
//!
//!	@file	KX_Particle.cpp
//!	@brief	kxParticleクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "KX.h"


//	インスタンスの実体生成
kxParticle	kxParticle::_instance;

//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
kxParticle::kxParticle(void)
{
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
kxParticle::~kxParticle(void)
{
}

//---------------------------------------------------------------------------
//	初期化
//---------------------------------------------------------------------------
void	kxParticle::init(void)
{
	ASSERT(_pParticle == NULL, "既にKX_Particleは初期化されています");

	//	データの初期化
	_pParticle = new ParticleData[PARTICLE_MAX];
	ZeroMemory(_pParticle, sizeof(ParticleData) * PARTICLE_MAX);
	ZeroMemory(_pTexture, sizeof(LPTEXTURE) * TEXTURE_MAX);
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	kxParticle::cleanup(void)
{
	//	パーティクルデータの解放
	SAFE_DELETES(_pParticle);

	//	テクスチャーの解放
	for( u8 i=0; i < TEXTURE_MAX; i++ ) {
		RESOURCE->cleanup(_pTexture[i]);
	}
}

//---------------------------------------------------------------------------
//	テクスチャーの読み込み
//!	@param	pFileName	[in] 読み込むファイル名
//!	@param	no			[in] セットするテクスチャー番号
//---------------------------------------------------------------------------
void	kxParticle::load(const char* pFileName, u8 no)
{
	if( no >= TEXTURE_MAX ) {
		MESSAGE("セット可能なテクスチャー番号の上限を超えています", "パーティクル");
		return;
	}

	//	テクスチャーを解放しておく
	RESOURCE->cleanup(_pTexture[no]);
	//	テクスチャーの読み込み
	_pTexture[no] = RESOURCE->loadTexture(pFileName);
}

//---------------------------------------------------------------------------
//	テクスチャーの解放
//!	@param	no			[in] 解放するテクスチャー番号
//---------------------------------------------------------------------------
void	kxParticle::cleanup(u8 no)
{
	if( no >= TEXTURE_MAX ) {
		MESSAGE("解放可能なテクスチャー番号の上限を超えています", "パーティクル");
		return;
	}

	//	テクスチャーを解放しておく
	RESOURCE->cleanup(_pTexture[no]);
}

//---------------------------------------------------------------------------
//	パーティクルのセット
//!	@param	particle	[in] パーティクルデータ
//---------------------------------------------------------------------------
void	kxParticle::setParticle(const PARTICLE_DATA& particle)
{
	//	使用されていないパーティクルデータを探し、新しいパーティクルをセットする
	for( u32 i=0; i < PARTICLE_MAX; i++ ) {
		if( _pParticle[i].isAlive() ) continue;
		_pParticle[i].set(particle);
		break;
	}
}

//---------------------------------------------------------------------------
//	パーティクルのセット
//!	@param	particle	[in] パーティクルデータ
//---------------------------------------------------------------------------
void	kxParticle::setParticle(u8 no, u8 type, u16 sFrame, ARGB sColor, u16 mFrame, ARGB mColor, u16 eFrame, ARGB eColor, const VECTOR3& pos, const VECTOR3& move, const VECTOR3& power, f32 rotate, f32 scale, f32 stretch, const char* pTechnique)
{
	PARTICLE_DATA	particle;

	particle.no		= no;
	particle.type	= type;

	particle.startFrame		= sFrame;
	particle.startColor		= sColor;
	particle.middleFrame	= mFrame;
	particle.middleColor	= mColor;
	particle.endFrame		= eFrame;
	particle.endColor		= eColor;

	particle.pos		= pos;
	particle.move		= move;
	particle.power		= power;

	particle.angle		= 0;
	particle.rotate		= rotate;
	particle.scale		= scale;
	particle.stretch	= stretch;

	if( pTechnique ) {
		strcpy(particle.technique, pTechnique);
	} else {
		strcpy(particle.technique, "Particle");
	}

	setParticle(particle);
}

//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	kxParticle::update(void)
{
	//	使用中のパーティクルを更新していく
	for( u32 i=0; i < PARTICLE_MAX; i++ ) {
		if( _pParticle[i].isAlive() == FALSE ) continue;
		_pParticle[i].update();
	}
}

//---------------------------------------------------------------------------
//	描画
//---------------------------------------------------------------------------
void	kxParticle::render(void)
{
	MATRIX mat;
	LPSHADER pShader = GRAPHICS->getShader3D();
	LPDIRECT3DDEVICE9 pDevice = KXSYSTEM->getDevice();

	//	マトリックスの設定
	MATH->matrixIdentity(&mat);
	if( pShader->getEffect() ) {
		pShader->setMatrix(mat);
	} else {
		KXSYSTEM->getDevice()->SetTransform(D3DTS_WORLD, &mat);
	}

	//	ビューマトリックスの逆行列取得
	D3DXMatrixInverse(&mat, NULL, &CAMERA->getView());

	pDevice->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);
	pDevice->SetRenderState(D3DRS_FOGENABLE, FALSE);

	//	頂点のタイプを設定
	pDevice->SetFVF(kxGraphics::D3DFVF_VERTEX);
	//	使用中のパーティクルを更新していく
	for( u32 i=0; i < PARTICLE_MAX; i++ ) {
		if( _pParticle[i].isAlive() == FALSE ) continue;

		//	テクスチャーの設定
		if( pShader->getEffect() ) {
			pShader->setTexture(_pTexture[_pParticle[i].getNo()]);
		} else {
			D3DMATERIAL9 m;
			ZeroMemory(&m, sizeof(m));
			m.Diffuse.a = m.Ambient.a = 1.0f;
			m.Diffuse.r = m.Ambient.r = 1.0f;
			m.Diffuse.g = m.Ambient.g = 1.0f;
			m.Diffuse.b = m.Ambient.b = 1.0f;
			GRAPHICS->setRenderState(kxGraphics::RS_ADD, _pTexture[_pParticle[i].getNo()], &m);
		}

		_pParticle[i].render(mat);
	}

	pDevice->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);
	pDevice->SetRenderState(D3DRS_FOGENABLE, TRUE);
}

//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
kxParticle::ParticleData::ParticleData(void)
: _isAlive(FALSE)
{
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
kxParticle::ParticleData::~ParticleData(void)
{
}

//---------------------------------------------------------------------------
//	パーティクルのセット
//!	@param	particle	[in] セットするパーティクルデータ
//---------------------------------------------------------------------------
void	kxParticle::ParticleData::set(const kxParticle::PARTICLE_DATA& particle)
{
	memcpy(&_particle, &particle, sizeof(PARTICLE_DATA));

	_isAlive = TRUE;
	_frame	 = 0;
	_color	 = 0;
}

//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	kxParticle::ParticleData::update(void)
{
	if( _frame >= _particle.endFrame ) {
		_isAlive = FALSE;
		return;
	}

	_particle.pos	+= _particle.move;
	_particle.move	+= _particle.power;
	_particle.angle	+= _particle.rotate;
	_particle.scale	*= _particle.stretch;

	f32	t;
	s32	a1, r1, g1, b1, a2, r2, g2, b2;

	if( _frame < _particle.startFrame ) {
		_color = 0;
	} else {
		if( _frame < _particle.middleFrame ) {
			t  = (_particle.middleFrame - _frame) / static_cast<f32>((_particle.middleFrame - _particle.startFrame));
			a1 = (_particle.startColor >> 24);
			r1 = (_particle.startColor >> 16) & 0xFF;
			g1 = (_particle.startColor >>  8) & 0xFF;
			b1 = (_particle.startColor		) & 0xFF;
		} else {
			t  = (_frame - _particle.endFrame) / static_cast<f32>((_particle.endFrame - _particle.middleFrame));
			a1 = (_particle.endColor >> 24);
			r1 = (_particle.endColor >> 16) & 0xFF;
			g1 = (_particle.endColor >>  8) & 0xFF;
			b1 = (_particle.endColor	  ) & 0xFF;
		}

		a2 = (_particle.middleColor >> 24);
		r2 = (_particle.middleColor >> 16) & 0xFF;
		g2 = (_particle.middleColor >>  8) & 0xFF;
		b2 = (_particle.middleColor		 ) & 0xFF;

		a1 = static_cast<s32>((a1 - a2) * t) + a1;
		r1 = static_cast<s32>((r1 - r2) * t) + r1;
		g1 = static_cast<s32>((g1 - g2) * t) + g1;
		b1 = static_cast<s32>((b1 - b2) * t) + b1;
		_color = (a1 << 24) + (r1 << 16) + (g1 << 8) + (b1);
	}

	_frame++;
}

//---------------------------------------------------------------------------
//	描画
//---------------------------------------------------------------------------
void	kxParticle::ParticleData::render(const MATRIX& mat)
{
	if( (_color >> 24) == 0 ) return;

	LPDIRECT3DDEVICE9 pDevice = KXSYSTEM->getDevice();
	LPSHADER pShader = GRAPHICS->getShader3D();

	f32	s, c;
	//	角度とサイズの設定
	s = sinf(_particle.angle) * _particle.scale;
	c = cosf(_particle.angle) * _particle.scale;

	f32	x1, x2, y1, y2;
	x1 = s - c;
	x2 = s + c;
	y1 = c + s;
	y2 = c - s;

	kxGraphics::LPVERTEX pVertex = _particle.v;

	//	頂点設定
	pVertex[0].x = _particle.pos.x + ((mat._11 * x1) + (mat._21 * y1));
	pVertex[0].y = _particle.pos.y + ((mat._12 * x1) + (mat._22 * y1));
	pVertex[0].z = _particle.pos.z + ((mat._13 * x1) + (mat._23 * y1));

	pVertex[1].x = _particle.pos.x + ((mat._11 * x2) + (mat._21 * y2));
	pVertex[1].y = _particle.pos.y + ((mat._12 * x2) + (mat._22 * y2));
	pVertex[1].z = _particle.pos.z + ((mat._13 * x2) + (mat._23 * y2));

	pVertex[2].x = _particle.pos.x - ((mat._11 * x2) + (mat._21 * y2));
	pVertex[2].y = _particle.pos.y - ((mat._12 * x2) + (mat._22 * y2));
	pVertex[2].z = _particle.pos.z - ((mat._13 * x2) + (mat._23 * y2));

	pVertex[3].x = _particle.pos.x - ((mat._11 * x1) + (mat._21 * y1));
	pVertex[3].y = _particle.pos.y - ((mat._12 * x1) + (mat._22 * y1));
	pVertex[3].z = _particle.pos.z - ((mat._13 * x1) + (mat._23 * y1));

	//	タイプ設定
	pVertex[0].tu = pVertex[2].tu = static_cast<f32>(_particle.type % 4) * 0.25f + 0.02f;
	pVertex[1].tu = pVertex[3].tu = pVertex[0].tu + 0.25f - 0.02f;
	pVertex[0].tv = pVertex[1].tv = static_cast<f32>(_particle.type / 4) * 0.25f + 0.02f;
	pVertex[2].tv = pVertex[3].tv = pVertex[0].tv + 0.25f - 0.02f;

	pVertex[0].nx = pVertex[1].nx = pVertex[2].nx = pVertex[3].nx = 0.0f;
	pVertex[0].ny = pVertex[1].ny = pVertex[2].ny = pVertex[3].ny = 0.0f;
	pVertex[0].nz = pVertex[1].nz = pVertex[2].nz = pVertex[3].nz = 1.0f;

	pVertex[0].color = pVertex[1].color = pVertex[2].color = pVertex[3].color = _color;

	//	シェーダーのタイプ設定
	pShader->setTechnique(_particle.technique);
	pShader->beginShader();

	for( u32 i=0; i < pShader->getPass(); i++ ) {
		pShader->beginPass(i);
		pShader->commitChanges();

		//	レンダリング
		pDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, pVertex, sizeof(kxGraphics::VERTEX));

		pShader->endPass();
	}

	pShader->endShader();

}


//============================================================================
//	END OF FILE
//============================================================================