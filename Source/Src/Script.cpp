//---------------------------------------------------------------------------
//!
//!	@file	Script.cpp
//!	@brief	スクリプトクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "Global.h"


//---------------------------------------------------------------------------
//	コストラクタ
//---------------------------------------------------------------------------
Script::Script(void)
{
	init();
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
Script::~Script(void)
{
}

//---------------------------------------------------------------------------
//	初期化
//---------------------------------------------------------------------------
void	Script::init(void)
{
	_waitCount	= 0;
	_endFlag	= FALSE;
}

//---------------------------------------------------------------------------
//	読み込み
//!	@param	pFileName	[in] 読み込むファイル名
//---------------------------------------------------------------------------
void	Script::load(const char* pFileName)
{
	init();

	char fileName[PATH_MAX];
	//	スクリプトデータの読み込み
	sprintf_s(fileName, sizeof(fileName), "DATA\\GAME\\SCRIPT\\%s.txt", pFileName);

	//	スクリプトデータの取得
	_data.load(fileName);
}

//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	Script::update(void)
{
	//	スクリプトが終わっていなければスクリプトを進める
	while( _data.getIP() < _data.getSize() ) {
		//	スクリプトが終了状態の場合、ループを抜ける
		if( _endFlag ) return;
		//	待ちカウントが0より大きければカウントをデクリメントし、ループから抜ける
		if( _waitCount > 0 ) {
			_waitCount--;
			return;
		}
		//	コマンドを取得する
		_data.getNextCommand();
		if( getScript() ) return;
	}
}

//---------------------------------------------------------------------------
//	スクリプトの取得
//!	@retval	TRUE	スクリプトの取得成功
//!	@retval	FALSE	スクリプトの取得失敗
//---------------------------------------------------------------------------
b32		Script::getScript(void)
{
	char* pCommand = _data.getCommand();

	//	待ちをセット
	if( strcmp(pCommand, "WAIT") == 0 ) {
		_waitCount = _data.getParamInt();
		return TRUE;
	}
	//	待ちをリセット
	if( strcmp(pCommand, "RESETWAIT") == 0 ) {
		_waitCount = 0;
		return TRUE;
	}
	//	シーンの変更
	if( strcmp(pCommand, "SCENE") == 0 ) {
		s32 scene = _data.getParamInt();
		SCENE->jumpScene(scene);
		return TRUE;
	}
	//	フェードイン
	if( strcmp(pCommand, "FADEIN") == 0 ) {
		ARGB color = _data.getParamDWORD();
		u32  speed = _data.getParamInt();
		_waitCount = 255 / speed;
		FADE->fadeIn(color, speed);
		return TRUE;
	}
	//	フェードアウト
	if( strcmp(pCommand, "FADEOUT") == 0 ) {
		ARGB color = _data.getParamDWORD();
		u32  speed = _data.getParamInt();
		_waitCount = 255 / speed;
		FADE->fadeOut(color, speed);
		return TRUE;
	}
	//	サウンド再生
	if( strcmp(pCommand, "PLAYSOUND") == 0 ) {
		u32 no	 = _data.getParamInt();
		b32 loop = _data.getParamInt();
		AUDIO->play(no, loop);
		return TRUE;
	}
	//	サウンド停止
	if( strcmp(pCommand, "STOPSOUND") == 0 ) {
		u32 no	 = _data.getParamInt();
		AUDIO->stop(no);
		return TRUE;
	}
	//	スクリプトの読み込み
	if( strcmp(pCommand, "LOADSCR") == 0 ) {
		load(_data.getParamStr());
		return TRUE;
	}
	//	ゲームを終了する
	if( strcmp(pCommand, "GAMEEND") == 0 ) {
		exit(0);
		return TRUE;
	}
	return FALSE;
}

//============================================================================
//	END OF FILE
//============================================================================