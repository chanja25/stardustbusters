//---------------------------------------------------------------------------
//!
//!	@file	Global.h
//!	@brief	インクルード
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#ifndef	__GLOBAL_H__
#define	__GLOBAL_H__

#pragma	once

//	システムヘッダーのインクルード
#include "SystemAll.h"

//---------------------------------------------------------------------------
//!	@name ゲーム用ヘッダーのインクルード
//---------------------------------------------------------------------------
//!@{

#include "SkeletonSystem.h"

#include "Node.h"
#include "List.h"
#include "SystemCollision.h"
#include "TaskBase.h"
#include "SystemTask.h"
#include "SceneBase.h"

#include "Fade.h"
#include "Script.h"
#include "GameScript.h"

#include "Score.h"

#include "Effect.h"

#include "Task2D.h"
#include "ScoreDisp.h"

#include "Display.h"

#include "Weapon.h"
#include "Gun.h"
#include "Laser.h"
#include "Missile.h"

#include "Character.h"
#include "Player.h"
#include "Enemy.h"
#include "Zako.h"
#include "Boss.h"

#include "MeshObj.h"
#include "Bullet.h"
#include "LaserBullet.h"
#include "MissileBullet.h"
#include "BossBullet.h"
#include "Stage.h"
#include "Sky.h"

#include "SceneLoad.h"
#include "SceneTitle.h"
#include "SceneGame.h"
#include "SceneClear.h"
#include "SystemScene.h"

//!@}


#endif	//~#if __GLOBAL_H__

//============================================================================
//	END OF FILE
//============================================================================