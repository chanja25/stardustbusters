//---------------------------------------------------------------------------
//!
//!	@file	Laser.cpp
//!	@brief	レーザークラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "Global.h"


//	弾速
const f32	Laser::BULLET_SPEED	= 2.0f;

//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
Laser::Laser(void)
: _numPower(0)
, _timer(0)
, _flag(TRUE)
{
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
Laser::~Laser(void)
{
}

//---------------------------------------------------------------------------
//	初期化
//---------------------------------------------------------------------------
void	Laser::init(void)
{
	//	メッシュの読み込み
	_obj.load("DATA\\GAME\\MESH\\WEAPON\\Laser.kmo");

	_numPower	= MAX_POWER;
	_chainSpeed	= 0;
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	Laser::cleanup(void)
{
}

//---------------------------------------------------------------------------
//	ショット
//---------------------------------------------------------------------------
void	Laser::shot(void)
{
	if( _shotFlg == FALSE || _numPower == 0 ) return;
	_chainTimer	= 0;
	_shotFlg	= FALSE;
	//	弾のエネルギーを減らす
	_numPower	= ( _numPower < ENERUGI )? 0: _numPower - ENERUGI;
	if( _numPower == 0 ){
		_flag	= FALSE;
		_timer	= 0;
	}

	//	弾の生成
	Bullet*	pBullet1 = new LaserBullet();
	Bullet*	pBullet2 = new LaserBullet();

	//	プレイヤーの取得
	Player* pPlayer = reinterpret_cast<Player*>(TASK->getList(SystemTask::LIST_PLAYER)->getFirstNode()->getObject());

	VECTOR3 pos;
	//	銃の発射口に位置を合わせてセットする
	pos = pPlayer->getBonePos(4);
	pBullet1->set(pos, VECTOR3(0, 0, 1), BULLET_SPEED);
	pos = pPlayer->getBonePos(8);
	pBullet2->set(pos, VECTOR3(0, 0, 1), BULLET_SPEED);

	//	タスクに追加
	TASK->addBottom(pBullet1, SystemTask::LIST_BULLET);
	TASK->addBottom(pBullet2, SystemTask::LIST_BULLET);

	//	弾の発射音を鳴らす
	AUDIO->play(SceneGame::SOUND_LASER);
}

//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	Laser::update(void)
{
	if( _flag == FALSE ) {
		//	エネルギーが切れた場合一定期間エネルギーが回復しないようにする
		_timer++;
		_shotFlg = FALSE;
		_flag = ( _timer >= LIMIT )? TRUE: FALSE;
	}

	if( _flag && _shotFlg == FALSE ) {
		//	エネルギーを常に一定量回復する
		_numPower = ( _numPower + RECOVERY > MAX_POWER )? MAX_POWER: _numPower + RECOVERY;
	}

	Weapon::update();
}

//============================================================================
//	END OF FILE
//============================================================================