//===========================================================================
//!
//!	@file		Boss.h
//!	@brief		ボスクラス
//!
//!	@author S.Kawamoto
//===========================================================================
#ifndef	__BOSS_H__
#define	__BOSS_H__

#pragma	once


//===========================================================================
//! ボスクラス
//===========================================================================
class Boss : public Enemy
{
public:
	//!	モード
	enum MODE {
		MODE_SET,
		MODE_SELECT,
		MODE_WAIT,
		MODE_LASER_SET,
		MODE_LASER_SHOT,
		MODE_LASER_LOST,
		MODE_ATTACK_SET,
		MODE_ATTACK,
		MODE_ATTACK_LOST,
		MODE_DEAD,
	};

	//! モーション
	enum MOTION {
		MOTION_WAIT,
		MOTION_DASH,
		MOTION_DASH_END,
		MOTION_GUN_SET,
		MOTION_GUN_LOST,
		MOTION_ATTACK,
		MOTINO_LASER_SET,
		MOTION_LASER_LOST,
		MOTION_GUN_SHOT,
		MOTION_DEAD,
	};

	//-------------------------------------------------------------
	//!	@name 初期化
	//-------------------------------------------------------------
	//!@{

	//!	コンストラクタ
	Boss(void);

	//!	デストラクタ
	~Boss(void);

	//!	初期化
	void	init(void);
	//!	解放
	void	cleanup(void);

	//!@}

	//!	レーザーの発射
	void	setLaser(void);

	//!	移動
	void	move(void);

	//!	処理
	void	mode(void);

	//!	更新
	void	update(void);

	void	render();
	
	//-------------------------------------------------------------
	//!	@name 取得参照
	//-------------------------------------------------------------
	//!@{

	//!	当たり位置の取得
	//!	@retval	当たり位置
	inline VECTOR3	getHitPos(void) { return getBonePos(_partPoint[HIT_BODY]); }

	//!@}

private:
	//!	部位の当たり判定用
	enum HIT {
		HIT_LEFT_HAAND,
		HIT_RIGHT_HAAND,
		HIT_LEFT_SHOULDER,
		HIT_RIGHT_SHOULDER,
		HIT_LEFT_LEG,
		HIT_RIGHT_LEG,
		HIT_BODY,
		HIT_HEAD,
		HIT_MAX,
	};
	//!	移動モード用
	enum MOVE {
		MOVE_WAIT,
		MOVE_LEFT,
		MOVE_RIGHT,
	};

	VECTOR3	_target;				//!< ターゲット

	u32		_moveMode;				//!< 移動モード
	f32		_bulletSpeed;			//!< 弾速

	f32		_partDist[HIT_MAX];		//!< 部分毎の当りサイズ
	u8		_partPoint[HIT_MAX];	//!< 部分毎の当たりポイント

	SystemCollision::Object _partHit[HIT_MAX];	//!< 当たり判定用オブジェクト
	SystemCollision::Object _attack2;			//!< 攻撃判定用オブジェクト
};

#endif	//~#if __BOSS_H__

//============================================================================
//	END OF FILE
//============================================================================