//---------------------------------------------------------------------------
//!
//!	@file	Score.cpp
//!	@brief	スコアクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "Global.h"


//	インスタンスの生成
Score	Score::_instance;

//---------------------------------------------------------------------------
//	コストラクタ
//---------------------------------------------------------------------------
Score::Score(void)
{
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
Score::~Score(void)
{
}

//---------------------------------------------------------------------------
//	初期化
//---------------------------------------------------------------------------
void	Score::init(void)
{
	_clearFlag = FALSE;

	//	初期スコア
	_score[0] = 2000;
	_score[1] = 4000;
	_score[2] = 6000;
	_score[3] = 8000;
	_score[4] = 10000;
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	Score::cleanup(void)
{
}

//---------------------------------------------------------------------------
//	スコアの追加
//!	@param	score	[in] 追加するスコア
//!	@retval	スコアの順位(ランク外の場合は0xFF)
//---------------------------------------------------------------------------
u8		Score::addScore(u32 score)
{
	u8 no = 0xFF;
	_nowScore = score;

	//	スコアを追加する
	for( s32 i=SCORE_MAX - 1; i >= 0; i-- ) {
		if( _score[i] < score ) {
			for( s32 j=0; j < i; j++ ) {
				_score[j] = _score[j + 1];
			}
			no = i;
			_score[i] = score;
			break;
		}
	}
	return no;
}

//============================================================================
//	END OF FILE
//============================================================================