//===========================================================================
//!
//!	@file		LaserBullet.h
//!	@brief		レーザーバレットクラス
//!
//!	@author S.Kawamoto
//===========================================================================
#ifndef	__LASERBULLET_H__
#define	__LASERBULLET_H__

#pragma once


//===========================================================================
//! レーザーバレットクラス
//===========================================================================
class LaserBullet : public Bullet
{
public:
	//-------------------------------------------------------------
	//!	@name 初期化
	//-------------------------------------------------------------
	//!@{

	//!	コンストラクタ
	LaserBullet(void);

	//!	デストラクタ
	~LaserBullet(void);

	//!	初期化
	void	init(void);
	//!	解放
	void	cleanup(void);

	//!@}

	//!	更新
	void	update(void);
	//!	描画
	void	render(void);
};

#endif	//~#if __LASERBULLET_H__

//============================================================================
//	END OF FILE
//============================================================================