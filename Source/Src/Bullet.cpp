//---------------------------------------------------------------------------
//!
//!	@file	Bullet.cpp
//!	@brief	バレットクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "Global.h"


//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
Bullet::Bullet(void)
: _move(0, 0, 0)
, _speed(0)
, _dist(0)
, _endDist(0)
, _timer(0)
{
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
Bullet::~Bullet(void)
{
}

//---------------------------------------------------------------------------
//	初期化
//---------------------------------------------------------------------------
void	Bullet::init(void)
{
	//	メッシュの読み込み
	_obj.load("DATA\\GAME\\MESH\\WEAPON\\bullet.kmo");

	//	当たり判定用オブジェクトの初期化
	_attack.init(	_attack.SHAPE_CAPSULE,
					_attack.ATT_BULLET,
					_attack.ATT_ENEMY	);
	_attack.setWork(_attack.WORK_POWER, 5);

	_scale	 = 0.002f;
	_dist	 = _scale * 10.0f;
	_endDist = 100.0f;
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	Bullet::cleanup(void)
{
}

//---------------------------------------------------------------------------
//	セット
//!	@param	pos		[in]	初期位置
//!	@param	vec		[in]	向き
//!	@param	speed	[in]	速度
//!	@param	scale	[in]	サイズ
//---------------------------------------------------------------------------
void	Bullet::set(const VECTOR3& pos, const VECTOR3& vec, f32 speed)
{
	//	パラメータの設定
	_oldPos = _pos = pos;
	MATH->normalize(&_move, vec);
	VECTOR3 v1, v2(0, 0, 1);
	f32	angle = acosf(MATH->dot(_move, v2));
	MATH->cross(&v1, _move, v2);
	if( v1 == VECTOR3(0, 0, 0) ) v1 = VECTOR3(0, 0, 1);
	else MATH->normalize(&v1, v1);
	MATH->axisRotationMatrix(&_rot, v1, angle);

	_speed	 = speed;
}

//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	Bullet::update(void)
{
	_oldPos = _pos;
	_pos += _move * _speed;
	_timer++;

	//	弾が当たっていればエフェクト表示
	if( _attack.getState() ) {
		EFFECT->damage(_attack.getHitPos());
	}
	if( _attack.getState() || (_timer * _speed > _endDist) ) {
		_state = STATE_INVALID;
		return;
	}

	//	当たり判定のセット
	_attack.setObject(_oldPos, _dist, _pos);
}

//---------------------------------------------------------------------------
//	描画
//---------------------------------------------------------------------------
void	Bullet::render(void)
{
	//	当たり判定用のカプセルを表示
	GRAPHICS->drawCapsule(_pos, _oldPos, 0.1f, 0xFFFFFF00, 8);

	//	マトリックスの設定
	MATRIX mat, trans;
	MATH->scalingMatrix(&mat, _scale, _scale, _scale);
	MATH->matrixTranslation(&trans, _pos);
	mat = mat * _rot * trans;
	_obj.setTransMatrix(mat);

	_obj.render("copy");
}

//============================================================================
//	END OF FILE
//============================================================================