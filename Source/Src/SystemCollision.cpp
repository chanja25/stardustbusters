//===========================================================================
//!
//!	@file		SystemCollision.cpp
//!	@brief		SystemCollisionクラス
//!
//===========================================================================
#include "Global.h"


//	インスタンスの実体生成
SystemCollision	SystemCollision::_instance;

//---------------------------------------------------------------------------
//	初期化
//---------------------------------------------------------------------------
void	SystemCollision::init(void)
{
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	SystemCollision::cleanup(void)
{
	//	攻撃用リストの解放
	_attackList.allDeleteNode();
	//	当たり用リストの解放
	_objectList.allDeleteNode();
}

//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	SystemCollision::update(void)
{
	//	当たりリストの先頭ノードを取得
	Node* pObjectNode = _objectList.getFirstNode();
	Node* pAttackNode;
	Object* pObject;
	Object* pAttack;

	//	当たりノードが存在すればループして当たり判定を取る
	while( pObjectNode ) {
		//	当たりオブジェクトの取得
		pObject = reinterpret_cast<Object*>(pObjectNode->getObject());

		//	攻撃リストの先頭ノードを取得
		pAttackNode = _attackList.getFirstNode();
		//	攻撃ノードが存在すればループして当たり判定を取る
		while( pAttackNode ) {
			//	攻撃オブジェクトの取得
			pAttack = reinterpret_cast<Object*>(pAttackNode->getObject());

			//	当たり判定を行う
			if( collision(pObject, pAttack) ) {
				//	当たっていればステータスをセットしてループを進める
				pObject->setState(pAttack->getAttribute());
				pAttack->setState(pObject->getAttribute());
				pObject->setWork(pObject->WORK_DAMAGE, pAttack->getWork(pAttack->WORK_POWER));
				pAttack->setWork(pAttack->WORK_DAMAGE, pObject->getWork(pObject->WORK_POWER));

				if( pAttack->getHitDelete() == FALSE ) _attackList.deleteNode(pAttackNode);
				break;
			}

			//	次の攻撃用ノードに進む
			pAttackNode = pAttackNode->getNext();
		}
		//	当たり判定が終わったオブジェクトからリストを外す
		_objectList.deleteNode(pObjectNode);
		//	次の当たり用ノードに進む
		pObjectNode = _objectList.getFirstNode();
	}

	//	当たり判定が終わったので全ての攻撃オブジェクトを消す
	_attackList.allDeleteNode();
}

//---------------------------------------------------------------------------
//	球と球の当たり判定
//!	@param	pos1	[in] 1つ目の球の位置
//!	@param	radius1	[in] 1つ目の球の半径
//!	@param	pos2	[in] 2つ目の球の位置
//!	@param	radius2	[in] 2つ目の球の半径
//!	@param	pOut	[out] 衝突した位置
//!	@retval	TRUE	衝突している
//!	@retval	FALSE	衝突していない
//---------------------------------------------------------------------------
b32		SystemCollision::collisionSphere(const VECTOR3& pos1, f32 radius1, const VECTOR3& pos2, f32 radius2, VECTOR3* pOut)
{
	//	2つの球の距離の2乗を求める
	f32 length = MATH->length2(pos1 - pos2);
	//	2つの球の大きさを足す
	f32 radius = radius1 + radius2;
	//	距離の2乗と2つの大きさの2乗を比較する
	if( length < (radius * radius) ) {
		if( pOut ) {
			//	衝突していた場合、衝突した場所も算出
			MATH->normalize(pOut, pos2 - pos1);
			*pOut = pos1 + *pOut * radius1;
		}
		return TRUE;
	}
	return FALSE;
}

//---------------------------------------------------------------------------
//	球とカプセルの当たり判定
//!	@param	pos		[in]  球の位置
//!	@param	radius1	[in]  球の半径
//!	@param	pos1	[in]  カプセルの1つ目の半球の位置
//!	@param	pos2	[in]  カプセルの2つ目の半球の位置
//!	@param	radius1	[in]  カプセルの半径
//!	@param	pOut	[out] 衝突した位置
//!	@retval	TRUE	衝突している
//!	@retval	FALSE	衝突していない
//---------------------------------------------------------------------------
b32		SystemCollision::collisionCapsule(const VECTOR3& pos, f32 radius1, const VECTOR3& pos1, const VECTOR3& pos2, f32 radius2, VECTOR3* pOut)
{
	//	カプセルの1つ目の半球との当たり判定を取る
	if( collisionSphere(pos, radius1, pos1, radius2, pOut) ) return TRUE;
	//	カプセルの2つ目の半球との当たり判定を取る
	if( collisionSphere(pos, radius1, pos2, radius2, pOut) ) return TRUE;

	//	2つ目の半球と球の内積を取りcosθを算出
	f32 cos = MATH->dot(pos - pos2, pos1 - pos2);
	//	cosθがマイナスならば鈍角なので当たっていない
	if( cos < 0 ) return FALSE;

	//	1つ目の半球と球の内積を取りcosθを算出
	cos = MATH->dot(pos - pos1, pos2 - pos1) / (MATH->length(pos - pos1) * MATH->length(pos2 - pos1));
	//	cosθがマイナスならば鈍角なので当たっていない
	if( cos < 0 ) return FALSE;

	//	コサインθのからサインθの値を求める
	f32 sin = sqrtf(1 - (cos * cos));
	f32 len = MATH->length(pos - pos1);

	//	最短距離の位置で当たっているか確認する
	if( (sin * len) < (radius1 + radius2) ) {
		if( pOut ) {
			//	衝突していた場合、衝突した場所も算出
			VECTOR3 vec;
			MATH->normalize(&vec, pos2 - pos1);
			*pOut = pos + vec * cos * len;
		}
		return TRUE;
	}
	return FALSE;
}

//---------------------------------------------------------------------------
//	当たり判定用オブジェクトのセット
//!	@param pObject	[in] セットするオブジェクト
//---------------------------------------------------------------------------
void	SystemCollision::setObject(SystemCollision::Object *pObject)
{
	//	攻撃属性が存在するか確認する
	if( pObject->getHitAttribute() == 0 )
		//	攻撃属性が存在していないので、当たりオブジェクトに設定
		_objectList.addNodeEnd(pObject->getNode());
	else
		//	攻撃属性が存在していたので、攻撃オブジェクトにセット
		_attackList.addNodeEnd(pObject->getNode());
}

//---------------------------------------------------------------------------
//	オブジェクトの当たり判定
//!	@param	pObject	[in] 当たりオブジェクト
//!	@param	pAttack	[in] 攻撃オブジェクト
//!	@retval	TRUE	衝突している
//!	@retval	FALSE	衝突していない
//---------------------------------------------------------------------------
b32		SystemCollision::collision(SystemCollision::Object* pObject, SystemCollision::Object* pAttack)
{
	VECTOR3 hitPos(0, 0, 0);
	b32		hit;

	//	攻撃の属性が当たり判定を行う属性か確認
	if( pObject->getAttribute() & pAttack->getHitAttribute() ) {
		//	オブジェクトの形が球か調べる
		if( pObject->getShape() == pObject->SHAPE_SPHERE ) {
			//	攻撃の形が球か調べる
			if( pAttack->getShape() == pAttack->SHAPE_SPHERE) {
				hit = collisionSphere(pObject->getPos(), pObject->getRadius(), pAttack->getPos(), pAttack->getRadius(), &hitPos);
				pObject->setHitPos(hitPos);
				pAttack->setHitPos(hitPos);
				return hit;
			}
			//	攻撃の形がカプセルか調べる
			if( pAttack->getShape() == pAttack->SHAPE_CAPSULE) {
				hit = collisionCapsule(pObject->getPos(), pObject->getRadius(), pAttack->getPos(), pAttack->getPos2(), pAttack->getRadius(), &hitPos);
				pObject->setHitPos(hitPos);
				pAttack->setHitPos(hitPos);
				return hit;
			}
		}

		//	オブジェクトの形がカプセルか調べる
		if( pObject->getShape() == pObject->SHAPE_CAPSULE ) {
			//	攻撃の形が球か調べる
			if( pAttack->getShape() == pAttack->SHAPE_SPHERE) {
				hit = collisionCapsule(pObject->getPos(), pObject->getRadius(), pAttack->getPos(), pAttack->getPos2(), pAttack->getRadius(), &hitPos);
				pObject->setHitPos(hitPos);
				pAttack->setHitPos(hitPos);
				return hit;
			}
			//	攻撃の形がカプセルか調べる
			if( pAttack->getShape() == pAttack->SHAPE_CAPSULE) {
				//	カプセル同士の当たり判定
				return FALSE;
			}
		}
	}

	return FALSE;
}


//===========================================================================
//	Objectクラス
//===========================================================================
//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
SystemCollision::Object::Object(void)
: _state(0)
{
	_node.setObject(this);
}

//---------------------------------------------------------------------------
//	オブジェクト当たりのセット
//!	@param	shape		 [in] 形
//!	@param	attribute	 [in] 自分の属性
//!	@param	hitAttribute [in] 当てる事ができる属性
//!	@param	hitDelete	 [in] TRUEの場合、衝突後も当たり判定を残す。FALSEの場合、当たり判定を消去する
//---------------------------------------------------------------------------
void	SystemCollision::Object::init(u32 shape, u32 attribute, u32 hitAttribute, b32 hitDelete)
{
	_shape		  = shape;
	_attribute	  = attribute;
	_hitAttribute = hitAttribute;
	_hitDelete	  = hitDelete;

	ZeroMemory(_work, sizeof(_work));
}

//---------------------------------------------------------------------------
//	オブジェクト当たりのセット
//!	@param	pos		[in] 位置
//!	@param	radius	[in] 半径
//!	@param	pos2	[in] 位置2(カプセルの場合使う)
//---------------------------------------------------------------------------
void	SystemCollision::Object::setObject(const VECTOR3& pos, f32 radius, const VECTOR3& pos2)
{
	_state		= 0;
	_pos		= pos;
	_pos2		= pos2;
	_radius		= radius;

	COLLISION->setObject(this);
}

//============================================================================
//	END OF FILE
//============================================================================