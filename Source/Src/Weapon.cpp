//---------------------------------------------------------------------------
//!
//!	@file	Weapon.cpp
//!	@brief	武器クラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "Global.h"


//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
Weapon::Weapon(void)
: _chainTimer(0)
, _shotFlg(FALSE)
, _mode(FALSE)
, _pos(0, -30, -30)
{
}

//---------------------------------------------------------------------------
//	パージ
//---------------------------------------------------------------------------
void	Weapon::purge(void)
{
	_mode = MODE_PURGE;
}

//---------------------------------------------------------------------------
//	移動
//---------------------------------------------------------------------------
void	Weapon::move(void)
{
	//	プレイヤーのノードを取得
	Node*	pNode = TASK->getList(SystemTask::LIST_PLAYER)->getFirstNode();

	if( pNode == NULL ) {
		//	プレイヤーが存在しなければ削除要請
		_state = STATE_INVALID;
		return;
	}

	//	プレイヤーを取得する
	Player* pPlayer = reinterpret_cast<Player*>(pNode->getObject());

	//	プレイヤーの位置とマトリックスを取得する
	VECTOR3 pos = pPlayer->getPos();
	MATRIX mat = pPlayer->getObj()->getTransMatrix();

	f32 dist;

	switch( _mode ) {
	case MODE_SET:	//	セット
		//	プレイヤーへのベクトルを取得する
		_move = pos - _pos;
		dist = MATH->length(_move);
		MATH->normalize(&_move, _move);

		//	距離が一定以内に近付けば次へ進む
		if( dist <= 0.05f ) {
			_mode = MODE_FREE;
			_move *= 0;
			_pos = pos;
			break;
		}
		//	距離から移動力を設定する
		_move *= 0.06f * dist;
		break;
	case MODE_FREE:	//	フリー
		//	プレイヤーが存在すれば弾の発射
		if( pPlayer->isAlive() ) shot();
		_pos = pos;
		break;
	case MODE_PURGE:	//	パージ
		//	落下させる
		_move.y -= 0.01f;
		_move.z -= 0.01f;
		//	一定以上落下すれば削除要請する
		if( _pos.y < -10.0f ) _state = STATE_INVALID;
		break;

	default:
		ASSERT(false, "存在しないもーどに移行しました(Weapon)");
		break;
	}

	//	移動する
	_pos += _move;

	//	マトリックスに位置を設定する
	mat._41 = _pos.x;
	mat._42 = _pos.y;
	mat._43 = _pos.z;
	_obj.setTransMatrix(mat);
}

//---------------------------------------------------------------------------
//	移動
//---------------------------------------------------------------------------
void	Weapon::update(void)
{
	move();

	//	弾の連射フラグ制御
	if( _chainTimer >= _chainSpeed ) {
		//	キーが押されていれば弾の発射フラグを立てる
		if( INPUT->isKeyPress(kxInput::KEY_SELECT) ) {
			_shotFlg	= TRUE;
			_chainTimer	= 0;
		}
	} else _chainTimer++;
}

//---------------------------------------------------------------------------
//	描画
//---------------------------------------------------------------------------
void	Weapon::render(void)
{
	_obj.render();
}

//============================================================================
//	END OF FILE
//============================================================================