//---------------------------------------------------------------------------
//!
//!	@file	Score.h
//!	@brief	スコアクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#ifndef	__SCORE_H__
#define __SCORE_H__

#pragma	once


//===========================================================================
//!	スコアクラス
//===========================================================================
class Score
{
public:
	//!	保存するスコアの最大数
	static const u8 SCORE_MAX = 5;

	//---------------------------------------------------------------------------
	//!	@name 初期化
	//---------------------------------------------------------------------------
	//!@{

	//!	初期化
	void	init(void);
	//!	解放
	void	cleanup(void);

	//!@}

	//!	スコアの追加
	u8		addScore(u32 score);

	//---------------------------------------------------------------------------
	//!	@name 取得参照
	//---------------------------------------------------------------------------
	//!@{

	//!	クリアフラグのセット
	//!	@param	flag [in] クリアフラグ
	inline void	setClearFlag(b32 flag) { _clearFlag = flag; }
	//!	クリアフラグの取得
	//!	@retval	クリアフラグ
	inline b32	getClearFlag(void) { return _clearFlag; }

	//!	スコアの取得
	//!	@param	no [in] 取得するスコアの番号
	//!	@retval	スコア
	inline u32	getScore(u8 no) { return _score[no]; }
	//!	スコアの取得
	//!	@retval	現在のスコア
	inline u32	getScore(void) { return _nowScore; }

	//!	インスタンスの取得
	//!	@retval	インスタンス
	inline static Score* getInstance(void) { return &_instance; }

	//!@}
private:
	//!	コンストラクタ
	Score(void);
	//!	デストラクタ
	~Score(void);

	static Score _instance;	//!< 唯一のインスタンス


	u32		_score[SCORE_MAX];	//!< スコア用

	b32		_clearFlag;			//!< クリアフラグ
	u32		_nowScore;			//!< 現在のスコア
};

//!	SystemSceneのインスタンス取得マクロ
#define	SCORE	(Score::getInstance())

#endif	//~#if __SCORE_H__

//============================================================================
//	END OF FILE
//============================================================================