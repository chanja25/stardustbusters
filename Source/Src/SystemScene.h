//---------------------------------------------------------------------------
//!
//!	@file	SystemScene.h
//!	@brief	システムシーンクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#ifndef	__SYSTEMSCENE_H__
#define __SYSTEMSCENE_H__

#pragma	once


//===========================================================================
//!	システムシーンクラス
//===========================================================================
class SystemScene : public SceneBase
{
public:
	//!	シーンの種類
	enum SCENE {
		SCENE_TITLE,
		SCENE_GAME,
		SCENE_CLEAR,
	};

	//---------------------------------------------------------------------------
	//!	@name 初期化
	//---------------------------------------------------------------------------
	//!@{

	//!	初期化
	void	init(void);
	//!	解放
	void	cleanup(void);

	//!@}

	//!	更新
	void	update(void);

	//!	シーンの変更
	void	jumpScene(u8 scene = 0xFF);

	//---------------------------------------------------------------------------
	//!	@name 取得参照
	//---------------------------------------------------------------------------
	//!@{

	//!	現在のシーンの取得
	//!	@retval	現在のシーン
	inline SceneBase*	getCurrentScene(void) { return _pCurrentScene; }
	//!	次のシーンのセット
	//!	@param	pNext	[in]	次のシーン
	inline void			setNextScene(SceneBase* pNext) { _pNextScene = pNext; }
	//!	次のシーンの番号取得
	//!	@retval	次のシーンの番号
	inline u8			getNext(void) { return _nextScene; }

	//!	インスタンスの取得
	//!	@retval	インスタンス
	inline static SystemScene* getInstance(void) { return &_instance; }

	//!@}
private:
	//!	コンストラクタ
	SystemScene(void);
	//!	デストラクタ
	~SystemScene(void);

	static SystemScene _instance;	//!< 唯一のインスタンス

	SceneBase*	_pCurrentScene;		//!< 現在のシーン
	SceneBase*	_pNextScene;		//!< 次のシーン

	u8			_nextScene;			//!< 次のシーンの番号
};

//!	SystemSceneのインスタンス取得マクロ
#define	SCENE	(SystemScene::getInstance())

#endif	//~#if __SYSTEMSCENE_H__

//============================================================================
//	END OF FILE
//============================================================================