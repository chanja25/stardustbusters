//===========================================================================
//!
//!	@file		Character.h
//!	@brief		キャラクタークラス
//!
//!	@author S.Kawamoto
//===========================================================================
#ifndef	__CHARACTER_H__
#define	__CHARACTER_H__

#pragma	once


//===========================================================================
//! キャラクタークラス
//===========================================================================
class Character : public TaskBase
{
public:
	//-------------------------------------------------------------
	//!	@name 初期化
	//-------------------------------------------------------------
	//!@{

	//!	コンストラクタ
	Character(void);

	//!	デストラクタ
	virtual ~Character(void){}

	//!	初期化
	virtual void	init(void) = 0;
	//!	解放
	virtual void	cleanup(void) = 0;

	//!@}

	//!	ダメージ判定
	void	hitDamage(SystemCollision::Object& object);
	//!	更新
	virtual void	update(void) = 0;
	//!	描画
	virtual void	render(void);


	//-------------------------------------------------------------
	//!	@name 取得参照
	//-------------------------------------------------------------
	//!@{

	//!	3DOBJの取得
	//!	@retval	3DOBJ
	inline KX3DOBJ*	getObj(void) { return &_obj; }
	//!	転送行列の取得
	//! @retval 転送行列
	inline MATRIX	getTransMatrix(void) { return _obj.getTransMatrix(); }
	//!	ボーンのマトリックス取得
	//!	@param	取得するボーン番号
	//!	@retval	ボーンのマトリックス取得
	inline MATRIX*	getBone(u16 no) { return _obj.getBone(no); }
	//!	ボーンの位置取得
	VECTOR3	getBonePos(u16 no);

	//!	位置のセット
	//!	@param	pos		[in]	表示位置
	inline void		setPos(const VECTOR3& pos) { _pos = pos; }
	//!	スケールのセット
	//!	@param	size	[in]	スケール
	inline void		setScale(f32 scale) { _scale = scale; }
	//!	向きのセット
	//!	@param	angle	[in]	向き
	inline void		setAngle(const VECTOR3& angle) { _angle = angle; }
	//!	位置の取得
	//!	@retval	位置
	inline VECTOR3	getPos(void) { return _pos; }
	//!	向きの取得
	//!	@retval	向き
	inline VECTOR3	setAngle(void) { return _angle; }

	//!	当たり位置の取得
	//!	@retval	当たり位置
	inline virtual VECTOR3	getHitPos(void) { return _pos; }


	//!	モードのセット
	//!	@param	mode	[in] モード
	inline void		setMode(s32 mode) { _mode = mode; }
	//!	モードの取得
	//!	@retval	現在のモード
	inline s32		getMode(void) { return _mode; }
	//!	HPのセット
	//!	@param	hp	[in] HP
	inline void		setHp(s32 hp) { _hp = hp; }
	//!	HPの取得
	//!	@retval	HP
	inline s32		getHp(void) { return _hp; }
	//!	最大HPのセット
	//!	@param	hp	[in] HP
	inline void		setMaxHp(s32 hp) { _maxhp = hp; }
	//!	最大HPの取得
	//!	@retval	HP
	inline s32		getMaxHp(void) { return _maxhp; }
	//!	生死の取得
	//!	@retval	生存しているか
	inline b32		isAlive(void) { return _hp > 0; }

	//!@}

protected:
	KX3DOBJ		_obj;			//!< モデル用

	VECTOR3		_pos;			//!< 表示位置
	VECTOR3		_move;			//!< 移動量
	f32			_speed;			//!< 速度
	f32			_scale;			//!< スケール
	f32			_dist;			//!< 当たりの範囲
	VECTOR3		_angle;			//!< 向き
	VECTOR3		_target;		//!< ターゲット
	f32			_targetDist;	//!< ターゲットまでの距離

	f32			_alpha;			//!< 透過値

	s32			_mode;			//!< モード
	s32			_hp;			//!< HP
	s32			_maxhp;			//!< 最大HP
	s32			_motion;		//!< モーション
	s32			_timer;			//!< タイマー

	SystemCollision::Object	_hit;	//!< 当たり判定用オブジェクト
};

#endif	//~#if __CHARACTER_H__

//============================================================================
//	END OF FILE
//============================================================================