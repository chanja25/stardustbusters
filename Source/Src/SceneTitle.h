//---------------------------------------------------------------------------
//!
//!	@file	SceneTitle.h
//!	@brief	タイトルシーンクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#ifndef	__SCENETITLE_H__
#define __SCENETITLE_H__

#pragma	once


//===========================================================================
//!	タイトルシーンクラス
//===========================================================================
class SceneTitle : public SceneBase
{
public:
	//---------------------------------------------------------------------------
	//!	@name 初期化
	//---------------------------------------------------------------------------
	//!@{

	//!	コンストラクタ
	SceneTitle(void);
	//!	デストラクタ
	~SceneTitle(void);

	//!	初期化
	void	init(void);
	//!	解放
	void	cleanup(void);

	//!@}

	//!	更新
	void	update(void);

private:
	//!	サウンド
	enum SOUND {
		SOUND_BGM,
	};

	//!	ステップ
	enum STEP {
		STEP_START,
		STEP_TEAM_LOGO_IN,
		STEP_TEAM_LOGO_OUT,
		STEP_TITLE,
		STEP_TITLE_SELECT,
		STEP_TITLE_OUT,
		STEP_SCORE_IN,
		STEP_SCORE,
		STEP_SCORE_OUT,
		STEP_EXIT,
	};

	//!	使用するテクスチャー
	enum TEXTURE {
		TEAM_LOGO,
		BACK,
		GAME_LOGO,
		ENTER,
		BUTTON1,
		BUTTON2,
		BUTTON3,
		RESULT,
		TEXTURE_MAX,
	};

	Task2D*		_pObj2D[TEXTURE_MAX];		//!< 画像表示用
	ScoreDisp*	_pScore[Score::SCORE_MAX];	//!< スコア表示用

	f32			_width;						//!< 画面の横幅
	f32			_height;					//!< 画面の盾幅

	u8			_step;						//!< 進行用
	u8			_select;					//!< 選択用
	u32			_count;						//!< カウント用
};


#endif	//~#if __SCENETITLE_H__

//============================================================================
//	END OF FILE
//============================================================================