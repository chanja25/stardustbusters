//===========================================================================
//!
//!	@file		Missile.h
//!	@brief		ミサイルクラス
//!
//!	@author S.Kawamoto
//===========================================================================
#ifndef	__MISSILE_H__
#define	__MISSILE_H__

#pragma once

//	敵クラスの宣言
class Enemy;

//===========================================================================
//! ミサイルクラス
//===========================================================================
class Missile : public Weapon
{
public:
	//-------------------------------------------------------------
	//!	@name 初期化
	//-------------------------------------------------------------
	//!@{

	//!	コンストラクタ
	Missile(void);
	//!	デストラクタ
	~Missile(void);

	//!	初期化
	void	init(void);
	//!	解放
	void	cleanup(void);

	//!@}

	//!	ロック
	void	lock(void);
	//!	ショット
	void	shot(void);

	//!	更新
	void	update(void);

	//!	残段率
	f32		getMeter(void) { return static_cast<f32>(_numLock) / static_cast<f32>(MAX_LOCK); }

private:
	//!	最大ロック数
	static const u32	MAX_LOCK   = 16;
	//!	ロック距離
	static const f32	LOCK_DIST;
	//!	ロック距離
	static const f32	LOCK_DIST_NEAR;
	//!	ロック速度
	static const u32	LOCK_SPEED = 5;
	//!	弾速
	static const f32	BULLET_SPEED;

	Enemy*	_pTarget[MAX_LOCK];	//!< ロック中の敵

	u32		_numLock;			//!< ロック数
	u32		_lockTimer;			//!< ロック用タイマー
};

#endif	//~#if __MISSILE_H__

//============================================================================
//	END OF FILE
//============================================================================