//---------------------------------------------------------------------------
//!
//!	@file	Character.cpp
//!	@brief	キャラクタークラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "Global.h"


//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
Character::Character(void)
: _pos(0, 0, 0)
, _move(0, 0, 0)
, _speed(0)
, _scale(0)
, _dist(0)
, _angle(0, 0, 0)
, _target(0, 0, 0)
, _targetDist(0)
, _alpha(1.0f)
, _mode(0)
, _hp(0)
, _maxhp(0)
, _motion(0)
, _timer(0)
{
}

//---------------------------------------------------------------------------
//	ダメージ判定
//!	@param	pObject	[in]	当たり判定用オブジェクト
//---------------------------------------------------------------------------
void	Character::hitDamage(SystemCollision::Object& object)
{
	//	現在の当たり判定の状態を確認
	u32	state = object.getState();

	if( state == object.ATT_NONE ) return;

	//	ザコにプレイヤーが当たった場合、ザコを消滅させる
	if( state == object.ATT_PLAYER ) {
		_hp = 0;
	}

	//	プレイヤーに敵が当たった場合、敵の威力分のダメージをHPから減らす
	if( state == object.ATT_ENEMY ) {
		_hp -= object.getWork(object.WORK_DAMAGE);
		//	当たりエフェクトの表示要請
		EFFECT->damage(object.getHitPos());
	}

	//	敵に弾が当たった場合、弾の威力分のダメージを減らす
	if( state == object.ATT_BULLET ) {
		_hp -= object.getWork(object.WORK_DAMAGE);
	}

	//	HPが‐にならないようにする
	_hp = ( _hp < 0 )? 0: _hp;
}

//---------------------------------------------------------------------------
//	描画
//---------------------------------------------------------------------------
void	Character::render(void)
{
	//	マトリックスの設定
	MATRIX mat, rot, trans;
	MATH->scalingMatrix(&mat, _scale, _scale, _scale);
	MATH->rotationMatrixXYZ(&rot, _angle.x, _angle.y, _angle.z);
	MATH->matrixTranslation(&trans, _pos);
	mat = mat * rot * trans;
	_obj.setTransMatrix(mat);

	//	シェーダーに透過値を設定し、描画
	GRAPHICS->getShader3D()->setValue("alpha", _alpha);
	_obj.render("Alpha");
}

//---------------------------------------------------------------------------
//	ボーンの位置取得
//!	@param	no	[in]	ボーン番号
//!	@retval	ボーンの位置
//---------------------------------------------------------------------------
VECTOR3	Character::getBonePos(u16 no)
{
	MATRIX mat = (*_obj.getBone(no)) * _obj.getTransMatrix();
	return VECTOR3(mat._41, mat._42, mat._43);
}

//============================================================================
//	END OF FILE
//============================================================================