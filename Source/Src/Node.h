//---------------------------------------------------------------------------
//!
//!	@file	Node.h
//!	@brief	ノードクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#ifndef	__NODE_H__
#define __NODE_H__

#pragma	once


class List;

//===========================================================================
//!	ノードクラス
//===========================================================================
class Node
{
public:
	//-------------------------------------------------------------
	//!	@name 初期化
	//-------------------------------------------------------------
	//!@{

	//!	コンストラクタ
	Node(void);
	//!	デストラクタ
	~Node(void){};

	//!@}

	//-------------------------------------------------------------
	//!	@name オブジェクトの管理
	//-------------------------------------------------------------
	//!@{

	//!	オブジェクトのセット
	//!	@param	pObject	[in] セットするオブジェクト
	inline void		setObject(void* pObject) { _pObject = pObject; }
	//!	所持しているオブジェクトの取得
	//!	@retval	所持しているオブジェクト
	inline void*	getObject(void)	{ return _pObject; }
	//!	所持しているオブジェクトの解放
	inline void		cleanupObject(void) { SAFE_DELETE(_pObject); }

	//!@}

	//-------------------------------------------------------------
	//!	@name リストの管理
	//-------------------------------------------------------------
	//!@{

	//!	所属するリストのセット
	//!	@param	pList	[in] セットするリスト
	inline void		setList(List* pList) { _pList = pList; }
	//!	所属しているリストの取得
	//!	@retval	所属しているリスト
	inline List*	getList(void) { return _pList; }
	//!	自分の次にノードを接続
	void	setListNext(Node* pNode);
	//!	自分の前にノードを接続
	void	setListPrev(Node* pNode);
	//!	リストから削除
	void	deleteList(void);

	//!@}

	//-------------------------------------------------------------
	//!	@name 取得参照
	//-------------------------------------------------------------
	//!@{

	//!	次にくるノードのセット
	//!	@param	pNext	[in] 次にくるノード
	inline void		setNext(Node* pNext) { _pNext = pNext; }
	//!	次にあるノードの取得
	//!	@retval	次にあるノード
	inline Node*	getNext(void) { return _pNext; }
	//!	前にくるノードのセット
	//!	@param	pNext	[in] 前にくるノード
	inline void		setPrev(Node* pPrev) { _pPrev = pPrev; }
	//!	前にあるノードの取得
	//!	@retval	前にあるノード
	inline Node*	getPrev(void) { return _pPrev; }

	//!@}

private:
	List*	_pList;		//!< 所属しているリスト

	void*	_pObject;	//!< 所持しているオブジェクト

	Node*	_pNext;		//!< 次のノード
	Node*	_pPrev;		//!< 前のノード
};

#endif	//~#if __NODE_H__

//============================================================================
//	END OF FILE
//============================================================================