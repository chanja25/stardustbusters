//===========================================================================
//!
//!	@file		SystemTask.h
//!	@brief		システムタスククラス
//!
//===========================================================================
#ifndef	__SYSTEMTASK_H__
#define	__SYSTEMTASK_H__

#pragma once


//===========================================================================
//! システムタスククラス
//===========================================================================
class SystemTask : public TaskBase
{
public:
	//!	リストの種類
	enum LIST {
		LIST_STAGE,
		LIST_ENEMY,
		LIST_BOSS,
		LIST_BULLET,
		LIST_WEAPON,
		LIST_PLAYER,
		LIST_2D,
		LIST_LOAD,
		LIST_MAX,
	};

	//-------------------------------------------------------------
	//!	@name 初期化
	//-------------------------------------------------------------
	//!@{

	//!	初期化
	void	init(void);
	//!	解放
	void	cleanup(void);

	//!@}

	//-------------------------------------------------------------
	//!	@name タスク管理
	//-------------------------------------------------------------
	//!@{

	//!	更新
	void	update(void);
	//!	リストの更新
	void	update(List* pList);
	//!	描画
	void	render(void);
	//!	リストの描画
	void	render(List* pList);

	//! 全リストの消去
	void	killAll(void);
	//! 対象のタスクの消去
	void	killTask(TaskBase* pTask);
	//!	対象のタスクリストの末尾に、タスク追加
	void	addBottom(TaskBase* pTask, u32 list, b32 deleteMode = TRUE);
	//!	対象のタスクリストの先頭に、タスク追加
	void	addTop(TaskBase* pTask, u32 list, b32 deleteMode = TRUE);

	//!	対象のリストを取得する
	//!	@retval	取得するリスト
	inline	List*	getList(u32 list) { return &_pList[list]; }

	//!@}

	//-------------------------------------------------------------
	//!	@name 取得参照
	//-------------------------------------------------------------
	//!@{

	inline	static SystemTask*	getInstance(void) { return &_instance; }

	//!@}

private:
	//!	コンストラクタ
	SystemTask(void);
	//!	デストラクタ
	~SystemTask(void){};

	static	SystemTask	_instance;	//!< インスタンス

	List*	_pList;					//!< リストポインタ
};

#define	TASK	(SystemTask::getInstance())

#endif	//~#if __SYSTEMTASK_H__

//============================================================================
//	END OF FILE
//============================================================================