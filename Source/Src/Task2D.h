//===========================================================================
//!
//!	@file		Task2D.h
//!	@brief		タスク2Dベースクラス
//!
//!	@author S.Kawamoto
//===========================================================================
#ifndef	__TASK2D_H__
#define	__TASK2D_H__

#pragma once


//===========================================================================
//! タスク2Dクラス
//===========================================================================
class Task2D : public TaskBase
{
public:
	//-------------------------------------------------------------
	//!	@name 初期化
	//-------------------------------------------------------------
	//!@{

	//!	コンストラクタ
	Task2D(void);
	//!	コンストラクタ
	Task2D(const char* pFileName);

	//!	デストラクタ
	virtual ~Task2D(void);

	//!	初期化
	virtual void	init(void);
	//!	解放
	virtual void	cleanup(void);

	//!	読み込み
	void	load(const char* pFileName);

	//!@}

	//!	更新
	virtual void	update(void);
	//!	描画
	virtual void	render(void);

	//-------------------------------------------------------------
	//!	@name 取得参照
	//-------------------------------------------------------------
	//!@{

	//!	データの設定
	void		set(f32 x, f32 y, f32 width, f32 height, f32 srcX, f32 srcY, f32 srcW, f32 srcH, const char* pTechnique, ARGB color = 0xFFFFFFFF, u32 flag = kxGraphics::RS_COPY);

	//!	表示位置のセット
	//!	@param	x,y	[in]	表示位置
	inline void	setPos(f32 x, f32 y) { _pos.x = x; _pos.y = y; }
	//!	表示サイズのセット
	//!	@param	w,h [in]	表示サイズ
	inline void	setSize(f32 w, f32 h) { _size.x = w; _size.y = h; }
	//!	ソース上の位置のセット
	//!	@param	x,y [in]	ソース上の位置
	inline void	setSrcPos(f32 x, f32 y) { _srcPos.x = x; _srcPos.y = y; }
	//!	ソース上のサイズのセット
	//!	@param	w,h [in]	ソース上のサイズ
	inline void	setSrcSize(f32 w, f32 h) { _srcSize.x = w; _srcSize.y = h; }
	//!	回転角のセット
	//!	@param	angle	[in]	回転角
	inline void	setAngle(f32 angle) { _angle = angle; }
	//!	描画色のセット
	//!	@param	color	[in]	描画色
	inline void	setColor(ARGB color) { _color = color; }
	//!	描画モードのセット
	//!	@param	flag	[in]	描画モード
	inline void	setFlag(u32 flag) { _flag = flag; }
	//!	テクニックのセット
	//!	@param	pTechnique	[in]	テクニック
	inline void	setTechnique(const char* pTechnique) { strcpy(_technique, pTechnique); }

	//!	中心の設定をセット
	//!	@param	center	[in]	中心の設定
	inline void	setCenter(kx2DObject::CENTER center) { _obj.setCenter(center); }
	//!@}

protected:
	KX2DOBJ		_obj;			//!< テクスチャー用

	VECTOR2		_pos;			//!< 表示位置
	VECTOR2		_size;			//!< 表示サイズ
	VECTOR2		_srcPos;		//!< ソース上の位置
	VECTOR2		_srcSize;		//!< ソース上のサイズ
	f32			_angle;			//!< 回転角
	ARGB		_color;			//!< 描画色
	u32			_flag;			//!< 描画モード
	char		_technique[32];	//!< テクニック
};

#endif	//~#if __TASK2D_H__

//============================================================================
//	END OF FILE
//============================================================================