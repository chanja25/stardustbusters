//===========================================================================
//!
//!	@file		MeshObj.h
//!	@brief		メッシュオブジェクトクラス
//!
//!	@author S.Kawamoto
//===========================================================================
#ifndef	__MESHOBJ_H__
#define	__MESHOBJ_H__

#pragma	once



//===========================================================================
//! メッシュオブジェクトクラス
//===========================================================================
class MeshObj : public TaskBase
{
public:
	//-------------------------------------------------------------
	//!	@name 初期化
	//-------------------------------------------------------------
	//!@{

	//!	コンストラクタ
	MeshObj(void){}

	//!	デストラクタ
	~MeshObj(void){}

	//!	初期化
	virtual void	init(void) = 0;
	//!	解放
	virtual void	cleanup(void) = 0;

	//!@}

	//!	更新
	virtual void	update(void) = 0;
	//!	描画
	virtual void	render(void) = 0;

	//-------------------------------------------------------------
	//!	@name 取得参照
	//-------------------------------------------------------------
	//!@{

	//!	3DOBJの取得
	//!	@retval	3DOBJ
	inline KXMESH*	getMesh(void) { return &_obj; }

	//!	位置のセット
	//!	@param	pos	[in]	表示位置
	inline void		setPos(const VECTOR3& pos) { _pos = pos; }
	//!	スケールのセット
	//!	@param	size	[in]	スケール
	inline void		setScale(f32 scale) { _scale = scale; }
	//!	向きのセット
	//!	@param	angle	[in]	向き
	inline void		setAngle(const VECTOR3& angle) { _angle = angle; }
	//!	位置の取得
	//!	@retval	位置
	inline VECTOR3	getPos(void) { return _pos; }
	//!	向きの取得
	//!	@retval	向き
	inline VECTOR3	setAngle(void) { return _angle; }
	//!	スケールの取得
	//!	@retval	スケール
	inline f32		setScale(void) { return _scale; }

	//!@}

protected:
	KXMESH		_obj;			//!< 3DOBJ用

	VECTOR3		_pos;			//!< 表示位置
	f32			_scale;			//!< スケール
	VECTOR3		_angle;			//!< 向き

private:
};

#endif	//~#if __MESHOBJ_H__

//============================================================================
//	END OF FILE
//============================================================================