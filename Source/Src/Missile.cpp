//---------------------------------------------------------------------------
//!
//!	@file	Missile.cpp
//!	@brief	ミサイルクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "Global.h"


//!	ロック距離
const f32	Missile::LOCK_DIST = 50.0f;
//!	ロック距離
const f32	Missile::LOCK_DIST_NEAR = 1.0f;
//	弾速
const f32	Missile::BULLET_SPEED = 0.5f;

//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
Missile::Missile(void)
{
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
Missile::~Missile(void)
{
}

//---------------------------------------------------------------------------
//	初期化
//---------------------------------------------------------------------------
void	Missile::init(void)
{
	//	メッシュ
	_obj.load("DATA\\GAME\\MESH\\WEAPON\\Missile.kmo");

	//	初期設定
	ZeroMemory(_pTarget, sizeof(_pTarget));

	_chainSpeed = 30;
	_numLock	= 0;
	_lockTimer	= 0;
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	Missile::cleanup(void)
{
}

//---------------------------------------------------------------------------
//	ロック
//---------------------------------------------------------------------------
void	Missile::lock(void)
{
	u32 i;
	f32 dist1 = LOCK_DIST;
	f32 dist2;
	//	プレイヤーの位置を取得する
	VECTOR3 pos	= reinterpret_cast<Player*>(TASK->getList(SystemTask::LIST_PLAYER)->getFirstNode()->getObject())->getPos();

	//	ロックした対象が存在するか確認
	for( i = 0; i < _numLock; i++ ) {
		//	ターゲットとの距離を取得する
		dist2 = _pTarget[i]->getHitPos().z - pos.z;

		//	ターゲットが存在していてかつ、一定内ならばロックを続ける
		if( _pTarget[i]->isAlive() && dist2 > LOCK_DIST_NEAR ) continue;

		//	i番目のターゲットをターゲットから外す
		_pTarget[i] = NULL;
		for( u32 j=0; i + j + 1 < _numLock; j++ ) {
			_pTarget[i + j] = _pTarget[i + j + 1];
			_pTarget[i + j + 1] = NULL;
		}

		_numLock--;
	}

	//	ロック数が最大ロック数に達していれば終了
	if( _numLock >= MAX_LOCK ) return;
	//	ボタンが押されていなければ終了
	if( INPUT->isKeyPress(kxInput::KEY_SELECT) == FALSE ) return;
	//	次のロックまで一定間待つ
	if( _lockTimer < LOCK_SPEED ) {
		_lockTimer++;
		return;
	}

	//	敵のノードを取得する
	Node*  pNode = TASK->getList(SystemTask::LIST_ENEMY)->getFirstNode();

	Enemy* pEnemy;
	Enemy* pTarget = NULL;

	//	ロック可能な敵を検索する
	while( pNode ) {
		//	敵をノードから取得
		pEnemy = reinterpret_cast<Enemy*>(pNode->getObject());
		//	ノードを次へ進める
		pNode = pNode->getNext();
		//	敵が生存しているか確認する
		if( pEnemy->isAlive() == FALSE ) continue;

		//	既にロックされていないか確認する
		for( i = 0; i < _numLock; i++ ) {
			if( _pTarget[i] == pEnemy ) break;
		}
		if( i < _numLock ) continue;

		//	距離の取得
		dist2 = pEnemy->getHitPos().z - pos.z;
		//	一定距離間内に入っているか確認する
 		if( dist1 > dist2 && dist2 > LOCK_DIST_NEAR ) {
			dist1 = dist2;
			pTarget = pEnemy;
		}
	}

	//	ボスのノードを取得
	pNode = TASK->getList(SystemTask::LIST_BOSS)->getFirstNode();
	if( pNode ) {
		//	ボスの取得
		pEnemy = reinterpret_cast<Enemy*>(pNode->getObject());

		//	ボスが既にロックされていないか確認する
		for( i = 0; i < _numLock; i++ ) {
			if( _pTarget[i] == pEnemy ) break;
		}

		//	敵が生存していてかつ、ロック可能な最大数を超えていないか確認
		if( pEnemy->isAlive() && i == _numLock ) {
			//	距離を取得する
			dist2 = pEnemy->getHitPos().z - pos.z;
			//	一定距離間内に入っているか確認する
 			if( dist1 > dist2 && dist2 > LOCK_DIST_NEAR ) {
				dist1 = dist2;
				pTarget = pEnemy;
			}
		}
	}

	//	ロック出来た敵が居ればセットする
	if( pTarget ) {
        _pTarget[_numLock] = pTarget;
		_numLock++;
		_lockTimer = 0;
	}
}

//---------------------------------------------------------------------------
//	ショット
//---------------------------------------------------------------------------
void	Missile::shot(void)
{
	if( _shotFlg == FALSE ) return;
	_shotFlg = FALSE;
	if( _numLock == 0 ) return;

	//	プレイヤーを取得する
	Player* pPlayer = reinterpret_cast<Player*>(TASK->getList(SystemTask::LIST_PLAYER)->getFirstNode()->getObject());

	VECTOR3 pos;
	//	ロックしている数だけミサイルを発射する
	for( u32 i=0; i < _numLock; i++ ) {
		//	ミサイルを生成
		MissileBullet*	pBullet = new MissileBullet();
		//	発射口の位置を取得し設定する
		pos = pPlayer->getBonePos((i % 2 + 1) * 4);
		pBullet->set(pos, VECTOR3(0, 0, 1), BULLET_SPEED);
		//	ターゲットの設定
		pBullet->setTarget(_pTarget[i]);

		//	タスクにセットする
		TASK->addBottom(pBullet, SystemTask::LIST_BULLET);
		//	ターゲットから外す
		_pTarget[i] = NULL;
	}

	//	ロック数を0にする
	_numLock = 0;

	//	ミサイル発射音を鳴らす
	AUDIO->play(SceneGame::SOUND_MISSILE);
}

//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	Missile::update(void)
{
	move();

	//	弾の連射フラグ制御
	if( _chainTimer >= _chainSpeed ) {
		if( INPUT->isKeyRelease(kxInput::KEY_SELECT) ) {
			_shotFlg	= TRUE;
			_chainTimer	= 0;
		}
	} else _chainTimer++;

	//	ロック制御
	lock();

	VECTOR3 pos;
	VECTOR3 move(0, 0, 0);
	VECTOR3 power(0, 0, 0);
	//	ロックした場所にロックオンマーカーを表示する
	for( u32 i=0; i < _numLock; i++ ) {
		if( _pTarget[i] == NULL ) break;
 		pos = _pTarget[i]->getHitPos();
		pos.z -= 0.5f;
		PARTICLE->setParticle(0, 14, 0, 0xffffffff, 0, 0xffffffff, 1, 0xffffff, pos, move, power, 0, 0.3f, 1.0f);
	}
}

//============================================================================
//	END OF FILE
//============================================================================