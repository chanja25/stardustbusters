//---------------------------------------------------------------------------
//!
//!	@file	System.cpp
//!	@brief	システムクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "SystemAll.h"

//	インスタンスのポインタ生成
System*	System::_pInstance = NULL;

//---------------------------------------------------------------------------
// コンストラクタ
//---------------------------------------------------------------------------
System::System(void)
: _hWnd(NULL)
, _isFullScreen(FALSE)
{
	ASSERT(_pInstance == NULL, "既にシステムは初期化されています");
	_pInstance = this;
}

//---------------------------------------------------------------------------
//	初期化
//!	@retval	TRUE	成功
//!	@retval	FALSE	失敗
//---------------------------------------------------------------------------
b32		System::initSystem(void)
{
	//	メモリリークがないかのチェック用マクロ
	CHECKMEMORY;

	//	Window名
	char* pName = "PaperRobot";

	//---------------------------------------------------------------------------
	//	ウインドウクラスの設定
	//---------------------------------------------------------------------------
	WNDCLASSEX wcex;
	HWND hWnd;

	wcex.cbSize			= sizeof(WNDCLASSEX);
	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= GetModuleHandle(NULL);
	wcex.hIcon			= LoadIcon(wcex.hInstance, "ICON");
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)GetStockObject(BLACK_BRUSH);
	wcex.lpszMenuName	= NULL;
	wcex.lpszClassName	= "System";
	wcex.hIconSm		= 0;

	RegisterClassEx(&wcex);

	//	720p
	_dispRect.left		= 0;
	_dispRect.top		= 0;
	_dispRect.right		= 1280 + _dispRect.left;
	_dispRect.bottom	= 720  + _dispRect.top;

	//---------------------------------------------------------------------------
	//	ウインドウの生成
	//---------------------------------------------------------------------------
	if( _isFullScreen == FALSE ){
		//	ウインドウ
		hWnd = CreateWindow(wcex.lpszClassName,
							pName,
							WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX,
							_dispRect.left,
							_dispRect.top,
							_dispRect.right,
							_dispRect.bottom,
							NULL,
							NULL,
							GetModuleHandle(NULL),
							NULL);
	} else {
		//	フルスクリーン
		hWnd = CreateWindow(wcex.lpszClassName,
							pName,
							WS_POPUP,
							_dispRect.left,
							_dispRect.top,
							_dispRect.right,
							_dispRect.bottom,
							NULL,
							NULL,
							GetModuleHandle(NULL),
							NULL);
		ShowCursor(FALSE);
	}

	//	ウインドウの表示
	ShowWindow(hWnd, SW_SHOW);
	UpdateWindow(hWnd);

	//	ウインドウハンドルを取得しておく
	_hWnd = hWnd;

	//	KXシステムの初期化
	if( KXSYSTEM->init(_hWnd, getWidth(), getHeight(), _isFullScreen) == FALSE )	return FALSE;

	//	リソース管理の初期化
	RESOURCE->init();

	//	フォントの初期化
	if( kxFont::init() == FALSE )	return FALSE;

	//	オーディオの初期化
	if( AUDIO->init() == FALSE )	return FALSE;

	//	インプットの初期化
	if( INPUT->init() == FALSE )	return FALSE;

	//	グラフィックスの初期化
	GRAPHICS->init();

	//	カメラの初期化
	CAMERA->init();

	//	パーティクルの初期化
	PARTICLE->init();

	//	タスクシステムの初期化
	#if USE_TASKSYSTEM
		TASK->init();
	#endif	//~if USE_TASKSYSTEM

	//	デバッグカメラの初期化
	#if	USE_DEBUGCAMERA
		DEBUGCAMERA->init();
	#endif	//~#if USE_DEBUGCAMERA

	//	プロファイラの初期化
	#if	USE_PROFILER
		kxProfiler::init();
	#endif	//~#if USE_PROFILER

	//	フレーム制御の初期化
	#if	USE_FRAME_CONTROL
		kxFrame::init();
	#endif	//~#if USE_FRAME_CONTROL

	return TRUE;
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	System::cleanupSystem(void)
{
	//	フレーム制御の解放
	#if	USE_FRAME_CONTROL
		kxFrame::cleanup();
	#endif	//~#if USE_FRAME_CONTROL

	//	プロファイラの解放
	#if	USE_PROFILER
		kxProfiler::cleanup();
	#endif	//~#if USE_PROFILER

	//	デバッグカメラの解放
	#if	USE_DEBUGCAMERA
		DEBUGCAMERA->cleanup();
	#endif	//~#if USE_DEBUGCAMERA

	//	タスクシステムの解放
	#if USE_TASKSYSTEM
		TASK->cleanup();
	#endif	//~#if USE_TASKSYSTEM

	//	パーティクルの解放
	PARTICLE->cleanup();

	//	カメラの解放
	CAMERA->cleanup();

	//	グラフィックスの解放
	GRAPHICS->cleanup();

	//	インプットの解放
	INPUT->cleanup();

	//	オーディオの解放
	AUDIO->cleanup();

	//	フォントの解放
	kxFont::cleanup();

	//	テクスチャーの解放
	RESOURCE->cleanup();

	//	KXシステムの解放
	KXSYSTEM->cleanup();
}

//---------------------------------------------------------------------------
//	メインループ
//---------------------------------------------------------------------------
s32		System::mainLoop(void)
{
	//	初期化
	if( initSystem() == FALSE ||
		init() == FALSE ) {
		cleanupSystem();
		return 0;
	}

	MSG		msg;

	//---------------------------------------------------------------------------
	// メインループ
	//---------------------------------------------------------------------------
	while( TRUE ) {
		if( PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE) ) {
			if( !GetMessage(&msg, NULL, 0, 0) ) break;
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		} else {
			//	フレーム制御
			#if	USE_FRAME_CONTROL
				if( FRAME->update() == FALSE ) continue;
			#endif	//~#if USE_FRAME_CONTROL

			//	プロファイラにフレームの開始を伝える
			START_PROFILE;

			//	更新時の処理時間計測開始
			BEGIN_PROFILE("Processing", kxProfiler::COLOR_GREEN);

			//	インプットの更新
			INPUT->update();

			//	カメラの更新
			CAMERA->update();

			//	更新
			if( update() ) {
				//	タスクの更新
				#if USE_TASKSYSTEM
					TASK->update();
				#endif	//~#if USE_TASKSYSTEM

				//	更新時の処理時間測定終了
				END_PROFILE("Processing");

				//	現在描画可能か確認する
				#if	USE_FRAME_CONTROL
					if( FRAME->updateRender() == FALSE ) continue;
				#endif	//~#if USE_FRAME_CONTROL

				//	描画時の処理時間計測
				BEGIN_PROFILE("Draw", kxProfiler::COLOR_BLUE);

				//	描画開始
				KXSYSTEM->beginScene();

				//	描画
				draw();

				//	タスクの描画
				#if USE_TASKSYSTEM
					TASK->render();
				#endif	//~#if USE_TASKSYSTEM

				//	ラインなどの描画
				GRAPHICS->render();

				//	FPSの表示
				#if	USE_FRAME_CONTROL
				{
					PROFILE("debug", kxProfiler::COLOR_BLACK);
					FONT->drawDebugText(0, 20, 0xff00ffff, "FPS:%d", FRAME->getFPS());

					//	プロファイルの表示
					DRAW_PROFILER;
				}
				#endif	//~#if USE_FRAME_CONTROL

				//	描画時の処理時間測定終了
				END_PROFILE("Draw");

				//	描画終了
				KXSYSTEM->endScene();
			}
		}
	}

	//	解放
	cleanup();
	cleanupSystem();

	return static_cast<s32>(msg.wParam);
}

//---------------------------------------------------------------------------
//	ウインドウとフルスクリーンの変更
//---------------------------------------------------------------------------
void	System::changeScreen(void)
{
	if( _isFullScreen ) {
		//	フルスクリーンならウインドウ画面に変更する
		SetMenu(_hWnd, _hMenu);
		SetWindowPos(_hWnd, HWND_NOTOPMOST,
					 _dispRect.left, _dispRect.top,
					 _dispRect.right - _dispRect.left,
					 _dispRect.bottom - _dispRect.top,
					 SWP_SHOWWINDOW);
		SetWindowLong(SYSTEM->getWindowHandle(), GWL_STYLE, WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX | WS_VISIBLE);
	} else {
		//	ウインドウ画面ならフルスクリーンに変更する
		_hMenu = GetMenu(_hWnd);
		SetMenu(_hWnd, NULL);
		GetWindowRect(_hWnd, &_dispRect);
		SetWindowLong(SYSTEM->getWindowHandle(), GWL_STYLE, WS_POPUP | WS_VISIBLE);
	}
	_isFullScreen = !_isFullScreen;
	KXSYSTEM->changeScreenMode();
}

//---------------------------------------------------------------------------
//	ウインドウプロシージャー
//!	@param	hwnd	[in]	ウィンドウハンドル
//!	@param	message	[in]	ウィンドウメッセージ
//!	@param	wparam	[in]	パラメータ引数１
//!	@param	lparam	[in]	パラメータ引数２
//!	@retval	メッセージ処理の結果
//---------------------------------------------------------------------------
LRESULT	CALLBACK System::WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch( msg ) {
	case WM_CREATE:
		return 0L;
	case WM_COMMAND:
		return 0L;
	case WM_KEYDOWN:
		//	キー入力
		switch( wParam ) {
		case VK_ESCAPE:
			//	ESCが押された時、終了処理をする
			PostMessage(hWnd, WM_CLOSE, 0, 0);
			return 0L;
		}
		break;
	case WM_DESTROY:
		//	ウインドウの破棄
		PostQuitMessage(0);
		return 0L;
	}

	//	デフォルトのウィンドウ処理
	return DefWindowProc(hWnd, msg, wParam, lParam);
}

//============================================================================
//	END OF FILE
//============================================================================