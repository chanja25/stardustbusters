//---------------------------------------------------------------------------
//!
//!	@file	Disp.cpp
//!	@brief	画面制御クラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "Global.h"


//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
Display::Display(void)
{
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
Display::~Display(void)
{
}

//---------------------------------------------------------------------------
//	初期化
//---------------------------------------------------------------------------
void	Display::init(void)
{
	//	必要な画像を読み込み
	ZeroMemory(_p2DObj, sizeof(_p2DObj));
	_p2DObj[HP_FRAME]		= new KX2DOBJ("DATA\\GAME\\2DOBJ\\hp_frame.png");
	_p2DObj[HP_HARI]		= new KX2DOBJ("DATA\\GAME\\2DOBJ\\hp_hari.png");
	_p2DObj[REMAIN]			= new KX2DOBJ("DATA\\GAME\\2DOBJ\\s_count.png");
	_p2DObj[SCORE_FRAME]	= new KX2DOBJ("DATA\\GAME\\2DOBJ\\s_frame.png");
	_p2DObj[SCORE_COUNT]	= new KX2DOBJ("DATA\\GAME\\2DOBJ\\s_count.png");
	_p2DObj[M_WINDOW]		= new KX2DOBJ("DATA\\GAME\\2DOBJ\\m_window.png");
	_p2DObj[W_FRAME]		= new KX2DOBJ("DATA\\GAME\\2DOBJ\\w_frame.png");
	_p2DObj[W_METER]		= new KX2DOBJ("DATA\\GAME\\2DOBJ\\w_meter.png");
	_p2DObj[W_AICON]		= new KX2DOBJ("DATA\\GAME\\2DOBJ\\w_aicon.png");
	_p2DObj[W_FLAG]			= new KX2DOBJ("DATA\\GAME\\2DOBJ\\w_flag.png");
	_p2DObj[B_FRAME]		= new KX2DOBJ("DATA\\GAME\\2DOBJ\\b_frame.png");
	_p2DObj[B_HP]			= new KX2DOBJ("DATA\\GAME\\2DOBJ\\b_hp.png");
	_p2DObj[B_AICON]		= new KX2DOBJ("DATA\\GAME\\2DOBJ\\b_aicon.png");

	//	画面サイズの取得
	_width	= static_cast<f32>(SYSTEM->getWidth());
	_height	= static_cast<f32>(SYSTEM->getHeight());

	//	初期設定
	_mFlag	= FALSE;
	_messageBoxY = _height;

	_rotPlayerHp = 0;

	_score = 0;

	//	針用画像の中心を画像の中心に設定する
	_p2DObj[HP_HARI]->setCenter(kx2DObject::CENTER_CENTER);
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	Display::cleanup(void)
{
	for( s32 i=0; i < TEXTURE_MAX; i++ ) {
		SAFE_DELETE(_p2DObj[i]);
	}
}

//---------------------------------------------------------------------------
//	メッセージウィンドウ関連更新
//---------------------------------------------------------------------------
void	Display::updateMessage(void)
{
	if( _mFlag ) {
		//	メッセージボックスを表示する
		_messageBoxY = ( _messageBoxY > _height - 128 )? _messageBoxY - 4: _height - 128;
	} else {
		//	メッセージボックスを隠す
		_messageBoxY = ( _messageBoxY < _height )? _messageBoxY + 4: _height;
	}
}

//---------------------------------------------------------------------------
//	プレイヤーHP関連更新
//---------------------------------------------------------------------------
void	Display::updatePlayerHP(void)
{
	//	プレイヤーのノードを取得する
	Node*	pNode = TASK->getList(SystemTask::LIST_PLAYER)->getFirstNode();

	f32 hp;
	if( pNode ) {
		//	プレイヤーが存在すればノードからプレイヤーを取り出す
		Player*	pPlayer = reinterpret_cast<Player*>(pNode->getObject());

		//	残りHPが何％か取得する
		hp = static_cast<f32>(pPlayer->getHp()) / static_cast<f32>(pPlayer->getMaxHp());
	} else {
		//	プレイヤーが存在しなければHPを0にする
		hp = 0;
	}

	//	針の角度を設定する
	f32 angle = hp * RAD*179 - RAD*91;
	_rotPlayerHp = ( angle >= _rotPlayerHp )? _rotPlayerHp + RAD : _rotPlayerHp - (RAD / 4);
}

//---------------------------------------------------------------------------
//	武器関連更新
//---------------------------------------------------------------------------
void	Display::updateWeapon(void)
{
	//	プレイヤーのノードを取得する
	Node*	pNode = TASK->getList(SystemTask::LIST_PLAYER)->getFirstNode();

	if( pNode ) {
		//	プレイヤーが存在すれば現在の武器の種類を取得する
		_weapon = reinterpret_cast<Player*>(pNode->getObject())->getWeaponType();
	} else _weapon = Player::WEAPON_NONE;
}

//---------------------------------------------------------------------------
//	スコアの表示
//---------------------------------------------------------------------------
void	Display::updateScore(void)
{
	//	現在のスコアを取得する
	u32 score = reinterpret_cast<SceneGame*>(SCENE->getCurrentScene())->getScore();
	//	一定量ずつ表示用スコアを上げていく
	_score = ( _score < score )? _score + 39: score;
}

//---------------------------------------------------------------------------
//	メッセージウィンドウの表示
//---------------------------------------------------------------------------
void	Display::drawMessage(void)
{
	_p2DObj[M_WINDOW]->render(384, _messageBoxY, 512, 128, 0, 0, 512, 128, "copy");
}

//---------------------------------------------------------------------------
//	プレイヤーHP関連表示
//---------------------------------------------------------------------------
void	Display::drawPlayerHP(void)
{
	//	プレイヤーのノードを取得
	Node*	pNode = TASK->getList(SystemTask::LIST_PLAYER)->getFirstNode();

	//	HP表示用のフレーム表示
	_p2DObj[HP_FRAME]->render(0, _height - 256, 512, 256, 0, 0, 512, 256, "copy");

	f32 width = 0;
	if( pNode ) {
		//	ノードからプレイヤーを取得
		Player*	pPlayer = reinterpret_cast<Player*>(pNode->getObject());

		//	残機の表示設定
		width = static_cast<f32>(pPlayer->getRemain()) * 64;
	}

	//	残機とHPの表示
	_p2DObj[HP_HARI]->render(104, _height - 125, 256, 128, _rotPlayerHp, 0, 0, 256, 128, "copy");
	_p2DObj[REMAIN]->render(225, _height - 110, 100, 100, width, 0, 64, 64, "copy");
}

//---------------------------------------------------------------------------
//	武器関連表示
//---------------------------------------------------------------------------
void	Display::drawWeapon(void)
{
	f32 height;

	//	武器の表示設定
	switch( _weapon ) {
	case Player::WEAPON_NONE:
		height = 0;
		break;
	case Player::WEAPON_GUN:
		height = 64;
		break;
	case Player::WEAPON_MISSILE:
		height = 128;
		break;
	case Player::WEAPON_LASER:
		height = 192;
		break;

	default:
		ASSERT(false, "エラー(武器関連表示)");
		break;
	}

	//	武器情報用のフレームを表示
	_p2DObj[W_FRAME]->render(_width - 256, _height - 256, 256, 256, 0, 0, 256, 256, "copy");

	//	現在の武器のアイコン表示
	_p2DObj[W_AICON]->render(_width - 170, _height - 140, 256, 64, 0, height, 256, 64, "copy");
	//	武器の換装フラグ表示
	_p2DObj[W_FLAG]->render(_width - 118, _height - 208, 128, 64, 0, (height == 0)? 0: 64.0f, 128, 64, "copy");

	if( _weapon != Player::WEAPON_NONE ) {
		//	ノードからプレイヤーを取得
		Node*	pNode = TASK->getList(SystemTask::LIST_PLAYER)->getFirstNode();

		//	現在の武器の状態を取得し、表示
		f32	meter = reinterpret_cast<Weapon*>(TASK->getList(SystemTask::LIST_WEAPON)->getFirstNode()->getObject())->getMeter();
		_p2DObj[W_METER]->render(_width - 176, _height - 57, 166 * meter, 50, 0, 0, 128 * meter, 64, "copy");
	}
}

//---------------------------------------------------------------------------
//	ボスHP関連表示
//---------------------------------------------------------------------------
void	Display::drawBossHP(void)
{
	//	ボスのノードを取得
	Node* pNode = TASK->getList(SystemTask::LIST_BOSS)->getFirstNode();

	if( pNode ) {
		//	ボスが存在すればノードからボスを取得
		Boss* pBoss = reinterpret_cast<Boss*>(pNode->getObject());
		//	現在のHPの割合を取得
		f32	hp = static_cast<f32>(pBoss->getHp()) / static_cast<f32>(pBoss->getMaxHp());

		//	ボス用のフレーム、アイコン、HPを表示する
		_p2DObj[B_FRAME]->render(_width - 500, -3, 512, 128, 0, 0, 512, 128, "copy");
		_p2DObj[B_HP]->render(_width - 363, 65, hp * 353, 39, 0, 0, hp * 256, 128, "copy");
		_p2DObj[B_AICON]->render(_width - 464, 20, 80, 80, 0, 0, 256, 256, "copy");
	}
}

//---------------------------------------------------------------------------
//	スコアの表示
//---------------------------------------------------------------------------
void	Display::drawScore(void)
{
	//	スコアの初期表示位置
	f32 x = 320;
	f32 y = 6;

	//	スコア用のフレームを表示
	_p2DObj[SCORE_FRAME]->render(0, 0, 512, 100, 0, 0, 256, 128, "copy");

	if( _score == 0 ) {
		//	スコアが0の場合、0を表示
		_p2DObj[SCORE_COUNT]->render(x, y, 80, 80, 0, 0, 64, 64, "copy");
		return;
	}

	s32 i, j, k, l;
	i = 0;
	j = 10;
	k = ( _score < 1000000 )? _score: k = 999999;

	//	スコアを1の桁から順に表示していく
	while( k != 0 ) {
		l = k % j;
		_p2DObj[SCORE_COUNT]->render(x - static_cast<f32>(i * 60), y, 80, 80, static_cast<f32>(l % 4 * 64), static_cast<f32>(l / 4 * 64), 64, 64, "copy");

		k /= 10;
		i++;
	}
}

//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	Display::update(void)
{
	updateMessage();
	updatePlayerHP();
	updateWeapon();
	updateScore();
}

//---------------------------------------------------------------------------
//	描画
//---------------------------------------------------------------------------
void	Display::render(void)
{
	drawMessage();
	drawPlayerHP();
	drawWeapon();
	drawBossHP();
	drawScore();
}

//============================================================================
//	END OF FILE
//============================================================================