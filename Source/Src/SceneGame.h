//---------------------------------------------------------------------------
//!
//!	@file	SceneGame.h
//!	@brief	ゲームシーンクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#ifndef	__SCENEGAME_H__
#define __SCENEGAME_H__

#pragma	once


//===========================================================================
//!	ゲームシーンクラス
//===========================================================================
class SceneGame : public SceneBase
{
public:
	//!	ステップ
	enum STEP {
		STEP_SETUP,
		STEP_MAIN,
		STEP_OVER,
		STEP_CLEAR,
	};

	//!	サウンド
	enum SOUND {
		SOUND_BGM,
		SOUND_BOSS,
		SOUND_GUN,
		SOUND_MISSILE,
		SOUND_LASER,
	};

	//---------------------------------------------------------------------------
	//!	@name 初期化
	//---------------------------------------------------------------------------
	//!@{

	//!	コンストラクタ
	SceneGame(void);
	//!	デストラクタ
	~SceneGame(void);

	//!	初期化
	void	init(void);
	//!	解放
	void	cleanup(void);

	//!@}

	//!	更新
	void	update(void);

	//-------------------------------------------------------------
	//!	@name 取得参照
	//-------------------------------------------------------------
	//!@{

	//!	スコアの加算
	//!	@param	score	[in] 加算するスコア
	void		addScore(s32 score) { _score += score; }
	//!	スコアの取得
	//!	@retval	スコア
	s32			getScore(void) { return _score; }
	//!	スクリプトの取得
	//!	@retval	スクリプト
	GameScript*	getScript(void) { return &_script; }

	//!@}

private:
	//!	使用するテクスチャー
	enum TEXTURE {
		HP_FRAME,
		HP_HARI,
		SCORE_FRAME,
		COUNT,
		M_WINDOW,
		W_FRAME,
		W_METER,
		W_AICON,
		W_FLAG,
		B_FRAME,
		B_HP,
		B_AICON,
		TEXTURE_MAX,
	};
	LPKX2DOBJ	_pTexture[TEXTURE_MAX];	//!< 使用する画像読み込み用

	//!	使用する3DOBJ
	enum OBJ3D {
		PLAYER,
		ENEMY_1,
		ENEMY_2,
		ENEMY_3,
		BOSS,
		OBJ3D_MAX,
	};
	LPKX3DOBJ	_p3DObj[OBJ3D_MAX];		//!< 使用する3DOBJ読み込み用

	//!	使用するメッシュ
	enum MESH {
		STAGE,
		SKY,
		GUN,
		MISSILE,
		LASER,
		BULLET,
		M_BULLET,
		L_BULLET,
		MESH_MAX,
	};
	LPKXMESH	_pMesh[MESH_MAX];		//!< 使用するメッシュ読み込み用

	Player*		_pPlayer;	//!< プレイヤー

	Stage*		_pStage;	//!< ステージ
	Sky*		_pSky;		//!< 空

	Display*	_pDisp;		//!< 画面上の2D制御用

	u32			_score;		//!< スコア

	u8			_step;		//!< 進行状態用
	u32			_count;		//!< カウント用

	GameScript	_script;	//!< スクリプト
};


#endif	//~#if __SCENEGAME_H__

//============================================================================
//	END OF FILE
//============================================================================