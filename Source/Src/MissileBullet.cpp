//---------------------------------------------------------------------------
//!
//!	@file	MissileBullet.cpp
//!	@brief	ミサイルバレットクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "Global.h"


//	ホーミング速度
const f32	MissileBullet::HOMING_SPEED	= 0.3f;

//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
MissileBullet::MissileBullet(void)
: _pTarget(NULL)
{
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
MissileBullet::~MissileBullet(void)
{
}

//---------------------------------------------------------------------------
//	初期化
//---------------------------------------------------------------------------
void	MissileBullet::init(void)
{
	//	メッシュの読み込み
	_obj.load("DATA\\GAME\\MESH\\WEAPON\\m_bullet.kmo");

	//	当たり判定用オブジェクトの初期化
	_attack.init(	_attack.SHAPE_CAPSULE,
					_attack.ATT_BULLET,
					_attack.ATT_ENEMY	);
	_attack.setWork(_attack.WORK_POWER, 20);

	_scale	 = 0.003f;
	_dist	 = _scale * 10.0f;
	_endDist = 100.0f;
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	MissileBullet::cleanup(void)
{
}

//---------------------------------------------------------------------------
//	セット
//!	@param	pos		[in]	初期位置
//!	@param	vec		[in]	向き
//!	@param	speed	[in]	速度
//!	@param	scale	[in]	サイズ
//---------------------------------------------------------------------------
void	MissileBullet::set(const VECTOR3& pos, const VECTOR3& vec, f32 speed)
{
	//	パラメータの設定
	_oldPos = _pos = pos;
	MATH->normalize(&_move, vec);
	VECTOR3 v1, v2(0, 0, 1);
	f32	angle = acosf(MATH->dot(_move, v2));
	MATH->cross(&v1, _move, v2);
	if( v1 == VECTOR3(0, 0, 0) ) v1 = VECTOR3(0, 0, 1);
	else MATH->normalize(&v1, v1);
	MATH->axisRotationMatrix(&_rot, v1, angle);

	_speed = speed;
}

//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	MissileBullet::update(void)
{
  	if( _pTarget ) {
		//	ターゲットが一定間内に存在するか確認する
		if( _pTarget->getPos().z - _pos.z > 0 ) {
			VECTOR3 vec;
			//	ターゲットへのベクトルを取得する
 			vec = _pTarget->getHitPos() - _pos;
			vec.z = 0;
			//	ターゲットまでへの距離を求める
			if( MATH->length(vec) > HOMING_SPEED ) {
				//	ターゲットへ近づける
				MATH->normalize(&vec, vec);
				_pos += vec * HOMING_SPEED;
			} else {
				//	ターゲットの位置にミサイルを合わせる
				_pos.x = _pTarget->getHitPos().x;
				_pos.y = _pTarget->getHitPos().y;
			}
			VECTOR3 pos;
			VECTOR3 move(0, 0, 0);
			VECTOR3 power(0, 0, 0);
			//	ターゲットの位置へロックオンマーカーを表示する
			pos = _pTarget->getHitPos();
			pos.z -= 0.5f;
			PARTICLE->setParticle(0, 13, 0, 0xffffffff, 0, 0xffffffff, 1, 0xffffff, pos, move, power, 0, 0.3f, 1.0f);

			//	ターゲットが消滅する時、ターゲットを外す
			if( _pTarget->getState() == STATE_INVALID ) _pTarget = NULL;
		} else {
			//	ターゲットを外す
			_pTarget = NULL;
		}
	}

	Bullet::update();
}

//============================================================================
//	END OF FILE
//============================================================================