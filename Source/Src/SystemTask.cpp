//---------------------------------------------------------------------------
//!
//!	@file	SystemTask.cpp
//!	@brief	システムタスククラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "Global.h"


//	インスタンスの実体生成
SystemTask	SystemTask::_instance;

//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
SystemTask::SystemTask(void)
: _pList(NULL)
{
}

//---------------------------------------------------------------------------
//	初期化
//---------------------------------------------------------------------------
void	SystemTask::init(void)
{
	ASSERT(_pList == NULL, "既にシステムタスクは初期化されています");

	//	リストを必要数生成する
	_pList = new List[LIST_MAX];
	ZeroMemory(_pList, sizeof(List) * LIST_MAX);

	//	属性の設定
	_attribute = ATT_UPDATE | ATT_RENDER;

	_deleteMode	= TRUE;
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	SystemTask::cleanup(void)
{
	//	全リストの消去
	killAll();
	//	リストの削除
	SAFE_DELETES(_pList);
}

//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	SystemTask::update(void)
{
	#if	DEBUG
		if( GetAsyncKeyState(VK_F3) & 0x8000 ) _attribute = ATT_RENDER;
		if( GetAsyncKeyState(VK_F4) & 0x8000 ) _attribute = ATT_ALL;
	#endif	//~#if	DEBUG

	//	属性を確認して、更新可能か確かめる
	if( (_attribute & ATT_UPDATE) == 0 ) return;

	//	リスト毎に更新処理を行う
	for( u32 i=0; i < LIST_MAX; i++ ) {
		update(&_pList[i]);
	}
}

//---------------------------------------------------------------------------
//	リストの更新
//!	@param	pLine	[in/out] 更新するリスト
//---------------------------------------------------------------------------
void	SystemTask::update(List* pLine)
{
	//	リストからノードを取得する
	Node* pNode = pLine->getFirstNode();

	TaskBase*	pTask;
	Node*		pNext;

	//	タスクを更新していく
	while( pNode ) {
		//	次のノードを取得しておく
		pNext = pNode->getNext();
		//	タスクを取り出す
		pTask = reinterpret_cast<TaskBase*>(pNode->getObject());

		//	ステータス毎の処理を行う
		switch( pTask->getState() ) {
		case STATE_SETUP:
			pTask->init();
			pTask->setState(STATE_RUNNING);
			//break;
		case STATE_RUNNING:
			if( pTask->getAttribute() & ATT_UPDATE ) {
				pTask->update();
			}
			break;
		case STATE_INVALID:
			pTask->setState(STATE_DELETE);
			break;
		case STATE_DELETE:
			killTask(pTask);
			break;

		default:
			ASSERT( false, "タスクが存在しない処理を行おうとしています" );
			break;
		}

		pNode = pNext;
	}
}

//---------------------------------------------------------------------------
//	描画
//---------------------------------------------------------------------------
void	SystemTask::render(void)
{
	//	属性を確認して、描画可能か確かめる
	if( (_attribute & ATT_RENDER) == 0 ) return;

	//	ライン毎に描画処理を行う
	for( u32 i=0; i < LIST_MAX; i++ ) {
		render(&_pList[i]);
	}
}

//---------------------------------------------------------------------------
//	リストの描画
//!	@param	pLine	[in] 描画するリスト
//---------------------------------------------------------------------------
void	SystemTask::render(List* pLine)
{
	//	リストからノードを取得する
	Node* pNode = pLine->getFirstNode();

	//	タスクの取得用ポインタ
	TaskBase* pTask;

	//	タスクを更新していく
	while( pNode ) {
		//	タスクを取り出す
		pTask = reinterpret_cast<TaskBase*>(pNode->getObject());

		//	描画処理を行う
		if( (pTask->getAttribute() & ATT_RENDER) && pTask->isAlive() ) {
			pTask->render();
		}

		//	次のノードへ進める
		pNode = pNode->getNext();
	}
}

//---------------------------------------------------------------------------
//	全リストの削除
//---------------------------------------------------------------------------
void	SystemTask::killAll(void)
{
	//	ノードの取得用ポインタ
	Node*		pNode;
	//	タスクの取得用ポインタ
	TaskBase*	pTask;
	//	Nextノードの取得用ポインタ
	Node*		pNext;

	//	リスト毎に描画処理を行う
	for( u32 i=0; i < LIST_MAX; i++ ) {
		pNode = _pList[i].getFirstNode();
		while( pNode ) {
			//	次のノードを取得しておく
			pNext = pNode->getNext();

			//	タスクを取り出し削除する
			pTask = reinterpret_cast<TaskBase*>(pNode->getObject());
			killTask(pTask);

			//	次のノードへ進める
			pNode = pNext;
		}
	}
}

//---------------------------------------------------------------------------
//	対象タスクの削除
//!	@param	pTask	[in/out] 削除するタスク
//---------------------------------------------------------------------------
void	SystemTask::killTask(TaskBase* pTask)
{
	if( pTask == NULL ) return;

	//	所属しているリストの取得
	List* pList = pTask->getNode()->getList();

	//	リストからの消去
	pList->deleteNode(pTask->getNode());

	//	タスクの解放
	pTask->cleanup();
	if( pTask->getDeleteMode() && _deleteMode ) {
 		SAFE_DELETE(pTask);
	}
}

//---------------------------------------------------------------------------
//	対象のリストの先頭に、タスクを追加
//!	@param	pTask	[in] 追加するタスク
//!	@param	list	[in] 追加するリストの種類
//---------------------------------------------------------------------------
void	SystemTask::addTop(TaskBase *pTask, u32 list, b32 deleteMode)
{
	ASSERT(list < LIST_MAX, "リストが存在しません");

	if( pTask == NULL ) return;

	//	削除モードの設定
	pTask->setDeleteMode(deleteMode);

	//	ノードをリストに追加する
	_pList[list].addNodeFirst(pTask->getNode());
}

//---------------------------------------------------------------------------
//	対象のリストの先頭に、タスクを追加
//!	@param	pTask	[in] 追加するタスク
//!	@param	list	[in] 追加するリストの種類
//---------------------------------------------------------------------------
void	SystemTask::addBottom(TaskBase *pTask, u32 list, b32 deleteMode)
{
	ASSERT(list < LIST_MAX, "リストが存在しません");

	if( pTask == NULL ) return;

	//	削除モードの設定
	pTask->setDeleteMode(deleteMode);

	//	ノードをリストに追加する
	_pList[list].addNodeEnd(pTask->getNode());
}

//============================================================================
//	END OF FILE
//============================================================================