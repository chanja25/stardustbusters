//===========================================================================
//!
//!	@file		Stage.h
//!	@brief		ステージクラス
//!
//!	@author S.Kawamoto
//===========================================================================
#ifndef	__STAGE_H__
#define	__STAGE_H__

#pragma	once


//===========================================================================
//! ステージクラス
//===========================================================================
class Stage : public MeshObj
{
public:
	//-------------------------------------------------------------
	//!	@name 初期化
	//-------------------------------------------------------------
	//!@{

	//!	コンストラクタ
	Stage(void);

	//!	デストラクタ
	~Stage(void);

	//!	初期化
	void	init(void);
	//!	解放
	void	cleanup(void);

	//!@}

	//!	更新
	void	update(void);
	//!	描画
	void	render(void);

private:
};

#endif	//~#if __STAGE_H__

//============================================================================
//	END OF FILE
//============================================================================