//---------------------------------------------------------------------------
//!
//!	@file	SceneTitle.cpp
//!	@brief	タイトルシーンクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "Global.h"


//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
SceneTitle::SceneTitle(void)
: _step(0)
, _select(0)
, _count(0)
{
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
SceneTitle::~SceneTitle(void)
{
}

//---------------------------------------------------------------------------
//	初期化
//---------------------------------------------------------------------------
void	SceneTitle::init(void)
{
	//	タスクの生成
	for( u32 i=0; i < TEXTURE_MAX; i++ ) {
		_pObj2D[i] = new Task2D();
	}
	for( u32 i=0; i < Score::SCORE_MAX; i++ ) {
		_pScore[i] = new ScoreDisp();
		_pScore[i]->setScore(SCORE->getScore(i));
	}

	//	サウンドの読み込み
	AUDIO->load(SOUND_BGM, "DATA\\TITLE\\SOUND\\Title.wav");

	//	ファイルの読み込み
	_pObj2D[TEAM_LOGO]->load("DATA\\TITLE\\TEAM_LOGO.png");
	_pObj2D[BACK]->load("DATA\\TITLE\\BACK.png");
	_pObj2D[GAME_LOGO]->load("DATA\\TITLE\\GAME_LOGO.png");
	_pObj2D[ENTER]->load("DATA\\TITLE\\ENTER.png");
	_pObj2D[BUTTON1]->load("DATA\\TITLE\\BUTTON.png");
	_pObj2D[BUTTON2]->load("DATA\\TITLE\\BUTTON.png");
	_pObj2D[BUTTON3]->load("DATA\\TITLE\\BUTTON.png");
	_pObj2D[RESULT]->load("DATA\\TITLE\\BUTTON.png");

	//	スコアの読み込み
	for( u32 i=0; i < Score::SCORE_MAX; i++ ) {
		_pScore[i]->load("DATA\\TITLE\\count.png");
	}

	//	画面のサイズを取得
	_width	= static_cast<f32>(SYSTEM->getWidth());
	_height	= static_cast<f32>(SYSTEM->getHeight());

	//	タスクの初期設定
	_pObj2D[TEAM_LOGO]->set(_width / 2 - 360, _height / 2 - 220, 720, 440, 0, 0, 512, 256, "copy");
	_pObj2D[BACK]->set(0, 0, _width, _height, 0, 0, 512, 512, "copy");
	_pObj2D[GAME_LOGO]->set(_width / 2 - 360, _height / 2 - 300, 720, 320, 0, 0, 512, 256, "copy");
	_pObj2D[ENTER]->set(_width / 2 - 160, _height / 2 + 64, 320, 96, 0, 0, 256, 64, "copy");
	_pObj2D[BUTTON1]->set(_width / 2 - 160, _height / 2 + 100, 320, 96, 0,   0, 512, 128, "copy");
	_pObj2D[BUTTON2]->set(_width / 2 - 160, _height / 2 + 100, 320, 96, 0, 256, 512, 128, "copy");
	_pObj2D[BUTTON3]->set(_width / 2 - 160, _height / 2 + 200, 320, 96, 0, 512, 512, 128, "copy");
	_pObj2D[RESULT]->set(_width / 2 - 256, _height / 2 - 320, 512, 128, 0, 768, 512, 128, "copy");

	//	スコアの設定
	for( u32 i=0; i < Score::SCORE_MAX; i++ ) {
		_pScore[i]->set(_width / 2 + 150, _height / 2 + 200 - static_cast<f32>(i) * 100, 100, 100, 0, 0, 64, 64, "copy");
	}

	//	タスクに追加する
	for( u32 i=0; i < TEXTURE_MAX; i++ ) {
		TASK->addBottom(_pObj2D[i], SystemTask::LIST_2D);
	}
	for( u32 i=0; i < Score::SCORE_MAX; i++ ) {
		TASK->addBottom(_pScore[i], SystemTask::LIST_2D);
	}

	//	サウンドの再生
	AUDIO->play(SOUND_BGM, TRUE);
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	SceneTitle::cleanup(void)
{
	//	タスクの中身を削除する
	TASK->killAll();

	//	サウンドの解放
	AUDIO->cleanupAll();
}

//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	SceneTitle::update(void)
{
	switch( _step ) {
	case STEP_START:	//	開始
		//	フェイドイン
		FADE->fadeIn(0, 3);
		_select = 0;

		//	全ての表示を消す
		for( u32 i=0; i < TEXTURE_MAX; i++ ) {
			_pObj2D[i]->setAttribute(TaskBase::ATT_NONE);
		}
		for( u32 i=0; i < Score::SCORE_MAX; i++ ) {
			_pScore[i]->setAttribute(SystemTask::ATT_NONE);
		}
		//	チームロゴの表示
		_pObj2D[TEAM_LOGO]->setAttribute(SystemTask::ATT_ALL);
		_step++;
	case STEP_TEAM_LOGO_IN:	//	ロゴの表示
		//	フェードインが終わっているか確認
		if( FADE->getFlag() ) _count++;
		if( _count > 90 ) {
			//	120フレーム経つと次に進む
			FADE->fadeOut(0, 3);
			_count = 0;
			_step++;
		}
		break;
	case STEP_TEAM_LOGO_OUT:	//	ロゴを消す
		//	フェードインが終わっているか確認
		if( FADE->getFlag() ) _count++;
		if( _count == 1 ) {
			//	チームロゴを消す
			_pObj2D[TEAM_LOGO]->setAttribute(SystemTask::ATT_NONE);
			//	背景、ロゴを表示する
			_pObj2D[BACK]->setAttribute(SystemTask::ATT_ALL);
			_pObj2D[GAME_LOGO]->setAttribute(SystemTask::ATT_ALL);
		}
		if( _count > 90 ) {
			//	90フレーム経つと、フェードインして次へ進む
			FADE->fadeIn(0, 3);
			_count = 0;
			_step++;
		}
		break;
	case STEP_TITLE:	//	タイトル表示
		_count++;
		//	一定間隔でENTERを表示する
		_pObj2D[ENTER]->setAttribute(SystemTask::ATT_ALL * ((_count >> 5) & 0x01));
		//	フェードインが終わってからスタートボタンを押すと進行する
		if( INPUT->isKeyPush(kxInput::KEY_START) && FADE->getFlag() ) {
			_pObj2D[ENTER]->setAttribute(SystemTask::ATT_NONE);
			_pObj2D[BUTTON1]->setAttribute(SystemTask::ATT_ALL);
			_pObj2D[BUTTON2]->setAttribute(SystemTask::ATT_ALL);
			_pObj2D[BUTTON3]->setAttribute(SystemTask::ATT_ALL);
			_count = 0;
			_step++;
		}
		break;
	case STEP_TITLE_SELECT:	//	タイトル表示（モード選択）
		//	ボタンの表示制御
		_pObj2D[BUTTON1]->set(_width / 2 - 160, _height / 2,		  320, 96, 0,   0 + 128.0f * (_select == 0), 512, 128, "copy");
		_pObj2D[BUTTON2]->set(_width / 2 - 160, _height / 2 + 100, 320, 96, 0, 256 + 128.0f * (_select == 1), 512, 128, "copy");
		_pObj2D[BUTTON3]->set(_width / 2 - 160, _height / 2 + 200, 320, 96, 0, 512 + 128.0f * (_select == 2), 512, 128, "copy");
		//	上下を押す事で選択
		if( INPUT->isKeyPush(kxInput::KEY_UP) ) {
			_select = (_select > 0)? _select - 1: 2;
		}
		if( INPUT->isKeyPush(kxInput::KEY_DOWN) ) {
			_select = (_select < 2)? _select + 1: 0;
		}
		//	スタートボタンで選択を決定する
		if( INPUT->isKeyPush(kxInput::KEY_START) ) {
			_pObj2D[BUTTON1]->set(_width / 2 - 160, _height / 2      , 320, 96, 0, 0   + 128.0f * (_select == 0), 512, 128, "copy");
			_pObj2D[BUTTON2]->set(_width / 2 - 160, _height / 2 + 100, 320, 96, 0, 256 + 128.0f * (_select == 1), 512, 128, "copy");
			_pObj2D[BUTTON3]->set(_width / 2 - 160, _height / 2 + 200, 320, 96, 0, 512 + 128.0f * (_select == 2), 512, 128, "copy");
			//	フェードアウトして次へ進む
			FADE->fadeOut(0, 3);
			_count = 0;
			_step++;
		}
		break;
	case STEP_TITLE_OUT:	//	タイトルを消す
		//	フェードアウトが終わって60フレームすると選択した所へ切り替わる
		if( FADE->getFlag() ) _count++;
		if( _count > 60 ) {
			//	ゲームシーンへ進む
			if( _select == 0 )		SCENE->jumpScene(SystemScene::SCENE_GAME);
			//	スコア表示へ進む
			else if( _select == 1 )	_step++;
			//	ゲームを終了する
			else					_step = STEP_EXIT;
			return;
		}
		break;
	case STEP_SCORE_IN:	//	スコア表示
		//	フェードアウトが終わっているか確認
		if( FADE->getFlag() ) {
			//	フェードインする
			FADE->fadeIn(0, 3);
			//	ロゴとボタンを消す
			_pObj2D[GAME_LOGO]->setAttribute(SystemTask::ATT_NONE);
			_pObj2D[BUTTON1]->setAttribute(SystemTask::ATT_NONE);
			_pObj2D[BUTTON2]->setAttribute(SystemTask::ATT_NONE);
			_pObj2D[BUTTON3]->setAttribute(SystemTask::ATT_NONE);
			//	リザルトとスコアを表示して次へ進む
			_pObj2D[RESULT]->setAttribute(SystemTask::ATT_ALL);
			for( u32 i=0; i < Score::SCORE_MAX; i++ ) {
				_pScore[i]->setAttribute(SystemTask::ATT_ALL);
			}
			_step++;
		}
		break;
	case STEP_SCORE:	//	スコア表示
		//	フェードインが終わっているか確認
		if( INPUT->isKeyPush(kxInput::KEY_START) && FADE->getFlag() ) {
			//	キー入力でフェードアウトして次へ進む
			FADE->fadeOut(0, 3);

			_step++;
		}
		break;
	case STEP_SCORE_OUT:	//	スコアを消す
		//	フェードアウトが終わっているか確認
		if( FADE->getFlag() ) {
			//	タイトルの最初へ戻る
			_step = STEP_START;
			_count = 0;
		}
		break;
	case STEP_EXIT:	//	ゲーム終了
		//	フェードアウトが終わっていれば終了する
		if( FADE->getFlag() ) PostQuitMessage(0);
		break;

	default:
		ASSERT(false, "存在しないステップに移行しました");
		break;
	}
}

//============================================================================
//	END OF FILE
//============================================================================