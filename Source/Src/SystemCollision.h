//===========================================================================
//!
//!	@file		SystemCollision.h
//!	@brief		SystemCollisionクラス
//!
//===========================================================================
#ifndef	__SYSTEMCOLLISION_H__
#define __SYSTEMCOLLISION_H__

#pragma once


//===========================================================================
//!	SystemCollisionクラス
//===========================================================================
class SystemCollision
{
public:
	//===========================================================================
	//!	Objectクラス
	//===========================================================================
	class Object {
	public:
		//!	当たりを取る形
		enum SHAPE {
			SHAPE_SPHERE,
			SHAPE_CAPSULE,
		};
		//!	当たりの属性
		enum ATTRIBUTE {
			ATT_NONE			= 0,
			ATT_PLAYER			= (1 << 0),
			ATT_ENEMY			= (1 << 1),
			ATT_BULLET			= (1 << 2),
		};
		//! 予備変数の使用用途
		enum WORK {
			WORK_POWER,
			WORK_DAMAGE,
			WORK_MAX,
		};

		//-------------------------------------------------------------
		//!	@name 初期化
		//-------------------------------------------------------------
		//!@{

		//!	コンストラクタ
		Object(void);
		//!	デストラクタ
		~Object(void){};

		//!	初期設定
		void	init(u32 shape, u32 attribute, u32 hitAttribute = 0, b32 hitDelete = FALSE);
		//!	オブジェクト当たりのセット
		void	setObject(const VECTOR3& pos, f32 radius, const VECTOR3& pos2 = VECTOR3(0, 0, 0));

		//!@}

		//-------------------------------------------------------------
		//!	@name 取得、設定
		//-------------------------------------------------------------
		//!@{

		//!	位置の取得
		//!	@retval	位置の取得
		inline VECTOR3	getPos(void) { return _pos; }
		//!	カプセル用の位置の取得
		//!	@retval	カプセル用の位置
		inline VECTOR3	getPos2(void) { return _pos2; }
		//!	衝突した位置の設定
		//!	@param	pos	[in] 衝突した位置
		inline void		setHitPos(const VECTOR3& pos) { _hitPos = pos; }
		//!	衝突した位置の取得
		//!	@retval	衝突した位置
		inline VECTOR3	getHitPos(void) { return _hitPos; }
		//!	半径の取得
		//!	@retval	半径
		inline f32		getRadius(void) { return _radius; }

		//!	ステータスのセット
		//!	@param	state	[in] ステータス
		inline void		setState(u32 state) { _state = state; }
		//!	ステータスの取得
		//!	@retval	状態
		inline u32		getState(void) { return _state; }
		//!	形の取得
		//!	@retval	形
		inline u32		getShape(void) { return _shape; }

		//!	属性の取得
		//!	@retval	属性
		inline u32		getAttribute(void) { return _attribute; }
		//!	攻撃属性の取得
		//!	@retval	攻撃属性
		inline u32		getHitAttribute(void) { return _hitAttribute; }
		//!	衝突時に攻撃判定を残すか否かのフラグの取得
		//!	@retval	TRUEの場合、衝突後も当たり判定を残す。FALSEの場合、当たり判定を消去する
		inline b32		getHitDelete(void) { return _hitDelete; }

		//!	予備変数の取得
		//!	@param	no		[in] 番号
		//!	@param	work	[in] 格納数値
		inline void		setWork(u32 no, u32 work) { _work[no] = work; }
		//!	予備変数の取得
		//!	@retval	予備変数
		inline u32		getWork(u32 no) { return _work[no]; }

		//!	ノードの取得
		//!	@retval	ノード
		inline Node*	getNode(void) { return &_node; }

		//!@}

	private:
		VECTOR3 _pos;			//!< 位置
		VECTOR3 _pos2;			//!< カプセル型等の為の予備位置
		VECTOR3 _hitPos;		//!< 衝突した位置
		f32		_radius;		//!< 半径
		u32		_state;			//!< 状態
		u32		_shape;			//!< 形
		u32		_attribute;		//!< 属性
		u32		_hitAttribute;	//!< 攻撃用属性
		b32		_hitDelete;		//!< 衝突時に攻撃判定を残すか否かのフラグ

		s32		_work[WORK_MAX];//!< 予備変数

		Node	_node;			//!< リスト管理用のノード
	};

	//-------------------------------------------------------------
	//!	@name 初期化
	//-------------------------------------------------------------
	//!@{

	//!	初期化
	void	init(void);
	//!	解放
	void	cleanup(void);

	//!@}

	//!	更新
	void	update(void);

	//-------------------------------------------------------------
	//!	@name 当たり判定
	//-------------------------------------------------------------
	//!@{

	//!	球と球の当たり判定
	b32		collisionSphere(const VECTOR3& pos1, f32 radius1, const VECTOR3& pos2, f32 radius2, VECTOR3* pOut = NULL);
	//!	球とカプセルの当たり判定
	b32		collisionCapsule(const VECTOR3& pos, f32 radius1, const VECTOR3& pos1, const VECTOR3& pos2, f32 radius2, VECTOR3* pOut = NULL);

	//!	当たりオブジェクト追加
	void	setObject(Object* pObject);
	//!	オブジェクトの当たり判定
	b32		collision(Object* pObject, Object* pAttack);

	//!@}


	//-------------------------------------------------------------
	//!	@name 取得参照
	//-------------------------------------------------------------
	//!@{

	//!	インスタンスの取得
	//!	@retval	インスタンス
	inline static	SystemCollision*	getInstance(void) { return &_instance; };

	//!@}

private:
	//	コンストラクタ
	SystemCollision(void){};
	//	デストラクタ
	~SystemCollision(void){};

	static	SystemCollision	_instance;	//!< 唯一のインスタンス

	List	_objectList;	//!< オブジェクトの当たり用リスト
	List	_attackList;	//!< 攻撃の当たり用リスト
};

//! SYSTEMCOLLISIONの取得関数のマクロ
#define	COLLISION	(SystemCollision::getInstance())

#endif	//~#if __SYSTEMCOLLISION_H__

//============================================================================
//	END OF FILE
//============================================================================