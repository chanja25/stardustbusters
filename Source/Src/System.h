//---------------------------------------------------------------------------
//!
//!	@file	System.h
//!	@brief	システムクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#ifndef	__SYSTEM_H__
#define __SYSTEM_H__

#pragma	once


//===========================================================================
//!	システムクラス
//===========================================================================
class System
{
public:
	//---------------------------------------------------------------------------
	//!	@name 初期化
	//---------------------------------------------------------------------------
	//!@{

	//! コンストラクタ
	System(void);

	//!@}

	//!	メインループ
	s32		mainLoop(void);

	//!	ウインドウとフルスクリーンの変更
	void	changeScreen(void);

	//---------------------------------------------------------------------------
	//!	@name 取得参照
	//---------------------------------------------------------------------------
	//!@{

	//!	ウインドウハンドルの取得
	//!	@retval	ウインドウハンドル
	inline	HWND	getWindowHandle(void) { return _hWnd; }
	//!	フルスクリーンハンドルの取得
	//!	@retval	TRUE	フルスクリーンモード
	//!	@retval	FALSE	ウィンドウモード
	inline	b32		getFullScreen(void)	{ return _isFullScreen; }
	//!	画面のサイズ、位置取得
	//!	@retval	画面の座標4点
	inline	RECT	getDispRect(void) { return _dispRect; }
	//!	画面のサイズ取得
	//!	@retval	横幅
	inline	u32		getWidth(void)	{ return _dispRect.right - _dispRect.left; }
	//!	画面のサイズ取得
	//!	@retval	縦幅
	inline	u32		getHeight(void)	{ return _dispRect.bottom - _dispRect.top; }

	//!	インスタンスの取得
	//!	@retval	インスタンス取得
	inline	static	System*	getInstance(void) { return _pInstance; }

	//!@}

	//---------------------------------------------------------------------------
	//!	@name 継承して実装する基本関数
	//---------------------------------------------------------------------------
	//!@{
protected:
	//!	初期化
	virtual b32		init(void){ return TRUE; }
	//!	解放
	virtual void	cleanup(void){}
	//!	更新
	virtual b32		update(void){ return TRUE; }
	//!	描画
	virtual void	draw(void){}

	//!@}

	b32		_isFullScreen;	//!< フルスクリーンフラグ
	RECT	_dispRect;		//!< 画面のサイズ
	HWND	_hWnd;			//!< ウインドウハンドル
private:
	//!	ウィンドウプロシージャ
	static	LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

	static	System*	_pInstance;	//!< 唯一のインスタンス

	//!	初期化
	b32		initSystem(void);
	//!	解放
	void	cleanupSystem(void);

	HMENU	_hMenu;	//!< メニューハンドル
};

//!	インスタンス取得用マクロ
#define	SYSTEM	(System::getInstance())

#endif	//~#if __SYSTEM_H__

//============================================================================
//	END OF FILE
//============================================================================