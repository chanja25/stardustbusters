//---------------------------------------------------------------------------
//!
//!	@file	SceneLoad.cpp
//!	@brief	ロードシーンクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "Global.h"


//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
SceneLoad::SceneLoad(void)
: _flag(TRUE)
, _endFlag(FALSE)
, _count(0)
{
}

//---------------------------------------------------------------------------
//	コンストラクタ
//!	@param	ロード表示フラグ
//---------------------------------------------------------------------------
SceneLoad::SceneLoad(b32 flag)
: _flag(flag)
, _endFlag(FALSE)
, _count(0)
{
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
SceneLoad::~SceneLoad(void)
{
}

//---------------------------------------------------------------------------
//	初期化
//---------------------------------------------------------------------------
void	SceneLoad::init(void)
{
	if( _flag ) {
		_width	= static_cast<f32>(SYSTEM->getWidth());
		_height	= static_cast<f32>(SYSTEM->getHeight());

		//	データの読み込み
		_pObj2D = new Task2D[LOADOBJ_MAX];
		_pObj2D[0].load("DATA\\LOAD\\Back.png");
		_pObj2D[0].set(0, 0, _width, _height, 0, 0, 512, 512, "copy");

		_pObj2D[1].load("DATA\\LOAD\\Anime.png");
		_pObj2D[1].set(_width - 380, _height - 48, 96, 96, 0, 0, 128, 128, "copy");
		_pObj2D[1].setCenter(kx2DObject::CENTER_CENTER);
		_pObj2D[2].load("DATA\\LOAD\\Loading.png");
		_pObj2D[2].set(_width - 330, _height - 96, 320, 96, 0, 0, 512, 64, "copy");

		//	タスクに追加する
		for( u32 i=0; i < LOADOBJ_MAX; i++ ) {
			TASK->addBottom(&_pObj2D[i], SystemTask::LIST_LOAD, FALSE);
		}

		//	フェードイン
		FADE->fadeIn(0, 5);
	}
	else FADE->fadeOut(0, 255);

	//	ロード開始
	DWORD threadParam, threadId = 1;
	_thread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)sceneLoad, &threadParam, 0, &threadId);
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	SceneLoad::cleanup(void)
{
	//	ロード中に終了した場合ロードを完了させてから終了する
	while( _endFlag == FALSE ) {
		GetExitCodeThread(_thread, &_exitCode);
		if( _exitCode != STILL_ACTIVE ) {
			CloseHandle(_thread);
			break;
		} else Sleep(0);
	}

	//	画像が使用されていなければ終了する
	if( _flag == FALSE ) return;

	//	タスクを解放する
	for( u32 i=0; i < LOADOBJ_MAX; i++ ) {
		TASK->killTask(&_pObj2D[i]);
	}
	SAFE_DELETES(_pObj2D);
}

//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	SceneLoad::update(void)
{
	if( _flag ) {
		//	画像を回転させる
		_pObj2D[1].setAngle(RAD * _count);
		_pObj2D[2].set(_width - 330, _height - 96, 280 + 13.3f * (_count >> 3 & 0x03), 96, 0, 0, 448 + 21.3f * (_count >> 3 & 0x03), 64, "copy");
		_count++;
	}

	//	ロードが終われば移行する
	GetExitCodeThread(_thread, &_exitCode);
	if( (_exitCode != STILL_ACTIVE) && (_endFlag == FALSE) ) {
		CloseHandle(_thread);
		if( _flag ) FADE->fadeOut(0, 5);
		_endFlag = TRUE;
	}

	//	フェイドアウトしたら次のシーンへ移行する
	if( _endFlag && FADE->getFlag() ) {
		SCENE->jumpScene();
		return;
	}
}

//---------------------------------------------------------------------------
//	シーンの読み込み
//---------------------------------------------------------------------------
DWORD	SceneLoad::sceneLoad(void* arg)
{
	SceneBase*	pNext = NULL;

	//	新シーンの生成
	switch( SCENE->getNext() ) {
	case SystemScene::SCENE_TITLE:
		pNext = new SceneTitle();
		break;
	case SystemScene::SCENE_GAME:
		pNext = new SceneGame();
		break;
	case SystemScene::SCENE_CLEAR:
		pNext = new SceneClear();
		break;

	default:
		ASSERT(false, "存在しないシーンを生成しようとしています。");
		break;
	}

	//	初期化
	if( pNext ) pNext->init();
	//	次のシーンにセット
	SCENE->setNextScene(pNext);

	return 0;
}

//============================================================================
//	END OF FILE
//============================================================================