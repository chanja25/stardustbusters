//---------------------------------------------------------------------------
//!
//!	@file	Player.cpp
//!	@brief	プレイヤークラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "Global.h"


//	プレイヤーの可動範囲設定
const f32 Player::UP_MAX	= 2.5f;
const f32 Player::DOWN_MAX	= -3.0f;
const f32 Player::LEFT_MAX	= -4.0f;
const f32 Player::RIGHT_MAX	= 4.0f;

//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
Player::Player(void)
: _angleZ(0)
, _keyFlag(0)
, _wpType(0)
, _nextWpType(0)
, _wflg(FALSE)
, _chainSpeed(0)
, _chainTimer(0)
, _shotFlg(TRUE)
{
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
Player::~Player(void)
{
}

//---------------------------------------------------------------------------
//	初期化
//---------------------------------------------------------------------------
void	Player::init(void)
{
	//	スキンメッシュの読み込み
	_obj.load("DATA\\GAME\\3DOBJ\\PLAYER\\Player.iem");

	//	初期設定
	_pos	= VECTOR3(0, 0, 0);
	_move	= VECTOR3(0, 0, 0);
	_speed	= 0.05f;
	_scale	= 0.004f;
	_angle	= VECTOR3(0, 0, 0);
	_alpha	= 1.0f;
	_dist	= 0.4f;

	_mode	= MODE_SET;

	_maxhp	= 100;
	_hp		= _maxhp;

	_bulletSpeed = 2.0f;
	_chainSpeed  = 5;

	_remain		 = 2;

	//	当たり判定の初期化
	_hit.init(_hit.SHAPE_SPHERE, _hit.ATT_PLAYER);
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	Player::cleanup(void)
{
}

//---------------------------------------------------------------------------
//	ショット
//---------------------------------------------------------------------------
void	Player::shot(void)
{
	if( _wpType != WEAPON_NONE ) return;

	//	弾が発射可能か確認する
	if( _shotFlg == FALSE ) return;
	_chainTimer	= 0;
	_shotFlg	= FALSE;

	//	弾の生成
	Bullet*	pBullet = new Bullet();

	VECTOR3 pos;
	//	弾を発射口に設定しセットする
	pos = getBonePos(0);
	pBullet->set(pos, VECTOR3(0, 0, 1), _bulletSpeed);
	TASK->addBottom(pBullet, SystemTask::LIST_BULLET);

	//	弾を発射口に設定しセットする
	pBullet = new Bullet();
	pos = getBonePos(2);
	pBullet->set(pos, VECTOR3(0, 0, 1), _bulletSpeed);
	TASK->addBottom(pBullet, SystemTask::LIST_BULLET);

	//	弾の発射音を鳴らす
	AUDIO->play(SceneGame::SOUND_GUN);
}

//---------------------------------------------------------------------------
//	移動
//---------------------------------------------------------------------------
void	Player::move(void)
{
	_move.x = _move.y = _move.z = 0;

	//	キー入力により弾の発射
	if( INPUT->isKeyPress(kxInput::KEY_SELECT) ) shot();

	//	キーによる移動
	if( _mode == MODE_MOVE ) {
		//	武器のノードを取得する
		Node* pNode = TASK->getList(SystemTask::LIST_WEAPON)->getFirstNode();
		if( pNode ) {
			//	武器が存在する場合、武器装填キーを押すとパージする
			if( INPUT->isKeyPush(kxInput::KEY_A) ||
				INPUT->isKeyPush(kxInput::KEY_B) ||
				INPUT->isKeyPush(kxInput::KEY_C) ) {
				reinterpret_cast<Weapon*>(pNode->getObject())->purge();
				_wpType = _nextWpType = WEAPON_NONE;
			}
		} else {
			if( INPUT->isKeyPush(kxInput::KEY_A) ) {
				//	キー入力により機銃を装填する
				TASK->addBottom(new Gun(), SystemTask::LIST_WEAPON);
				_nextWpType = WEAPON_GUN;
			} else if( INPUT->isKeyPush(kxInput::KEY_B) ) {
				//	キー入力によりミサイルを装填する
				TASK->addBottom(new Missile(), SystemTask::LIST_WEAPON);
				_nextWpType = WEAPON_MISSILE;
			} else if( INPUT->isKeyPush(kxInput::KEY_C) ) {
				//	キー入力によりレーザーを装填する
				TASK->addBottom(new Laser(), SystemTask::LIST_WEAPON);
				_nextWpType = WEAPON_LASER;
			}

			//	武器が変更された場合武器の装填モードに変更する
			if( _wpType != _nextWpType ) {
				_mode = MODE_SETWEAPON;
				_wpType = _nextWpType;
				_keyFlag = 0;
				_timer = 0;
				return;
			}
		}

		VECTOR2 vec;
		//	キーにより移動力を取得する
		vec.y = ( INPUT->isKeyPress(kxInput::KEY_UP) )? 1.0f: ( INPUT->isKeyPress(kxInput::KEY_DOWN) )? -1.0f: 0;
		vec.x = ( INPUT->isKeyPress(kxInput::KEY_RIGHT) )? 1.0f: ( INPUT->isKeyPress(kxInput::KEY_LEFT) )? -1.0f: 0;

		//	角度の制御
		if( vec.x < 0 ) {
			_angleZ = ( _angleZ < RAD * 44 )? _angleZ + RAD: RAD * 45;
		} else if( vec.x > 0 ) {
			_angleZ = ( _angleZ > RAD * -44 )? _angleZ - RAD: RAD * -45;
		} else {
			if( _angleZ > 0 ) _angleZ -= RAD;
			else if( _angleZ < 0 ) _angleZ += RAD;
			if( abs(_angleZ) < RAD / 2 ) _angleZ = 0;
		}

		//	移動量の設定
		_move.x = vec.x * _speed;
		_move.y = vec.y * _speed;

		//	入力によるモードの変更
		if( vec.x > 0 && (_keyFlag == 1) ) _mode = MODE_ROLL_RIGHT;
		if( vec.x < 0 && (_keyFlag == 2) ) _mode = MODE_ROLL_LEFT;

		//	キーの入力情報を取得
		if( INPUT->isKeyRelease(kxInput::KEY_RIGHT)	) _keyFlag = 1;
		if( INPUT->isKeyRelease(kxInput::KEY_LEFT)	) _keyFlag = 2;

		//	入力が行われてから時間のカウント
		if( _keyFlag != 0 ) {
			if( _timer++ > 5 ) {
				_keyFlag = 0;
				_timer = 0;
			}
		}
		//	モードが変わっていればタイマーを初期化
		if( _mode != MODE_MOVE ) {
			_keyFlag = 0;
			_timer = 0;
		}
	}

	switch( _mode ) {
	case MODE_ROLL_RIGHT:
		//	右にロール
		_move.x = _speed * 4;
		_angle.z -= RAD * 36;
		if( ++_timer >= 10 ) {
			_mode = MODE_MOVE;
			_angle.z = 0;
			_timer = 0;
		}
		break;
	case MODE_ROLL_LEFT:
		//	左にロール
		_move.x = -_speed * 4;
		_angle.z += RAD * 36;
		if( ++_timer >= 10 ) {
			_mode = MODE_MOVE;
			_angle.z = 0;
			_timer = 0;
		}
		break;
	}
}

//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	Player::update(void)
{
	Weapon* pWeapon;

	switch( _mode ) {
	case MODE_SET:			//	初期設定
		_angleZ	 = 0;
		_angle.z = 0;
		_pos.x	 = _pos.y = 0;
		_pos.z	 = -20.0f;
		_move.x	 = _move.y = 0;
		_move.z	 = 0.05f;
		_alpha	 = 0.5f;
		_strongTime	= 0;
		_mode++;
		//break;
	case MODE_WAIT:			//	一定期間待つ
		_pos += _move;
		if( _pos.z > 0 ) {
			_pos.z = 0;
			_mode++;
		}
		break;
	case MODE_MOVE:			//	移動
	case MODE_ROLL_LEFT:	//	左にロール
	case MODE_ROLL_RIGHT:	//	右にロール
		move();
		break;
	case MODE_SETWEAPON:	//	武器を装填する
		//	武器を取得する
		pWeapon = reinterpret_cast<Weapon*>(TASK->getList(SystemTask::LIST_WEAPON)->getFirstNode()->getObject());
		//	武器の装填が完了していた場合、移動モードへ移行する
		if( pWeapon->getMode() == Weapon::MODE_FREE ) _mode = MODE_MOVE;

		//	角度の制御
		if( _angleZ > 0 ) _angleZ -= RAD;
		else if( _angleZ < 0 ) _angleZ += RAD;
		if( abs(_angleZ) < (RAD / 2) ) _angleZ = 0;
		break;
	case MODE_DEAD:			//	死亡
		//	現在の移動方向へ墜落していく
		if( _move.x > _speed )	_move.x = _speed;
		if( _move.x < -_speed ) _move.x = -_speed;
		if( _move.y > 0 ) _move.y = 0;
		_move.y	-= 0.001f;
		_pos	+= _move;
		//	死亡エフェクト
		EFFECT->deadFire( _pos, _dist);
		//	一定距離まで落ちたか確認
		if( _pos.y < -10.0f ) {
			if( _remain > 0 ) {
				//	残機が残っていた場合が再び復活する
				_hp = _maxhp;
				_remain--;
				_mode = MODE_SET;
				_hit.setState(_hit.ATT_NONE);

				//	武器のノードを取得する
				Node* pNode = TASK->getList(SystemTask::LIST_WEAPON)->getFirstNode();
				if( pNode ) {
					//	武器を装備していた場合はパージしておく
					reinterpret_cast<Weapon*>(pNode->getObject())->purge();
					_wpType = _nextWpType = WEAPON_NONE;
				}
				return;
			} else {
				//	残機が残っていないので削除要請をする
				_state = STATE_INVALID;
				//	ゲームオーバーへ移行
				reinterpret_cast<SceneGame*>(SCENE->getCurrentScene())->getScript()->load("GameOver");
			}
		}
		break;

	default:
		ASSERT(false, "存在しないモードに入りました(Player)");
		break;
	}

	//	弾の連射フラグ制御
	if( _shotFlg == FALSE ) _chainTimer++;
	if( _chainTimer >= _chainSpeed ) {
		_shotFlg = TRUE;
	}

	//	マトリックスの設定
	MATRIX mat, trans, rot;
	MATH->scalingMatrix(&mat, _scale, _scale, _scale);
	MATH->rotationMatrixXYZ(&rot, _angle.x, _angle.y, _angle.z + _angleZ);
	MATH->matrixTranslation(&trans, _pos);
	mat = mat * rot * trans;
	_obj.setTransMatrix(mat);

	VECTOR3	pos;
	//	ブースト表示
	VECTOR3 vec(mat._31, mat._32, mat._33);
	pos = vec * -180.0f + _pos;
	MATH->normalize(&vec, -vec);
	EFFECT->boost(pos, vec, 0.1f);

	_pos += _move;

	//	生存判定
	if( isAlive() ) {
		//	移動範囲制御
		_pos.y = ( _pos.y > UP_MAX )?	 UP_MAX:	_pos.y;
		_pos.y = ( _pos.y < DOWN_MAX )?  DOWN_MAX:	_pos.y;
		_pos.x = ( _pos.x < LEFT_MAX )?  LEFT_MAX:	_pos.x;
		_pos.x = ( _pos.x > RIGHT_MAX )? RIGHT_MAX:	_pos.x;

		//	カメラの設定
		CAMERA->setTarget(VECTOR3(0, _pos.y, 0));
		CAMERA->setPos(VECTOR3(0, _pos.y + 2, -7));

		//	ダメージ判定
		hitDamage(_hit);

		//	ダメージを食らった場合、バイブレーションをかける
		if( _hit.getState() ) CAMERA->setVibration(20, 10);
		if( _strongTime > 90 ) {
			//	無敵期間内か確認する
			_hit.setObject(_pos, _dist);
			_alpha = 1.0f;
		} else if( _mode != MODE_WAIT ) _strongTime++;
	} else _mode = MODE_DEAD;

	//	当たり判定用球の表示要請
	GRAPHICS->drawSphere(_pos, _dist, 0xFFFFFF00, 8);
}

//---------------------------------------------------------------------------
//	描画
//---------------------------------------------------------------------------
void	Player::render(void)
{
	//	シェーダーに透過値を設定して、表示
	GRAPHICS->getShader3D()->setValue("alpha", _alpha);
	_obj.render("Alpha");
}

//============================================================================
//	END OF FILE
//============================================================================