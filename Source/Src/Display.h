//---------------------------------------------------------------------------
//!
//!	@file	Display.h
//!	@brief	画面制御クラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#ifndef	__DISPLAY_H__
#define __DISPLAY_H__

#pragma	once


//===========================================================================
//!	画面制御クラス
//===========================================================================
class Display : public TaskBase
{
public:
	//---------------------------------------------------------------------------
	//!	@name 初期化
	//---------------------------------------------------------------------------
	//!@{

	//!	コンストラクタ
	Display(void);
	//!	デストラクタ
	~Display(void);

	//!	初期化
	void	init(void);
	//!	解放
	void	cleanup(void);

	//!@}

	//!	メッセージウィンドウ関連更新
	void	updateMessage(void);
	//!	プレイヤー関連更新
	void	updatePlayerHP(void);
	//!	武器関連更新
	void	updateWeapon(void);
	//!	スコア表示
	void	updateScore(void);

	//!	メッセージウィンドウ関連更新
	void	drawMessage(void);
	//!	プレイヤー関連更新
	void	drawPlayerHP(void);
	//!	武器関連更新
	void	drawWeapon(void);
	//!	ボス関連表示更新
	void	drawBossHP(void);
	//!	スコア表示
	void	drawScore(void);

	//!	更新
	void	update(void);
	//!	描画
	void	render(void);

	//-------------------------------------------------------------
	//!	@name 取得参照
	//-------------------------------------------------------------
	//!@{

	//!	メッセージボックスの表示フラグ
	void	setMessageFlag(b32 flag) { _mFlag = flag; }

	//!@}

private:
	//!	使用するテクスチャー
	enum TEXTURE {
		HP_FRAME,
		HP_HARI,
		REMAIN,
		SCORE_FRAME,
		SCORE_COUNT,
		W_FRAME,
		W_METER,
		W_AICON,
		W_FLAG,
		B_FRAME,
		B_HP,
		B_AICON,
		M_WINDOW,
		TEXTURE_MAX,
	};
	LPKX2DOBJ	_p2DObj[TEXTURE_MAX];	//!< 2Dオブジェクト

	b32			_mFlag;					//!< メッセージボックスのセットフラグ
	f32			_messageBoxY;			//!< メッセージボックスのY座標

	f32			_width;					//!< 画面の横幅
	f32			_height;				//!< 画面の縦幅

	f32			_rotPlayerHp;			//!< プレイヤーのHPゲージの傾き

	u32			_score;					//!< 表示用スコア
	s32			_weapon;				//!< 武器の種類
};

#endif //~#if __DISPLAY_H__

//============================================================================
//	END OF FILE
//============================================================================