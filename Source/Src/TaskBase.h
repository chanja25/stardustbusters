//===========================================================================
//!
//!	@file		TaskBase.h
//!	@brief		タスクベースクラス
//!
//===========================================================================
#ifndef	__TASKBASE_H__
#define	__TASKBASE_H__

#pragma once


//===========================================================================
//! タスクベースクラス
//===========================================================================
class TaskBase
{
public:
	//	タスクの状態
	enum STATE {
		STATE_SETUP		= (1 << 0),
		STATE_RUNNING	= (1 << 1),
		STATE_INVALID	= (1 << 2),
		STATE_DELETE	= (1 << 3),
	};

	//	タスクの属性
	enum ATTRIBUTE {
		ATT_NONE,
		ATT_UPDATE	= (1 << 0),
		ATT_RENDER	= (1 << 1),
		ATT_ALL		= ATT_UPDATE | ATT_RENDER,
	};

	//-------------------------------------------------------------
	//!	@name 初期化
	//-------------------------------------------------------------
	//!@{

	//!	コンストラクタ
	TaskBase(void)
	: _state(STATE_SETUP)
	, _attribute(ATT_ALL)
	{
		//	ノードのオブジェクトに自分をセット
		_node.setObject(this);
	}

	//!	デストラクタ
	virtual ~TaskBase(void){};

	//!	初期化
	virtual	void	init(void) = 0;
	//!	解放
	virtual	void	cleanup(void) = 0;

	//!@}

	//!	更新
	virtual	void	update(void) = 0;
	//!	描画
	virtual	void	render(void) = 0;

	//-------------------------------------------------------------
	//!	@name 取得参照
	//-------------------------------------------------------------
	//!@{

	//!	状態セット
	//!	@param	state [in] セットする状態
	inline	void	setState(u32 state) { _state = state; }
	//!	状態取得
	//!	@retval	現在の状態
	inline	u32		getState(void) { return _state; }

	//!	属性セット
	//!	@param	attribute [in] セットする属性
	inline	void	setAttribute(u32 attribute) { _attribute = attribute; }
	//!	属性取得
	//!	@retval	現在の属性
	inline	u32		getAttribute(void) { return _attribute; }

	//!	削除フラグセット
	//!	@param	deleteMode [in] セットする属性
	inline	void	setDeleteMode(b32 deleteMode) { _deleteMode = deleteMode; }
	//!	削除フラグ取得
	//!	@retval	現在の属性
	inline	b32		getDeleteMode(void) { return _deleteMode; }

	//!	生存チェック
	//!	@retval TRUE	生存
	//!	@retval	FALSE	死亡
	inline	b32		isAlive(void) { return (_state & (STATE_SETUP | STATE_RUNNING)); }
	//!	ノードの取得

	//!	@retval	扱っているノード
	inline	Node*	getNode(void) { return &_node; }

	//!@}
protected:
	u32		_state;			//!< タスク状態
	u32		_attribute;		//!< タスク属性
	b32		_deleteMode;	//!< タスク上で自動削除を行うかのフラグ

	Node	_node;			//!< 扱うノード

private:
};

#endif	//~#if __TASKBASE_H__

//============================================================================
//	END OF FILE
//============================================================================