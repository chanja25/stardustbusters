//===========================================================================
//!
//!	@file		Player.h
//!	@brief		プレイヤークラス
//!
//!	@author S.Kawamoto
//===========================================================================
#ifndef	__PLAYER_H__
#define	__PLAYER_H__

#pragma once


//===========================================================================
//! プレイヤークラス
//===========================================================================
class Player : public Character
{
public:
	//!	モード
	enum MODE {
		MODE_SET,
		MODE_WAIT,
		MODE_MOVE,
		MODE_ROLL_LEFT,
		MODE_ROLL_RIGHT,
		MODE_SETWEAPON,
		MODE_DEAD,
	};

	//!	武器の種類
	enum WEAPON {
		WEAPON_NONE,
		WEAPON_GUN,
		WEAPON_MISSILE,
		WEAPON_LASER,
	};

	//-------------------------------------------------------------
	//!	@name 初期化
	//-------------------------------------------------------------
	//!@{

	//!	コンストラクタ
	Player(void);

	//!	デストラクタ
	~Player(void);

	//!	初期化
	void	init(void);
	//!	解放
	void	cleanup(void);

	//!@}

	//!	ショット
	void	shot(void);
	//!	移動
	void	move(void);

	//!	更新
	void	update(void);
	//!	描画
	void	render(void);

	//-------------------------------------------------------------
	//!	@name 取得参照
	//-------------------------------------------------------------
	//!@{

	//!	残機の取得
	//!	@retval	残機
	inline s32	getRemain(void) { return _remain; }
	//!	武器の種類取得
	//!	@retval	武器の種類
	inline s32	getWeaponType(void) { return _wpType; }

	//!@}

private:
	//!	移動可能範囲(上)
	const static f32 UP_MAX;
	//!	移動可能範囲(下)
	const static f32 DOWN_MAX;
	//!	移動可能範囲(左)
	const static f32 LEFT_MAX;
	//!	移動可能範囲(右)
	const static f32 RIGHT_MAX;

	s32		_remain;		//!< 残機

	f32		_angleZ;		//!< Z軸の角度
	u8		_keyFlag;		//!< キー入力による

	s32		_wpType;		//!< 武器の種類
	s32		_nextWpType;	//!< 次の武器のタイプ
	b32		_wflg;			//!< 武器の装填フラグ

	f32		_bulletSpeed;	//!< 弾速
	s32		_chainSpeed;	//!< 連射速度
	s32		_chainTimer;	//!< 連射タイマー
	s32		_shotFlg;		//!< ショットフラグ

	s32		_strongTime;	//!< 無敵時間用
};

#endif	//~#if __PLAYER_H__

//============================================================================
//	END OF FILE
//============================================================================