//---------------------------------------------------------------------------
//!
//!	@file	GameScript.cpp
//!	@brief	ゲームスクリプトクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "Global.h"

//---------------------------------------------------------------------------
//	スクリプトの取得
//!	@retval	TRUE	スクリプトの取得成功
//!	@retval	FALSE	スクリプトの取得失敗
//---------------------------------------------------------------------------
b32		GameScript::getScript(void)
{
	if( Script::getScript() ) return TRUE;

	//	現在のコマンドを取得
	char* pCommand = _data.getCommand();

	//	クリア時
	if( strcmp(pCommand, "CLEAR") == 0 ) {
		SCORE->setClearFlag(TRUE);
		SCENE->jumpScene(SystemScene::SCENE_CLEAR);
		return TRUE;
	}
	//	ゲームオーバー時
	if( strcmp(pCommand, "GAMEOVER") == 0 ) {
		SCENE->jumpScene(SystemScene::SCENE_CLEAR);
		return TRUE;
	}
	//	敵をセット
	if( strcmp(pCommand, "ENEMY") == 0 ) {
		Enemy*	pEnemy = new Zako(_data.getParamInt());

		VECTOR3 pos;
		pos.x = _data.getParamFloat();
		pos.y = _data.getParamFloat();
		pos.z = _data.getParamFloat();
		pEnemy->set(pos);

		TASK->addBottom(pEnemy, SystemTask::LIST_ENEMY);
		return TRUE;
	}
	//	ボスをセット
	if( strcmp(pCommand, "BOSS") == 0 ) {
		Enemy* pBoss = new Boss();

		TASK->addBottom(pBoss, SystemTask::LIST_BOSS);
		return TRUE;
	}

	return FALSE;
}

//============================================================================
//	END OF FILE
//============================================================================