//---------------------------------------------------------------------------
//!
//!	@file	SceneGame.cpp
//!	@brief	ゲームシーンクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "Global.h"


//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
SceneGame::SceneGame(void)
: _score(0)
, _step(0)
, _count(0)
{
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
SceneGame::~SceneGame(void)
{
}

//---------------------------------------------------------------------------
//	初期化
//---------------------------------------------------------------------------
void	SceneGame::init(void)
{
	//	クリアフラグをリセット
	SCORE->setClearFlag(FALSE);

	//	カメラの設定
	CAMERA->setPos(VECTOR3(0, 2, -7));
	CAMERA->setTarget(VECTOR3(0, 0, 0));
	CAMERA->setProjection(RAD * 45, 1.0f, 1500.0f);
	CAMERA->setFog(.0f, 70.0f, 0x5555ff);

	//	パーティクル用画像のロード
	PARTICLE->load("DATA\\GAME\\2DOBJ\\particle.png", 0);

	//	サウンドの読み込み
	AUDIO->load(SOUND_BGM,		"DATA\\GAME\\SOUND\\BGM.wav");
	AUDIO->load(SOUND_BOSS,		"DATA\\GAME\\SOUND\\BOSS.wav");
	AUDIO->load(SOUND_GUN,		"DATA\\GAME\\SOUND\\SE\\gun.wav");
	AUDIO->load(SOUND_MISSILE,	"DATA\\GAME\\SOUND\\SE\\missile.wav");
	AUDIO->load(SOUND_LASER,	"DATA\\GAME\\SOUND\\SE\\laser.wav");

	//	テクスチャーの読み込み
	ZeroMemory(_pTexture, sizeof(LPTEXTURE) * TEXTURE_MAX);
	_pTexture[HP_FRAME]		= new KX2DOBJ("DATA\\GAME\\2DOBJ\\hp_frame.png");
	_pTexture[HP_HARI]		= new KX2DOBJ("DATA\\GAME\\2DOBJ\\hp_hari.png");
	_pTexture[SCORE_FRAME]	= new KX2DOBJ("DATA\\GAME\\2DOBJ\\s_frame.png");
	_pTexture[COUNT]		= new KX2DOBJ("DATA\\GAME\\2DOBJ\\s_count.png");
	_pTexture[M_WINDOW]		= new KX2DOBJ("DATA\\GAME\\2DOBJ\\m_window.png");
	_pTexture[W_FRAME]		= new KX2DOBJ("DATA\\GAME\\2DOBJ\\w_frame.png");
	_pTexture[W_METER]		= new KX2DOBJ("DATA\\GAME\\2DOBJ\\w_meter.png");
	_pTexture[W_AICON]		= new KX2DOBJ("DATA\\GAME\\2DOBJ\\w_aicon.png");
	_pTexture[W_FLAG]		= new KX2DOBJ("DATA\\GAME\\2DOBJ\\w_flag.png");
	_pTexture[B_FRAME]		= new KX2DOBJ("DATA\\GAME\\2DOBJ\\b_frame.png");
	_pTexture[B_HP]			= new KX2DOBJ("DATA\\GAME\\2DOBJ\\b_hp.png");
	_pTexture[B_AICON]		= new KX2DOBJ("DATA\\GAME\\2DOBJ\\b_aicon.png");

	//	3DOBJの読み込み
	ZeroMemory(_p3DObj, sizeof(LPKX3DOBJ) * OBJ3D_MAX);
	_p3DObj[PLAYER]		= new KX3DOBJ("DATA\\GAME\\3DOBJ\\PLAYER\\player.iem");
	_p3DObj[ENEMY_1]	= new KX3DOBJ("DATA\\GAME\\3DOBJ\\ENEMY\\enemy1.iem");
	_p3DObj[ENEMY_2]	= new KX3DOBJ("DATA\\GAME\\3DOBJ\\ENEMY\\enemy2.iem");
	_p3DObj[ENEMY_3]	= new KX3DOBJ("DATA\\GAME\\3DOBJ\\ENEMY\\enemy3.iem");
	_p3DObj[BOSS]		= new KX3DOBJ("DATA\\GAME\\3DOBJ\\ENEMY\\Boss.iem");

	//	MESHの読み込み
	ZeroMemory(_pMesh, sizeof(LPKXMESH) * MESH_MAX);
	_pMesh[STAGE]		= new KXMESH("DATA\\GAME\\MESH\\STAGE\\stage.kmo");
	_pMesh[SKY]			= new KXMESH("DATA\\GAME\\MESH\\STAGE\\sky.kmo");
	_pMesh[GUN]			= new KXMESH("DATA\\GAME\\MESH\\WEAPON\\gun.kmo");
	_pMesh[MISSILE]		= new KXMESH("DATA\\GAME\\MESH\\WEAPON\\missile.kmo");
	_pMesh[LASER]		= new KXMESH("DATA\\GAME\\MESH\\WEAPON\\laser.kmo");
	_pMesh[BULLET]		= new KXMESH("DATA\\GAME\\MESH\\WEAPON\\bullet.kmo");
	_pMesh[M_BULLET]	= new KXMESH("DATA\\GAME\\MESH\\WEAPON\\m_bullet.kmo");
	_pMesh[L_BULLET]	= new KXMESH("DATA\\GAME\\MESH\\WEAPON\\l_bullet.kmo");

	//	プレイヤーの生成
	_pPlayer = new Player();

	//	ステージの生成
	_pStage	= new Stage();
	_pSky	= new Sky();

	//	画面上の制御クラス生成
	_pDisp	= new Display();
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	SceneGame::cleanup(void)
{
	u32 i;

	//	スコアをセット
	SCORE->addScore(_score);

	//	セットされていない場合は直接解放しておく
	if( TASK->getList(SystemTask::LIST_STAGE)->getFirstNode() == NULL ) {
		SAFE_DELETE(_pPlayer);
		SAFE_DELETE(_pStage);
		SAFE_DELETE(_pSky);
		SAFE_DELETE(_pDisp);
	}

	//	タスクの開放
	TASK->killAll();

	//	読み込んだファイルを解放
	for( i = 0; i < TEXTURE_MAX; i++ ) {
		SAFE_DELETE(_pTexture[i]);
	}
	for( i = 0; i < OBJ3D_MAX; i++ ) {
		SAFE_DELETE(_p3DObj[i]);
	}
	for( i = 0; i < MESH_MAX; i++ ) {
		SAFE_DELETE(_pMesh[i]);
	}

	//	サウンドの解放
	AUDIO->cleanupAll();
}

//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	SceneGame::update(void)
{
	//	ゲームの状態毎の処理
	switch( _step ) {
	case STEP_SETUP:
		//	スクリプトの読み込み
		_script.load("Game");

		//	タスクにセットする
		TASK->addBottom(_pPlayer, SystemTask::LIST_PLAYER);
		TASK->addBottom(_pStage, SystemTask::LIST_STAGE);
		TASK->addBottom(_pSky, SystemTask::LIST_STAGE);
		TASK->addBottom(_pDisp, SystemTask::LIST_2D);

		AUDIO->play(SOUND_BGM, TRUE);

		_step++;
		//break;
	case STEP_MAIN:
		_count++;
		break;

	default:
		ASSERT(false, "存在しないステップに移行しました(SceneGame)");
		break;
	}

	//	スクリプトの更新
	_script.update();

	#if DEBUG
		//	エンターキーでタイトルに戻る
		if( INPUT->isKeyPush(kxInput::KEY_START) ) {
			SCENE->jumpScene(SystemScene::SCENE_TITLE);
		}
	#endif	//~#if DEBUG
}

//============================================================================
//	END OF FILE
//============================================================================