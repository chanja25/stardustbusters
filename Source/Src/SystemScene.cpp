//---------------------------------------------------------------------------
//!
//!	@file	SystemScene.cpp
//!	@brief	システムシーンクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "Global.h"


//	インスタンスの実体生成
SystemScene	SystemScene::_instance;

//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
SystemScene::SystemScene(void)
: _pCurrentScene(NULL)
, _pNextScene(NULL)
, _nextScene(0xFF)
{
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
SystemScene::~SystemScene(void)
{
}

//---------------------------------------------------------------------------
//	初期化
//---------------------------------------------------------------------------
void	SystemScene::init(void)
{
	ASSERT(_pCurrentScene == NULL, "既にシステムシーンは初期化されています");
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	SystemScene::cleanup(void)
{
	//	現在のシーンの解放
	if( _pCurrentScene ) {
		_pCurrentScene->cleanup();
		SAFE_DELETE(_pCurrentScene);
	}

	//	読み込み中のシーンがあれば解放しておく
	if( _pNextScene ) {
		_pNextScene->cleanup();
		SAFE_DELETE(_pNextScene);
	}
}

//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	SystemScene::update(void)
{
	if( _pCurrentScene ) _pCurrentScene->update();
}

//---------------------------------------------------------------------------
//	シーンの変更
//!	@param	pScene	[in] 変更するシーン
//---------------------------------------------------------------------------
void	SystemScene::jumpScene(u8 scene)
{
	//	現在のシーンの解放
	if( _pCurrentScene ) {
		_pCurrentScene->cleanup();
		SAFE_DELETE(_pCurrentScene);
	}

	//	シーンが読み込み済みの場合は次のシーンをカレントにセットする
	if( _pNextScene ) {
		_pCurrentScene	= _pNextScene;
		_pNextScene		= NULL;
		_nextScene		= 0xFF;
		return;
	}

	_nextScene = scene;

	//	新シーンの生成
	switch( scene ) {
	case SCENE_TITLE:
		_pCurrentScene = new SceneTitle();
		break;
	case SCENE_GAME:
		_pCurrentScene = new SceneLoad();
		break;
	case SCENE_CLEAR:
		_pCurrentScene = new SceneClear();
		break;

	default:
		_pCurrentScene = NULL;
		break;
	}
	ASSERT(_pCurrentScene, "シーンが存在しいません");

	//	新しいシーンの初期化
	_pCurrentScene->init();
}

//============================================================================
//	END OF FILE
//============================================================================