//---------------------------------------------------------------------------
//!
//!	@file	BossBullet.cpp
//!	@brief	ボスバレットクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "Global.h"


//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
BossBullet::BossBullet(void)
{
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
BossBullet::~BossBullet(void)
{
}

//---------------------------------------------------------------------------
//	初期化
//---------------------------------------------------------------------------
void	BossBullet::init(void)
{
	//	メッシュの読み込み
	_obj.load("DATA\\GAME\\MESH\\WEAPON\\l_bullet.kmo");

	//	当たり判定用オブジェクトの初期化
	_attack.init(	_attack.SHAPE_CAPSULE,
					_attack.ATT_BULLET,
					_attack.ATT_PLAYER	);
	_attack.setWork(_attack.WORK_POWER, 1);

	_scale	 = 0.005f;
	_dist	 = _scale * 10.0f;
	_endDist = 100.0f;
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	BossBullet::cleanup(void)
{
}

//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	BossBullet::update(void)
{
	_oldPos = _pos;
	_pos += _move * _speed;
	_timer++;

	if( _attack.getState() ) {
		//	弾が当たっていた場合、エフェクトを出す
		EFFECT->damage(_pos);
	}

	//	弾が一定距離離れると削除する
	if( _pos.z < -10.0f ) {
		_state = STATE_INVALID;
		return;
	}
	//	当たり判定のセット
	_attack.setObject(_oldPos, _dist, _pos);
}

//---------------------------------------------------------------------------
//	描画
//---------------------------------------------------------------------------
void	BossBullet::render(void)
{
	//	マトリックスの設定
	MATRIX mat, trans;
	MATH->scalingMatrix(&mat, _scale, _scale, _scale);
	MATH->matrixTranslation(&trans, _pos);
	mat = mat * _rot * trans;
	_obj.setTransMatrix(mat);

	//	加算合成で描画
	_obj.render("add");
}

//============================================================================
//	END OF FILE
//============================================================================