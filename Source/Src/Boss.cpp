//---------------------------------------------------------------------------
//!
//!	@file	Boss.cpp
//!	@brief	ボスクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "Global.h"


//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
Boss::Boss(void)
: _target(0, 0 ,0)
{
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
Boss::~Boss(void)
{
}

//---------------------------------------------------------------------------
//	初期化
//---------------------------------------------------------------------------
void	Boss::init(void)
{
	//	スキンメッシュの読み込み
	_obj.load("DATA\\GAME\\3DOBJ\\ENEMY\\Boss.iem");

	_maxhp	= 3000;
	_hp		= _maxhp;

	_pos	= VECTOR3(0, -1, 70);
	_move	= VECTOR3(0, 0, 0);
	_scale	= 0.02f;
	_angle	= VECTOR3(0, RAD * 180, 0);
	_alpha	= 1.0f;
	_dist	= 0.5f;
	_speed	= 0.04f;

	_score	= 10000;
	_mode	= MODE_SET;

	_bulletSpeed = 2.0f;

	_partDist[HIT_LEFT_HAAND]	   = 0.6f;
	_partPoint[HIT_LEFT_HAAND]	   = 3;
	_partDist[HIT_RIGHT_HAAND]	   = 0.6f;
	_partPoint[HIT_RIGHT_HAAND]	   = 14;
	_partDist[HIT_LEFT_SHOULDER]   = 1.0f;
	_partPoint[HIT_LEFT_SHOULDER]  = 7;
	_partDist[HIT_RIGHT_SHOULDER]  = 1.0f;
	_partPoint[HIT_RIGHT_SHOULDER] = 10;
	_partDist[HIT_LEFT_LEG]		   = 1.0f;
	_partPoint[HIT_LEFT_LEG]	   = 18;
	_partDist[HIT_RIGHT_LEG]	   = 1.0f;
	_partPoint[HIT_RIGHT_LEG]	   = 21;
	_partDist[HIT_BODY]			   = 1.0f;
	_partPoint[HIT_BODY]		   = 15;
	_partDist[HIT_HEAD]			   = 1.0f;
	_partPoint[HIT_HEAD]		   = 11;

	_obj.setMotion(MOTION_DASH);

	//	当たり判定の初期化
	_attack.init(	_attack.SHAPE_SPHERE,
					_attack.ATT_ENEMY,
					_attack.ATT_PLAYER	);
	_attack.setWork(_attack.WORK_POWER, 3);
	_attack2.init(	_attack.SHAPE_SPHERE,
					_attack.ATT_ENEMY,
					_attack.ATT_PLAYER	);
	_attack2.setWork(_attack.WORK_POWER, 10);

	_hit.init(_hit.SHAPE_SPHERE, _hit.ATT_ENEMY);
	for( u32 i=0; i < HIT_MAX; i++ ) {
		_partHit[i].init(_partHit[i].SHAPE_SPHERE, _partHit[i].ATT_ENEMY);
	}

	//	ボス用BGMに変更する
	AUDIO->stop(SceneGame::SOUND_BGM);
	AUDIO->play(SceneGame::SOUND_BOSS, TRUE);
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	Boss::cleanup(void)
{
	Enemy::cleanup();

	//	プレイヤーが存在すればクリアに移行する
	Node* pNode = TASK->getList(SystemTask::LIST_PLAYER)->getFirstNode();
	if( pNode ) {
		if( reinterpret_cast<Player*>(pNode->getObject())->isAlive() ) {
			reinterpret_cast<SceneGame*>(SCENE->getCurrentScene())->getScript()->load("GameClear");
		}
	}
}

//---------------------------------------------------------------------------
//	レーザーの発射
//---------------------------------------------------------------------------
void	Boss::setLaser(void)
{
	Bullet*	pBullet;

	MATRIX	mat;
	VECTOR3 pos;
	//	頭上から６発のレーザーを発射する
	for( u32 i=0; i < 6; i++ ) {
		pBullet = new BossBullet();
		mat = *getBone(5 + i) * getTransMatrix();
		pos.x = mat._41 + mat._31 * 20 + getTransMatrix()._31 * 100;
		pos.y = mat._42 + mat._32 * 20 + getTransMatrix()._32 * 100;
		pos.z = mat._43 + mat._33 * 20 + getTransMatrix()._33 * 100;
		pBullet->set(pos, VECTOR3(0, 0, -1), _bulletSpeed);

		TASK->addBottom(pBullet, SystemTask::LIST_BULLET);
	}

	//	レーザーのサウンドを再生
	AUDIO->play(SceneGame::SOUND_LASER);
}

//---------------------------------------------------------------------------
//	移動
//---------------------------------------------------------------------------
void	Boss::move(void)
{
	//	首付近の位置を取得する
	VECTOR3 pos = getBonePos(4);
	MATRIX mat = getTransMatrix();
	pos.x += mat._21 * 30;
	pos.y += mat._22 * 30;
	pos.z += mat._23 * 30;

	//	プレイヤーと自分の位置を比較し、移動する
	if( pos.y - _target.y < 0 ) _pos.y += _speed;
	else					    _pos.y -= _speed;

	//	移動時に機体を傾けていく
	if( _moveMode == MOVE_RIGHT ) {
		_pos.x += _speed;
		_angle.z = ( _angle.z < RAD * 30 )? _angle.z + RAD: RAD * 30;
	} else if( _moveMode == MOVE_LEFT ) {
		_pos.x -= _speed;
		_angle.z = ( _angle.z > RAD * -30 )? _angle.z - RAD: RAD * -30;
	}
}

//---------------------------------------------------------------------------
//	モード処理
//---------------------------------------------------------------------------
void	Boss::mode(void)
{
	u32	  com;

	//	プレイヤーが存在すれば、ターゲットにプレイヤーの位置を設定する
	Node* pNode = TASK->getList(SystemTask::LIST_PLAYER)->getFirstNode();
	if( pNode ) {
		Player* pPlayer = reinterpret_cast<Player*>(pNode->getObject());
		if( pPlayer->isAlive() ) {
			_target = pPlayer->getPos();
		}
	}

	switch( _mode ) {
	case MODE_SET:
		//	初期設定
		_move.z = -0.4f;
		if( _pos.z <= 8.0f ) {
			_move.z = 0;
			_pos.z = 8.0f;
			_obj.setMotion(MOTION_DASH_END);
			_mode = MODE_WAIT;
		}
		break;
	case MODE_SELECT:
		//	行動設定
		_move = VECTOR3(0, 0, 0);
		com = rand() % 5;
		if( com < 1 )	   _mode = MODE_WAIT;
		else if( com < 3 ) _mode = MODE_LASER_SET;
		else if( com < 5 ) _mode = MODE_ATTACK_SET;
		_timer = 0;
		break;
	case MODE_WAIT:
		//	数フレーム待ちを入れる
		if( _timer >= 90 ) _mode = MODE_SELECT;
		break;
	case MODE_LASER_SET:
		//	レーザー発射準備
		if( _timer == 1 ) _obj.setMotion(MOTINO_LASER_SET);
		if( _obj.getFrame() == 411 ) {
			//	発射準備完了
			_mode++;
			_timer = 0;
		}
		break;
	case MODE_LASER_SHOT:
		//	レーザー発射
		setLaser();
		//	ターゲット当たるように移動
		move();
		if( _timer >= 90 ) {
			_timer = 0;
			com = rand() % 7;
			_moveMode = MOVE_WAIT;
			//	一定確率で終了する
			if( com < 2 )	   _mode++;
			else if( com < 5 ) {
				//	ターゲットの位置へ移動する
				if( _target.x - getBonePos(4).x < 0 ) _moveMode = MOVE_LEFT;
				else								  _moveMode = MOVE_RIGHT;
			}
		}
		break;
	case MODE_LASER_LOST:
		//	レーザーの構えを解く
		if( _timer == 1 ) _obj.setMotion(MOTION_LASER_LOST);
		if( _obj.getMotion() == MOTION_WAIT ) {
			//	行動設定へ戻る
			_mode = MODE_SELECT;
		}
		break;
	case MODE_ATTACK_SET:
		//	格闘準備
		if( _timer == 1 ) _obj.setMotion(MOTION_DASH);

		//	ターゲットへのベクトルを取得
		_move = _target;
		MATH->normalize(&_move, _move - getBonePos(_partPoint[HIT_BODY]));

		//	ターゲットへ向かい移動していく
		_move *= 0.15f;
		_move.z = -0.18f;
		if( _pos.z <= 2.0f ) {
			//	格闘準備終了
			_pos.z = 2.0f;
			_move = VECTOR3(0, 0, 0);
			_obj.setMotion(MOTION_ATTACK, 5);
			_mode++;
		}
		break;
	case MODE_ATTACK:
		//	格闘攻撃
		_attack.setObject(getBonePos(_partPoint[HIT_LEFT_LEG]), _dist);
		_attack2.setObject(getBonePos(_partPoint[HIT_RIGHT_LEG]), _dist);
		if( _obj.getMotion() == MOTION_WAIT ) {
			//	攻撃終了
			_mode++;
		}
		break;
	case MODE_ATTACK_LOST:
		//	距離まで戻っていく
		_move.z = 0.18f;
		if( _pos.z >= 8.0f ) {
			//	行動設定へ戻る
			_pos.z = 8.0f;
			_move.z = 0;
			_mode = MODE_SELECT;
		}
		break;
	case MODE_DEAD:
		//	死亡
		if( _timer == 0 )  _obj.setMotion(MOTION_DEAD, 3.0f);
		if( _alpha <= 0 ) {
			_alpha = 0;
			//	タスクの削除要請
			_state = STATE_INVALID;
		}
		//	落下しながら透過していく
		_move.x = _move.z = 0;
		_move.y = -0.01f;
		_alpha -= 0.003f;
		//	死亡用エフェクトを出す
		for( u32 i=0; i < HIT_MAX; i++ ) {
			EFFECT->deadFire(getBonePos(_partPoint[i]), _partDist[i] * 0.3f); 
		}
		_timer++;
		break;

	default:
		ASSERT(false, "存在しないモードに移行しました(Boss)");
		break;
	}

	//	姿勢制御
	if( _moveMode == MOVE_WAIT ) {
		if( _angle.z > 0 )		_angle.z -= RAD;
		else if( _angle.z < 0 )	_angle.z += RAD;
		if( abs(_angle.z) < RAD / 2 ) _angle.z = 0;
	}
}

//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	Boss::update(void)
{
	//	モード別処理
	mode();

	//	移動する
	_pos += _move;
	_timer++;

	//	生存確認
	if( isAlive() ) {
		hitDamage(_hit);
		_hit.setObject(_pos, _dist);

		//	当たり判定用の球を表示要請
		for( u32 i=0; i < HIT_MAX; i++ ) {
			GRAPHICS->drawSphere(getBonePos(_partPoint[i]), _partDist[i], 0xFFFF0000, 8);
			//	ダメージ処理
			hitDamage(_partHit[i]);
			_partHit[i].setObject(getBonePos(_partPoint[i]), _partDist[i]); 
		}
	} else if( _mode != MODE_DEAD ) {
		//	死亡モードへ移行
		_timer = 0;
		_mode = MODE_DEAD;
	}

	//	スキンメッシュのフレームを更新する
	_obj.animation();
}
void	Boss::render()
{
	//	マトリックスの設定
	MATRIX mat, rot, trans;
	MATH->scalingMatrix(&mat, _scale, _scale, _scale);
	MATH->rotationMatrixXYZ(&rot, _angle.x, _angle.y, _angle.z);
	MATH->matrixTranslation(&trans, _pos);
	mat = mat * rot * trans;
	_obj.setTransMatrix(mat);

	//	透過値をシェーダーに設定して、描画
	GRAPHICS->getShader3D()->setValue("alpha", _alpha);
	_obj.render("Alpha");
}

//============================================================================
//	END OF FILE
//============================================================================