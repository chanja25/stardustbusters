//---------------------------------------------------------------------------
//!
//!	@file	SceneClear.cpp
//!	@brief	タイトルシーンクラス
//!
//!	@author S.Kawamoto
//---------------------------------------------------------------------------
#include "Global.h"


//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
SceneClear::SceneClear(void)
: _step(0)
, _count(0)
{
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
SceneClear::~SceneClear(void)
{
}

//---------------------------------------------------------------------------
//	初期化
//---------------------------------------------------------------------------
void	SceneClear::init(void)
{
	u32 i;

	//	タスクの生成
	for( i = 0; i < TEXTURE_MAX; i++ ) {
		_pObj2D[i] = new Task2D();
	}
	_pScore = new ScoreDisp();

	//	ファイルの読み込み
	if( SCORE->getClearFlag() ) {
		_pObj2D[BACK]->load("DATA\\END\\CLEAR_BG.png");
		_pObj2D[LOGO]->load("DATA\\END\\CLEAR.png");
	} else {
		_pObj2D[BACK]->load("DATA\\END\\OVER_BG.png");
		_pObj2D[LOGO]->load("DATA\\END\\GAMEOVER.png");
	}
	_pObj2D[RESULT]->load("DATA\\END\\Result.png");
	_pScore->load("DATA\\END\\count.png");

	//	画面のサイズ取得
	_width	= static_cast<f32>(SYSTEM->getWidth());
	_height	= static_cast<f32>(SYSTEM->getHeight());

	//	タスクの初期設定
	_pObj2D[BACK]->set(0, 0, _width, _height, 0, 0, 512, 512, "copy");
	_pObj2D[LOGO]->set(_width / 2 - 360, _height / 2 - 500, 720, 720, 0, 0, 512, 512, "copy");
	_pObj2D[RESULT]->set(_width / 2 - 360, _height / 2 - 330, 720, 180, 0, 0, 512, 128, "copy");

	//	スコアの取得
	u32 score = SCORE->getScore();
	_pScore->setScore(score);
	_pScore->set(_width / 2 + 150, _height / 2 - 100, 100, 100, 0, 0, 64, 64, "copy");

	//	タスクに追加する
	for( i = 0; i < TEXTURE_MAX; i++ ) {
		TASK->addBottom(_pObj2D[i], SystemTask::LIST_2D);
	}
	TASK->addBottom(_pScore, SystemTask::LIST_2D);
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	SceneClear::cleanup(void)
{
	//	タスクの中身を削除する
	TASK->killAll();
}

//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	SceneClear::update(void)
{
	switch( _step ) {
	case STEP_START:	//	開始
		//	フェイドイン
		FADE->fadeIn(0, 3);

		//	リザルト、スコアを消し、次へ
		_pObj2D[RESULT]->setAttribute(TaskBase::ATT_NONE);
		_pScore->setAttribute(TaskBase::ATT_NONE);
		_step++;
	case STEP_LOGO_IN:	//	ロゴを表示する
		//	フェードインが終わっているか確認
		if( FADE->getFlag() ) {
			if( INPUT->isKeyPush(kxInput::KEY_START) ) {
				//	キー入力でフェードアウトして次へ
				FADE->fadeOut(0, 3);
				_step++;
			}
		}
		break;
	case STEP_LOGO_OUT:	//	ロゴを消す
		//	フェードアウトが終わっているか確認
		if( FADE->getFlag() ) {
			//	ロゴを消し、リザルトを表示する
			_pObj2D[LOGO]->setAttribute(TaskBase::ATT_NONE);
			_pObj2D[RESULT]->setAttribute(TaskBase::ATT_ALL);
			//	スコアを表示する
			_pScore->setAttribute(TaskBase::ATT_ALL);
			//	フェードインして次へ
			FADE->fadeIn(0, 3);
			_step++;
		}
		break;
	case STEP_RESULT_IN:	//	リザルトを表示する
		//	フェードインが終わっているか確認する
		if( FADE->getFlag() ) {
			if( INPUT->isKeyPush(kxInput::KEY_START) ) {
				//	キー入力でフェードアウトして次へ
				FADE->fadeOut(0, 3);
				_step++;
			}
		}
		break;
	case STEP_RESULT_OUT:	//	リザルトを消す
		//	フェードアウトが終わったか確認
		if( FADE->getFlag() ) {
			//	タイトルへ移行する
			SCENE->jumpScene(SystemScene::SCENE_TITLE);
		}
		break;
	}
}

//============================================================================
//	END OF FILE
//============================================================================